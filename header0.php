    <!-- Header Container  -->
    <header id="header" class=" typeheader-2">
        <!-- Header Top -->
        <div class="header-top hidden-compact">
            <div class="container">
                <div class="row">
                    <div class="navbar-logo col-lg-2 col-md-3 col-sm-12 col-xs-2 visible-xs">
                        <div class="navbar-header">
                            <button type="button" id="show-megamenu" data-toggle="collapse" class="navbar-toggle">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                    </div>

                    <!-- Logo -->
                    <div class="navbar-logo col-lg-2 col-md-3 col-sm-12 col-xs-8">
                        <div class="logo"><a href="index.php"><img src="image/catalog/150x80stroke.svg" class="svg-logo"
                                    title="SHoooz - The Shoeroom" alt="Your Store" /></a>


                        </div>
                    </div>
                    <!-- //end Logo -->


                    <div class="header-top-left col-lg-4 col-md-4 col-sm-6 col-xs-12">

                        <div class="search-header-w">
                            <div class="icon-search hidden-lg hidden-md hidden-sm hidden-xs"><i
                                    class="fa fa-search"></i></div>
                            <div id="sosearchpro" class="sosearchpro-wrapper so-search ">
                                <form method="GET" action="">
                                    <div id="search0" class="search input-group form-group">
                                        <!-- <div class="select_category filter_type  icon-select hidden-sm hidden-xs">
                                            <select class="no-border" name="category_id">
                                                <option value="0">All Categories</option>
                                                <option value="78">Apparel</option>
                                                <option value="77">Cables &amp; Connectors</option>
                                                <option value="82">Cameras &amp; Photo</option>
                                                <option value="80">Flashlights &amp; Lamps</option>
                                                <option value="81">Mobile Accessories</option>
                                                <option value="79">Video Games</option>
                                                <option value="20">Jewelry &amp; Watches</option>
                                                <option value="76">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Earings</option>
                                                <option value="26">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wedding Rings</option>
                                                <option value="27">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Men Watches</option>
                                            </select>
                                        </div> -->

                                        <input class="autosearch-input form-control" type="text" value="" size="50"
                                            autocomplete="off" placeholder="Keyword here..." name="search">
                                        <span class="input-group-btn">
                                            <button type="submit" class="button-search btn btn-primary"
                                                name="submit_search"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                    <input type="hidden" name="route" value="product/search" />
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="pos-cart collapsed-block col-lg-6 col-md-6 col-sm-6 col-xs-2">
                        <ul class="top-link list-inline lang-curr">
                            <li class="hidden-xs">
                                <a class="btn-group" href="#">
                                    <i class="fa fa-lock"></i>Login & Signup
                                </a>
                            </li>
                            <li class="currency">
                                <div class="btn-group currencies-block">
                                    <div class="shopping_cart">
                                        <div id="cart" class="btn-shopping-cart">

                                            <a data-loading-text="Loading... "
                                                class="btn-group top_cart dropdown-toggle" data-toggle="dropdown"
                                                aria-expanded="true">
                                                <div class="shopcart">
                                                    <span class="icon-c">
                                                        <i class="fa fa-shopping-bag"></i>
                                                    </span>
                                                    <div class="shopcart-inner">
                                                        <p class="text-shopping-cart">
                                                            My cart
                                                        </p>

                                                        <span class="total-shopping-cart cart-total-full">
                                                            <span class="items_cart">02</span><span class="items_cart2">
                                                                item(s)</span><span class="items_carts"> - $162.00
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </a>

                                            <ul class="dropdown-menu pull-right shoppingcart-box" role="menu">
                                                <!--- <li>
                                        <table class="table table-striped">
                                            <tbody>
                                                <tr>
                                                    <td class="text-center" style="width:70px">
                                                        <a href="#">
                                                            <img src="image/catalog/demo/product/80/1.jpg" style="width:70px" alt="Yutculpa ullamcon" title="Yutculpa ullamco" class="preview">
                                                        </a>
                                                    </td>
                                                    <td class="text-left"> <a class="cart_product_name" href="#">Yutculpa ullamco</a> 
                                                    </td>
                                                    <td class="text-center">x1</td>
                                                    <td class="text-center">$80.00</td>
                                                    <td class="text-right">
                                                        <a href="#" class="fa fa-edit"></a>
                                                    </td>
                                                    <td class="text-right">
                                                        <a onclick="cart.remove('2');" class="fa fa-times fa-delete"></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center" style="width:70px">
                                                        <a href="#">
                                                            <img src="image/catalog/demo/product/80/2.jpg" style="width:70px" alt="Xancetta bresao" title="Xancetta bresao" class="preview">
                                                        </a>
                                                    </td>
                                                    <td class="text-left"> <a class="cart_product_name" href="#">Xancetta bresao</a> 
                                                    </td>
                                                    <td class="text-center">x1</td>
                                                    <td class="text-center">$60.00</td>
                                                    <td class="text-right">
                                                        <a href="#" class="fa fa-edit"></a>
                                                    </td>
                                                    <td class="text-right">
                                                        <a onclick="cart.remove('1');" class="fa fa-times fa-delete"></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <li>
                                        <div>
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td class="text-left"><strong>Sub-Total</strong>
                                                        </td>
                                                        <td class="text-right">$140.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-left"><strong>Eco Tax (-2.00)</strong>
                                                        </td>
                                                        <td class="text-right">$2.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-left"><strong>VAT (20%)</strong>
                                                        </td>
                                                        <td class="text-right">$20.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-left"><strong>Total</strong>
                                                        </td>
                                                        <td class="text-right">$162.00</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <p class="text-right"> <a class="btn view-cart" href="#"><i class="fa fa-shopping-cart"></i>View Cart</a>&nbsp;&nbsp;&nbsp; <a class="btn btn-mega checkout-cart" href="#"><i class="fa fa-share"></i>Checkout</a> 
                                            </p>
                                        </div>
                                    </li>-->
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </li>


                        </ul>



                    </div>
                </div>
            </div>
        </div>
        <!-- //Header Top -->

        <!-- Header center -->
        <div class="header-middle">
            <div class="container">
                <div class="row">

                    <!-- Search -->
                    <div class="middle2 col-lg-8 col-lg-offset-2 col-md-8">
                        <div class="responsive so-megamenu megamenu-style-dev">
                            <nav class="navbar-default">
                                <div class=" container-megamenu  horizontal open ">


                                    <div class="megamenu-wrapper">
                                        <span id="remove-megamenu" class="fa fa-times"></span>
                                        <div class="megamenu-pattern">
                                            <div class="container-mega">
                                                <ul class="megamenu" data-transition="slide" data-animationtime="250">
                                                    <li class="">
                                                        <a href="#">Home </a>

                                                    </li>
                                                    <li class="with-sub-menu hover">
                                                        <p class="close-menu"></p>
                                                        <a href="#" class="clearfix">
                                                            <strong>Women</strong>

                                                            <b class="caret"></b>
                                                        </a>
                                                        <div class="sub-menu" style="width: 100%; right: auto;">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="column">

                                                                            <div>
                                                                                <ul class="row-list">
                                                                                    <li><a href="#">Sandals </a></li>
                                                                                    <li><a href="#">Bellies</a></li>
                                                                                    <li><a href="#">Boots</a></li>
                                                                                    <li><a href="#">Heels</a></li>



                                                                                </ul>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="column">

                                                                            <div>
                                                                                <ul class="row-list">

                                                                                    <li><a href="#">Sports Shoes</a>
                                                                                    </li>
                                                                                    <li><a href="#">Sneakers</a></li>
                                                                                    <li><a href="#">Loafers</a></li>
                                                                                    <li><a href="#">Under 899
                                                                                        </a></li>


                                                                                </ul>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="column">

                                                                            <div>
                                                                                <ul class="row-list">
                                                                                    <li>
                                                                                        <img
                                                                                            src="image/catalog/menu/megabanner/women's-wear.jpg" />
                                                                                    </li>

                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="with-sub-menu hover">
                                                        <p class="close-menu"></p>
                                                        <a href="#" class="clearfix">
                                                            <strong>Men</strong>

                                                            <b class="caret"></b>
                                                        </a>
                                                        <div class="sub-menu" style="width: 100%; right: auto;">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="column">

                                                                            <div>
                                                                                <ul class="row-list">
                                                                                    <li><a href="#">Sports Shoes </a>
                                                                                    </li>
                                                                                    <li><a href="#">Sneakers</a></li>
                                                                                    <li><a href="#">Casual Shoes</a>
                                                                                    </li>
                                                                                    <li><a href="#">Loafers</a></li>



                                                                                </ul>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="column">

                                                                            <div>
                                                                                <ul class="row-list">

                                                                                    <li><a href="#">Formal Shoes</a>
                                                                                    </li>
                                                                                    <li><a href="#">Boots</a></li>
                                                                                    <li><a href="#">Canvas</a></li>
                                                                                    <li><a href="#">Under 899
                                                                                        </a></li>


                                                                                </ul>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="column">

                                                                            <div>
                                                                                <ul class="row-list">
                                                                                    <li>
                                                                                        <img
                                                                                            src="image/catalog/menu/megabanner/men's-wear.jpg" />
                                                                                    </li>

                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="with-sub-menu hover">
                                                        <p class="close-menu"></p>
                                                        <a href="#" class="clearfix">
                                                            <strong>New Arrivals </strong>
                                                            <img class="label-hot" src="image/catalog/menu/hot-icon.png"
                                                                alt="icon items">

                                                            <b class="caret"></b>
                                                        </a>
                                                        <div class="sub-menu" style="width: 100%; display: none;">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="row">
                                                                            <div class="col-md-6 img img1">
                                                                                <a href="#"><img
                                                                                        src="image/catalog/menu/megabanner/image-1.jpg"
                                                                                        alt="banner1"></a>
                                                                            </div>
                                                                            <div class="col-md-6 img img2">
                                                                                <a href="#"><img
                                                                                        src="image/catalog/menu/megabanner/image-2.jpg"
                                                                                        alt="banner2"></a>
                                                                            </div>


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <a href="#" class="title-submenu">Women </a>
                                                                        <div class="row">
                                                                            <div class="col-md-12 hover-menu">
                                                                                <div class="menu">
                                                                                    <ul>
                                                                                        <li><a href="#">Sports Shoes</a>
                                                                                        </li>
                                                                                        <li><a href="#">Sneakers</a>
                                                                                        </li>
                                                                                        <li><a href="#">Loafers</a></li>
                                                                                        <li><a href="#">Under 899
                                                                                            </a></li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <a href="#" class="title-submenu">Men</a>
                                                                        <div class="row">
                                                                            <div class="col-md-12 hover-menu">
                                                                                <div class="menu">
                                                                                    <ul>
                                                                                        <li><a href="#">Formal Shoes</a>
                                                                                        </li>
                                                                                        <li><a href="#">Boots</a></li>
                                                                                        <li><a href="#">Canvas</a></li>
                                                                                        <li><a href="#">Under 899
                                                                                            </a></li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li class="">
                                                        <p class="close-menu"></p>
                                                        <a href="#" class="clearfix">
                                                            <strong>Offer Zone</strong>

                                                        </a>

                                                    </li>



                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </nav>
                        </div>

                    </div>
                    <!-- //end Search -->


                </div>

            </div>
        </div>
        <!-- //Header center -->

    </header>
    <!--Header Container  -->