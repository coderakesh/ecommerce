
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    
    <!-- Basic page needs
    ============================================ -->
    <title>Spare parts, Apex Marketing</title>
    <meta charset="utf-8">
     <link rel="shortcut icon" type="image/png" href="ico/favicon.ico"/>  
    <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <?php include('link.php'); ?>
    <style type="text/css">
         body{font-family:'Roboto', sans-serif} 
		 
.about-text p {font-size: 16px;line-height: 25px;font-weight: 400;color: #333;}


    </style>

</head>

<body class="res layout-1">
    
    <div id="wrapper" class="wrapper-fluid banners-effect-5">
    

    <!-- Header Container  -->
    <?php include('header.php'); ?>
    <!-- //Header Container  -->
    <section style="background: #ececec;margin-bottom:30px;">
    <div class="container">
    <ul class="breadcrumb">
			<li><a href="index.php"><i class="fa fa-home"></i></a></li>
			<li><a href="#">Car We Deal</a></li>
			 
		</ul>
    </div>
    </section>
	<!-- Main Container  -->
	<div class="main-container container">
		 		
		<div class="row">
			<div id="content" class="col-sm-12">
				<div class="about-us about-demo-3">
					<div class="row">
						<div class="col-lg-5 col-md-5 about-image"> <img src="image/catalog/about.jpg" alt="About Us"> </div>
						<div class="col-lg-7 col-md-7 about-info">
							<h2 class="about-title">About Us</h2>
							<div class="about-text">
								<p>We specialize in spare parts of mostly all brands. Please call us in case you need spare parts of :
Hyndai, Toyota, Tata, Ford, Skoda, Renault, Honda, Maruti Suzuki, Chevrolet, Mahindra, Fiat and
Volkswagen.</p>
								<p>
Growth, achievement, results are the epitome of expansion. Indian automotive market is growing
with the growth of the Indian society along with the growth of the economy over the past few years.
Toyota, Honda, Ford, Audi, VW, Skoda - with the presence of all these foreign branded cars in the
market, came the opportunity for the service after market - spare parts.</p>
							</div>
						</div>
					</div>
					 
				</div>
				
			</div>
		</div>
	</div>
	<!-- //Main Container -->


	<!-- Footer Container -->
    <?php include('footer.php'); ?>
    <!-- //end Footer Container -->

    </div>
	
	
<!-- Include Libs & Plugins
	============================================ -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
	<script type="text/javascript" src="js/themejs/libs.js"></script>
	<script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
	<script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
	<script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
	<script type="text/javascript" src="js/datetimepicker/moment.js"></script>
	<script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
	
	
	<!-- Theme files
	============================================ -->
	
	
	<script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
	<script type="text/javascript" src="js/themejs/addtocart.js"></script>
	<script type="text/javascript" src="js/themejs/application.js"></script>
	
	
</body>
</html>