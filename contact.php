
<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    
    <!-- Basic page needs
    ============================================ -->
    <title>Spare parts, Apex Marketing</title>
    <meta charset="utf-8">
     <link rel="shortcut icon" type="image/png" href="ico/favicon.ico"/>  
    <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <?php include('link.php'); ?>
    <style type="text/css">
         body{font-family:'Roboto', sans-serif} 
		 
.typeheader-1 .header-middle .middle-right {padding-top: 20px;}
.font-12{font-size: 12px;}.typefooter-01 {margin-top: 0px;background: #ffffff !important;}
.info-contact {margin-bottom: 25px;}.typefooter-1 .infos-footer ul {margin-top: 20px;}


    </style>

</head>

<body class="res layout-1">
    
    <div id="wrapper" class="wrapper-fluid banners-effect-5">
    

    
   <!-- Header Container  -->
    <?php include('header.php'); ?>
    <!-- //Header Container  -->
    
    <section style="background: #ececec;margin-bottom:30px;">
    <div class="container">
    <ul class="breadcrumb">
			<li><a href="index.php"><i class="fa fa-home"></i></a></li>
			<li><a href="#">Contact</a></li>
			 
		</ul>
    </div>
    </section>
	<!-- Main Container  -->
	<div class="main-container container" style="max-width: 1170px !important;">
				
		<div class="row">
			<div id="content" class="col-sm-12">
				 
				<div class="info-contact clearfix">
					
					<div class="col-lg-6 col-sm-6 col-xs-12 contact-form">
						<form class="form-horizontal"  action="contact-file.php" method="post" enctype="multipart/form-data">
							<fieldset>
								<legend> Help & Support </legend>
                                <p>Our customer service team is available from Monday to Sunday.</p>
								<div class="form-group required">
							
							<div class="col-sm-6"><label class="control-label" for="input-name">Your Name</label>
								<input type="text" name="name"  id="name" class="form-control" required="required">
								</div>
                            <div class="col-sm-6"><label class="control-label" for="input-name">Mobile  Number</label>
								<input type="text"  name="mobile"  id="mobile" class="form-control" required="required">
								</div>
							</div>
							<div class="form-group required">
								
								<div class="col-sm-12">
                                    <label class="control-label" for="input-email" required="required">E-Mail Address</label>
									<input type="text" name="email" id="email" class="form-control">
									</div>
								</div>
								<div class="form-group required">
									
									<div class="col-sm-12">
                                        <label class="control-label" for="input-enquiry" required="required">Enquiry</label>
										<textarea name="message" rows="4" id="message" class="form-control"></textarea>
									</div>
								</div>
							</fieldset>
							<div class="buttons">
								<div class="pull-left">
									 <button class="icon-mail btn2-st2 btn-7 btn-7b" type="submit">Submit</button>
								</div>
							</div>
						</form>
					</div>
                    <div class="col-lg-6 col-sm-6 col-xs-12 info-store">
						<div class="typefooter-1 typefooter-01">
							<div class="name-store">
								<h3>Apex - CAR SPARE PARTS</h3>
							</div>
							<div class="infos-footer">
                             
                            <ul class="menu">
                                <li class="adres" style="margin-bottom: 10px;">
                                   	263, Zone II, M.P. Nagar, Bhopal, Madhya Pradesh, India
                                </li>
                                <li class="phone">
                                   +0755-4285542,     (+91) 810 959 5945, +91-9826091432
                                </li>
                                <li class="mail">
                                    <a href="javascript:location='mailto:\u0020\u0061\u0070\u0065\u0078\u0063\u0061\u0072\u0070\u0061\u0072\u0074\u0073\u0040\u0067\u006d\u0061\u0069\u006c\u002e\u0063\u006f\u006d';void 0"><script type="text/javascript">document.write('\u0020\u0061\u0070\u0065\u0078\u0063\u0061\u0072\u0070\u0061\u0072\u0074\u0073\u0040\u0067\u006d\u0061\u0069\u006c\u002e\u0063\u006f\u006d')</script></a>
                                </li>
                                <li class="time">
                                    Open time: 11:00AM - 8:00PM
                                </li>
                            </ul>
                        </div>
						</div>
					</div>
				</div>
				
				 
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d545.0218203207056!2d77.436642409957!3d23.22651846101983!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x397c42435002b509%3A0xabccaf42f4139cd0!2sAPEX+CAR+PARTS!5e0!3m2!1sen!2sin!4v1545056210243" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
				
			</div>
		</div>
	</div>
	<!-- //Main Container -->
	

    <!-- Footer Container -->
    <?php include('footer.php'); ?>
    <!-- //end Footer Container -->

    </div>
	
	
	
<!-- Include Libs & Plugins
============================================ -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="js/themejs/libs.js"></script>
<script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
<script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
<script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
<script type="text/javascript" src="js/datetimepicker/moment.js"></script>
<script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/contact_form_api.js" ></script> 

<!-- Theme files
============================================ -->


<script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
<script type="text/javascript" src="js/themejs/addtocart.js"></script>
<script type="text/javascript" src="js/themejs/application.js"></script>
	
</body>
</html>