<?php 
include('database.php');

include('functions.php');

include('session.php');

?>

<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
 <head>
 <? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>States</h1>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">states</li>
      </ul>
    </div>
    <div class="row  ">
      
	  <div class="col-md-12">
<div class="row">

<div class="col-md-12">
<div class="panel panel-primary">
<div class="panel-heading">
<h4 class="panel-title">Edit state</h4>
</div>
<div class="panel-body">

<!----------------- php code --------------------->
<?php 
 	$sid = $_REQUEST['sid'];
	 
	 $db->select('localization_states','*',NULL,'del_status="1" and sid="'.$sid.'"','sid DESC'); 
	
     $states = $db->getResult();
	 
	 $states = $states[0];
	 
?>
 <form  class="form-horizontal" action="edit_localization_states1.php" method="post" enctype="multipart/form-data" id="operator" >
 				<input type="hidden" value="<?php echo $sid; ?>" name="sid" />
						<div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">State name</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" name="state" id="textfield1" class="form-control" value="<?php echo $states['state']; ?>">
                          </div>
                        </div>
						
<div class="form-group">
                <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Update </button>
                  <button type="button" class="btn">Cancel</button>
                </div>
              </div>
</form>


</div>
</div>
</div>


</div>

</div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<? include('bottom_link.php'); ?>

</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
