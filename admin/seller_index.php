

<div class="row">

<div class="col-md-6">
<div class="row">
<div class="col-md-12">
<div class="tile box">
<div class="block">
<div class="FL">
<h3 class="title">Catalog</h3>
</div>
<div class="FL">
<a href="#"><h3 class="title" align="right">View Detail</h3></a></div>
</div>


<h5 class="brd">In stock & Out of stock</h5>
<div class="block2">
<div class="FL">
<h3 class="value">In stock</h3>
<p class="digit">0</p>
</div>
<div class="FL">
<h3 align="right" class="value">Out of stock</h3>
<p align="right" class="digit">0</p>
</div>
</div>
</div>
</div>
</div>
</div>




<div class="col-md-6">
<div class="row">
<div class="col-md-6">
<div class="tile tile-orange">
<div class="img">
<i class="fa fa-credit-card-alt"></i></div>
<div class="content">
<p class="big">128</p>
<p class="title">Credit Balance</p>
</div>
</div>
</div>

<div class="col-md-6">
<div class="tile tile-dark-blue">
<div class="img">
<i class="fa fa-area-chart"></i></div>
<div class="content">
<p class="big">+160</p>
<p class="title">All Sales</p>
</div>
</div>
</div>
</div>

<div class="row">
<div class="col-md-6">
<div class="tile tile-dark-blue">
<div class="img">
<i class="fa fa-comments"></i></div>
<div class="content">
<p class="big">128</p>
<p class="title">Total Visits</p>
</div>
</div>
</div>

<div class="col-md-6">
<div class="tile tile-orange">
<div class="img">
<i class="fa fa-shopping-cart"></i></div>
<div class="content">
<p class="big">+160</p>
<p class="title">Total Order</p>
</div>
</div>
</div>
</div>
</div>
</div>





<div class="row">
<div class="col-md-7">
<div class="box">
<div class="box-title">
<h3><i class="fa fa-bar-chart-o"></i> Visitors Chart</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
</div>
<div class="box-content">
<div id="visitors-chart" style="margin-top:20px; position:relative; height: 290px;"></div>
</div>
</div>
</div>
<div class="col-md-5">
<div class="box">
<div class="box-title">
<h3><i class="fa fa-bar-chart-o"></i> Recently Added Product</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
</div>
<div class="box-content">
<ul class="weekly-stats">
<li class="stats-list"> 
<p><img src="facebook.png" alt="" /></p>
<h5><a href="#">Women's Footwear Women's Footwear Women's Footwear Women's Footwear</a></h5>
</li>
<li class="stats-list"> 
<p><img src="facebook.png" alt="" /></p>
<h5><a href="#">Women's Footwear</a></h5>
</li>


<li class="stats-list">
<h6 style=""><a href="#">View All</a></h6>
</li>
</ul>
</div>
</div>
</div>
</div> 