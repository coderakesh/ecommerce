<?php 
include('database.php');

include('functions.php');

include('session.php');

?>

<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
<head>
 <? include('upper-link.php'); ?>

 </head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> Slider</h1>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Slider</li>
      </ul>
    </div>
    <div class="row  ">
 	  <div class="col-md-12">
<div class="row">

<div class="col-md-12">
<div class="panel panel-primary">
<div class="panel-heading">
<h4 class="panel-title">Add Master Slider</h4>
</div>
<div class="panel-body">
 <form  class="form-horizontal" action="slider1.php" method="post" enctype="multipart/form-data" id="operator" >
 			<div class="form-group">
                            <label class="col-sm-3 col-lg-3 control-label">Slider Type</label>
                            <div class="col-sm-9 col-lg-5 controls">
                    <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="stype" id="">
<option value="">--Select--</option>
<option value="slider">Slider</option>
<option value="boxslider">Box Slider</option>
<option value="box">Box</option>
<option value="position" >Position</option>
</select>                    </div>
                          </div>
            	 <div class="form-group">
                            <label class="col-sm-3 col-lg-3 control-label">Slider Name</label>
                            <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" value="" class="form-control" name="sname" >
                            </div>
                          </div>
  <div class="form-group">
                            <label class="col-sm-3 col-lg-3 control-label">Html Class</label>
                            <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" value="" class="form-control" name="htmlelemntid" >
                            </div>
                          </div>     <div class="form-group">
                            <label class="col-sm-3 col-lg-3 control-label">Width</label>
                            <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" value="" class="form-control" name="swidth" >
                            </div> </div>  <div class="form-group">
                            <label class="col-sm-3 col-lg-3 control-label">Height</label>
                            <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" value="" class="form-control" name="sheight" >
                            </div>
                          </div> <div class="form-group">
                            <label class="col-sm-3 col-lg-3 control-label"> Page </label>
                            <div class="col-sm-9 col-lg-5 controls">
 <input type="radio" name="page_type" value="1" checked > &nbsp; Home Page&nbsp; &nbsp; <input type="radio" name="page_type" value="0" > &nbsp;Category &nbsp;
                             </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 col-lg-3 control-label"> Show In Brand </label>
                            <div class="col-sm-9 col-lg-5 controls">
 <input type="radio" name="is_brand" value="1" checked > &nbsp; Yes; <input type="radio" name="is_brand" value="0" > &nbsp;No &nbsp;
                             </div>
                          </div> <div class="form-group">
                            <label class="col-sm-3 col-lg-3 control-label">Select Category</label>
                            <div class="col-sm-9 col-lg-5 controls">
   <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="cat_id" id="imgposition">
                              <option>--Select--</option>
                         <?   $parent_cats=mysql_query("select * from  catalog_category where status=0   order by category_id ASC  "); 	   while($parentcat = mysql_fetch_array($parent_cats)){ ?>      
     <option value="<? echo $parentcat['category_id']; ?>" ><? echo $parentcat['name']; ?></option>    <? } ?>                       </select>                            </div>
                          </div><div class="form-group">
                            <label class="col-sm-3 col-lg-3 control-label">SQL Query</label>
                            <div class="col-sm-9 col-lg-5 controls">
  <input type="text" value="" class="form-control" name="sql_qry" >
                            </div>
                          </div>   
                                <div class="form-group">
                            <label class="col-sm-3 col-lg-3 control-label">Order Number</label>
                            <div class="col-sm-9 col-lg-5 controls">
  <input type="number" value="" class="form-control" name="ord_num" >
                            </div>
                          </div>
 	  
                        <div class="form-group">
                            <label class="col-sm-3 col-lg-3 control-label"></label>
                            <div class="col-sm-9 col-lg-5 controls">
     <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                       </div>
                          </div>
   
</form>

<div class="col-md-12">
<div class="box box-magenta">
<div class="box-title">
<h3>  Slider</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a>
</div>
</div>
<div class="box-content">
<?php $db->select('frontpage_tslider','*',NULL,'','imgid DESC');
	  $res = $db->getResult();
  ?>
<table class="table table-bordered">
<thead>
<tr>
<th>S.no</th>
<th>Image</th>
<th>Status</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php $i=1; foreach($res as $values ){ ?>
<tr>
<td><?php echo $i; ?></td>
<td><img src="<?php echo $values['img'] ?>" height="50" width="50"></td>
<td> <?php echo $values['cstatus'] ?></td>
<td><a class="btn btn-primary btn-sm" href="frontpage_tsliderupdate.php?id=<?php echo $values['imgid'];?>&cstatus=<?php echo $values['cstatus'];?>"><i class="fa fa-edit"></i> <?php echo $values['cstatus'] ?></a></td>
</tr>
<?php $i++; } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
 </div>

</div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->
 <? include('bottom_link.php'); ?>

</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
