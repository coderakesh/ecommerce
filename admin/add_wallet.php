<?php 
include('database.php');

include('functions.php');

include('session.php');

$operator_id = $_REQUEST['operator_id'];

?>

<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
<head> 
<? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
 
<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); 
$user_id = $_REQUEST['uid'];
?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> <? echo get_user_fulname($user_id); ?> Wallet  </h1>
        <h4>Edit  Wallet Amount</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Edit  Wallet Amount </li>
      </ul>
    </div>
	<? if($_REQUEST['status']==1){ ?><div class="alert alert-success  "  id="success" >
<button class="close" data-dismiss="alert">&times;</button>
<strong>Success!</strong> Wallet amount update successfully.</div><? } ?>
    <div class="row  ">
      <div class="col-md-4">
        <div class="box box-green">
          <div class="box-title">
            <h3><i class="fa fa-bars"></i>Edit  Wallet Amount </h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
          </div>
          <div class="box-content">
		  <?php 
		   $db->select('users','*','',"user_id='".$user_id."'",'',''); $res = $db->getResult(); $res = $res[0]; 
		   		$checked = 'checked="checked"';
		   	?>
            <form  class="form-horizontal" action="add_wallet1.php" method="post" enctype="multipart/form-data" id="operator" >
            <input type="hidden" value="<? echo $user_id ; ?>" name="user_id" >
			  <div class="row">
                <div class="col-md-12 ">
                  <!-- BEGIN Left Side -->
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label"> Wallet Amount</label>
                    <div class="col-sm-9 col-lg-10 controls">
				       <input type="text" name="wallet_amount" id="textfield1" placeholder="" value=" " class="form-control">
                    </div>
                  </div>        </div>
                <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
				  <input type="submit" class="btn btn-primary" value="Save" onClick="return confirm('Are you sure to add amount');" >
                    <button type="button" class="btn">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
	  <div class="col-md-8">

<div class="box">
<div class="box-title">
<h3><i class="fa fa-table"></i>Wallet History</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a>
</div>
</div>
<div class="box-content">
<div class="clearfix"></div>
<div class="table-responsive" style="border:0">
 
<table class="table table-advance" id="table1">
<thead>
<tr>
<th>S.no.</th>
<th>Previous Amount</th>
<th>Add Amount</th>
<th>Deduct Amount</th>
<th class="">Current Amount</th>
 <th>Date</th>
<th>Time</th>
<th>Mode</th>
<th>Added By</th>
 </tr>
</thead>
<tbody>
<?php $i=1;
$qre = mysql_query("select * from wallet_amount_history where user_id = ".$user_id." order by wallet_id desc");
while($values = mysql_fetch_array($qre)){ ?>
<tr class="table-flag-blue">
<td><?php echo $i; ?></td>
<td><?php if($values['previous_amt']!=''){ echo $values['previous_amt'];} else{ echo '0'; } ?></td>
<td><?php if($values['amount']!=''){ echo $values['amount'];} else{ echo '0'; } ?></td>
<td><?php if($values['deduct_amount']!=''){ echo $values['deduct_amount'];} else{ echo '0'; } ?></td>
<td><?php if($values['current_amount']!=''){ echo $values['current_amount'];} else{ echo '0'; } ?></td>
<td><?php echo $values['cdate']; ?></td>
<td><?php echo $values['ctime']; ?></td>
<td><?php if($values['mode']==0){ echo 'Admin';} else{ echo User; } ?></td>
<td><?php if($values['mode']==0){ echo 'Jhoomerwala';} else{ echo get_user_fulname($values['added_by']); } ?></td>

 
  </tr>
<?php $i++; } ?>
</tbody>
</table>


</div>
</div>
</div>

</div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->
<? include('bottom_link.php'); ?>

</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
