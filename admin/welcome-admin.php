<?php 
include('database.php');

include('functions.php');

include('session.php');

?>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Dashboard  </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<!--base css styles-->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/font-awesome-4.1.0/css/font-awesome.css">
<!--page specific css styles-->

<!--flaty css styles-->
<link rel="stylesheet" href="css/flaty.css">
<link rel="stylesheet" href="css/flaty-responsive.css">

<link rel="shortcut icon" href="img/favicon.html">
</head>
<body>

<!-- BEGIN Theme Setting -->
<div id="theme-setting">
<a href="#"><i class="fa fa-gears fa fa-2x"></i></a>
<ul>
<li>
<span>Skin</span>
<ul class="colors" data-target="body" data-prefix="skin-">
<li class="active"><a class="blue" href="#"></a></li>
<li><a class="red" href="#"></a></li>
<li><a class="green" href="#"></a></li>
<li><a class="orange" href="#"></a></li>
<li><a class="yellow" href="#"></a></li>
<li><a class="pink" href="#"></a></li>
<li><a class="magenta" href="#"></a></li>
<li><a class="gray" href="#"></a></li>
<li><a class="black" href="#"></a></li>
</ul>
</li>
<li>
<span>Navbar</span>
<ul class="colors" data-target="#navbar" data-prefix="navbar-">
<li class="active"><a class="blue" href="#"></a></li>
<li><a class="red" href="#"></a></li>
<li><a class="green" href="#"></a></li>
<li><a class="orange" href="#"></a></li>
<li><a class="yellow" href="#"></a></li>
<li><a class="pink" href="#"></a></li>
<li><a class="magenta" href="#"></a></li>
<li><a class="gray" href="#"></a></li>
<li><a class="black" href="#"></a></li>
</ul>
</li>
<li>
<span>Sidebar</span>
<ul class="colors" data-target="#main-container" data-prefix="sidebar-">
<li class="active"><a class="blue" href="#"></a></li>
<li><a class="red" href="#"></a></li>
<li><a class="green" href="#"></a></li>
<li><a class="orange" href="#"></a></li>
<li><a class="yellow" href="#"></a></li>
<li><a class="pink" href="#"></a></li>
<li><a class="magenta" href="#"></a></li>
<li><a class="gray" href="#"></a></li>
<li><a class="black" href="#"></a></li>
</ul>
</li>
<li>
<span></span>
<a data-target="navbar" href="#"><i class="fa fa-square-o"></i> Fixed Navbar</a>
<a class="hidden-inline-xs" data-target="sidebar" href="#"><i class="fa fa-square-o"></i> Fixed Sidebar</a></li>
</ul>
</div>
<!-- END Theme Setting -->

<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->

<!-- BEGIN Container -->
<div class="container" id="main-container">
<!-- BEGIN Sidebar -->
<?php include('leftmenu.php'); ?>
<!-- END Sidebar -->

<!-- BEGIN Content -->
<div id="main-content">
<!-- BEGIN Page Title -->
<div class="page-title">
<div>
<h1><i class="fa fa-file-o"></i> Hello, Admin <small>(Dashboard)</small></h1>
<h4>Overview, stats, chat and more</h4>
</div>
</div>
<!-- END Page Title -->

<!-- BEGIN Breadcrumb -->
<!--   <div id="breadcrumbs">
<ul class="breadcrumb">
<li class="active"><i class="fa fa-home"></i> Home</li>
</ul>
</div>  -->
<!-- END Breadcrumb -->


<div class="row">
 <div class="col-md-6">
<div class="row">
<div class="col-md-12">
<div class="tile box">
<div class="block">
<div class="FL">
<h3 class="title">Catalog</h3>
</div>
<div class="FL">
<a href="#"><h3 class="title" align="right">View Detail</h3></a>
</div>
</div>


<h5 class="brd">In stock & Out of stock</h5>
<div class="block2">
<div class="FL">
<h3 class="value">In stock</h3>
<p class="digit">0</p>
</div>
<div class="FL">
<h3 align="right" class="value">Out of stock</h3>
<p align="right" class="digit">0</p>
</div>
</div>

</div>
</div>
</div>


</div>




<div class="col-md-6">
<div class="row">
<div class="col-md-6">
<div class="tile tile-orange">
<div class="img">
<i class="fa fa-user-plus"></i>
</div>
<div class="content">
<p class="big"><? echo getTotsell();?></p>
<p class="title">Total Seller</p>
</div>
</div>
</div>

<div class="col-md-6">
<div class="tile tile-dark-blue">
<div class="img">
<i class="fa fa-users"></i>
</div>
<div class="content">
<p class="big"><? echo getTotuser();?></p>
<p class="title">Total User</p>
</div>
</div>
</div>
</div>

<div class="row">
<div class="col-md-6">
<div class="tile tile-dark-blue">
<div class="img">
<i class="fa fa-user-times"></i>
</div>
<div class="content">
<p class="big"><? echo getPendsell();?></p>
<p class="title">Seller Pending</p>
</div>
</div>
</div>

<div class="col-md-6">
<div class="tile tile-orange">
<div class="img">
<i class="fa fa-cart-arrow-down"></i>
</div>
<div class="content">
<p class="big">+<? echo getPendord(); ?></p>
<p class="title">Order Pending</p>
</div>
</div>
</div>
</div>
</div>
</div>





<div class="row">
<div class="col-md-7">
<div class="box box-green1">
<div class="box-title">
<h3><i class="fa fa-table"></i> Latest Order</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a>
</div>
</div>
<div class="box-content">
<table class="table table-striped">
<thead>
<tr>
<th>Order Id</th>
<th>Ordered Date</th>
<th>Status</th>
<th>View</th>
</tr>
</thead>
<tbody>
<tr>
<td>1001</td>
<td>18-01-16</td>
<td><span class="label label-success">C</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1002</td>
<td>18-01-16</td>
<td><span class="label label-important">P</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1003</td>
<td>18-01-16</td>
<td><span class="label label-success">C</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1004</td>
<td>18-01-16</td>
<td><span class="label label-important">P</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1005</td>
<td>18-01-16</td>
<td><span class="label label-success">C</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1006</td>
<td>18-01-16</td>
<td><span class="label label-important">P</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1007</td>
<td>18-01-16</td>
<td><span class="label label-success">C</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1008</td>
<td>18-01-16</td>
<td><span class="label label-important">P</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1009</td>
<td>18-01-16</td>
<td><span class="label label-success">C</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
</tbody>
<tfoot>
<tr>
<td colspan="4"><a href="#"><span class="label label-info"> View All <i class="fa fa-caret-right"></i></span></a></td>
</tr>
</tfoot>

</table>
</div>
</div>
</div>
<div class="col-md-5">

<div class="tile tile-blue-dark ">
<div class="img">
<i class="fa fa-shopping-bag"></i>
</div>
<div class="content">
<p class="big">+<? echo getordsuccess(); ?></p>
<p class="title">Total Selling</p>
</div>
</div>



<div class="box box-green1">
<div class="box-title">
<h3><i class="fa fa-bar-chart-o"></i> Visitors Chart</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a>
</div>
</div>
<div class="box-content">
<div id="visitors-chart" style="margin-top:20px; position:relative; height: 290px;"></div>
</div>
</div>
</div>
</div>

<?php include('footer.php'); ?>

<a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
<!-- END Content -->
</div>
<!-- END Container -->


<!--basic scripts-->
<script src="../../ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/jquery/jquery-2.1.1.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/jquery-cookie/jquery.cookie.js"></script>

<!--page specific plugin scripts-->
<script src="assets/flot/jquery.flot.js"></script>
<script src="assets/flot/jquery.flot.resize.js"></script>
<script src="assets/flot/jquery.flot.pie.js"></script>
<script src="assets/flot/jquery.flot.stack.js"></script>
<script src="assets/flot/jquery.flot.crosshair.js"></script>
<script src="assets/flot/jquery.flot.tooltip.min.js"></script>
<script src="assets/sparkline/jquery.sparkline.min.js"></script>

<!--flaty scripts-->
<script src="js/flaty.js"></script>
<script src="js/flaty-demo-codes.js"></script>
</body>

</html>
