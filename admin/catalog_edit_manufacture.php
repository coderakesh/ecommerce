<?php 
include('database.php');

include('functions.php');

include('session.php');

?>

<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
<head>
<? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Edit Manufacturers</h1>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Edit Manufacturers</li>
      </ul>
    </div>
    <div class="row  ">
      
	  <div class="col-md-12">
<div class="row">
<div class="col-md-12">
<div class="panel panel-primary">
<div class="panel-heading">
<h4 class="panel-title">Edit Manufacturers</h4>
</div>
<div class="panel-body">
<form  class="form-horizontal" action="catalog_product_edit_manuf1.php" method="post" enctype="multipart/form-data" id="operator" >
              <div class="row">
                <div class="col-md-12 ">
                  <!-- BEGIN Left Side -->
                  <?php
				  $manu_id = $_REQUEST['manu_id'];
			       $sql1="select * from catalog_product_manufacturers where manu_id='$manu_id'";
				   $db->sql($sql1);
			       $res = $db->getResult();
				$res =  $res[0] ;
					?>
                    <input type="hidden" value="<?php echo $manu_id;?>" name="manuid"/>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Name</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <input type="text" name="mname" id="textfield1" class="form-control" value="<?php echo $res[mname] ?>">
                    </div>
                  </div>
				  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Description</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <textarea name="mdescription" id="textarea2" rows="5" class="form-control"><?php echo $res[mdescription] ?></textarea>
                    </div>
                  </div>
                	<div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label">logo</label>
                    <div class="col-sm-9 col-lg-10 controls">
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                        <input type="hidden" value="" name="">
                        <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;"> <img src="../<?php echo $res[mlogo]; ?>" alt=""></div>
                        <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                        <div> <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span> <span class="fileupload-exists">Change</span>
                          <input type="file" class="file-input" name="mlogo"/>
                          </span> <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a></div>
                      </div>
                    </div>
                  </div>
				  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Meta title</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <input type="text" name="mmeta_title" id="textfield1"  class="form-control" value="<?php echo $res[mmeta_title] ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Meta keywords</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <textarea name="mmeta_keywords" id="textarea2" rows="5" class="form-control"><?php echo $res[mmeta_keyword] ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Meta Description</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <textarea name="mmeta_description" id="textarea2" rows="5" class="form-control"><?php echo $res[mmeta_description] ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                      <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                      <button type="button" class="btn">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </form></div>
</div>
</div>
</div>
</div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container --><? include('bottom_link.php'); ?>

</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
