<?php 
include('database.php');

include('functions.php');

include('session.php');

$category_id = $_REQUEST['category_id'];

?>
<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
<head>
<? include('links.php'); ?>

</head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> Category </h1>
        <h4>Edit category</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Edit category</li>
      </ul>
    </div>
    <div class="row  ">
      <div class="col-md-12">
        <div class="box box-green">
          <div class="box-title">
            <h3><i class="fa fa-bars"></i>Edit category</h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
          </div>
          <div class="box-content">
		  <?php $db->select('catalog_category','*', '',"category_id='".$category_id."'",'','','category_id DESC','');

				$category = $db->getResult(); 
				
				$category = $category[0];
				?>
            <form  class="form-horizontal" action="catalog_edit_category1.php" method="post" enctype="multipart/form-data" id="operator" >
			<input type="hidden" value="<?php echo $category_id; ?>" name="category_id" />
              <div class="row">
                <div class="col-md-12 ">
                  <!-- BEGIN Left Side -->
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Category name</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <input type="text" name="name" id="textfield1" class="form-control" value="<?php echo $category['name']; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Page title</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <input type="text" name="page_title" id="textfield1"  va class="form-control" value="<?php echo $category['page_title']; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Parent category</label>
                    <div class="col-sm-9 col-lg-5 controls">
					  <input type="text"  class="form-control" id="state"  name="parent_id1" value="<?php echo get_nested_category($category['category_id']); ?>"/>
					  <input type="hidden" id="state_abbrev" name="parent_id" value="<?php echo $category['parent_id']; ?>" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">HSN Code</label>
                    <div class="col-sm-9 col-lg-5 controls">
					  <input type="text"  class="form-control"   name="hsncode" value="<?php echo  ($category['hsncode']); ?>"/>
				  </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Display mode</label>
                    <div class="col-sm-9 col-lg-5 controls">
                         <input type="radio" name="display_mode" value="Y"  <? if($category['display_mode']=='Y') { ?> checked <? } ?>> Yes&nbsp;&nbsp;
                       <input type="radio" name="display_mode" value="N"  <? if($category['display_mode']=='N') { ?> checked <? } ?>> No         </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label">Right Slider Image Upload</label>
                    <div class="col-sm-9 col-lg-10 controls">
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                        <input type="hidden" value="" name="">
                        <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo "../".$category['image']; ?>" alt=""></div>
                        <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                        <div> <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span> <span class="fileupload-exists">Change</span>
                          <input type="file" class="file-input" name="category_image">
                          </span> <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a></div>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label">Main Image In Cat List Upload</label>
                    <div class="col-sm-9 col-lg-10 controls">
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                         <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo "../".$category['main_img']; ?>" alt=""></div>
                        <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                        <div> <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span> <span class="fileupload-exists">Change</span>
 <input type="file" class="file-input" name="main_image">
                          </span> <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a></div>
                      </div>
                    </div>
                  </div>
                    <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Select Icon</label>
                    <div class="col-sm-9 col-lg-5 controls">
                    <? if($category['icon']!=''){ ?><img src="<?php echo "../".$category['icon']; ?>" alt=""><? }?>
                    <input type="file" name="icon" class="form-control" >
                    </div>
                  </div>  
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Meta keywords</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <textarea name="meta_keywords" id="textarea2" rows="5" class="form-control"><?php echo $category['meta_keyword']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Meta Description</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <textarea name="meta_description" id="textarea2" rows="5" class="form-control"><?php echo $category['meta_description']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Description</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <textarea name="description" id="textarea2" rows="5" class="form-control"><?php echo $category['description']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Top order</label>
                    <div class="col-sm-9 col-lg-5 controls">
                	<select name="top" class="form-control">
                        <option value="top" <?php if($category['top']=='top'){ ?> selected <? } ?>>Top Menu</option>
                        <option value="menu" <?php if($category['top']=='menu'){ ?> selected <? } ?>>  Menu</option>
                      <option value="sub_menu"  <?php if($category['top']=='sub_menu'){ ?> selected <? } ?>>Sub Menu</option>                        </select>
                     </div>
                  </div>
        
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">For Seller</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <label class="checkbox">
                      <input type="radio" name="is_seller" value="Y" <? if($category['is_seller']=='Y') { ?> checked <? } ?>> Yes&nbsp;&nbsp;
                       <input type="radio" name="is_seller" value="N" <? if($category['is_seller']=='N') { ?> checked <? } ?>> No
                      </label>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Home Product Category</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <label class="checkbox">
                      <input type="radio" name="home_product" <? if($category['home_product']=='Y'){ ?> checked <? } ?> value="Y"> Yes&nbsp;&nbsp;
                       <input type="radio" name="home_product"  <? if($category['home_product']=='N'){ ?> checked <? } ?>value="N"> No
                      </label>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                      <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Update</button>
                      <button type="button" class="btn">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->
<!--basic scripts-->
<script src="../../ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/jquery/jquery-2.1.1.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/jquery-cookie/jquery.cookie.js"></script>
<!-------------------- for search----------------->
<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript">
$(function() {
            $("#state").autocomplete({
                source: "get_catalog_category.php",
                minLength: 2,
                select: function(event, ui) {
                  
                    $('#abbrev').val(ui.item.abbrev);
					 $('#state_abbrev').val(ui.item.country_id);
                }
            });
            
        });
</script>
<!-------------------- for search----------------->
<!--page specific plugin scripts-->
<script type="text/javascript" src="assets/chosen-bootstrap/chosen.jquery.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script type="text/javascript" src="assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="assets/jquery-pwstrength/jquery.pwstrength.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js"></script>
<script type="text/javascript" src="assets/dropzone/downloads/dropzone.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="assets/clockface/js/clockface.js"></script>
<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-switch/static/js/bootstrap-switch.js"></script>
<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/bootstrap3/dataTables.bootstrap.js"></script>
<!--flaty scripts-->
<script src="js/flaty.js"></script>
<script src="js/flaty-demo-codes.js"></script>
</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
