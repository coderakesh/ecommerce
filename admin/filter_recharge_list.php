<?php include('database.php');

include('functions.php');

include('session.php');
?>
<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
<head>
 <? include('links.php'); ?>

</head>
<body>
<!-- BEGIN Theme Setting -->
<? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> Filter Recharge List </h1>
        <h4>filter the list of Recharge</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Filter Recharge List</li>
      </ul>
    </div>
    <div class="row  ">
<div class="col-md-12">
<div class="box box-green">
<div class="box-title">
<h3><i class="fa fa-bars"></i> Search Recharge</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a>
</div>
</div>
<div class="box-content">
<form action="search_recharge.php" method="post" onSubmit="return validateform()"class="form-horizontal">
<div class="row">
   <div class="col-md-6 ">
  <!-- BEGIN Left Side -->
<div class="form-group">
<label for="textfield1" class="col-xs-3 col-lg-3 control-label">Start Date</label>
<div class="col-sm-9 col-lg-9 controls">
<input type="text" name="dp1" id="dp1" placeholder="Select Start Date"  class="form-control date-picker" value="<? echo date('Y/m/d'); ?>">
</div>
</div>
<div class="form-group">
<label for="password1" class="col-xs-3 col-lg-3 control-label">Select Status</label>
<div class="col-sm-9 col-lg-9 controls">
 <select class="form-control input-sm" tabindex="1" name="p_status" id="p_status">
 <option value="">Select</option>
 <option value="SUCCESS">Success</option>
 <option value="FAILURE">Failure</option>
 </select>
                 
</div>
</div>
  </div>
  <!-- END Left Side -->
   <div class="col-md-6 ">
  <!-- BEGIN Right Side -->
<div class="form-group">
<label for="textfield2" class="col-xs-3 col-lg-3 control-label">End Date</label>
<div class="col-sm-9 col-lg-9 controls">
<input type="text" name="dp2" id="dp2" placeholder="Select End Date"  class="form-control date-picker" value="<? echo date('Y/m/d'); ?>"></div>
</div>
<div class="form-group">
<label for="password2" class="col-xs-3 col-lg-3 control-label">Payment Mode</label>
<div class="col-sm-9 col-lg-9 controls">
<select class="form-control input-sm" tabindex="1" name="p_mode" id="p_mode">
 <option value="">Select</option>
 <option value="0">Wallet</option>
 <option value="1">Gateway</option>
 </select>
</div>
</div>
</div>

<div class="form-group">
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-5">
   <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>Search</button>
  
</div>
</div>
  <!-- END Right Side -->
   </div>
</div>
 </form>
</div>
</div>
</div>
</div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<? include('bottom_link.php'); ?>

<script>
function validateform()
{
	var dt1 = $("#dp1").val();
	var dt2 = $("#dp2").val();
	var st = $("#p_status").val();
	var md = $("#p_mode").val();
	var valid = true;
	if(dt1=='' && st=='' && md=='' && dt2=='')
	{
		alert("please select the feilds to filter");
		valid=false;
		return valid;
	}
}
</script>
</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
