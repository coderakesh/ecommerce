<?php 
include('database.php');

include('functions.php');

include('session.php');

?>

<!DOCTYPE html>
<html>
 <head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><? echo  backend_prefix(); ?></title> 
       <link rel="stylesheet" href="css/css/bootstrap.css" />

<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/font-awesome-4.1.0/css/font-awesome.css">
<!--page specific css styles-->
<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen.min.css" />
<link rel="stylesheet" type="text/css" href="assets/jquery-tags-input/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="assets/jquery-pwstrength/jquery.pwstrength.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-fileupload/bootstrap-fileupload.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.css" />
<link rel="stylesheet" type="text/css" href="assets/dropzone/downloads/css/dropzone.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="assets/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-switch/static/stylesheets/bootstrap-switch.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<!--flaty css styles-->
<link rel="stylesheet" href="css/flaty.css">
<link rel="stylesheet" href="css/flaty-responsive.css">
<link rel="shortcut icon" href="img/favicon.html">
<link rel="stylesheet" href="assets/data-tables/bootstrap3/dataTables.bootstrap.css" />

  <link rel="stylesheet" type="text/css" href="validation/livevalid.css" />
   <style type="text/css" title="currentStyle">
   tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
			tfoot { display:table-header-group ; }
</style>		<link rel="stylesheet" href="css/colorbox.css" />
        <link rel="stylesheet" href="css/tooltip.css"><link rel="stylesheet" href="css/multiple-select.css" />  </head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Product attribute</h1>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Product attribute</li>
      </ul>
    </div>
    <div class="row  ">
      
	  <div class="col-md-12">
<div class="row">

<div class="col-md-12">
<div class="panel panel-primary">
<div class="panel-heading">
<h4 class="panel-title">Add Attribute</h4>
</div>
<div class="panel-body">
 <form  class="form-horizontal" action="catalog_product_attribute1.php" method="post" enctype="multipart/form-data" id="operator" >
						<div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Display Name</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" name="aname" id="textfield1" class="form-control">
                          </div>
                        </div> <div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Backend Name</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" name="apublic_name" id="textfield1" class="form-control">
                          </div>
                        </div>
                        <div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Show in select </label>
                          <div class="col-sm-9 col-lg-5 controls">
<input type="radio" name="status" value="1" >Yes&nbsp;   <input type="radio" name="status" value="0"  checked>No&nbsp;                       </div>
                        </div>
						<div class="form-group" style="display:none">
<label for="textfield2" class="control-label col-xs-3 col-lg-2" id="b1">Select Categories </label>
<div class="col-sm-9 col-lg-5 controls"  id="b2">
<select  name="catlist[]" id="ms"    class="blank12    " multiple="multiple" >
         <?php $query = mysql_query("select   * from catalog_category  ")or die(mysql_error());
while($fetch1 = mysql_fetch_assoc($query)){
$qwe =  mysql_query("SELECT category_id,GetAncestry(category_id) as parents,name from catalog_category where category_id = ".$fetch1['category_id']);  $rwe = mysql_fetch_array($qwe); 
$ary = array(); $ary = explode(',',$rwe['parents']); $cnt = count($ary);
   if(($cnt=='1') && ($rwe['parents']!='')){
 ?><option value="<?php echo $fetch1['category_id'];?>"><?php echo $fetch1['name'];?> > <? echo get_category_name($rwe['parents']); ?></option>
	<?	} 
 } ?>  
</optgroup>    </select></div>
</div>
<div class="form-group">
                <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                 </div>
              </div>
</form>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-10">
   <a href="catalog_product_attribute_value.php" class="btn btn-primary"><i class="fa fa-check"></i> Add attribute value</a><br><br>
</div>
<div class="col-md-12">
<div class="box box-magenta">
<div class="box-title">
<h3><i class="fa fa-table"></i> Attribute list</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
</div>
<div class="box-content">
<?php $db->select('catalog_product_attribute','*',NULL,'','attribute_id DESC');
	  $res = $db->getResult();

	  //$res = $res[0];
	  //print_r($res);
 ?>
<table class="table table-bordered">
<thead>
<tr>
<th>S.no</th>
<th>Attribute Name</th><th>Backend Attribute name</th>

<th>Action</th>
</tr>
</thead>
<tbody>
<?php $i=1; foreach($res as $values ){ ?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $values['aname'] ?></td>
<td><?php echo $values['apublic_name'] ?></td>
<td><a class="btn btn-primary btn-sm" href="catalog_product_attributeedit.php?atrid=<? echo $values['attribute_id']; ?>"><i class="fa fa-edit"></i> Edit</a></td>
</tr>
<?php $i++; } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div> 
<? include('bottom_link.php'); ?>

<script src="js/multiple-select.js"></script>
<script>
    $(function() {
        $('#ms').change(function() {
            console.log($(this).val());
        }).multipleSelect({
            width: '100%'
        });
    });
</script>
</body>
</html>
