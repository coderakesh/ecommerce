<?php 
include('database.php');
include('functions.php');
include('session.php');
?>
    <!DOCTYPE html>
    <html>

    <head>
        <? include('links.php'); ?>
            <!---------------- add tree --------------->
            <link rel="stylesheet" href="tree/jquery.treeview.css" />

            <link rel="stylesheet" href="tree/red-treeview.css" />
            <script src="ckeditor/ckeditor.js"></script>
            <script type="text/javascript">
                function PreviewImage(no) {
                    var oFReader = new FileReader();
                    oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
                    oFReader.onload = function(oFREvent) {
                        document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
                        $('#cover' + no).val(oFREvent.target.result);
                        /* $('#cover_'+no).val(no-1);
                         alert($('#cover_'+no).val());*/

                    };

                }

                function change1() {
                    $('#cover_1').attr('checked', 'checked');
                };
            </script>
            <script language="Javascript">
                /**
                                 * DHTML textbox character counter script. Courtesy of SmartWebby.com (https://www.smartwebby.com/dhtml/)

                                 */

                maxL = 70;
                var bName = navigator.appName;

                function taLimit(taObj) {
                    if (taObj.value.length == maxL) return false;
                    return true;
                }

                function taCount(taObj, Cnt) {
                    objCnt = createObject(Cnt);
                    objVal = taObj.value;
                    if (objVal.length > maxL) objVal = objVal.substring(0, maxL);
                    if (objCnt) {
                        if (bName == "Netscape") {
                            objCnt.textContent = maxL - objVal.length;
                        } else {
                            objCnt.innerText = maxL - objVal.length;
                        }

                    }
                    return true;
                }

                function createObject(objId) {
                    if (document.getElementById) return document.getElementById(objId);
                    else if (document.layers) return eval("document." + objId);
                    else if (document.all) return eval("document.all." + objId);
                    else return eval("document." + objId);
                }
            </script>
            <script language="Javascript">
                /**
                 * DHTML textbox character counter script. Courtesy of SmartWebby.com (https://www.smartwebby.com/dhtml/)
                 */

                maxL1 = 160;
                var bName = navigator.appName;

                function taLimit1(taObj) {

                    if (taObj.value.length == maxL1) return false;
                    return true;
                }

                function taCount1(taObj, Cnt) {
                    objCnt = createObject(Cnt);
                    objVal = taObj.value;
                    if (objVal.length > maxL1) objVal = objVal.substring(0, maxL1);
                    if (objCnt) {
                        if (bName == "Netscape") {
                            objCnt.textContent = maxL1 - objVal.length;
                        } else {
                            objCnt.innerText = maxL1 - objVal.length;
                        }

                    }
                    return true;
                }

                function createObject(objId) {
                    if (document.getElementById) return document.getElementById(objId);
                    else if (document.layers) return eval("document." + objId);
                    else if (document.all) return eval("document.all." + objId);
                    else return eval("document." + objId);

                }
            </script>

            <!---------------- add tree --------------->

            <style>
                .tagsinput {
                    width: 400px !important;
                }
            </style>

    </head>

    <body>
        <!-- BEGIN Theme Setting -->
        <? include('right_bar.php'); ?>
            <!-- END Theme Setting -->
            <!-- BEGIN Navbar -->
            <?php  include('header.php'); ?>
                <!-- END Navbar -->
                <!-- BEGIN Container -->
                <div class="container" id="main-container">

                    <!-- BEGIN Sidebar -->

                    <?php include('leftmenu.php'); ?>

                        <!-- END Sidebar -->

                        <!-- BEGIN Content -->

                        <div id="main-content">

                            <!-- BEGIN Page Title -->

            <?php $product_id = $_REQUEST['product_id']; 

				 $db->select('volumetric_cal','*','',"types='SKU'",'','');
 				 $sku = $db->getResult();
                   $sku = $sku[0];
				   $checked ='checked="checked"';
				   $selected = 'selected="selected"';
		  		if($product_id==''){
                $action = "catalog_add_product1.php"; $title ='Add';
                }else{
				 $db->select('catalog_product','*','',"product_id='".$product_id."'",'','');
 				$product = $db->getResult();
                   $product = $product[0];
				   $title ='Edit';
				   //print_r($product);
 				$action = "catalog_edit_product1.php";
				} 

		  ?>
                                <div class="page-title">
                                    <div>
                                        <h1><i class="fa fa-file-o"></i> Product </h1>
                                        <h4>
                        <? echo $title; ?> new product</h4>
                                    </div>
                                </div>
                                <!-- END Page Title -->
                                <!-- BEGIN Breadcrumb -->
                                <div id="breadcrumbs">
                                    <ul class="breadcrumb">
                                        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i
                                class="fa fa-angle-right"></i></span></li>
                                        <li class="active">
                                            <? echo $title; ?> new product</li>
                                    </ul>
                                </div>
                                <div class="row  ">
                                    <div class="col-md-12">
                                        <div class="box box-green">
                                            <div class="box-title">
                                                <h3><i class="fa fa-bars"></i>
                                <? echo $title; ?> product</h3>

                                                <div class="box-tool"> <a data-action="collapse" href="#"><i
                                        class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i
                                        class="fa fa-times"></i></a></div>
                                            </div>
                                            <div class="box-content">
                                                <form class="form-horizontal" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="operator">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="tabbable tabs-left">
                                                                <ul id="myTab3" class="nav nav-tabs active-red">
                                                                    <li class="active"><a href="#info" data-toggle="tab"> Information</a>
                                                                    </li>
                                                                    <li><a href="#price" data-toggle="tab">Price</a></li>
                                                                    <li><a href="#seo" data-toggle="tab"> SEO</a></li>
                                                                    <li><a href="#associations" data-toggle="tab"> Associations</a></li>
                                                                    <li><a href="#image" data-toggle="tab"> Images</a></li>
                                                                    <li><a href="#combination" data-toggle="tab">Combinations / Quantity<br>
                                                        management</a></li>
                                                                    <li class="hide"><a href="#quantity" data-toggle="tab"> Quantity</a>
                                                                    </li>
                                                                    <li><a href="#feature" data-toggle="tab">Features</a></li>
                                                                    <li><a href="#brand" data-toggle="tab"> Brand</a></li>
                                                                    <li class="hide"><a href="#homepagemanagement" data-toggle="tab"> Home
                                                        Page Management</a></li>
                                                                </ul>
                                                                <div id="myTabContent3" class="tab-content" style="height:450px;">
                                                                    <div class="tab-pane fade in active" id="info">
                                                                        <div class="row">
                                                                            <div class="col-md-10 ">
                                                                                <div class="form-group" style="padding-top:6px;">
                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Product Type
                                                                                    </label>
                                                                                    <div class="col-sm-9 col-lg-9 controls" style="    padding-top: 10px;">
                                                                                        <input type="radio" value="PRD" checked="checked" <?php if($product[ 'prodtype']=='PRD' ) echo $checked; ?> name="producttype" id="producttype"> &nbsp;&nbsp;Product &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <input type="radio" value="VPD" name="producttype" <?php if($product[ 'prodtype']=='VPD' ) echo $checked; ?> id="producttype"> &nbsp;&nbsp;Virtual Product
                                                                                        <div style="float:right">
                                                                                            <input type="checkbox" value="Y" name="packInd" <?php if($product[ 'packInd']=='Y' ) echo $checked; ?>> &nbsp;&nbsp;Is Pack &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--    <div class="form-group" style="padding-top:6px;">

                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label"> </label>

                              <div class="col-sm-9 col-lg-9 controls" style="">

                                </div>

                             </div>-->

                                                                                <div class="form-group">
                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label ">Product name </label>
                                                                                    <span>(searching of existing product if avalable)</span>
                                                                                    <div class="col-sm-9 col-lg-5 controls">

                                                                                        <input type="text" name="product_name" id="textfield1" required class="form-control" placeholder="Name of product" value="<? echo $product['name']; ?>">

                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group ">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Reference code (SKU Code / Style No.)</label>

                                                                                    <span>(Reference code for product )</span>
                                                                                    <div class="col-sm-9 col-lg-5 controls">

                                                                                        <? 		  	$sku = mysql_fetch_array(mysql_query("select * from volumetric_cal where types='SKU' "));
                ?>

                                                                                            <input type="text" name="refcode" class="form-control" placeholder="Reference code for product" value="<? if($product['ref_code']!=''){ echo $product['ref_code'];} else{ echo $sku['divid_val'];} ?>">

                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group  hide">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Bar code
                                                                                    </label>

                                                                                    <span>(Bar code of product )</span>
                                                                                    <div class="col-sm-9 col-lg-5 controls">

                                                                                        <input type="text" name="barcode" id="textfield1" class="form-control" placeholder="Reference code for product" value="<? echo $product['barcode']; ?>">

                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Short Description
                                                                                    </label>

                                                                                    <div class="col-sm-9 col-lg-9 controls">

                                                                                        <textarea name="short_description" id="textarea2" rows="5" class="form-control ckeditor">
                                                                                            <? if(isset($product['short_description']) && $product['short_description']!=''){
                                                                            echo $product['short_description'];
                                                                            }else{
                                                                                echo "<p style='font-size:12px;'>
                                                                                 CAR BRAND -  <br>
                                                                                 CAR MODLE -  <br>
                                                                                 FUEL TYPE - <br>
                                                                                 PRODUCT BY -<br>
                                                                                 QUANTITY -  <br>
                                                                                 WARRANTY -                                                                    
                                                                               </p> ";
                                                                            }?>

                                                                                        </textarea>
                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Full Description
                                                                                    </label>

                                                                                    <div class="col-sm-9 col-lg-9 controls">

                                                                                        <textarea name="description" id="textarea2" rows="5" class="form-control ckeditor">
                                                                                            <? echo $product['description']; ?>
                                                                                        </textarea>

                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group" style="padding-top:6px;">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Technical Specification
                                                                                    </label>

                                                                                    <div class="col-sm-9 col-lg-5 controls" style="    padding-top: 6px;">

                                                                                        <input type="radio" value="Yes" name="technical_specification" class="techSpec" <?php if($product[ 'technspecification']=='Yes' ) echo $checked; ?>> &nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                                                                        <input type="radio" value="No" name="technical_specification" class="techSpec" <?php if(($product[ 'technspecification']!='No' ) && ($product[ 'technspecification']!='Yes' )) { echo $checked; } else if($product[ 'technspecification']=='No' ) { echo $checked; } ?>> &nbsp;&nbsp;No </div>

                                                                                </div>

                                                                                <div class="form-group" id="techDetail" style=" <?php if($product['technspecification']=='Yes') {  echo " display:block "; } else{  echo "display:none ";  } ?>">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label"> Technical Description
                                                                                    </label>

                                                                                    <div class="col-sm-9 col-lg-9 controls">

                                                                                        <textarea name="techndescription" id="textarea3" rows="5" class="form-control ckeditor">
                                                                                            <? echo $product['techndescription']; ?>
                                                                                        </textarea>

                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group" style="padding-top:6px;">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Key Features
                                                                                    </label>

                                                                                    <div class="col-sm-9 col-lg-5 controls" style="    padding-top: 6px;">

                                                                                        <input type="radio" value="Y" name="key_feature" class="keyfeature" <?php if($product[ 'keyfeature']=='Y' ) echo $checked; ?>> &nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <input type="radio" value="N" name="key_feature" class="keyfeature" <?php if(($product[ 'keyfeature']!='N' ) && ($product[ 'keyfeature']!='Y' )) { echo $checked; } else if($product[ 'keyfeature']=='N' ) { echo $checked; }?>> &nbsp;&nbsp;No </div>

                                                                                </div>

                                                                                <div class="form-group" id="keyDetail" style=" <?php if($product['keyfeature']=='Y') {  echo " display:block "; }   else  {  echo "display:none ";  } ?>">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Key Features Description
                                                                                    </label>

                                                                                    <div class="col-sm-9 col-lg-9 controls">

                                                                                        <textarea name="keyfeaturedes" id="textarea3" rows="5" class="form-control ckeditor">
                                                                                            <? echo $product['keyfeaturedes']; ?>
                                                                                        </textarea>

                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group" style="padding-top:6px;">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">EMI Option
                                                                                    </label>

                                                                                    <div class="col-sm-9 col-lg-5 controls" style="    padding-top: 6px;">

                                                                                        <input type="radio" value="Y" name="emi" class=" " <?php if($product[ 'emi']=='Y' ) echo $checked; ?>> &nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                                                                        <input type="radio" value="N" name="emi" class=" " <?php if(($product[ 'emi']!='N' ) && ($product[ 'emi']!='Y' )) { echo $checked; } else if($product[ 'emi']=='N' ) { echo $checked; }?>> &nbsp;&nbsp;No </div>

                                                                                </div>

                                                                                <div class="form-group" style="padding-top:6px;">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Display Mode
                                                                                    </label>

                                                                                    <div class="col-sm-9 col-lg-5 controls" style="padding-top: 6px;">

                                                                                        <input type="radio" value="Y" checked="checked" name="display_mode" id="textfield1" <?php if($product[ 'displaymode']=='Y' ) echo $checked; ?>> &nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                                                                        <input type="radio" value="N" name="display_mode" id="textfield1" <?php if(($product[ 'displaymode']!='N' ) && ($product[ 'displaymode']!='Y' )) { echo $checked; } else if($product[ 'displaymode']=='N' ) { echo $checked; }?>> &nbsp;&nbsp;No </div>

                                                                                </div>

                                                                                <div class="form-group" style="padding-top:6px; <? if($type!='admin'){ ?> display:none <? } ?> ">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Offer Mode
                                                                                    </label>

                                                                                    <div class="col-sm-9 col-lg-5 controls" style="padding-top: 6px;">

                                                                                        <input type="radio" value="Y" name="offer_mode" id="textfield1" <?php if($product[ 'offermode']=='Y' ) echo $checked; ?>> &nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                                                                        <input type="radio" value="N" name="offer_mode" id="textfield1" <?php if(($product[ 'offermode']=='N' )) { echo $checked; } ?>> &nbsp;&nbsp;No </div>

                                                                                </div>

                                                                                <div class="form-group" style="padding-top:6px;">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Where to Display
                                                                                    </label>

                                                                                    <div class="col-sm-9 col-lg-5 controls" style="     padding-top: 10px;">

                                                                                        <select name="visibility" id="visibility" class="form-control">

                                                                                            <option value="both" <? if($product[ 'visibility']=='both' ){ echo $selected; } ?>>Everywhere</option>

                                                                                            <option value="catalog" <? if($product[ 'visibility']=='catalog' ){ echo $selected; } ?>>Catalog only</option>

                                                                                            <option value="search" <? if($product[ 'visibility']=='search' ){ echo $selected; } ?>>Search only</option>

                                                                                            <option value="none" <? if($product[ 'visibility']=='none' ){ echo $selected; } ?>>Nowhere</option>

                                                                                        </select>
                                                                                    </div>

                                                                                    <div class="form-group" style="padding-top:6px;">

                                                                                        <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Delivery Days
                                                                                        </label>

                                                                                        <div class="col-sm-9 col-lg-5 controls" style="     padding-top: 10px;">

                                                                                            <select name="delivery_day" id="delivery_day" class="form-control">

                                                                                                <? if($product_id!=''){

								  for($i=7;$i<=45;$i++){ ?>

                                                                                                    <option value="<?=$i;?>" <? if($product[ 'delivery_day']==$i){ echo $selected; } ?>>
                                                                                                        <?=$i;?> Days</option>

                                                                                                    <?  }} else  if($type=='seller'){ 

				 $db->select('seller_registration','default_deliver_day ',NULL,"id='".$master_user_id."'",''); 

				 $days = $db->getResult(); $days = $days[0]; 

  for($i=7;$i<=45;$i++){ ?>

                                                                                                        <option value="<?=$i;?>" <? if($product[ 'default_deliver_day']==$i){ echo $selected; } ?>>
                                                                                                            <?=$i;?> Days
                                                                                                        </option>

                                                                                                        <?  }} else {   for($i=7;$i<=45;$i++){ ?>

                                                                                                            <option value="<?=$i;?>">
                                                                                                                <?=$i;?> Days
                                                                                                            </option>

                                                                                                            <?  } } ?>

                                                                                            </select>

                                                                                        </div>

                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group hide" style="padding-top:6px;">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Condition of product
                                                                                    </label>

                                                                                    <div class="col-sm-9 col-lg-5 controls" style="padding-top: 10px;">

                                                                                        <select name="productcondition" id="productcondition" style="

    width: 37%;

">

                                                                                            <option value="new" <? if($product[ 'productcondition']=='new' ){ echo $selected; } ?>>New</option>

                                                                                            <option value="old" <? if($product[ 'productcondition']=='old' ){ echo $selected; } ?>>Old</option>

                                                                                            <option value="used" <? if($product[ 'productcondition']=='used' ){ echo $selected; } ?>>Used</option>

                                                                                        </select>
                                                                                    </div>

                                                                                </div>

                                                                                <? $rrr = "SELECT tag_name FROM search_tag WHERE product_id='".$product_id."'";

							$sql= mysql_query($rrr);

 							while($fet12 = mysql_fetch_row($sql)){

								$arr.=$fet12[0].',';

								}

							?>

                                                                                    <div class="form-group">

                                                                                        <label for="textfield1" class="col-xs-3 col-lg-3 control-label ">Add Tags for searching </label>

                                                                                        <span style="color:#FF0000">(tags for searching & use
                                                                    tab key for multiple tags )</span>
                                                                                        <div class="col-sm-8 col-lg-6 controls">

                                                                                            <input id="tag-input-1" name="search_tags" value="<? echo rtrim($arr, " , "); ?>" type="text" class="form-control tags" style="display: none;">

                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group hide">

                                                                                        <label for="textfield1" class="col-xs-3 col-lg-3 control-label ">Available Quantity </label>

                                                                                        <span>(tags for searching )</span>
                                                                                        <div class="col-sm-9 col-lg-5 controls">

                                                                                            <input name="default_qty" value="<? echo $product['default_qty']; ?>" type="text" class="form-control  ">

                                                                                        </div>
                                                                                    </div>

                                                                                    <!--  <div class="form-group">

                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label ">seller </label>

                             <span>(tags for searching )</span> <div class="col-sm-9 col-lg-5 controls">

                                <input type="text" name="seller" id="textfield1" class="form-control tagify-container" placeholder="seller detail by default self" >

                              </div>

                            </div>

                            -->

                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                    <div class="tab-pane fade" id="price">

                                                                        <div class="col-md-10 ">

                                                                            <!--<div class="form-group">

                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Pre-tax wholesale price</label>

                            <div class="col-sm-9 col-lg-5 controls">

                              <input type="text" name="wholesale_price" id="textfield1" class="form-control">

                            </div>

                          </div>-->

                                                                            <div class="form-group">

                                                                                <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Net price
                                                                                </label>

                                                                                <div class="input-group col-lg-5">

                                                                                    <span class="input-group-addon">Rs</span>

                                                                                    <input class="form-control changes" type="text" placeholder="amount" id="pretax_price" name="retail_price" value="<? echo $product['retail_price']; ?>">

                                                                                </div>

                                                                            </div>

                                                                            <div class="form-group">

                                                                                <label for="textfield1" class="col-xs-3 col-lg-3 control-label" style="padding-top: 2px;">specific price( discount on price )</label>

                                                                                <div class="input-group col-lg-5">

                                                                                    <div class="input-group">

                                                                                        <input type="text" placeholder="Percent" class="form-control changes" id="discount" name="discount" value="<? echo $product['discount']; ?>">

                                                                                        <span class="input-group-addon">%</span>

                                                                                    </div>

                                                                                </div>

                                                                            </div>

                                                                            <div class="form-group">

                                                                                <label class="col-sm-3 col-lg-3 control-label">Tax rule
                                                                                </label>

                                                                                <div class="col-sm-9 col-lg-5 controls" style="padding-left: 0px; ">

                                                                                    <select class="form-control changes" data-placeholder="Choose a Category" tabindex="1" name="tax_class" id="tax_id" style="width: 105%;">

                                                                                        <option value="">Select</option>

                                                                                        <?php $db->select('localization_tax','*',NULL,'','tax_id DESC');

	 							 $res = $db->getResult();

								 foreach($res as $feature){

								  ?>

                                                                                            <option value="<?php echo  $feature['tax_value']; ?>" <? if($feature[ 'tax_value']==$product[ 'tax_class']){ echo $selected; } ?>
                                                                                                >
                                                                                                <?php echo $feature['tax_name']; ?>
                                                                                            </option>

                                                                                            <?php } ?>

                                                                                    </select>

                                                                                </div>

                                                                            </div>

                                                                            <div class="form-group">

                                                                                <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Retail price with tax</label>

                                                                                <div class="input-group col-lg-5">

                                                                                    <span class="input-group-addon">Rs</span>

                                                                                    <input class="form-control changes" type="text" placeholder="amount" id="priceTax" name="retail_with_tax" value="<? echo $product['retail_price_with_tax']; ?>">

                                                                                </div>

                                                                            </div>

                                                                            <div class="form-group">

                                                                                <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Unit</label>

                                                                                <div class="input-group col-lg-5">

                                                                                    <span class="input-group-addon">Per</span>

                                                                                    <select name="unit_id" id="unit" style=" width: 37% ;height: 108%;" class="changes">

                                                                                        <option value="kg" <? if($product[ 'unit_id']=='kg' ){ echo $selected; } ?>>kg</option>

                                                                                        <option value="item" <? if($product[ 'unit_id']=='peice' ){ echo $selected; } ?>>peice</option>

                                                                                    </select>

                                                                                </div>

                                                                            </div>

                                                                            <div class="form-group">

                                                                                <label for="textfield1" class="col-xs-3 col-lg-3 control-label" style="padding-top: 2px;">Show Icon On Product</label>

                                                                                <div class="input-group col-lg-5">

                                                                                    <select name="icon" id="unit" class="form-control">

                                                                                        <option value="On sale" <? if($product[ 'icon']=='On sale' ){ echo $selected; } ?>>On sale</option>

                                                                                        <option value="Offer" <? if($product[ 'icon']=='Offer' ){ echo $selected; } ?>>Offer</option>

                                                                                        <option value="Discount" <? if($product[ 'icon']=='Discount' ){ echo $selected; } ?>>Discount</option>

                                                                                    </select>

                                                                                </div>

                                                                            </div>

                                                                            <div class="form-group">

                                                                                <label for="textfield1" class="col-xs-3 col-lg-3 control-label" style="padding-top: 2px;">Product Weight For India
                                                                                </label>

                                                                                <div class="input-group col-lg-5">

                                                                                    <input class="form-control changes" type="text" placeholder="weight in kg" id="weight" name="weight" value="<? echo $product['weight']; ?>">

                                                                                </div>
                                                                                <div class="form-group">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label" style="padding-top: 2px;">Product Weight For Outside India
                                                                                    </label>

                                                                                    <div class="input-group col-lg-5">

                                                                                        <input class="form-control changes" type="text" placeholder="weight in kg" id="weight_outside" name="weight_outside" value="<? echo $product['weight_outside']; ?>">

                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label" style="padding-top: 2px;">Calculate Parameter
                                                                                    </label>

                                                                                    <div class="input-group col-lg-5">

                                                                                        <select name="cal_param" id="cal_param" class="form-control cal_weight">

                                                                                            <option value="cm" <? if($product[ 'cal_param']=='cm' ){ echo $selected; } ?>>Cm</option>

                                                                                            <option value="inch" <? if($product[ 'cal_param']=='inch' ){ echo $selected; } ?>>inch</option>
                                                                                        </select>

                                                                                        <?  	$db->select('volumetric_cal','divid_val',NULL,"types='cm' and status = 'Y'",'');

 $volm = $db->getResult(); //print_r($cover_img);

 $volm = $volm[0]; ?>

                                                                                            <input type="hidden" value="<? echo $volm['divid_val']; ?>" id="cal_type">

                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label" style="padding-top: 2px;">Produt Length</label>

                                                                                    <div class="input-group col-lg-5">

                                                                                        <input class="form-control changes  cal_weight" type="text" placeholder="" id="plength" name="plength" value="<? echo $product['plength']; ?> ">

                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label" style="padding-top: 2px;">Produt Height</label>

                                                                                    <div class="input-group col-lg-5">

                                                                                        <input class="form-control changes  cal_weight" type="text" placeholder="" id="pheight" name="pheight" value="<? echo $product['pheight']; ?>">

                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label" style="padding-top: 2px;">Produt Breadth</label>

                                                                                    <div class="input-group col-lg-5">

                                                                                        <input class="form-control changes  cal_weight" type="text" placeholder="" id="pbreadth" name="pbreadth" value="<? echo $product['pbreadth']; ?>">

                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">

                                                                                    <label for="textfield1" class="col-xs-3 col-lg-3 control-label" style="padding-top: 2px;">Volumetric Weight</label>

                                                                                    <div class="input-group col-lg-5">

                                                                                        <input class="form-control changes" type="text" placeholder="" id="vol_weight" name="vol_weight" value="<? echo $product['vol_weight']; ?>">

                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                    <div class="tab-pane fade" id="seo">

                                                                        <div class="col-md-10 ">

                                                                            <div class="form-group">

                                                                                <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Meta title
                                                                                </label>

                                                                                <div class="col-sm-9 col-lg-9 controls">

                                                                                    <div class="input-group">

                                                                                        <span class="input-group-addon" id=myCounter>70</span>

                                                                                        <textarea class="form-control" id="textfield1" onKeyPress="return taLimit(this)" onKeyUp="return taCount(this,'myCounter')" name="meta_title" rows=2 wrap="physical" cols=40>

                                                                                            <? echo $product['meta_title']; ?>
                                                                                        </textarea>
                                                                                    </div>

                                                                                </div>

                                                                            </div>

                                                                            <div class="form-group">

                                                                                <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Meta Keywards
                                                                                </label>

                                                                                <div class="col-sm-9 col-lg-9 controls">

                                                                                    <div class="input-group">

                                                                                        <span class="input-group-addon" id=myCounter></span>

                                                                                        <textarea class="form-control" id="textfield1" onKeyPress="return taLimit(this)" onKeyUp="return taCount(this,'myCounter')" name="meta_keywords" rows=2 wrap="physical" cols=40>

                                                                                            <? echo $product['meta_keywords']; ?>
                                                                                        </textarea>
                                                                                    </div>

                                                                                </div>

                                                                            </div>

                                                                            <div class="form-group">

                                                                                <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Meta description
                                                                                </label>

                                                                                <div class="col-sm-9 col-lg-9 controls">
                                                                                    <div class="input-group">

                                                                                        <span class="input-group-addon" id=myCounter1 style=" padding: 6px 8px;">160</span>
                                                                                        <textarea class="form-control" id="textfield1" onKeyPress="return taLimit1(this)" onKeyUp="return taCount1(this,'myCounter1')" name="meta_description" rows=2 wrap="physical" cols=40>

                                                                                            <? echo $product['meta_description']; ?>
                                                                                        </textarea>
                                                                                    </div>

                                                                                </div>

                                                                            </div>

                                                                            <div class="form-group">

                                                                                <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Friendly URL
                                                                                </label>

                                                                                <div class="col-sm-9 col-lg-5 controls">

                                                                                    <div class="input-group">

                                                                                        <input type="text" placeholder="..." class="form-control" name="friendly_url" value="<? echo $product['friendly_url']; ?>">

                                                                                        <span class="input-group-btn">

                                                                        <!--<button class="btn btn-primary" type="button">Gentrate!</button>--></span>

                                                                                    </div>

                                                                                </div>

                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                    <div class="tab-pane fade" id="associations">

                                                                        <div class="col-sm-9 col-lg-2 controls"></div>

                                                                        <div class="col-sm-9 col-lg-9 controls">

                                                                            <div id="sidetree">

                                                                                <div class="treeheader">&nbsp;</div>

                                                                                <div id="sidetreecontrol"><a href="?#" class="btn btn-primary">Collapse All</a>&nbsp;<a href="?#" class="btn btn-primary">Expand All</a>
                                                                                </div>

                                                                                <? if($type!='seller'){       ?>
                                                                                    <ul id="tree">

                                                                                        <li>
                                                                                            <a href="?/index.cfm">

                                                                                                <input type="checkbox" value="" name="category_id[]"> &nbsp;

                                                                                                <strong>Home</strong></a>

                                                                                            <?php  $cat_checked = array();

							 $db->select('product_category_mapping','*',NULL,"productid='".$product_id."'",'productid DESC');

							 $proCat = $db->getResult();

 							foreach($proCat as $proCats ){ $cat_checked [] = $proCats['categoryid']; }  

							 $qry="SELECT * FROM catalog_category";

 									$result=mysql_query($qry);

 									$arrayCategories = array();

 while($row = mysql_fetch_assoc($result)){ 

 $arrayCategories[$row['category_id']] = array("parent_id" => $row['parent_id'], "name" =>$row['name'],"category_id" =>$row['category_id']);   

  									}  //print_r($arrayCategories); print_r($cat_checked);

									?>

                                                                                                <?php if(mysql_num_rows($result)!=0){ createTree_checked($arrayCategories,0,'','',$cat_checked); }	?>

                                                                                        </li>

                                                                                    </ul>

                                                                                    <? }  else {  ?>
                                                                                        <ul id="tree">

                                                                                            <li>
                                                                                                <a href="?/index.cfm">

                                                                                                    <input type="checkbox" value="" name="category_id[]"> &nbsp;

                                                                                                    <strong>Home</strong></a>

                                                                                                <?php  $cat_checked = array();

							 $db->select('product_category_mapping','*',NULL,"productid='".$product_id."'",'productid DESC');

							 $proCat = $db->getResult();

		foreach($proCat as $proCats ){ $cat_checked [] = $proCats['categoryid']; } 

					  	 	  $qry="SELECT * FROM catalog_category where status = 0";

 									$result=mysql_query($qry);

 									$arrayCategories = array(); 

 while($row = mysql_fetch_assoc($result)){ //$num12 = 1;

  $qwe =  mysql_query("SELECT category_id,GetAncestry(category_id) as parents,name from catalog_category where category_id =  ".$row['category_id']);  $rwe = mysql_fetch_array($qwe);  

$ary = array(); $ary = explode(',',$rwe['parents']); $cnt = count($ary);

 if($row['parent_id']==''){  $qry =	   mysql_query("select * from seller_select_cat where cat_id='".$row['category_id']."' and seller_id =".$master_user_id);

  $num12 = 2;//mysql_num_rows($qry);

  if(($num12!=0)){

 $arrayCategories[$row['category_id']] = array("parent_id" => $row['parent_id'], "name" =>$row['name'],"category_id" =>$row['category_id']);   

}

 }

else if($cnt==1){  

$q_num = mysql_query("select * from seller_select_cat where cat_id ='".$row['category_id']."' and seller_id =".$master_user_id);

   $num_cat =2;// mysql_num_rows($q_num);

if($num_cat!=0){   $arrayCategories[$row['category_id']] = array("parent_id" => $row['parent_id'], "name" =>$row['name'],"category_id" =>$row['category_id']);   

  }  

 }  

else{   $arrayCategories[$row['category_id']] = array("parent_id" => $row['parent_id'], "name" =>$row['name'],"category_id" =>$row['category_id']);  } 	} //  print_r($arrayCategories); print_r($cat_checked);

  if(mysql_num_rows($result)!=0){ createTree_checked($arrayCategories,0,'','',$cat_checked); }	?>

                                                                                            </li>

                                                                                        </ul>

                                                                                        <? } ?>

                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                    <div class="tab-pane fade" id="combination">
                                                                        <? $qry = mysql_query("select * from  product_comb where productid = '$product_id' ");
					                                     $num_com = mysql_num_rows($qry); ?>
                                                                            <div id="add_combo" <? if($num_com!=0){ ?> style="display:none"
                                                                                <? } ?> >
                                                                                    <?php if($product_id!=''){ ?>
                                                                                        <div class="form-group">
                                                                                            <label for="textfield1" class="col-xs-3 col-lg-2 control-label"></label>
                                                                                            <div class="col-sm-9 col-lg-5 controls">
                                                                                                <input type="checkbox" value="1" name="lov" class="">&nbsp;(List of value)
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Select attribute
                                                                                            </label>
                                                                                            <div class="col-sm-9 col-lg-5 controls">
                                                                                                <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="attribute_id" id="attribute">
                                                                                                    <option value="">Select</option>
                                                                                                    <?php $db->select('catalog_product_attribute','*',NULL,'','attribute_id DESC');
	 						                                    	 $res = $db->getResult();
								                                     foreach($res as $feature){ ?>
                                                                                                        <option value="<?php echo $feature['attribute_id']; ?>">
                                                                                                            <?php echo $feature['aname']; ?>
                                                                                                        </option>
                                                                                                        <?php } ?>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Attribute value
                                                                                            </label>
                                                                                            <div class="col-sm-9 col-lg-5 controls">
                                                                                                <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="attribute_id" id="attribute_value">
                                                                                                    <option value="">Select</option>
                                                                                                </select>
                                                                                            </div>
                                                                                            <button type="button" style="display:none !important;" class="btn btn-primary" id="addattribute" style="float:left; margin-left:20px"><i class="fa fa-check"></i> Add Attribute</button>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                                                                                <img src="../images/product_load.gif" id="img_load" style="display:none; ">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="product_attribute_data" class="form-group" style="display:none">
                                                                                            <div class="col-md-12">
                                                                                                <div class="panel panel-primary">
                                                                                                    <div class="panel-heading">
                                                                                                        <h4 class="panel-title">Attribute list</h4>
                                                                                                    </div>
                                                                                                    <div class="panel-body">
                                                                                                        <div class="col-md-12">
                                                                                                            <div class="box">
                                                                                                                <div class="box-content">
                                                                                                                    <table class="table" id="att_length">
                                                                                                                        <thead>
                                                                                                                            <tr>
                                                                                                                                <th>Attribute name</th>
                                                                                                                                <th>Attribute value</th>
                                                                                                                                <th>Action</th>
                                                                                                                            </tr>
                                                                                                                        </thead>
                                                                                                                        <tbody id="com_attr">
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group hide">
                                                                                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label ">Reference code (SKU Code / Style No.)</label>
                                                                                            <div class="input-group col-lg-5">
                                                                                                <input class="form-control changes" type="text" placeholder=" " id="ref" name="ref" value=" ">
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group">
                                                                                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Barcode</label>
                                                                                            <div class="input-group col-lg-5">
                                                                                                <input class="form-control changes" type="text" placeholder=" " name="bar" value=" ">
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group hide">
                                                                                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Impact on price </label>
                                                                                            <div class="input-group col-lg-3">
                                                                                                <select name="impacpricetind" id="" class="form-control changes">
                                                                                                    <option value="None">None</option>
                                                                                                    <option value="Increase">Increase</option>
                                                                                                    <option value="Decrease">Decrease</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group hide">
                                                                                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Impact on price value
                                                                                            </label>
                                                                                            <div class="input-group col-lg-5">
                                                                                                <span class="input-group-addon">Rs</span>
                                                                                                <input class="form-control changes" type="text" placeholder=" " id="priceTax" name="impactpricevalue" value=" ">
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group hide">

                                                                                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Impact on weight
                                                                                            </label>

                                                                                            <div class="input-group col-lg-5">

                                                                                                <select name="impactweightind" id="" style="     width: 37%; height: 108% " class="form-control changes">

                                                                                                    <option value="None">None</option>

                                                                                                    <option value="Increase">Increase</option>

                                                                                                    <option value="Reduce">Reduce</option>

                                                                                                </select>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group hide">

                                                                                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Impact on weight value
                                                                                            </label>

                                                                                            <div class="input-group col-lg-5">

                                                                                                <input class="form-control changes" type="text" placeholder=" " name="impactweightvalue" value=" ">

                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group hide">

                                                                                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Available start date
                                                                                            </label>

                                                                                            <div class="input-group col-lg-5">

                                                                                                <input class="form-control changes date-picker" type="text" placeholder=" " name="avastartdate" value=" ">

                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group hide">

                                                                                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label"> Available end date

                                                                                            </label>

                                                                                            <div class="input-group col-lg-5">

                                                                                                <input class="form-control changes  date-picker" type="text" placeholder=" " name="avaenddate" value=" ">

                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group ">

                                                                                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Quantity

                                                                                            </label>

                                                                                            <div class="input-group col-lg-5">

                                                                                                <input class="form-control changes " type="text" placeholder=" " name="com_qty" value=" ">

                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group ">

                                                                                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Price </label>

                                                                                            <div class="input-group col-lg-5">

                                                                                                <input class="form-control changes " type="text" placeholder=" " name="comb_price" value=" ">

                                                                                            </div>

                                                                                        </div>
                                                                                        <div class="form-group">

                                                                                            <div class="input-group col-lg-8">

                                                                                                <input type="hidden" name="product_com" value="<? echo $product_id;?>">

                                                                                            </div>
                                                                                        </div>

                                                                                        <hr>

                                                                                        <div class="form-group">

                                                                                            <label class="control-label col-lg-3">Image</label>

                                                                                            <div class="col-lg-9">

                                                                                                <ul id="id_image_attr" class="list-inline">
                                                                                                    <?  $db->select('product_image','*',NULL,"productid='".$product_id."'",'id DESC');

 $res1 = $db->getResult();

$i=1; foreach($res1 as $values ){ ?>
                                                                                                        <li>

                                                                                                            <input type="checkbox" name="id_image_attr[]" value="<? echo $values['id']; ?>" id=" ">

                                                                                                            <label for="id_image_attr_<? echo   $values['id']; ?>">

                                                                                                                <img class="img-thumbnail" src="../upload/product/<? echo $product_id; ?>/<? echo $values['filename']; ?>" width="100" height="100">

                                                                                                            </label>

                                                                                                        </li>

                                                                                                        <? } ?>

                                                                                                </ul>

                                                                                            </div>

                                                                                        </div>

                                                                                        <?php }else{?>
                                                                                            <div class="alert alert-warning">

                                                                                                <button class="close" data-dismiss="alert">×</button>

                                                                                                <strong></strong> First save the product.</div>
                                                                                            <?php } ?>

                                                                            </div>

                                                                            <div id="edit_combo" class="form-group" style="display:none"></div>
                                                                            <div id="combo_list" <? if($num_com==0){ ?> style="display:none"
                                                                                <? } ?>>

                                                                                    <div class="col-md-12">

                                                                                        <div class="panel panel-primary">

                                                                                            <div class="panel-heading">

                                                                                                <h4 class="panel-title">Combination list</h4>

                                                                                            </div>

                                                                                            <div class="panel-body">

                                                                                                <div class="col-md-12">

                                                                                                    <div class="box">

                                                                                                        <div class="box-content">

                                                                                                            <table class="table">

                                                                                                                <thead>

                                                                                                                    <tr>

                                                                                                                        <th>Attribute-value pair
                                                                                                                        </th>

                                                                                                                        <!--  <th>Impact on price</th>

                  <th>Impact on weight</th>

                  <th>Reference Code  (SKU Code / Style No.) </th>-->

                                                                                                                        <th>Bar Code</th>
                                                                                                                        <th>Quantity</th>

                                                                                                                        <th>Price</th>

                                                                                                                        <th>Action</th>

                                                                                                                    </tr>

                                                                                                                </thead>

                                                                                                                <tbody>

                                                                                                                    <?   $db->select('product_comb','*',NULL,"productid='".$product_id."'",'id DESC');

 $res11 = $db->getResult(); 

$i=1; foreach($res11 as $values1){ ?>

                                                                                                                        <tr>
                                                                                                                            <?    $db->select('product_comb_att_mapping','*',NULL,"combinationid='".$values1['id']."'",'id DESC');

 $res112 = $db->getResult(); 

$i=1;?>

                                                                                                                                <td>
                                                                                                                                    <?php foreach($res112 as $values12 ){ echo get_attribute_name($values12['attrid']).'-'.get_attribute_value($values12['attrvalueid']).'&nbsp;'; } ?>
                                                                                                                                </td>
                                                                                                                                <?  ?>

                                                                                                                                    <!--<td><? echo $values1['impactpricevalue']; ?></td>

                  <td><? echo $values1['impactweightvalue']; ?></td>

                  <td><? echo $values1['refrencecode']; ?></td>-->
                                                                                                                                    <td>
                                                                                                                                        <? echo $values1['barcode']; ?>
                                                                                                                                    </td>

                                                                                                                                    <td>
                                                                                                                                        <? echo $values1['qty']; ?>
                                                                                                                                    </td>

                                                                                                                                    <td>
                                                                                                                                        <? echo $values1['price']; ?>
                                                                                                                                    </td>

                                                                                                                                    <input type="hidden" name="comboid_edit" value="<? echo $values1['id']; ?> " class="combo_edit" id="comboid_<? echo $values1['id']; ?>">

                                                                                                                                    <td>
                                                                                                                                        <div class="combo_edit btn btn-primary" id="comboedit_<? echo $values1['id']; ?>">
                                                                                                                                            Edit</div>
                                                                                                                                    </td>

                                                                                                                                    <? } ?>
                                                                                                                        </tr>
                                                                                                                </tbody>

                                                                                                            </table>

                                                                                                        </div>

                                                                                                    </div>

                                                                                                </div>

                                                                                            </div>

                                                                                        </div>

                                                                                    </div>

                                                                                    <div id="desc-product-newCombination" class="btn btn-primary pull-right">New combination </div>

                                                                            </div>
                                                                    </div>

                                                                    <div class="tab-pane fade" id="quantity">

                                                                        <? 

						  if($num_com!=0){ ?>

                                                                            <div class="col-lg-6">

                                                                                <table class="table">

                                                                                    <thead>

                                                                                        <tr>

                                                                                            <th><span class="title_box">Quantity</span></th>

                                                                                            <th><span class="title_box">Designation</span></th>

                                                                                        </tr>

                                                                                    </thead>

                                                                                    <tbody>

                                                                                        <?   $db->select('product_comb','*',NULL,"productid='".$product_id."'",'id DESC');

 $res13 = $db->getResult(); 

$i=1; foreach($res13 as $values13){ ?>

                                                                                            <tr>
                                                                                                <td class="available_quantity" id="qty_72">

                                                                                                    <span style="display: none;">
                                                                            <? echo $values13['qty']; ?></span>

                                                                                                    <input type="text" name="qty[]" class="form-control" value="<? echo $values13['qty']; ?>" style="width:50%">

                                                                                                    <input type="hidden" name="combid[]" class="form-control" value="<? echo $values13['id']; ?>">

                                                                                                </td>

                                                                                                <? $db->select('product_comb_att_mapping','*',NULL,"combinationid='".$values13['id']."'",'id DESC');

 $res114 = $db->getResult(); 

$i=1; ?>
                                                                                                    <td>
                                                                                                        <?php  foreach($res114 as $values14 ){echo get_product_name($product_id).'&nbsp;'.get_attribute_name($values14['attrid']).'-'.get_attribute_value($values12['attrvalueid']); } ?>
                                                                                                    </td>

                                                                                            </tr>
                                                                                            <? } ?>

                                                                                    </tbody>
                                                                                </table>

                                                                            </div>

                                                                            <? }else{?>
                                                                                <div class="alert alert-warning">

                                                                                    <button class="close" data-dismiss="alert">×</button>

                                                                                    <strong></strong> First create the product combination.</div>
                                                                                <?php } ?>

                                                                    </div>

                                                                    <div class="tab-pane fade" id="feature">

                                                                        <?			if($product_id!=''){							$num_fea=$db->getRowCount('product_featues','*',NULL,'productid="'.$product_id.'"','id DESC');

?>
                                                                            <div <? if($num_fea!=0){?> style="display:none;"
                                                                                <? } ?> id="add_feature">

                                                                                    <div class="form-group">

                                                                                        <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Select feature
                                                                                        </label>

                                                                                        <div class="col-sm-9 col-lg-5 controls">

                                                                                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="attribute_id" id="feature1">

                                                                                                <option value="">Select</option>

                                                                                                <?php $db->select('catalog_product_features','*',NULL,'','feature_id DESC');

	 							 $res = $db->getResult();

								 foreach($res as $feature){ ?>

                                                                                                    <option value="<?php echo $feature['feature_id']; ?>">
                                                                                                        <?php echo $feature['feature_name']; ?>
                                                                                                    </option>

                                                                                                    <?php } ?>

                                                                                            </select>

                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="form-group">

                                                                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">

                                                                                            <button type="button" class="btn btn-primary" id="add_features"><i class="fa fa-check"></i> Add feature
                                                                                            </button>

                                                                                            <button type="button" class="btn">Cancel</button>

                                                                                        </div>

                                                                                    </div>

                                                                                    <div id="feature_data"></div>

                                                                            </div>

                                                                            <div class="col-md-12" <? if($num_fea==0){?> style="display:none;"
                                                                                <? } ?> id="feature_list">

                                                                                    <div class="panel panel-primary">

                                                                                        <div class="panel-heading">

                                                                                            <h4 class="panel-title">Feature list</h4>

                                                                                        </div>

                                                                                        <div class="panel-body">

                                                                                            <div class="col-md-12">

                                                                                                <div class="box">

                                                                                                    <div class="box-content">

                                                                                                        <table class="table" id=" ">

                                                                                                            <thead>

                                                                                                                <tr>

                                                                                                                    <th>S.No</th>

                                                                                                                    <th>Attribute name</th>

                                                                                                                    <th>Action</th>

                                                                                                                </tr>

                                                                                                            </thead>

                                                                                                            <?   $db->select('product_featues','*',NULL,"productid='".$product_id."'",'id DESC');

 $res33 = $db->getResult(); 

/* print_r($res33);*/

$i=1; foreach($res33 as $values33){ ?>

                                                                                                                <tr>

                                                                                                                    <th>
                                                                                                                        <?php echo $i;?>
                                                                                                                    </th>

                                                                                                                    <th><span><?php echo get_feature_name($values33['featureid']); ?></span>
                                                                                                                    </th>

                                                                                                                    <td>
                                                                                                                        <input type='button' onClick="remove_featue(<?php echo $values33['id'] ?>)" value='Remove ' class="btn btn-danger">
                                                                                                                    </td>

                                                                                                                </tr>

                                                                                                                <?php $i=$i+1; } ?>

                                                                                                        </table>

                                                                                                    </div>

                                                                                                </div>

                                                                                            </div>

                                                                                        </div>
                                                                                        <br />
                                                                                        <div id="new_feature" class="btn btn-primary pull-right">Add New Feature </div>

                                                                                    </div>

                                                                            </div>

                                                                            <? } else { ?>

                                                                                <div class="alert alert-warning">

                                                                                    <button class="close" data-dismiss="alert">×</button>

                                                                                    <strong></strong> First save the product.</div>

                                                                                <? } ?>

                                                                    </div>

                                                                    <div class="tab-pane fade" id="image">

                                                                        <? //if($product_id!=''){ 

										$nummmm=$db->getRowCount('product_image','*',NULL,'productid="'.$product_id.'"','id DESC');

?>

                                                                            <div class="row" <? if($nummmm!=0){?> style="display:none;"
                                                                                <? } ?> id="add_newimage1" >

                                                                                    <input type='button' value='Add Image' id='addButton' class="btn btn-lime" style="

    margin-left: 29px;">

                                                                                    <input type='button' value='Remove Image' id='removeButton' class="btn btn-danger">

                                                                                    <br>

                                                                                    <br>

                                                                                    <div id='TextBoxesGroup'>

                                                                                        <div class="container col-sm-12" style="

   padding-bottom: 10px;

">

                                                                                            <div class="col-sm-3 col-lg-3 controls">

                                                                                                <input type="radio" name="cover_image" id="cover_1">

                                                                                                <img id="uploadPreview1" src="img/No_image.png" width="60" height="60" /> </div>

                                                                                            <div class="col-sm-8col-lg-8 controls">

                                                                                                <div class="fileupload fileupload-new" data-provides="fileupload">

                                                                                                    <div class="input-group" style="

   width: 549px;

   padding-left: 15px;

">

                                                                                                        <div class="form-control uneditable-input">
                                                                                                            <i class="fa fa-file fileupload-exists"></i>
                                                                                                            <span class="fileupload-preview"></span>
                                                                                                        </div>

                                                                                                        <div class="input-group-btn">
                                                                                                            <a class="btn bun-default btn-file">
                                                                                                                <span class="fileupload-new">Select
                                                                                        file</span> <span class="fileupload-exists">Change</span>

                                                                                                                <input type="file" name="files[]" class="file-input" id="uploadImage1" onBlur="change1();" onChange="PreviewImage(1);">

                                                                                                            </a>
                                                                                                        </div>

                                                                                                    </div>

                                                                                                </div>

                                                                                            </div>

                                                                                        </div>

                                                                                    </div>

                                                                            </div>

                                                                            <? 

					?>

                                                                                <div class="col-md-12" <? if($nummmm==0){?> style="display:none;"
                                                                                    <? } ?> id="image_list1">

                                                                                        <div class="panel panel-primary">

                                                                                            <div class="panel-heading">

                                                                                                <h4 class="panel-title">
                                                                    <? echo $nummmm; ?> Images List</h4>

                                                                                            </div>

                                                                                            <div class="panel-body">

                                                                                                <div class="col-md-12">

                                                                                                    <div class="box">

                                                                                                        <div class="box-content">

                                                                                                            <table class="table">

                                                                                                                <thead>

                                                                                                                    <tr>

                                                                                                                        <th>S.No</th>

                                                                                                                        <th>Image</th>

                                                                                                                        <th align="center">Cover Photo
                                                                                                                        </th>

                                                                                                                        <th>Action</th>

                                                                                                                    </tr>

                                                                                                                </thead>

                                                                                                                <tbody>

                                                                                                                    <?  $db->select('product_image','*',NULL,"productid='".$product_id."'",'id DESC');

 $res1 = $db->getResult();

$i=0; foreach($res1 as $values ){ ?>

                                                                                                                        <tr>

                                                                                                                            <td>
                                                                                                                                <?php echo $i=$i+1; ?>
                                                                                                                            </td>

                                                                                                                            <td><img class="img-thumbnail" src="../upload/product/<? echo $product_id; ?>/<? echo $values['filename']; ?>" width="50" height="50">
                                                                                                                            </td>

                                                                                                                            <td>
                                                                                                                                <input type='radio' onClick="cover_pic(<?php echo $values['id'] ?>)" value='' name="coverphoto" <? if($product[ 'coverphoto']==$values[ 'id']){ echo $checked;} ?> >
                                                                                                                            </td>

                                                                                                                            <td>
                                                                                                                                <input type='button' onClick="remove_image(<?php echo $values['id'] ?>)" value='Remove Image' class="btn btn-danger">
                                                                                                                            </td>

                                                                                                                        </tr>

                                                                                                                        <?php } ?>

                                                                                                                </tbody>

                                                                                                            </table>

                                                                                                        </div>

                                                                                                    </div>

                                                                                                </div>

                                                                                            </div>

                                                                                        </div>

                                                                                        <div id="new_image_add1" class="btn btn-primary pull-right">Add New Image </div>

                                                                                </div>

                                                                                <? //}  ?>

                                                                    </div>

                                                                    <!--- brand -->

                                                                    <div class="tab-pane fade" id="brand">
                                                                        <? if($product_id!='' or 1==1){ 
                                                                             $db->select('catalog_product','brand','',"product_id='".$product_id."'",'','');
                                                                             $brand = $db->getResult();

 		                                                                ?>
                                                                            <div class="row" id="newHomePage">
                                                                                
                                                                                <? $db->select('master_brand','*',NULL,"",'id DESC');

                                                                                $resval5 = $db->getResult();
                                                                                 $k=0;
                                                                                foreach($resval5 as $resval15){ 
                                                                                ?> 
                                                                                
                                                                                        <div class="col-xs-2 col-lg-2">
                                                                                            <label for="" class="col-xs-10 col-lg-10 control-label">
                                                                                                <? echo $resval15['value']; ?>
                                                                                            </label>
                                
                                                                                            <label class="checkbox" style='float:right'>
                                                                                                    <input type="radio" value="<?php echo $resval15['id']; ?>" name="brand" id="brand" <?php if($resval15['id']==$brand[0]['brand']) echo 'checked'; ?> > &nbsp;
                                                                                            </label>
                                                                                        </div>
                                                                               
                                                                                    <? $k++;} ?>
                                                                            </div>

                                                                            <? } else { ?>

                                                                                <div class="alert alert-warning">
                                                                                    <button class="close" data-dismiss="alert">×</button>
                                                                                    <strong></strong> First save the product.</div>

                                                                                <? } ?>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!--brand-->

                                                    <!--supplier-->

                                                    <div class="tab-pane fade" id="supplier">

                                                        <? if($product_id!=''){ 

?>

                                                            <div class="row" id="newHomePage">

                                                                <div class="col-md-10 ">
                                                                    <? $db->select('master_home_categories','*',NULL,"",'id DESC');

$resval5 = $db->getResult(); $k=0;foreach($resval5 as $resval15){ 

$db->select('product_homepage_cat','*','',"productid='".$product_id."' and homecatid='".$resval15['id']."' ",'','');

$homecat = $db->getResult(); $homecat =$homecat[0];

?>
                                                                        <div class="form-group">

                                                                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">
                                                                                <? echo $resval15['value']; ?>
                                                                            </label>

                                                                            <input type="hidden" value="<? echo $resval15['id']; ?>" name="home_cat<? echo $k; ?>[] ">

                                                                            <input type="hidden" value="<? echo $resval15['id']; ?>" name="home_cats[]">

                                                                            <div class="col-sm-9 col-lg-5 controls">

                                                                                <label class="checkbox">

                                                                                    <input type="radio" value="Y" name="home_val<? echo $k; ?>[]" <?php if($homecat[ 'value']=='Y' ) echo $checked; ?>> &nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                                                                    <input type="radio" value="N" name="home_val<? echo $k; ?>[]" <?php if(($homecat[ 'value']!='N' ) && ($homecat[ 'value']!='Y' )) { echo $checked; } else if($homecat[ 'value']=='N' ) { echo $checked; }?>> &nbsp;&nbsp;No

                                                                                </label>

                                                                            </div>

                                                                        </div>

                                                                        <? $k++;} ?>

                                                                </div>

                                                            </div>

                                                            <? } else { ?>

                                                                <div class="alert alert-warning">

                                                                    <button class="close" data-dismiss="alert">×</button>

                                                                    <strong></strong> First save the product.</div>

                                                                <? } ?>

                                                    </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                                <!-- supplier-->

                                <div class="form-group">

                                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">

                                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save
                                        </button>

                                        <button type="button" class="btn">Cancel</button>

                                    </div>

                                </div>

                                </form>

                        </div>

                </div>

                </div>

                </div>

                <?php include('footer.php'); ?>

                    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a>
                    </div>

                    <!-- END Content -->

                    </div>

                    <!-- END Container -->
                    <!--basic scripts-->

                    <? include('bottom_link.php'); ?>
                        <? include('js/product_addjs.php'); ?>
    </body>

    </html>