<?php 
include('database.php');

include('functions.php');

include('session.php');

?>

<!DOCTYPE html>
<html>
 <head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><? echo  backend_prefix(); ?></title> 
       <link rel="stylesheet" href="css/css/bootstrap.css" />
<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/font-awesome-4.1.0/css/font-awesome.css">
<!--page specific css styles-->
<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen.min.css" />
<link rel="stylesheet" type="text/css" href="assets/jquery-tags-input/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="assets/jquery-pwstrength/jquery.pwstrength.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-fileupload/bootstrap-fileupload.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.css" />
<link rel="stylesheet" type="text/css" href="assets/dropzone/downloads/css/dropzone.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="assets/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-switch/static/stylesheets/bootstrap-switch.css" />
<link rel="stylesheet" type="text/css" href="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<!--flaty css styles-->
<link rel="stylesheet" href="css/flaty.css">
<link rel="stylesheet" href="css/flaty-responsive.css">
<link rel="shortcut icon" href="img/favicon.html">
<link rel="stylesheet" href="assets/data-tables/bootstrap3/dataTables.bootstrap.css" />

  <link rel="stylesheet" type="text/css" href="validation/livevalid.css" />
   <style type="text/css" title="currentStyle">
   tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
			tfoot { display:table-header-group ; }
</style>		<link rel="stylesheet" href="css/colorbox.css" />
        <link rel="stylesheet" href="css/tooltip.css"><link rel="stylesheet" href="css/multiple-select.css" />  </head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Shipping Charges</h1>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Shipping Charges</li>
      </ul>
    </div>
    <div class="row  ">
      
	  <div class="col-md-12">
<div class="row">

<div class="col-md-12">
<div class="panel panel-primary">
<div class="panel-heading">
<h4 class="panel-title">Add Shipping Charge</h4>
</div>
<div class="panel-body">
 <form  class="form-horizontal" action="master_shipment1.php" method="post" enctype="multipart/form-data" id="operator" >
						<div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">In KG</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" name="weight" id="textfield1" class="form-control" value="1" readonly>
                          </div>
                        </div> <div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Shipping Amount</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" name="samount" id="textfield1" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
<label for="textfield2" class="control-label col-xs-3 col-lg-2" id="b1">Courier Type </label>
<div class="col-sm-9 col-lg-5 controls"  id="b2">
<select  name="courier_type" id=""    class="  form-control" > 
<option value="0" >Surface Shippment </option>
<option value="1"> Air Shippment</option>  </select></div>
	  
</div>
<div class="form-group">
<label for="textfield2" class="control-label col-xs-3 col-lg-2" id="b1">Select Country Type </label>
<div class="col-sm-9 col-lg-5 controls"  id="b2">
<select  name="country"      class="country  form-control" > 
<option value="0" >With In India </option>
<option value="1"> Outside In India</option>  </select></div>
	  
</div>
			
            <div class="form-group country_id" style="display:none">
<label for="textfield2" class="control-label col-xs-3 col-lg-2" id="b1">Select Country </label>
<div class="col-sm-9 col-lg-5 controls"  id="b2">
<select name="country_id" class="form-control "  ><option value="" >--Select Country--</option>
                <?php  $st =  "SELECT * FROM all_countries  WHERE `id` !=101"; 
 				$rr = mysql_query($st);
				while($country = mysql_fetch_array($rr)){
				?>
                <option value="<?php echo $country['id']?>" > <?php echo $country['name']; ?></option>               
                 <?php } ?>
                </select></div>
	  
</div>		 
<div class="form-group">
                <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                 </div>
              </div>
</form> 


</div>
</div>
</div>
<div class="col-md-12">
<div class="box box-magenta">
<div class="box-title">
<h3><i class="fa fa-table"></i> Shipping Charge list</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a>
</div>
</div>
<div class="box-content">
<?php /*$db->select('master_shipment','*',NULL,'','id DESC');
	  $res = $db->getResult();
*/
$db->select('all_countries','*',NULL,'','id DESC');
	  $res = $db->getResult();

	  //$res = $res[0];
	  //print_r($res);
	  
 ?>
 <table class="table table-bordered">
<thead>
<tr>
<th>S.no</th>
<th>Weight</th><th>Amount</th>
<th>Courier Type</th>
<th> Level</th>
</tr>
</thead>
<tbody>
<?php $i=1; foreach($res as $value ){
$c_id =(mysql_query("select  * from master_shipment_country  where country_id = ".$value['id']));
$c_id_num =mysql_num_rows($c_id);
 if($c_id_num!=0){
 while($values = mysql_fetch_array($c_id)){
    $q =   mysql_query("select * from master_shipment    where  id=".$values['ship_id']);
$ship_del = mysql_fetch_array($q);
  ?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $ship_del['weight'] ?></td>

<td><?php echo $values['amt'] ?> Rs.</td>
<td><?php if($ship_del['courier_type']==0){ echo ' Surface Shippment';} else{ echo ' Air Shippment';} ?></td>
<td><?php echo getcountry($values['country_id']); ?> </td>
   </tr>
<?php $i++; } }} ?>
</tbody>
</table> 
</div>
</div>
</div>

</div>

</div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div> 
<? include('bottom_link.php'); ?>

<script src="js/multiple-select.js"></script>
<script>
    $(function() {  
        $('#ms').change(function() { 
            console.log($(this).val());
        }).multipleSelect({
            width: '100%'
        });
		$('.country').change(function(){
var c_val = $(this).val(); 
if(c_val==0){
$('.country_id').css('display','none');
}
else{
$('.country_id').css('display','block');
} 		});
		
    });
</script>
</body>
</html>
