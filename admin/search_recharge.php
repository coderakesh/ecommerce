<?php include('database.php');

include('functions.php');

include('session.php');
?>
<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
<head>
 <? include('links.php'); ?>

</head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>
<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> Search Result </h1>
        <h4>Result on the bases of your Search </h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active"> Search Result</li>
      </ul>
    </div>
    <div class="row  ">
      <div class="col-md-12">
        <div class="box">
          <div class="box-title">
            <h3><i class="fa fa-table"></i> Search Result</h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a> </div>
          </div>
          <div class="box-content">
            <div class="clearfix"></div>
            <div class="table-responsive" style="border:0">
			<?php 
			extract($_POST);
$sql1="select * from recharge_mobile where 1=1 ";
if($dp1!='' && $dp2!=''){
$sql1 = $sql1." and cdate between '$dp1' and '$dp2' " ;
}

if($p_status!=''){
$sql1 = $sql1." and cstatus='$p_status' ";
}
if($p_mode!=''){
$sql1 = $sql1." and  payment_mode='$p_mode' ";
}
$sql1 = $sql1." order by rmobid desc";
$db->sql($sql1);

$res = $db->getResult();
if($res==null)
{
	echo "No Record Found";
}
 ?>
     <?php if($res!=null){?>  
     <table class="table table-advance" id="table1">
<thead>
<tr>
        <th>User Id</th>
        <th>Email</th>
        <th>Mobile No</th>
        <th>Operator Id</th>
        <th>Circle Id</th>
        <th>Amount</th>
        <th>Date</th>
		<th>Time</th>
         <th>Recharge Status</th>
        <th>Payment Status</th>
        <th>Payment Mode</th>
 		<th></th>
		<th>View Details</th>
  </thead>   <?php }?>
<tbody>
<?php $i=1; foreach($res as $values ){ ?>
                  <tr class="table-flag-blue">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $values['email'];?></td>
                    <td><?php echo $values['mobile_number']; ?></td>
                    <td><?php echo get_operator_name($values['operator_id']); ?></td>
                    <td><?php echo get_circle_name($values['circle_id']); ?></td>
                    <td><?php echo $values['amount']; ?></td>
                    <td><?php echo $values['cdate']; ?></td>
                    <td><?php echo $values['ctime']; ?></td>
                     <td><?php echo $values['cstatus']; ?></td>
                    <td><?php echo $values['payment_status']; ?></td>
                    <?php
					if($values['payment_mode']==0)
					{
						$p_status="Wallet";
					}
					else
					{
						$p_status="Gateway";
					}?>
                    <td><?php echo $p_status?></td>
 					<td><a href="add_wallet.php?uid=<? echo $values['user_id']; ?>" >Add Wallet Amount</a></td>
       					<td><a href="recharge_details.php?uid=<? echo $values['user_id']; ?>" class="btn btn-success" > View</a></td>
            </tr>
                  <?php $i++; } ?>
</tbody>
</table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container --> <? include('bottom_link.php'); ?>

</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
