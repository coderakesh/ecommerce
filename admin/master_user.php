<?php 
include('database.php');

include('functions.php');

include('session.php');

?>
<!DOCTYPE html>
<html>
<head>
<? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
<? include('right_bar.php'); ?>
<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <!--<h1><i class="fa fa-file-o"></i>Add User</h1>-->
        
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <!-- END Breadcrumb -->
    <!-- BEGIN Main Content -->
    <div class="alert alert-success hide"  id="success" >
      <button class="close" data-dismiss="alert">&times;</button>
      <strong>Success!</strong> Menu add successfully.</div>
    <div class="row">
      <div class="col-md-12 col-lg-6">
        <div class="box">
          <div class="box-title">
            <h3><i class="fa fa-bars"></i> Add New User</h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a> </div>
          </div>
          <div class="box-content">
            <form action="master_user1.php" class="form-horizontal" method="post" id="menu_form">
            	<div class="form-group">
                	<label class="col-md-4"><strong>Name:</strong></label>
                    <div class="col-md-8">
                    	<input type="text" class="form-control" name="name" id="cname">
                    </div>
                </div>
            	<div class="form-group">
                	<label class="col-md-4"><strong>Mobile:</strong></label>
                    <div class="col-md-8">
                    	<input type="text" class="form-control" name="mobno" id="mobno">
                    </div>
                </div>
            	<div class="form-group">
                	<label class="col-md-4"><strong>Address:</strong></label>
                    <div class="col-md-8">
                    	<input type="text" class="form-control" name="address" id="address">
                    </div>
                </div>
            	<div class="form-group">
                	<label class="col-md-4"><strong>Username:</strong></label>
                    <div class="col-md-8">
                    	<input type="text" class="form-control" name="username" id="username">
                    </div>
                </div>
            	<div class="form-group">
                	<label class="col-md-4"><strong>Password:</strong></label>
                    <div class="col-md-8">
                    	<input type="text" class="form-control" name="password" id="password">
                    </div>
                </div>
            	<div class="form-group">
                	<label class="col-md-4"><strong>User Type</strong></label>
                    <div class="col-md-8">
                    	<select name="type" id="type" class="form-control">
                        	<option value="admin">Admin</option>
                        	<option value="user" selected>User</option>
                        </select>
                    </div>
                </div>
                <div style="min-height:200px; max-height:200px; overflow:auto; border:1px  solid #ccc;">
                	<?php
						$sql = mysql_query("select * from menu where status='1'  order by pos") or die(mysql_error());
						while($row = mysql_fetch_assoc($sql))
						{
						?>
                        	<div class="col-md-12">
                            	<input type="checkbox" class="menu" id="mid_<?php echo $row['mid']; ?>" value="<? echo $row['mid']; ?>" name="menuid[]"> <label style="font-size:12px; color:#336699"><strong><?php echo $row['menu_name']; ?></strong></label>
                                <?php
									$sql2 = mysql_query("select * from submenu where mid='".$row['mid']."' and status='1' order by pos") or die(mysql_error());
									while($row2 = mysql_fetch_assoc($sql2))
									{
									?>
                                    <div class="col-md-12" style="padding-left:30px;">
                                        <input type="checkbox" class="mid_<?php echo $row['mid']; ?> submenu" value="<? echo $row2['sid']; ?>" name="submenuid[]"> <label style="font-size:12px;"><strong><?php echo $row2['s_name']; ?></strong></label>
                                    </div>    
                                    <?php
									}
								?>	
                            </div>
                        <?php
						}
					?>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                    	<input type="submit" id="submit" class="btn btn-primary" value="Add User" style="width:100%;" >
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-lg-6">
        <div class="box">
          <div class="box-title">
            <h3><i class="fa fa-table"></i> User List</h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
          </div>
          <div class="box-content">
            <div class="clearfix"></div>
            <div class="table-responsive" style="border:0" id="reload_tab">
            	<table border="1" class="table">	
                	<tr>
                    	<th>Name</th>
                        <th>Mobile No.</th>
                        <th>Address</th>
                        <th>UserName</th>
                        <th>Password</th>
                        <th>User Type</th>
                        <th>Edit</th>
                    </tr>
                    <?php $qry = mysql_query("Select * from master_user"); 
							while($row1 = mysql_fetch_array($qry))
							{
					 ?>
                    <tr>
                    	<td><? echo $row1['name']; ?></td>
                        <td><? echo $row1['mobno']; ?></td>
                        <td><? echo $row1['address']; ?></td>
                        <td><? echo $row1['username']; ?></td>
                        <td><? echo $row1['password']; ?></td>
                        <td><? echo $row1['type']; ?></td>
                        <td><a href="master_user_edit.php?id=<? echo $row1['master_user_id']; ?>">Edit</a></td>
                    </tr>
                    <? } ?>
                </table>            
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END Main Content -->
    <? include('footer.php'); ?>
  </div>
  <!-- END Content -->
</div>
<!-- END Container -->
<? include('bottom_link.php'); ?>
</body>
</html>
<script>
	$(document).on("click", ".menu", function(){
		var menu_id = $(this).attr("id"); 
		if($(this).prop("checked")==true)
		{
			$("."+menu_id).prop({"checked":true});
		}
		else if($(this).prop("checked")==false)
		{
			$("."+menu_id).prop({"checked":false});
		}
	});	
		$(document).on("click", "#submit", function(){
			var cname = $("#cname").val();
			var mobno = $("#mobno").val();
			var username = $("#username").val();
			var password = $("#password").val();
			
			if(cname=="")
			{
				alert("Please Fill Name !!!!");
				 $("#cname").focus();
				return false;
			}
			if(mobno=="")
			{
				alert("Please Fill Mobile No !!!!");
				$("#mobno").focus();
				return false;
			}
			if(username=="")
			{
				alert('Please Fill User Name !!!!');
				$("#username").focus();
				return false;
			}
			if(password=="")
			{
				alert('Please Fill PassWord !!!!');
				$("#password").focus();
				return false;
			}
			//confirm("Are You Sure Do You Want To Save");
	});
</script>
