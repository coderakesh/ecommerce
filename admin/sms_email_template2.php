<?php include('database.php');

include('functions.php');

include('session.php');
?>
<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
 <head>
  <? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
  <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> SMS and Email Templates </h1>
        <h4>Update SMS</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <?php
	               $sms_type=$_REQUEST[type];
			       $sql1="select sms_email_data,sms_name from sms_email_template where sms_email_type='$sms_type'";
				   $db->sql($sql1);
			       $res = $db->getResult();
				$res =  $res[0] ;
				$resn= $res[1];
					?>
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active"><?php echo $res['sms_name'];?></li>
      </ul>
    </div>
    <div class="row  ">
      <div class="col-md-12">
        <div class="box box-green">
          <div class="box-title">
            <h3><i class="fa fa-bars"></i><?php echo $res['sms_name'];?></h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
          </div>
          <div class="box-content">
            <form  class="form-horizontal" action="update_sms2.php" method="post" enctype="multipart/form-data" id="service" >
              <input type="hidden" value="<?php echo $sms_type;?>" name="stype"/>
              <div class="row">
                <div class="col-md-12 ">
                  <!-- BEGIN Left Side -->
                  <div class="form-group">
                    <label for="updeted_sms" class="col-xs-1 col-lg-1 control-label"><?php echo $res['sms_name'];?></label>
                    <div class="col-sm-9 col-lg-10 controls"><textarea  cols="80" style="width: 100%" id="fieldID" name="updeted_sms" rows="15"><?php echo $res['sms_email_data']; ?></textarea></div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                    <button type="button" class="btn btn-primary" name="change_password" id="update_sms" > <i class="fa fa-check"></i>Update</button>
                    <button type="button" class="btn">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->
<!--basic scripts-->
<script src="../../ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/jquery/jquery-2.1.1.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/jquery-cookie/jquery.cookie.js"></script>
<script type="text/javascript" src="../validation/livevalidation_standalone.compressed.js"></script>
<script>
//for name
var f2 = new LiveValidation('service_name');
f2.add( Validate.Presence, { failureMessage: "Service Name is Required" } );

</script>
<script type="text/javascript">
  $("#update_sms").on('click', function () {
	    
		$("#service").submit();
	         
	      });

</script>
<!--page specific plugin scripts-->
<script type="text/javascript" src="assets/chosen-bootstrap/chosen.jquery.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script type="text/javascript" src="assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="assets/jquery-pwstrength/jquery.pwstrength.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js"></script>
<script type="text/javascript" src="assets/dropzone/downloads/dropzone.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="assets/clockface/js/clockface.js"></script>
<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-switch/static/js/bootstrap-switch.js"></script>
<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/bootstrap3/dataTables.bootstrap.js"></script>
<!--flaty scripts-->
<script src="js/flaty.js"></script>
<script src="js/flaty-demo-codes.js"></script>





<!----------- for Editor  --------------------->
<script src="richtext/js/tinymce/tinymce.dev.js"></script>
<script src="richtext/js/tinymce/plugins/table/plugin.dev.js"></script>
<script src="richtext/js/tinymce/plugins/paste/plugin.dev.js"></script>
<script src="richtext/js/tinymce/plugins/spellchecker/plugin.dev.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "#fieldID",
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code ",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons",
    image_advtab: true,
	 convert_urls: false,
    templates: [
        {title: 'Test template 1', content: 'Test 1'},
        {title: 'Test template 2', content: 'Test 2'}
    ]
});
</script>
<!------------ only for editor --------------------->
</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
