<?php 
include('database.php');

include('functions.php');

include('session.php');

?>
<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
<head>
<? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); 
  $courierid = $_REQUEST['courierid']; 
		  				   
				   $checked ='checked="checked"';
				   
				   $selected = 'selected="selected"';

		  		if($courierid==''){
				
				$action = "catalog_add_courier1.php"; $title ='Add';
 				}else{
				 $db->select('courier_detail','*','',"id='".$courierid."'",'','');
				   
				   $courier = $db->getResult();
				   
				   $courier = $courier[0];
				   $title ='Edit';
				   //print_r($product);
 				$action = "catalog_edit_courier1.php";
				
				} ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> Courier </h1>
        <h4><? echo $title; ?> new courier</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active"><? echo $title; ?> new courier</li>
      </ul>
    </div>
    <div class="row  ">
      <div class="col-md-12">
        <div class="box box-green">
          <div class="box-title">
            <h3><i class="fa fa-bars"></i><? echo $title; ?> Courier</h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
          </div>
          <div class="box-content">
            <form  class="form-horizontal" action="<? echo $action; ?>" method="post" enctype="multipart/form-data" id="operator" >
              <div class="row">
                <div class="col-md-12 ">
                  <!-- BEGIN Left Side -->
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Courier name</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <input type="text" name="name" id="textfield1" class="form-control" value="<? echo $courier['name']; ?>">
                      <input type="hidden" name="courierid" id="textfield1" class="form-control" value="<? echo $courier['id']; ?>">
                    </div>
                  </div>
 
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Mobile Number</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <input type="text" name="mobile" id="textfield1" class="form-control" value="<? echo $courier['mobile']; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Adddress</label>
                    <div class="col-sm-9 col-lg-5 controls">
                       <textarea name="address" class="form-control"><? echo $courier['address']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">City</label>
                    <div class="col-sm-9 col-lg-5 controls">
                      <input type="text" name="city" id="textfield1" class="form-control" value="<? echo $courier['city']; ?>">
                    </div>
                  </div>

<? if($courierid!='') {?> <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Select State</label>
                    <div class="col-sm-9 col-lg-5 controls">
                    <?  $db->select('pincode','distinct(State) as state','',"",'','');
 				   $pincode = $db->getResult();
				   ?><select name="state" id="state" class="form-control">
                   <option value="" >--Select--</option>
				   <? foreach($pincode as $pin){ ?>
 					<option value="<? echo $pin['state'];?>"><? echo $pin['state'];?></option>
                <? } ?>    </select> </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Select City</label>
                    <div class="col-sm-9 col-lg-5 controls">
          <select name="city" id="city" class="form-control col-md-12 blank" >
<option value="">Select</option>
 				   </select> </div>
                  </div>
                  <div><div  id="pincode" ></div><input type="button" value="add" id="add_pincode" class="btn btn-primary" style="display:none ; float:left" ></div>
                  <div id="pincode_data"></div>
                  
<? } ?>      <div id="pin_list" >
                  <?  $num_list = $db->getRowCount('courier_pincode_mapping','*',NULL,'courierid="'.$courierid.'"','id DESC');
				  if($num_list!=0){
				   ?><div class="col-md-12"><br /><br />
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h4 class="panel-title">Pincode</h4>
    </div>
    <div class="panel-body">
      <div class="col-md-12">
        <div class="box">
          <div class="box-content">
             <table class="table">
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Pincode</th>
                  <th>State</th>
                  <th>City</th>
                   <th>Action</th>
                </tr>
              </thead>
              <tbody>
			  <?php $i=1;
			  $courier =$_REQUEST['courier'];

$db->select('courier_pincode_mapping','*',NULL,'courierid="'.$courierid.'"','id DESC');
						 
$res = $db->getResult(); foreach($res as $values ){  
$db->select('pincode','*',NULL,'id="'.$values['pincodeid'].'"','id DESC');
$pin_detail = $db->getResult();  $pin_detail = $pin_detail[0]; ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo  get_pincode($values['pincodeid']); ?></td>
                  <td><?php echo   ($pin_detail['State']); ?></td>
                  <td><?php echo   ($pin_detail['District']); ?></td>
                   <td><span class="btn btn-danger" onClick="remove_pincode(<?php echo $values['id'] ?>)" >Remove</span></td>
                </tr>
				<?php $i++; } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
						 
<? }?>                  </div>              <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                      <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                      <button type="button" class="btn">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->
 <? include('bottom_link.php'); ?>

<script type="text/javascript">
 
	/* for fetch city */	

	$(document).on('change ','#state',function(){
 		state = $('#state').val();
	//	var dataString = 'state='+state+'&types=states';
	$("#city").load('getdistct.php?state='+state+'&types=states');


});	 /* for city */
	/* for fetch city */	

	$(document).on('change ','#city',function(){
 		city = $('#city').val();
		$('#add_pincode').css('display','block');
 $("#pincode").load('getdistct.php?city='+city+'&types=pincode');

  });	 /* for city */
  
  // for add pincode 
  
 $("#add_pincode").click(function(){
var state = $("#state").val();
var city = $("#city").val();
var pincode = [];
i = 0;
 $('.pincodes').each(function(){
 	 if($(this).is(':checked')){
    pincode[i++] = $(this).val(); 	 }	 
	 	 });  
var pins = pincode;
 var dataString = 'courier='+<?php echo $courierid; ?>+'&pincode='+state+'&city='+city+'&types=add_pincode'+'&pins='+pins;

//alert(dataString);
$.ajax
({
type: "POST",
url: "getdistct.php",
data: dataString,
cache: false,
success: function(html){
	$('#pin_list').css('display','none');
$("#pincode_data").load('getdistct.php?product_data=pincode_list&courier=<?php echo $courierid;  ?>');
$("#state").val('');
$("#city").val('');
} 
});


});

 //for remove pincode
function remove_pincode(id)
 { 
var dataString = 'id='+id+'&type=remove_pincode';
$.ajax
({
type: "POST",
url: "getdistct.php",
data: dataString,
cache: false,
success: function(html){
		$('#pin_list').css('display','none');

$("#pincode_data").load('getdistct.php?product_data=pincode_list&courier=<?php echo $courierid;  ?>');
 
} 
});
 }
</script>
 
</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
