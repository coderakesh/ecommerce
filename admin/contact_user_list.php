<?php 

error_reporting(0);

include('database.php');

include('functions.php');

include('session.php');
 ?>
 <!DOCTYPE html>
<html>
<head>
<? include('links.php'); ?>
</head>
 
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Contact us</h1>
        <h4>Contactus User list</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Contact us User list</li>
      </ul>
    </div>
    <div class="row  ">
      <div class="col-md-12">
<div class="box">
<div class="box-title">
<h3><i class="fa fa-table"></i>Contact us User list</h3>&nbsp;&nbsp;&nbsp;&nbsp;<button onclick="self.location.href = 'excel_contact_download.php';">click here to trigger CSV download</button>
<div class="box-tool">
<a class="btn btn-success" href="master_mail.php"><i class="fa fa-plus"></i> Add new</a>
 </div>
</div>
<div class="box-content">
<div class="clearfix"></div>
<div class="table-responsive" style="border:0">

<?php 

/*
$db->select('master_mail','*',NULL,'mail_type='.$_REQUEST['mail_type'],'id DESC');
$res = $db->getResult();
 */
 
  $sele = "SELECT * FROM `contactus_user` ";
 // $sel = "SELECT * FROM sms_email_template WHERE `mail_type`='".$_REQUEST['mail_type']."'";
 $sq  = mysql_query($sele); 
 ?>

<table class="table table-advance" id="table1">
<thead>
<tr>
<th>S.No</th>
<th>User name</th>
<th>Reason to contact</th>
<th>User Query</th>
<th>Email-id</th>
<th>Mobile</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php $i=1;
 while($valu = mysql_fetch_array($sq) ){
 ?>
<tr class="table-flag-blue">
<td><?php echo $i; ?></td>
<td><?php echo $valu['user_name']; ?></td>  
<td><?php echo $valu['subject_to_contact']; ?></td>  
<td><?php echo $valu['user_query']; ?></td>  
<td><?php echo $valu['user_email']; ?></td>  
<td><?php echo $valu['user_mobile']; ?></td>  
<td class="visible-md visible-lg">
<div class="btn-group">
<a class="btn btn-sm show-tooltip" title="" href="a.php?id=<?php echo $valu['cid']; ?>" data-original-title="Edit"><i class="fa fa-edit"></i></a>
</div>
</td>
</tr>
<?php $i++; } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container --> 
<? include('bottom_link.php'); ?>

</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
