<?php 
include('database.php');

include('functions.php');

include('session.php');

?>

<!DOCTYPE html>
<html>
<head>
<? include('links.php'); ?>
</head>


<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Master Brand Product</h1>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Master Brand Product</li>
      </ul>
    </div>
    <div class="row  ">
      
	  <div class="col-md-12">
<div class="row">

<div class="col-md-12">
<div class="panel panel-primary">
<div class="panel-heading">
<h4 class="panel-title">Add Brand </h4>
</div>
<div class="panel-body">
 <form  class="form-horizontal" action="master_brand_product1.php" method="post" enctype="multipart/form-data" id="operator" >
						<div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Name</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" name="brand_name" id="textfield1" class="form-control" required>
                          </div>
                        </div>
						<div class="form-group" >
                         <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Type</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <label for="brand"> Branded </label> <input type="radio" value="1" checked="checked" name="c" id="brand" />  &nbsp; &nbsp; 
                             <label for="unbrand">Unbranded </label> <input type="radio" value="0" name="brand" id="unbrand" /> 
						  </div>
                        </div>
                   
                        
                        
                        
				<div class="form-group">
                <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                  <button type="button" class="btn">Cancel</button>
                </div>
              </div>
</form>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-10">
  <!-- <a href="catalog_product_features_value.php" class="btn btn-primary"><i class="fa fa-check"></i> Add feature value</a><br><br>-->

</div>
<div class="col-md-12">
<div class="box box-magenta">
<div class="box-title">
<h3><i class="fa fa-table"></i>Brand list</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a>
</div>
</div>
<div class="box-content">
<?php $db->select('master_brand','*',NULL,'','id DESC');
	  $res = $db->getResult();

	  //$res = $res[0];
	  //print_r($res);
 ?>
<table class="table table-bordered">
<thead>
<tr>
<th>S.no</th>
<th>Brand Name</th>
<th>Branded</th>
<th>Edit</th>
<!--<th>Delete</th>-->
</tr>
</thead>
<tbody>
<?php $i=1; foreach($res as $values ){ ?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $values['value'] ?></td>
<td><?php if ($values['brand'] =='1'){echo 'Yes'; } elseif ($values['brand'] =='0'){echo 'No'; } ?></td>
<td><a class="btn btn-primary btn-sm" href="master_brand_product_edit.php?f_id=<?php echo $values['id'] ?>"><i class="fa fa-edit"></i> Edit</a></td>
<!--<td><a class="btn btn-primary btn-sm" href="master_Brand_del.php?d_id=<?php /*?><?php echo $values['id'] ?><?php */?>"><i class="fa fa-edit"></i> Delete</a></td>-->
</tr>
<?php $i++; } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>


</div>

</div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container --> 
<? include('bottom_link.php'); ?>

</body>

</html>
