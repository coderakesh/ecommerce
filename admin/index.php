 <html>

  <head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>  Admin</title>

<meta name="description" content="">

<meta name="viewport" content="width=device-width, initial-scale=1.0">



<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->



<!--base css styles-->

<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">

<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">



<!--page specific css styles-->



<!--flaty css styles-->

<link rel="stylesheet" href="css/flaty.css">

<link rel="stylesheet" href="css/flaty-responsive.css">



<link rel="stylesheet" type="text/css" href="validation/livevalid.css" />



<link rel="shortcut icon" href="img/favicon.html">
</head>

<body class="login-page">



<!-- BEGIN Main Content -->

<div class="login-wrapper">

<!-- BEGIN Login Form -->

<form id="form-login" action="login1.php" method="post" >

<div align="center" style="display:none"><img src="../img/logo/logo.png" width="200" height="60"  align="" /></div>

<h3 align="center">Login to your account</h3>

<hr/>

<div class="form-group">

<div class="controls">

<input type="text" placeholder="Username" class="form-control" name="username" id="username" />
</div>
</div>

<div class="form-group">

<div class="controls">

<input type="password" placeholder="Password" class="form-control" name="password" id="password" />
</div>
</div>

<div class="form-group">

<div class="controls">

<button type="submit" class="btn btn-primary form-control">Sign In</button>
</div>
</div>

<hr/>
</form>

<!-- END Login Form -->



<!-- BEGIN Forgot Password Form -->

<form id="form-forgot" action="" method="get" style="display:none">

<h3>Get back your password</h3>

<hr/>

<div class="form-group">

<div class="controls">

<input type="text" placeholder="Email" class="form-control" />
</div>
</div>

<div class="form-group">

<div class="controls">

<button type="submit" class="btn btn-primary form-control">Recover</button>
</div>
</div>

<hr/>

<p class="clearfix">

<a href="#" class="goto-login pull-left">← Back to login form</a></p>
</form>

<!-- END Forgot Password Form -->



<!-- BEGIN Register Form -->

<form id="form-register" action="https://themes.shamsoft.net/flaty/index.html" method="get" style="display:none">

<h3>Sign up</h3>

<hr/>

<div class="form-group">

<div class="controls">

<input type="text" placeholder="Email" class="form-control" />
</div>
</div>

<div class="form-group">

<div class="controls">

<input type="text" placeholder="Username" class="form-control" />
</div>
</div>

<div class="form-group">

<div class="controls">

<input type="password" placeholder="Password" class="form-control" />
</div>
</div>

<div class="form-group">

<div class="controls">

<input type="password" placeholder="Repeat Password" class="form-control" />
</div>
</div>

<div class="form-group">

<div class="controls">

<label class="checkbox">

<input type="checkbox" value="remember" /> I accept the <a href="#">user aggrement</a></label>
</div>
</div>

<div class="form-group">

<div class="controls">

<button type="submit" class="btn btn-primary form-control">Sign up</button>
</div>
</div>

<hr/>

<p class="clearfix">

<a href="#" class="goto-login pull-left">← Back to login form</a></p>
</form>

<!-- END Register Form -->
</div>

<!-- END Main Content -->





<!--basic scripts-->

<script src="../../ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="assets/jquery/jquery-2.1.1.min.js"><\/script>')</script>

<script src="assets/bootstrap/js/bootstrap.min.js"></script>



<!----------------- for validation ------------------>

<script type="text/javascript" src="validation/livevalidation_standalone.compressed.js"></script>	

<!----------------- for validation ------------------>



<script type="text/javascript">



var f1 = new LiveValidation('username');

f1.add( Validate.Presence );



var f2 = new LiveValidation('password');

f2.add( Validate.Presence );



$(".btn-primary").on('click', function () {  



var valid1 = livevalidatForm();	

  

if(valid1==true){

 

var test = $("#form-login").submit();

 

  }



});

</script>





</script>
</body>




</html>

