<?php
if (isset($_POST) && !empty($_FILES["uploadFile"]["tmp_name"])) {
    $mimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
    if (in_array($_FILES['uploadFile']['type'], $mimes)) {
        $flag = true;
    } else {
        die("Only Comma Delimited (.CSV) file Type allowed");
?>
			 <script language="javascript"> alert("Only Comma Delimited (.CSV) file Type allowed");location.href="multi_product_upload.php"; </script> 
			 <?php
        exit;
    }
    include ('../admin/database.php');
    include ('../admin/functions.php');
    $fileName = $_FILES["uploadFile"]["tmp_name"];
    $file = fopen($fileName, "r");
    $header = fgetcsv($file);
    // $linecount = count(file($fileName));
    $sm = array();
    $all_rows = array();
	$data = array();
	$cols=count($header);

    while ($column = fgetcsv($file)) {
		$all_rows[] = array_combine($header, $column);
		
    }

	$keys = array_keys($all_rows[0]);
	$abc=array_flip($keys);
	$data = array_change_key_case($abc, CASE_LOWER);
	$indexForAttr=$data['seller'];// for getting attr from there
	
    foreach ($all_rows as $key => $data) {
        
        if ($data['Productname'] == '') {
			?>
									<script language="javascript"> alert("Only Specified Format Allowed Please Upload Like Sample");location.href="multi_product_upload.php"; </script> 
									<?php
            exit;
        }
        $sku = $data['SKUcode'];
        $seller = $data['Seller'];
        if (($sku != '') && ($seller != '')) {
            $name = mysql_real_escape_string($data['Productname']);
            $cat = addslashes($data['Category']);
            $subcat = addslashes($data['Subcategory']);
            $retail_price = $data['MRP'];
            $retail_with_tax = $data['Selling Price'];
            $discount = $data['Discount%'];
            $weight = $data['Product Weight'];
            $dimension = $data['Packaging Dimension (LxBxH) Inches'];
            $wholesale_price = $data['wholesale_price'];
            $discounts = (($retail_price - $retail_with_tax) * 100 / $retail_price);
            $discount = number_format($discounts, 2, '.', '');
            $dim = array();
            //$dim               = explode('+', $dimension);
            $dim = explode('*', $dimension);
            $plength = $dim[0];
            $pheight = $dim[2];
            $pbreadth = $dim[1];
            $volum = ($plength * $pheight * $pbreadth) / 1728;
            $vol_weight = number_format((float)$volum, 2, '.', '');
            $bar = $data['Barcode'];
            $com_qty = $data['Quantity'];
            $short_description = mysql_real_escape_string($data['Short Description']);
            $description = mysql_real_escape_string($data['Description']);
            $cover_image = $data['Image1'];
            $img2 = $data['Image2'];
            $img3 = $data['Image3'];
            $img4 = $data['Image4'];
            $color = $data['Color'];
            $brand = $data['Brand'];
            // SEO
            $meta_title = mysql_real_escape_string($data['Meta Title']);
            $meta_keywords = mysql_real_escape_string($data['Meta Keywords']);
            $meta_description = mysql_real_escape_string($data['Meta Description']);
            $searchtags = $data['Search Tags'];
            $technicalSpecification = mysql_real_escape_string($data['Technical Specification']);
            $techndescription = mysql_real_escape_string($data['Technical Description']);
            $keyFeature = mysql_real_escape_string($data['Key Features']);
            $keyfeaturedes = mysql_real_escape_string($data['Key Features Description']);
            $size = $data['Size'];
            $tax_class = $data['tax'];
            $cal_param = $data['cal_param'];
            $delivery_day = $data['Delivery Days'];
            if ($delivery_day == '') {
                $delivery_day = '7';
            }
            $sku_count = $db->getRowCount('catalog_product', 'product_id', NULL, "ref_code='" . $sku . "'", '');
            $db->select('seller_registration', 'id', NULL, "company_name='" . $seller . "' and	approve='Y'", '');
            $seller_id = $db->getResult();
            $seller_id = $seller_id[0]['id'];
            $seller_count = mysql_num_rows(mysql_query("select id,company_name from seller_registration where company_name='" . $seller . "' and	approve='Y'"));
            if (($sku_count == 0) && ($seller_count != 0))
            // if(($sku_count==0))
            {
                $sql = "insert into catalog_product(name,ref_code,retail_price,retail_price_with_tax,discount,weight,plength,pheight,pbreadth,short_description,description,added_type,added_by,status,displaymode,vol_weight,create_date,delivery_day,wholesale_price,meta_title,meta_keywords,meta_description,technspecification,techndescription,keyfeature,keyfeaturedes,tax_class,cal_param) values('" . $name . "','" . $sku . "','" . $retail_price . "','" . $retail_with_tax . "','" . $discount . "','" . $weight . "','" . $plength . "','" . $pheight . "','" . $pbreadth . "','" . $short_description . "','" . $description . "','seller','" . $seller_id['id'] . "','Y','Y','" . $vol_weight . "','" . date('Y/m/d') . "','" . $delivery_day . "','" . $wholesale_price . "','" . $meta_title . "','" . $meta_keywords . "','" . $meta_description . "','" . $technicalSpecification . "','" . $techndescription . "','" . $keyFeature . "','" . $keyfeaturedes . "','" . $tax_class . "','" . $cal_param . "')";
                mysql_query($sql);
                $product_id = mysql_insert_id();
                $parent_cat = $db->select('catalog_category', 'category_id', NULL, "name='" . $cat . "'", '');
                $parent_cat = $db->getResult();
                $parent_cat = $parent_cat[0];
                $sub_cat = $db->select('catalog_category', 'category_id', NULL, "name='" . $subcat . "' and parent_id ='" . $parent_cat['category_id'] . "'", '');
                $sub_cat = $db->getResult();
                $sub_cat = $sub_cat[0];
                if ($searchtags != '') {
                    foreach (explode(',', $searchtags) as $searchtags_ami) {
                        $rr = "INSERT INTO `search_tag` (`tag_name`, `product_id`) VALUES ('" . $searchtags_ami . "', '" . $product_id . "')";
                        mysql_query($rr);
                    }
				}
				
                if ($product_id != '') {
                    $qry = "insert into product_category_mapping(productid,categoryid) values('" . $product_id . "','" . $sub_cat['category_id'] . "')";
                    mysql_query($qry);
                    if ($com_qty != '') {
                        $db->insert('product_comb', array('productid' => $product_id, 'barcode' => $bar, 'qty' => $com_qty, 'price' => $retail_with_tax)) or die(mysql_error());
						$combid = mysql_insert_id();
						
				//code to create combination nwl
				
				for($i=($indexForAttr+1);$i<=$cols;$i++){
					$hattr = $header[$i];
					$hattrval = $data[$header[$i]];
					if($data[$header[$i]] !=''){
					  $qry="select * from catalog_product_attribute where aname='$hattr'";
						$ncat=mysql_query($qry);
						if(mysql_num_rows($ncat)!=0){
							$attr=mysql_fetch_array($ncat);
						    $attrid=$attr['attribute_id'];
						}else{
							mysql_query("insert into catalog_product_attribute (aname,apublic_name,view_display) values('$hattr','$hattr','1') ") or die("error 201: ".mysql_error());
							$attrid= mysql_insert_id();
						}
						
					  $qry="SELECT * FROM `catalog_product_attribute_value` where `attribute_id`='$attrid' and `attribute_value`='$hattrval' ";
                        $ncat=mysql_query($qry);
						if(mysql_num_rows($ncat)!=0){
							$attrv=mysql_fetch_array($ncat);
						    $attr_val_id=$attrv['attribute_value_id'];
						}else{
							mysql_query("insert into catalog_product_attribute_value (attribute_id,attribute_value) values ('$attrid','$hattrval')") or die("error 202: ".mysql_error());
					     	$attr_val_id= mysql_insert_id();
						}
                        $qry6="insert into product_comb_att_mapping(combinationid,attrid,attrvalueid,productid) values('$combid','" . $attrid . "','" . $attr_val_id . "','" . $product_id . "') ";
						mysql_query($qry6);
					}

				}
                //code to create combination  nwl
						
                        
                    } //$com_qty != ''
                    
                } //$product_id != ''
                if (!file_exists("../upload/product/" . $product_id)) {
                    mkdir("../upload/product/" . $product_id, 0777, true);
                }
                $pathp = "../upload/product/" . $product_id . "/";
                $image = array();
                if ($cover_image != '') {
                    $filename = parse_url($cover_image);
                    $ext = pathinfo($filename['path'], PATHINFO_EXTENSION);
                    $img = '1.' . $ext; //new image name with extentions
                    $data = file_get_contents_curl($cover_image); //user defined fn file_get_contents_curl
                    file_put_contents("$pathp" . "$img", $data);
                    array_push($image, $img);
                }
                if ($img2 != '') {
                    $filename = parse_url($img2);
                    $ext = pathinfo($filename['path'], PATHINFO_EXTENSION);
                    $img = '2.' . $ext; //new image name with extentions
                    $data = file_get_contents_curl($cover_image); //user defined fn file_get_contents_curl
                    file_put_contents("$pathp" . "$img", $data);
                    array_push($image, $img);
                }
                if ($img3 != '') {
                    $filename = parse_url($img3);
                    $ext = pathinfo($filename['path'], PATHINFO_EXTENSION);
                    $img = '3.' . $ext; //new image name with extentions
                    $data = file_get_contents_curl($cover_image); //user defined fn file_get_contents_curl
                    file_put_contents("$pathp" . "$img", $data);
                    array_push($image, $img);
                }
                if ($img4 != '') {
                    $filename = parse_url($img4);
                    $ext = pathinfo($filename['path'], PATHINFO_EXTENSION);
                    $img = '4.' . $ext; //new image name with extentions
                    $data = file_get_contents_curl($cover_image); //user defined fn file_get_contents_curl
                    file_put_contents("$pathp" . "$img", $data);
                    array_push($image, $img);
                }
                for ($k = 0;$k < count($image);$k++) {
                    mysql_query("insert into product_image(productid,filename) values('" . $product_id . "','" . $image[$k] . "')");
                }
                $covers = mysql_fetch_array(mysql_query("select id from  product_image where productid=" . $product_id . " order by id desc limit 0,1 "));
                mysql_query("update catalog_product set coverphoto = '" . $covers['id'] . "'  where 	product_id = " . $product_id);
            } //($sku_count == 0) && ($seller_id['id'])
            else {
                $db->select('catalog_product', 'product_id', NULL, "ref_code='" . $sku . "'", '');
                $pro = $db->getResult();
                $pro = $pro[0];
                $product_id = $pro['product_id'];
                $parent_cat = $db->select('catalog_category', 'category_id', NULL, "name='" . $cat . "'", '');
                $parent_cat = $db->getResult();
                $parent_cat = $parent_cat[0];
                $sub_cat = $db->select('catalog_category', 'category_id', NULL, "name='" . $subcat . "' and parent_id ='" . $parent_cat['category_id'] . "'", '');
                $sub_cat = $db->getResult();
                $sub_cat = $sub_cat[0];
                
                $db->insert('product_comb', array('productid' => $product_id, 'barcode' => $bar, 'qty' => $com_qty, 'price' => $retail_with_tax)) or die(mysql_error()); //"productid='$product_id'"
                $combid = mysql_insert_id();
                /**$comc = mysql_query("select max(id) as id,qty from product_comb where productid='$product_id' and barcode='$bar' and price='$retail_with_tax' ");
                $ccount = mysql_num_rows($comc);
                if ($ccount == 0 && $com_qty != '') {
                    $db->insert('product_comb', array('productid' => $product_id, 'barcode' => $bar, 'qty' => $com_qty, 'price' => $retail_with_tax)) or die(mysql_error()); //"productid='$product_id'"
                    $combid = mysql_insert_id();
                } else {
                    $ndata = mysql_fetch_assoc($comc);
                    $combid = $ndata['id'];
                    $oldqty = $ndata['qty'];
                    $newqty = $com_qty + $oldqty;
                    mysql_query("update product_comb set qty='$newqty' where id='$combid'") or die(mysql_error());
                }**/

				//code to create combination nwl
				
				for($i=($indexForAttr+1);$i<=$cols;$i++){
					$hattr = $header[$i];
					$hattrval = $data[$header[$i]];
					if($data[$header[$i]] !=''){
					  $qry="select * from catalog_product_attribute where aname='$hattr'";
						$ncat=mysql_query($qry);
						if(mysql_num_rows($ncat)!=0){
							$attr=mysql_fetch_array($ncat);
						    $attrid=$attr['attribute_id'];
						}else{
							mysql_query("insert into catalog_product_attribute (aname,apublic_name,view_display) values('$hattr','$hattr','1') ") or die("error 201: ".mysql_error());
							$attrid= mysql_insert_id();
						}
						
					  $qry="SELECT * FROM `catalog_product_attribute_value` where `attribute_id`='$attrid' and `attribute_value`='$hattrval' ";
                        $ncat=mysql_query($qry);
						if(mysql_num_rows($ncat)!=0){
							$attrv=mysql_fetch_array($ncat);
						    $attr_val_id=$attrv['attribute_value_id'];
						}else{
							mysql_query("insert into catalog_product_attribute_value (attribute_id,attribute_value) values ('$attrid','$hattrval')") or die("error 202: ".mysql_error());
					     	$attr_val_id= mysql_insert_id();
						}
                        $qry6="insert into product_comb_att_mapping(combinationid,attrid,attrvalueid,productid) values('$combid','" . $attrid . "','" . $attr_val_id . "','" . $product_id . "') ";
						mysql_query($qry6);
					}

				}
				
                //code to create combination  nwl
                
            }
        } //($sku != '') && ($seller != '')
        
    } //$all_rows as $key => $val
    
?>

<script language="javascript"> alert("Done");location.href="multi_product_upload.php"; 
</script> 

<?php
} //isset($_POST['upload']) && !empty($_FILES["uploadFile"]["tmp_name"])

//print_r($data);
//fun to get image form url
function file_get_contents_curl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}
?> 