<?php 
include('database.php');

include('functions.php');

include('session.php');

?>
<!DOCTYPE html>
<html>
<head>
 <? include('links.php'); ?>
      <!----------- for Editor  --------------------->
<script src="richtext/js/tinymce/tinymce.dev.js"></script>
<script src="richtext/js/tinymce/plugins/table/plugin.dev.js"></script>
<script src="richtext/js/tinymce/plugins/paste/plugin.dev.js"></script>
<script src="richtext/js/tinymce/plugins/spellchecker/plugin.dev.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: ".elm1",
    theme: "modern",
   plugins: [
"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
"save table contextmenu directionality emoticons template textcolor paste  textcolor colorpicker "
],
external_plugins: {
//"moxiemanager": "/moxiemanager-php/plugin.js"
},
content_css: "css/development.css",
add_unload_trigger: false,
autosave_ask_before_unload: false,
menubar : false,
	toolbar1: "save newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
		toolbar2: "cut copy paste pastetext | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media help code | insertdatetime preview | forecolor backcolor",
		toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft | insertfile insertimage",
		menubar: false,
		toolbar_items_size: 'small',

		style_formats: [
			{title: 'Bold text', inline: 'b'},
			{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
			{title: 'Example 1', inline: 'span', classes: 'example1'},
			{title: 'Example 2', inline: 'span', classes: 'example2'},
			{title: 'Table styles'},
			{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		],

		templates: [
			{title: 'My template 1', description: 'Some fancy template 1', content: 'My html'},
			{title: 'My template 2', description: 'Some fancy template 2', url: 'development.html'}
		],
		 spellchecker_callback: function(method, data, success) {
			if (method == "spellcheck") {
				var words = data.match(this.getWordCharPattern());
				var suggestions = {};

				for (var i = 0; i < words.length; i++) {
					suggestions[words[i]] = ["First", "second"];
				}

				success({words: suggestions, dictionary: true});
			}

			if (method == "addToDictionary") {
				success();
			}
		}

});
</script><!------------ only for editor --------------------->
</head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->

  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
 		    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> Mailer </h1>
        <h4> Mail Content </h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active"> Mail Content </li>
      </ul>
    </div>
    <div class="row  ">
      <div class="col-md-12 col-sm-12 col-lg-12">
        <div class="box box-green">
          <div class="box-title">
            <h3><i class="fa fa-bars"></i> Content</h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
          </div>
          
          <?  $db= "SELECT * FROM sms_email_template WHERE temp_id='".$_REQUEST['id']."'";
		 			$sq = mysql_query($db);
					$res = mysql_fetch_array($sq);
			 ?>
          
          <div class="box-content">
            <form  class="form-horizontal" action="#" method="post" enctype="multipart/form-data" id="operator" >
              <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12"> 
                
               
                      <div class="tab-pane fade in active">
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-lg-12">
						
                     <div class="form-group">
                      <label for="textfield1" class="col-xs-3 col-lg-3 control-label ">Subject name </label>
                     <div class="col-sm-9 col-lg-5 controls">
                        <input type="text" name="subject" id="subject" required class="form-control" placeholder="Name of subject" value="<? echo $res['sms_name']; ?>">
                      </div>
                    </div>       
                            
                            
                    <div class="form-group hide">
                      <label for="textfield1" class="col-xs-3 col-lg-3 control-label ">Mail Type </label>
                     <div class="col-sm-9 col-lg-5 controls">
                        <input type="text" name="mail_type" id="mail_type" class="" required class="form-control" placeholder="Mail Type" value="<? echo $res['sms_email_type']; ?>">
                      </div>
                    </div>       
                             
                          	<div class="form-group">
                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Mail Content</label>
                              <div class="col-sm-9 col-md-9 col-lg-9 controls">
                                <textarea name="content" id="content" rows="5"  class="elm1" ><? echo $res['sms_email_data'];?></textarea>
                              </div>
                            </div>
                            
                            
                            <div class="form-group">
                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label">SMS Content</label>
                              <div class="col-sm-9 col-md-9 col-lg-9 controls">
                                <textarea name="sms_data" id="smscontent" rows="5"  class="elm1" ><? echo $res['sms_data'];?></textarea>
                              </div>
                            </div>
                             
                          </div>
                        </div>
        
                      </div>
                   
                  </div>
                </div>
             
              <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                  <!--button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button-->
                  
                  <input type="submit" name="submit" value="Save" class="fa fa-check btn btn-primary" />
                  
                  <button type="button" class="btn">Cancel</button>
                </div>
              </div>
            </form>
          </div>

          <? if(isset($_POST['submit'])){		
	  	$upd = "UPDATE `sms_email_template` SET `sms_email_data` = '".mysql_real_escape_string($_POST['content'])."', `sms_data` = '".$_POST['sms_data']."', `sms_email_type` = '".$_POST['mail_type']."', `sms_name` = '".$_POST['subject']."' WHERE temp_id='".$_REQUEST['id']."'";
				$rr = mysql_query($upd);
 ?><script> window.location = 'master_mail_list.php';</script>
	<?			}
				
		   ?>
          
        </div>
      </div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container --><? include('bottom_link.php'); ?>


</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
