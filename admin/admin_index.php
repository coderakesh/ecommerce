

<div class="row">
 <div class="col-md-6">
<div class="row">
<div class="col-md-12">
<div class="tile box">
<div class="block">
<div class="FL">
<h3 class="title">Catalog</h3>
</div>
<div class="FL">
<a href="#"><h3 class="title" align="right">View Detail</h3></a></div>
</div>


<h5 class="brd">In stock & Out of stock</h5>
<div class="block2">
<div class="FL">
<h3 class="value">In stock</h3>
<p class="digit">0</p>
</div>
<div class="FL">
<h3 align="right" class="value">Out of stock</h3>
<p align="right" class="digit">0</p>
</div>
</div>
</div>
</div>
</div>
</div>




<div class="col-md-6">
<div class="row">
<div class="col-md-6">
<div class="tile tile-orange">
<div class="img">
<i class="fa fa-user-plus"></i></div>
<div class="content">
<p class="big"><? echo getTotsell();?></p>
<p class="title">Total Seller</p>
</div>
</div>
</div>

<div class="col-md-6">
<div class="tile tile-dark-blue">
<div class="img">
<i class="fa fa-users"></i></div>
<div class="content">
<p class="big"><? echo getTotuser();?></p>
<p class="title">Total User</p>
</div>
</div>
</div>
</div>

<div class="row">
<div class="col-md-6">
<div class="tile tile-dark-blue">
<div class="img">
<i class="fa fa-user-times"></i></div>
<div class="content">
<p class="big"><? echo getPendsell();?></p>
<p class="title">Seller Pending</p>
</div>
</div>
</div>

<div class="col-md-6">
<div class="tile tile-orange">
<div class="img">
<i class="fa fa-cart-arrow-down"></i></div>
<div class="content">
<p class="big">+<? echo getPendord(); ?></p>
<p class="title">Order Pending</p>
</div>
</div>
</div>
</div>
</div>
</div>
 
<div class="row">
<div class="col-md-7">
<div class="box box-green1">
<div class="box-title">
<h3><i class="fa fa-table"></i> Latest Order</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
</div>
<div class="box-content">
<table class="table table-striped">
<thead>
<tr>
<th>Order Id</th>
<th>Ordered Date</th>
<th>Status</th>
<th>View</th>
</tr>
</thead>
<tbody>
<tr>
<td>1001</td>
<td>18-01-16</td>
<td><span class="label label-success">C</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1002</td>
<td>18-01-16</td>
<td><span class="label label-important">P</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1003</td>
<td>18-01-16</td>
<td><span class="label label-success">C</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1004</td>
<td>18-01-16</td>
<td><span class="label label-important">P</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1005</td>
<td>18-01-16</td>
<td><span class="label label-success">C</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1006</td>
<td>18-01-16</td>
<td><span class="label label-important">P</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1007</td>
<td>18-01-16</td>
<td><span class="label label-success">C</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1008</td>
<td>18-01-16</td>
<td><span class="label label-important">P</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
<tr>
<td>1009</td>
<td>18-01-16</td>
<td><span class="label label-success">C</span></td>
<td><a href="#"><span class="label label-info"> View <i class="fa fa-caret-right"></i></span></a></td>
</tr>
</tbody>
<tfoot>
<tr>
<td colspan="4"><a href="#"><span class="label label-info"> View All <i class="fa fa-caret-right"></i></span></a></td>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
<div class="col-md-5">

<div class="tile tile-blue-dark ">
<div class="img">
<i class="fa fa-shopping-bag"></i></div>
<div class="content">
<p class="big">+<? echo getordsuccess(); ?></p>
<p class="title">Total Selling</p>
</div>
</div>



<div class="box box-green1">
<div class="box-title">
<h3><i class="fa fa-bar-chart-o"></i> Visitors Chart</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
</div>
<div class="box-content">
<div id="visitors-chart" style="margin-top:20px; position:relative; height: 290px;"></div>
</div>
</div>
</div>
</div>
