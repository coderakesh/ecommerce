<?php 

error_reporting(0);
header('Access-Control-Allow-Origin: *');
include('database.php');

include('functions.php');
 include('session.php');
  ?>
 <!DOCTYPE html>
<html>
  <head> <? include('links.php'); ?></head>

<body>
<!-- BEGIN Theme Setting -->
  <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Cancel/Return Order</h1>
       </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index.php">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Cancel/Return list</li>
      </ul>
    </div>
    <div class="row">
      <div class="col-md-12 col-lg-12">
<div class="box box-green">
<div class="box-title">
<h3><i class="fa fa-bars"></i>Cancel/Return  List</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a>
</div>
</div>
</div>
</div>

 <div class="col-md-12 col-lg-12">
 <form action="cancel_return_rqst1.php" method="post" >
 
<div class="box">
 	<div class="box-content">
    <div align="center" id="show_val" style="display:none;">
<strong>     
<input type="hidden" id="final_add" value="0"  name="final_add">
<input type="hidden" id="d_pin" value="0"  name="o_pin">
<input type="hidden" id="o_pin" value="0"  name="d_pin">
<input type="hidden" value="<? echo getCourierToken(); ?>" id="token" >
<input type="hidden" value="" id="cl" >
<input type="hidden" value="" id="payment_mode" >
<input type="hidden"  value="0" id="delivery_charges" name="delivery_charges" >
<input type="hidden"  value="0" id="return_charges" name="return_charges" >
<input type="hidden"  value="0" id="pickup_charges" name="pickup_charges" >
<input type="hidden"  value="0" id="DL" name="DL" >
<input type="hidden"  value="0" id="FS" name="FS" >
<input type="hidden"  value="0" id="DTO" name="DTO" >
<input type="hidden"  value="0" id="RTO" name="RTO" >
<input type="hidden"  value="0" id="ST" name="ST" >
<input type="hidden"  value="0" id="COD" name="COD" >
 </strong><table class="table table-bordered">
  <tbody>
<tr>
<th>Total Quatity :</th>
<td><span id="tot_qty" ></span></td>
<th>Total Product Price :</th>
<td> <span id="tot_price" ></span> Rs.</td>
</tr>
<tr>
<th> Total Weight :</th>
<td> <span id="tot_weight" ></span>  Grams</td>
<th>Total volumetric Weight :</th>
<td><span id="tot_volumn" ></span> Grams </td>
</tr>
<tr>
<th>Total Courier Charges</td>
<td><span id="tot_charge" ></span> Rs.</td>
<th>DTO Charges</th>
<td><span id="dto_charge" ></span> Rs.</td>
</tr>
<tr>
<th>Fuel Charges</td>
<td><span id="fuel_charge" ></span> Rs. </td>
<th>Select Status</td>
<td><select name="status" class="form-control" required ><option value="0">--Select</option>
<option value="1" > Create Way bill </option>
<option value="2" > Delete Request </option>
</select> </td>
 
 
</tr>
</tbody>
</table>
 <input type="submit" value="Generate Waybill" class="btn way_btn btn-success " >
 </div>
 <br><div class="clearfix"></div>
 
<br/><br/>
<div class="clearfix"></div>
<div class="table-responsive" id="reload_tab">
<div  id="div_print"     style="width:100%;  overflow-x: scroll; scrollbar-arrow-color:
blue; scrollbar-face-color: #e7e7e7; scrollbar-3dlight-color: #a0a0a0; scrollbar-darkshadow-color:
#888888; background-color:#FFFFFF "> 
 <table class="table table-advance" id=" ">
<thead>
<tr><th style="width:18px">S. No.</th>
<th>Order Num</th>
<th>Product Name</th>
<th>Qty</th> <th>Amount</th><th>Weight</th><th>Volumetric Weight</th>
<th>Customer Pincode</th>
<th>Customer City</th>
<th>Customer Name</th>
<th>Seller Pincode</th>
 <th>Order Date</th>
<th>Cancel Date</th>
 </tr> </thead>  <tbody>  <? $db->select('cancel_refund_ord','*',NULL,"status=0",'');
 $cancel_refund = $db->getResult();   	 $i=1;
   foreach($cancel_refund as $cancel){  
 if($_SESSION['type']=='seller'){ $qu = "SELECT * FROM orders INNER JOIN orderdetail ON orders.oid=orderdetail.oid WHERE orderdetail.seller_id='".$_SESSION['master_user_id']."'  AND sell_type='seller'  and orderdetail.odid = ".$cancel['odid']."  and  orderdetail.status = 0 ORDER BY orders.oid DESC"; } 
else { $qu = "SELECT * FROM orders INNER JOIN orderdetail ON orders.oid=orderdetail.oid WHERE     orderdetail.odid = ".$cancel['odid']."    and  orderdetail.status = 0   ORDER BY orders.oid DESC"; } 
  $qry = mysql_query($qu); $row = mysql_fetch_array($qry);
 	$db->select('waybill_master','*',NULL,"id='".$row['waybill_id']."'",'');
 $waybill_del = $db->getResult();  $waybill_del = $waybill_del[0];
 	$db->select('catalog_product','coverphoto,weight,vol_weight',NULL,"product_id='".$row['pid']."'",'');
 $product = $db->getResult(); //print_r($cover_img);
 $product = $product[0];
 $weight = $row['qty']*$product['weight'];
  $vol_weight = $row['qty']*$product['vol_weight'];
 	$db->select('catalog_product','coverphoto,weight,vol_weight',NULL,"product_id='".$row['pid']."'",'');
 $product = $db->getResult(); //print_r($cover_img);
 $product = $product[0];
  	$db->select('profile_address','*',NULL,"pcaddid='".$row['addressid']."'",'');
 $address = $db->getResult(); //print_r($cover_img);
  $address = $address[0]; ?> <tr class="table-flag-blue">
<td><input type="checkbox" class="checkbox" id="opt<? echo $i; ?>" name="ord[]" value="<? echo $row['odid']; ?>"/>
<input type="hidden" value="<? echo $row['addressid']; ?>" id="addopt<? echo $i; ?>"  name="address">
<input type="hidden" value="<? echo $address['pincode']; ?>" id="pincodeopt<? echo $i; ?>"  name="pincode">
<input type="hidden" value="<? echo 'Pickup';//$row['paymenttype']; ?>" id="paymodeopt<? echo $i; ?>"  name="pincode">  </td>
<td><? echo $row['order_num']; ?></td>
<td><img class="em-product-main-img" src="../<? echo return_img_small($product['coverphoto'],$row['pid']); ?>" alt='' title=""  width="35" height="35"/><? echo get_product_name($row['pid']); ?> </td>
<td id="aopt<? echo $i; ?>"><? echo $row['qty']; ?></td>
<td id="popt<? echo $i; ?>"><? echo $row['price']; ?></td>
 <td id="bopt<? echo $i; ?>"><? echo $weight; ?></td>
<td id="copt<? echo $i; ?>"><? echo $vol_weight; ?></td>
<td> <? echo  $address['pincode']; ?></td>
<td> <? echo getcity($address['city']); ?></td>
<td> <? echo get_user_fulname($row['uid']); ?></td>
<td><? $db->select('seller_pickup_address','*',NULL,"id='".$waybill_del['seller_pick_add']."'",'');
 $o_pincode = $db->getResult(); //print_r($cover_img);
 $o_pincode =  $o_pincode[0]; $o_pin =  $o_pincode['pincode']; echo $o_pin.'/'.$o_pincode['company-name']; ?> 
<input type="hidden" value="<? echo $o_pin; ?>" id="spincodeopt<? echo $i; ?>"  name="pincode">
<input type="hidden" value="<? echo $o_pincode['company-name']; ?>" id="clopt<? echo $i; ?>"  name="client_address">

</td>
<td> <? echo  $row['odate']; ?></td>
<td> <? echo  date("d/m/Y",$cancel['dates']); ?></td>
</tr>    <? $i++;  } ?>
 </tbody>
</table>
</div> 
</div>
</div></div> </form>
 </div></div>      </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->
 <? include('bottom_link.php'); ?>

<script>
	    //For  checkboxes

$('.checkbox').click(function() {
    var total = 0;  
    var total1 = 0;  
    var total2 = 0;  var total_price =0;  x = [];
	chk_count = 	$('input[type="checkbox"]:checked').length;  
if(chk_count==0){ $('#final_add').val('0');
$('#payment_mode').val('');
  $('#show_val').css('display','none');
  }
  else{
  $('#show_val').css('display','block');
  }
        $('.checkbox').each(function() {
        if ($(this).is(":checked")){
//    var add =  	$('#add'+tst).val();
var tst = ($(this).attr('id')); 
add =  $('#add'+tst).val(); 
pay = $('#paymode'+tst).val(); 
 final_add =  $('#final_add').val(); 
 payment_mode =  $('#payment_mode').val();  
 	 if(final_add==0){
$('#final_add').val($('#add'+tst).val());		 
$('#d_pin').val($('#pincode'+tst).val());	 	 
	$('#o_pin').val($('#spincode'+tst).val());	 
	$('#cl').val($('#cl'+tst).val());	 
$('#payment_mode').val($('#paymode'+tst).val());		 
 } 	 final_add1 =  $('#final_add').val(); 	
	 payment_mode1 =  $('#payment_mode').val(); 	
	if((final_add1==add) && (payment_mode1==pay) ){  
	 total1 = parseFloat(total1) + parseFloat($('#a'+tst).text()); 
   total = parseFloat(total) + parseFloat($('#b'+tst).text());
  total2 = parseFloat(total2) + parseFloat($('#c'+tst).text()); 
  total_price = parseFloat(total_price) + parseFloat($('#p'+tst).text());   }	
 else{   
	alert('Please checked the same user and same payment type');	$('#'+tst).attr( "checked", false );
 }
	$('#tot_qty').text(total1);
	$('#tot_weight').text(total);
	$('#tot_volumn').text(total2);
	$('#tot_price').text(total_price);
} });
  var o_pin = $('#o_pin').val();
 var d_pin = $('#d_pin').val();
var token = $('#token').val();
var amt = 	$('#tot_price').text();
var tot_weight =	parseFloat($('#tot_weight').text());
var tot_volumn =	parseFloat($('#tot_volumn').text());
var cl =	$('#cl').val();
 if(tot_weight<tot_volumn)  {
  var gm = tot_volumn;}
else { 
 var gm = tot_weight;} 
 if(payment_mode1=='COD'){
   create_url = 'https://track.delhivery.com/kinko/api/invoice/charges/json/?token='+token+'&cl='+cl+'&pt=cod&o_pin='+o_pin+'&d_pin='+d_pin+'&cod='+amt+'&gm='+gm;
 }  else{     create_url = 'https://track.delhivery.com/kinko/api/invoice/charges/json/?token='+token+'&cl='+cl+'&pt=prepaid&o_pin='+o_pin+'&d_pin='+d_pin+'&gm='+gm;
  }     console.log(create_url);
     $.ajax({  type:"POST",
 url:create_url,
 async:true,
 dataType : 'jsonp',   //you may use jsonp for cross origin request
 crossDomain:true,  contentType: "application/json; charset=utf-8",
 success: function(data){
     del_charg =  data['response']['all_charges']['DL'];     dto =  data['response']['all_charges']['DTO'];
 rto =  data['response']['all_charges']['RTO'];  delivery_charges =  data['response']['delivery_charges']; 
return_charges =  data['response']['return_charges'];  pickup_charges =  data['response']['pickup_charges']; 
    cod =  data['response']['all_charges']['COD'];
   fuel_charg_per = data['response']['all_charges']['FS'];
   ser_tax_per  = data['response']['all_charges']['ST'];
 tot_charge = data['response']['delivery_charges'];
 fuel_charg = del_charg*fuel_charg_per/100 ;
  after_fuel = fuel_charg + del_charg;   ser_tax = after_fuel*ser_tax_per/100;    	$('#tot_charge').text(pickup_charges);
  dto_fuel_charg = dto*fuel_charg_per/100 ; 
  after_fuel_dto =dto_fuel_charg + dto; ser_tax_dto = after_fuel_dto*ser_tax_per/100; 
   	$('#fuel_charge').text(dto_fuel_charg);
   	$('#service_charge').text(ser_tax_dto);
   	$('#cod').text(cod);
   	$('#FS').val(dto_fuel_charg);
   	$('#DL').val(del_charg);
   	$('#DTO').val(dto);
   	$('#COD').val(cod);
   	$('#ST').val(ser_tax_per);
   	$('#RTO').val(rto);
   	$('#delivery_charges').val(delivery_charges);
   	$('#return_charges').val(return_charges);
   	$('#pickup_charges').val(pickup_charges);
  	$('#dto_charge').text(dto);
 }
 });
 
 
  }); </script>
 
</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>

