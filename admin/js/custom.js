(function($) {

    "use strict";

    function toogleMenuPro_4_8356() {
        var $ = jQuery;
        var wi = $(window).width();
        var wrapperMenu = $(".wrapper-4_8356");
        var container = $("#toogle_menu_4_8356");
        if (EM.SETTING.DISABLE_RESPONSIVE != 0) {
            if ((wi <= 767 || isPhone == true)) {
                wrapperMenu.addClass('em-menu-mobile');
            } else {
                wrapperMenu.removeClass('em-menu-mobile');
                container.show();
            }
        } else {
            wrapperMenu.removeClass('em-menu-mobile');
            container.show();
        }
    };

    /* Toggle menuleft */
    function toogleMenuPro_13_2552() {
        var $ = jQuery;
        var wi = $(window).width();
        var wrapperMenu = $(".wrapper-13_2552");
        var container = $("#toogle_menu_13_2552");
        if (EM.SETTING.DISABLE_RESPONSIVE != 0){
            if ((wi <= 767 || isPhone == true)){
                wrapperMenu.addClass('em-menu-mobile');
            } else {
                wrapperMenu.removeClass('em-menu-mobile');
                container.show();
            }
        } else {
            wrapperMenu.removeClass('em-menu-mobile');
            container.show();
        }
    };

    /* Toolbar Language */
    function emHoverUlLanguage() {
        if (!isMobile) {
            var sLan = $('#em-hoverUl-language');
            var len = sLan.length;
            if (len) {
                sLan.insertUlLanguage();
                sLan.selectUl();
            }
        }
    }
    /* Toolbar Currency */
    function emHoverUlCurrency() {
        if (!isMobile) {
            var sCur = $('#em-hoverUl-currency');
            var len = sCur.length;
            if (len) {
                sCur.insertUl();
                sCur.selectUl();
            }
        }
    }

    /* Toolbar Search */
    var timeoutSearch = null;

    function hideSearch(e) {
        var $_container = e.children().find('.em-container-js-search');
        if (timeoutSearch) clearTimeout(timeoutSearch);
        timeoutSearch = setTimeout(function() {
            timeoutSearch = null;
            $_container.hide(300, function() {
                $(this).css('overflow', 'inherit');
            });
        }, 200);
    }
    // Show Cart
    function showSearch(e) {
        var $_container = e.children().find('.em-container-js-search');
        if (timeoutSearch) clearTimeout(timeoutSearch);
        timeoutSearch = setTimeout(function() {
            timeoutSearch = null;
            $_container.show(300, function() {
                $(this).css('overflow', 'inherit');
            });
        }, 200);
    }
    /* Toolbar Login */
    var tmlink;

    function showlink(el) {
        clearTimeout(tmlink);
        tmlink = setTimeout(function() {
            el.slideDown();
        }, 200);
    }

    function hidelink(el) {
        clearTimeout(tmlink);
        tmlink = setTimeout(function() {
            el.slideUp();
        }, 200);
    }

    function effectLoginForm() {
        var sLogin = $('#em-account-login-form');
        var sLink = $('#link-login');
        var sDivLink = $('#em-login-link');
        if (sLogin.length > 0) {
            //hover login form
            if (isMobile) {
                sLink.attr('href', 'javascript:void(0);');
                sLink.click(function(e) {
                    sLogin.slideToggle();
                });
            } else {
                sDivLink.mouseover(function() {
                    showlink(sLogin);
                });
                sDivLink.mouseout(function() {
                    hidelink(sLogin);
                });
            }
            // Popup Login Form
        }
    }

    /* Top Cart */
    var timeout = null;

    function hideCart(e) {
        var $_container = e.children().find('.em-container-js-topcart');
        if (timeout) clearTimeout(timeout);
        timeout = setTimeout(function() {
            timeout = null;
            $_container.hide(300, function() {
                $(this).css('overflow', 'inherit');
            });
        }, 200);
    }
    // Show Cart
    function showCart(e) {
        var $_container = e.children().find('.em-container-js-topcart');
        if (timeout) clearTimeout(timeout);
        timeout = setTimeout(function() {
            timeout = null;
            $_container.show(300, function() {
                $(this).css('overflow', 'inherit');
            });
        }, 200);
    }

    /* Menu toogle_menu_4_7164 */
    function toogleMenuPro_4_7164() {
        var $ = jQuery;
        var wi = $(window).width();
        var wrapperMenu = $(".wrapper-4_7164");
        var container = $("#toogle_menu_4_7164");
        if (EM.SETTING.DISABLE_RESPONSIVE != 0) {
            if ((wi <= 767 || isPhone == true)) {
                wrapperMenu.addClass('em-menu-mobile');
            } else {
                wrapperMenu.removeClass('em-menu-mobile');
                container.show();
            }
        } else {
            wrapperMenu.removeClass('em-menu-mobile');
            container.show();
        }
    };

    /* Mobile Navigation */
    function fixNavigationMobileView() {
        var $_winW = $(window).width();
        var $_mmenu = $('#em-mheader-menu-content');
        var elem = $('#em-mheader-menu-content');
        var $_parent = $('#em-mheader-wrapper-menu');
        var $_iconNav = $('#em-mheader-menu-icon');
        if (!isPhone || $_winW > 767) {
            $_mmenu.removeClass();
            $_iconNav.removeClass('active');
            elem.removeClass('show');
            $_parent.removeClass('active');
        }
    } 

    function fixNavOverFlow() {
        var $_iconNav = $('#em-mheader-menu-icon');
        var elem = $('#em-mheader-menu-content');
        var $_parent = $('#em-mheader-wrapper-menu');
        $_iconNav.click(function(e) {
            e.preventDefault();
            var self = $(this);
            var isSkipContentOpen = elem.hasClass('show') ? 1 : 0;
            self.removeClass('active');
            elem.removeClass('show');
            $_parent.removeClass('active');
            if (isSkipContentOpen) {
                self.removeClass('active');
                $_parent.removeClass('active');
            } else {
                self.addClass('active');
                elem.addClass('show');
                $_parent.addClass('active');
            }
        });
        if (isPhone) {
            $_parent.on("swipeleft", function() {
                var self = $(this);
                if (self.hasClass('active')) {
                    elem.removeClass('show');
                    $_iconNav.removeClass('active');
                    self.removeClass('active');
                }
            });
        }
    };



    /* Scroll Top */
    function backToTop() {
        // hide #back-top first
        var sBackTop = $('#back-top');
        if (sBackTop.length) {
            //var sClickBackTop = $('#back-top a');
            sBackTop.hide();
            // fade in #back-top
            if (!isMobile) {
                $(window).scroll(function() {
                    if ($(this).scrollTop() > 100) {
                        sBackTop.fadeIn();
                    } else {
                        sBackTop.fadeOut();
                    }
                });
                // scroll body to 0px on click
                $('a', sBackTop).click(function() {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                    return false;
                });
            }
        }
    };


    /* Newsletter */
    function showPopUpLogin() {
        if (jQuery('#em-popup').length > 0) {
            $.fancybox.open('#em-popup', {
                width: 'auto',
                height: 'auto',
                openEffect: 'elastic',
                closeEffect: 'elastic'
            });
        }
    }

    jQuery(document).ready(function() {
        if (EM.SETTING.DISABLE_RESPONSIVE != 0) {
            fixNavigationMobileView();
            fixNavOverFlow();
        }

        toogleMenuPro_4_8356();
        toogleMenuPro_13_2552();
        emHoverUlLanguage();
        emHoverUlCurrency();

        $("body").on("mouseover", ".em-wrapper-js-search", function(e) {
            e.preventDefault();
            var $_this = $(this);
            showSearch($_this);
        });
        $("body").on("mouseout", ".em-wrapper-js-search", function(e) {
            e.preventDefault();
            var $_this = $(this);
            hideSearch($_this);
        });
        effectLoginForm();

        /* top-cart */
        if (isMobile == true) {
            var sLink = $('.em-amount-js-topcart');
            var sCartContent = $(".em-wrapper-js-topcart");
            sLink.attr('href', 'javascript:void(0)');
            sCartContent.click(function(event) {
                var $_this = $(this);
                var $_container = $_this.children().find('.em-container-js-topcart');
                $_container.slideToggle();
                $_this.toggleClass('em-click-topcart');
            });
        } else {
            // Hide Cart
            var sCartContent = $(".em-wrapper-js-topcart");
            sCartContent.bind('mouseover', function(e) {
                e.preventDefault();
                var $_this = $(this);
                showCart($_this);
            }).bind('mouseout', function(e) {
                e.preventDefault();
                var $_this = $(this);
                hideCart($_this);
            });
        }

        /* Main Slider */
        var sync1 = $('#em_owlcarousel_18_18282_sync1');
        sync1.owlCarousel({
            singleItem: true,
            responsiveRefreshRate: 200,
            paginationSpeed: 2000,
            rewindSpeed: 1000,
            lazyLoad: true,
            slideSpeed: 200,
            navigation: false,
            pagination: false,
            navigationText: false,
            transitionStyle: 'fade',
            autoPlay: true,
        });

         

  
     
         
    
        /* Best Seller Slider */
        var owl = $("#em_minifilter_products_2d1580d33630882c6a0307c8f6145570").find('.owl-carousel');
        if (owl.length)
        {
            owl.owlCarousel(
            {
                //Basic Speeds
                slideSpeed: 800,
                rewindSpeed: 800,
                //Autoplay
                autoPlay: false,
                lazyLoad: false,
                stopOnHover: false,
                mouseDrag: false,
                touchDrag: false,
                // Navigation
                navigation: true,
                navigationText: ["Previous", "Next"],
                pagination: false,
                paginationNumbers: false,
                // Responsive 
                responsive: true,
                items: 1,
                /*items above 1200px browser width*/
                itemsDesktop: [1200, 1],
                /*//items between 1199px and 981px*/
                itemsDesktopSmall: [992, 1],
                itemsTablet: [768, 1],
                itemsMobile: [480, 1],
                // CSS Styles
                baseClass: "owl-carousel",
                theme: "owl-theme",
                transitionStyle: false,
                addClassActive: true,
                scrollPerPage: false,
                afterMove: function()
                {
                    var $_img = jQuery('#em_minifilter_products_2d1580d33630882c6a0307c8f6145570').find('img.em-img-lazy');
                    if ($_img.length)
                    {
                        var timeout = setTimeout(function()
                        {
                            $_img.trigger("appear");
                        }, 200);
                    }
                },
            });
        }

        /* Sale OFF Slider */
        var owl = $("#em_minifilter_products_70f6653a44797dd14bcef64425f9c305").find('.owl-carousel');
        if (owl.length)
        {
            owl.owlCarousel(
            {
                //Basic Speeds
                slideSpeed: 800,
                rewindSpeed: 800,
                //Autoplay
                autoPlay: false,
                lazyLoad: false,
                stopOnHover: false,
                mouseDrag: false,
                touchDrag: false,
                // Navigation
                navigation: true,
                navigationText: ["Previous", "Next"],
                pagination: false,
                paginationNumbers: false,
                // Responsive 
                responsive: true,
                items: 1,
                /*items above 1200px browser width*/
                itemsDesktop: [1200, 1],
                /*//items between 1199px and 981px*/
                itemsDesktopSmall: [992, 1],
                itemsTablet: [768, 1],
                itemsMobile: [480, 1],
                // CSS Styles
                baseClass: "owl-carousel",
                theme: "owl-theme",
                transitionStyle: false,
                addClassActive: true,
                scrollPerPage: false,
                afterMove: function()
                {
                    var $_img = jQuery('#em_minifilter_products_70f6653a44797dd14bcef64425f9c305').find('img.em-img-lazy');
                    if ($_img.length)
                    {
                        var timeout = setTimeout(function()
                        {
                            $_img.trigger("appear");
                        }, 200);
                    }
                },
            });
        }

        backToTop();
        showPopUpLogin();
    });

    

})(jQuery);
jQuery(document).on('resize orientationchange', window, function() {
    if (EM.SETTING.DISABLE_RESPONSIVE != 0) {
        toogleMenuPro_4_8356();
        toogleMenuPro_13_2552();
    }
});
jQuery(window).resize(jQuery.throttle(300, function() {
    if (EM.SETTING.DISABLE_RESPONSIVE != 0) {
        fixNavigationMobileView();
    }
}));


//<![CDATA[
var isMobile = /iPhone|iPod|iPad|Phone|Mobile|Android|hpwos/i.test(navigator.userAgent);
var isPhone = /iPhone|iPod|Phone|Android/i.test(navigator.userAgent);
var isWindowPhone = '';
var language = 'images/language/';
var urlsite = '';
var layout = '2columns-left';
var product_zoom = null;
if (typeof EM == 'undefined') EM = {};
EM.SETTING = {
    USE_TAB: '1',
    DISABLE_VARIATION: '1',
    DISABLE_RESPONSIVE: '1',
    AJAXCART_AUTOCLOSE: '0',
    DISABLE_AJAX_ADDTO: '1',
    DISABLE_COLLAPSE: '1',
    STICKY_MENU_SEARCH_CART: '1',
    RIGHT_TO_LEFT: '0',
    COLOR_SWATCHES: '1',
};
//]]> 
 
 
 if (typeof jQuery == 'undefined') {
    document.write('<script src="js/jquery.min.js"><\/script>');
}
var index=0; var count=0;
var arr = new Array();
$( document ).ready(function() {
 shok();
});

function shok()
{ 	createCookie('did',1,1);
	if(readCookie('cart')!=null && readCookie('cart')!="")
	{
		//var v= new Array();
		arr= JSON.parse(readCookie('cart'));
		//alert(readCookie('cart'));
		if(arr.length!=0){
			cartIn(arr);
		}
		else { $('.proceed').text("Your cart is empty"); }
	} 	//document.getElementById('result').innerHTML=v;
}
 
function cartIn(v)
{
 	//var did= readCookie('did');
	var total=0;
			for	(var i = 0; i < v.length; i++) {
				var itemDetails = v[i].split("#");
				total += Number(itemDetails[2]);
				index += Number(itemDetails[4]);
				count++; 			}
			$('.items span').text(index);
			$('.total').text(total);
			$('#CartItems').html("");
			for	(i = 0; i < v.length; i++) {  
				itemDetails= v[i].split("#");
				$('<div id="cart'+itemDetails[0]+'" class="item-in-cart clearfix"><div class="image"><img src="'+itemDetails[1]+'" width="60" height="60" alt="cart item" /></div><div class="desc detail"><a href="">'+itemDetails[3]+'</a></div> <div class="amt-detail"><div class="quantity"><span class="light-clr qty"> Qty: '+itemDetails[4]+' &nbsp; <a id="'+itemDetails[0]+'#'+itemDetails[5]+'" href="#" onclick="removeItems(this.id)" class="icon-remove-sign" title="Remove Item"></a></span></div>  <div class="price"> <strong>Rs '+itemDetails[2]+'</strong></div></div></div>').appendTo('#CartItems');
			}
			$('.proceed').html("");
			$('.proceed').html('<a href="mycart.php" style="" class="btn mybutton pull-right bold   fancybox fancybox.iframe ">CHECKOUT <i class="icon-shopping-cart"></i></a>');
 } 
		function changeCart(ref) 
		{  
 			var dist= '1';
			var dname='1';
 			var combo_atr= $(".final_combo").val();
 			var combo_id= $("#comb_id").val();  
  			//if(dist!="" && dist.length == 10 && dname!="")
			//{
			var bol=0;
			for	(var i = 0; i < arr.length; i++) {
				var itemDetails = arr[i].split("#");
				if(itemDetails[0]==ref){
				//	var unit_price=Number(itemDetails[2]);
 					var price=Number(itemDetails[2]);
					var next= $('#'+ref+'price').text();
					price += Number(next);
					var qty=Number(itemDetails[4]);
             qty +=1; 
 arr[i]= itemDetails[0]+'#'+itemDetails[1]+'#'+price.toString()+'#'+itemDetails[3]+'#'+qty+'#'+dname+'#'+dist+'#'+combo_atr+'#'+combo_id ;
	 		bol=1;
					index=index+1; 
 					$('.items span').text(index);  
				}
			}
			if(bol==0)
			{
				var data= ref;
				data += '#'+$('#'+ref+'img').attr("src");
				data += '#'+$('#'+ref+'price').text();
				data += '#'+$('#'+ref+'name').text();
				data += '#1';
			//	data +='#'+$("#distributor").val();
				//data +='#'+dname;
				data +='#2';
				 data +='#3';
				 data +='#'+combo_atr;
				arr[count]=data;
				++index; count++;
				//createCookie('index',index,1);
				$('.items span').text(index); 
			}
			//cartIn(arr);
			var total=0;
			for	(i = 0; i < arr.length; i++) {
				var itemDetails = arr[i].split("#");
				total += Number(itemDetails[2]);
			}
			$('.total').text(total);
			$('#CartItems').html("");
			for	(i = 0; i < arr.length; i++) {
				itemDetails= arr[i].split("#");
 				$('<div id="cart'+itemDetails[0]+'" class="item-in-cart clearfix"><div class="image"><img src="'+itemDetails[1]+'" width="60" height="60" alt="cart item" /></div><div class="desc detail"><a href="product.html">'+itemDetails[3]+'</a></div> <div class="amt-detail"><div class="quantity"><span class="light-clr qty"> Qty: '+itemDetails[4]+' &nbsp; <a id="'+itemDetails[0]+'#'+itemDetails[5]+'" href="#" onclick="removeItems(this.id)" class="icon-remove-sign" title="Remove Item"></a></span></div><input type="hidden" value='+itemDetails[8]+' id="comb_id" >   <div class="price"> <strong>Rs '+itemDetails[2]+'</strong></div></div></div>').appendTo('#CartItems');
			}
			$('.proceed').html("");
			$('.proceed').html('<a href="mycart.php" class="btn  btn-primary pull-right bold higher">CHECKOUT <i class="icon-shopping-cart"></i></a>');
		//	 $.alert.open('info', 'Product add sucessfully!');

    $(window).scrollTop(0);
			var str= JSON.stringify(arr);
			createCookie('cart',str,1);          
		//}
		}
		
		// for add wish list item
				function changeCartWish(ref) 
		{  
 			var dist= '1';
			var dname='1';
 			var combo_atr= $(".final_combo"+ref).val();
 		 			var combo_id= $("#comb_id"+ref).val();
 
 //if(dist!="" && dist.length == 10 && dname!="")
			//{
			var bol=0;
			for	(var i = 0; i < arr.length; i++) {
				var itemDetails = arr[i].split("#");
				if(itemDetails[0]==ref){
				//	var unit_price=Number(itemDetails[2]);
 					var price=Number(itemDetails[2]);
					var next= $('#'+ref+'price').text();
					price += Number(next);
					var qty=Number(itemDetails[4]);
					qty +=1;
					arr[i]= itemDetails[0]+'#'+itemDetails[1]+'#'+price.toString()+'#'+itemDetails[3]+'#'+qty+'#'+dname+'#'+dist+'#'+combo_atr+'#'+combo_id ;
					bol=1;
					index=index+1; 
 					$('.items span').text(index);  
				}
			}
			if(bol==0)
			{
				var data= ref;
				data += '#'+$('#'+ref+'img').attr("src");
				data += '#'+$('#'+ref+'price').text();
				data += '#'+$('#'+ref+'name').text();
				data += '#1';
			//	data +='#'+$("#distributor").val();
				//data +='#'+dname;
				data +='#2';
				 data +='#3';
				 data +='#'+combo_atr;
				arr[count]=data;
				++index; count++;
				//createCookie('index',index,1);
				$('.items span').text(index); 
			}
			//cartIn(arr);
			var total=0;
			for	(i = 0; i < arr.length; i++) {
				var itemDetails = arr[i].split("#");
				total += Number(itemDetails[2]);
			}
			$('.total').text(total);
			$('#CartItems').html("");
			for	(i = 0; i < arr.length; i++) {
				itemDetails= arr[i].split("#");
			 
				$('<div id="cart'+itemDetails[0]+'" class="item-in-cart clearfix"><div class="image"><img src="'+itemDetails[1]+'" width="60" height="60" alt="cart item" /></div><div class="desc detail"><a href="product.html">'+itemDetails[3]+'</a></div> <div class="amt-detail"><div class="quantity"><span class="light-clr qty"> Qty: '+itemDetails[4]+' &nbsp; <a id="'+itemDetails[0]+'#'+itemDetails[5]+'" href="#" onclick="removeItems(this.id)" class="icon-remove-sign" title="Remove Item"></a></span></div>  <div class="price"> <strong>Rs '+itemDetails[2]+'</strong></div></div></div>').appendTo('#CartItems');
			}
			$('.proceed').html("");
			$('.proceed').html('<a href="mycart.php" class="btn  btn-primary pull-right bold higher">CHECKOUT <i class="icon-shopping-cart"></i></a>');
			
			var str= JSON.stringify(arr);
			createCookie('cart',str,1);  
			removeWishItems(ref);
		//}
		}
		// for wishlist item add
		
		function removeItems(ref)
		{  
			var refer= ref.split("#");
			var pos=1000;
			var total=0;
			for	(var i = 0; i < arr.length; i++) {
				var itemDetails = arr[i].split("#");
				total += Number(itemDetails[2]);
				if(itemDetails[0]==refer[0] && itemDetails[5]==refer[1]){
					pos=i;
					index=index-Number(itemDetails[4]);
				}
			}
			if(pos!=1000)
			{
				var check=arr[pos];
				itemDetails = arr[pos].split("#");
				//index=index-Number(itemDetails[4]);
				$('.items span').text(index);
				var price=Number(itemDetails[2]);
				$('.total').text(total-price);
				$('#cart'+itemDetails[0]).css('display','none');
				
				arr.splice(pos, 1);
				var len = arr.length;
				count--;
				if(arr.length==0)
				{
					$('.total').text(0);
					$('.items span').text(0);
					$('.proceed').html('Your cart is empty');
					$('#cart'+itemDetails[0]).css('display','none');
					index=0;
					count=0;
				}
				
			}
			var str= JSON.stringify(arr);
			createCookie('cart',str,1); 
			
			//createCookie('index',index,1);
		}
	
			function removeWishItems(ref)
		{  
 			 	 $.ajax({
				  method : "POST", 
 				 url: "delete_wishitem.php?pid="+ref,
 				}).done(function(data) {	
 				$('#wish_'+ref).css('display','none');
	});	

		}

// for add to cart
