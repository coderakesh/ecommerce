<?php
include('database.php');
include('functions.php');
include('session.php');
date_default_timezone_set("Asia/Calcutta");
$name                   = $_POST['product_name'];
// Code changes Gaurav D
$productType            = $_POST['producttype']; // Added for product type
$refrenceCode           = $_POST['refcode'];
$barcode                = $_POST['barcode'];
$technicalSpecification = $_POST['technical_specification'];
$techndescription       = mysql_real_escape_string($_POST['techndescription']);
$keyFeature             = $_POST['key_feature'];
$emi                    = $_POST['emi'];
$keyfeaturedes          = mysql_real_escape_string($_POST['keyfeaturedes']);
$displayMode            = $_POST['display_mode'];
$offerMode              = $_POST['offer_mode'];
$visibility             = $_POST['visibility'];
$productcondition       = $_POST['productcondition'];
$searchtags             = $_POST['search_tags'];
$packInd                = $_POST['packInd'];
$friendly_url           = $_POST['friendly_url'];
$default_qty            = $_POST['default_qty'];
$cal_param              = $_POST['cal_param'];
$pheight                = $_POST['pheight'];
$plength                = $_POST['plength'];
$pbreadth               = $_POST['pbreadth'];
$vol_weight             = $_POST['vol_weight'];
// END Code changes Gaurav
$sku                    = $_POST['sku'];
$weight                 = $_POST['weight'];
$weight_outside         = $_POST['weight_outside'];
$page_title             = $_POST['page_title'];
$short_description      = mysql_real_escape_string($_POST['short_description']);
$description            = mysql_real_escape_string($_POST['description']);
// price
$delivery_day           = $_POST['delivery_day'];
$wholesale_price        = $_POST['wholesale_price'];
$retail_price           = $_POST['retail_price'];
$tax_class              = $_POST['tax_class'];
//$tax_class = explode ( ",", $_POST ['tax_class'] );
//$tax_class = $tax_class [0];
$retail_with_tax        = $_POST['retail_with_tax'];
$discount               = $_POST['discount'];
$unit_id                = $_POST['unit_id'];
$icon                   = $_POST['icon'];
// SEO
$meta_title             = $_POST['meta_title'];
$meta_keywords          = $_POST['meta_keywords'];
$meta_description       = $_POST['meta_description'];
$date                   = date('Y/m/d');
$product_image          = $_POST['product_image'];
$from_date              = $_POST['from_date'];
$to_date                = $_POST['to_date'];
$featured_product       = $_POST['featured_product'];
$hot_product            = $_POST['hot_product'];
$shop_narrival          = $_POST['shop_narrival'];
$shop_brand             = $_POST['shop_brand'];
$brand             = $_POST['brand'];
//$brand= "'".implode("','",$_REQUEST['brand'])."'";
if ($type == 'seller') {
    $status = 'N';
} else {
    $status = 'Y';
}
$db->insert('catalog_product', array(
    'name' => $name,
    'prodtype' => $productType,
    'ref_code' => $refrenceCode,
    'barcode' => $barcode,
    'sku' => $sku,
    'weight' => $weight,
    'weight_outside' => $weight_outside,
    'cal_param' => $cal_param,
    'plength' => $plength,
    'pheight' => $pheight,
    'pbreadth' => $pbreadth,
    'vol_weight' => $vol_weight,
    'status' => $status,
    'tax_class' => $tax_class,
    'from_date' => $from_date,
    'to_date' => $to_date,
    'featured_product' => $featured_product,
    'special_deal' => '',
    'hot_product' => $hot_product,
    'wholesale_price' => $wholesale_price,
    'retail_price' => $retail_price,
    'retail_price_with_tax' => $retail_with_tax,
    'discount' => $discount,
    'unit_id' => $unit_id,
    'icon' => $icon,
    'meta_title' => $meta_title,
    'meta_keywords' => $meta_keywords,
    'meta_description' => $meta_description,
    'description' => $description,
    'short_description' => $short_description,
    'technspecification' => $technicalSpecification,
    'techndescription' => $techndescription,
    'keyfeature' => $keyFeature,
    'emi' => $emi,
    'keyfeaturedes' => $keyfeaturedes,
    'displaymode' => $displayMode,
    'offermode' => $offerMode,
    'visibility' => $visibility,
    'productcondition' => $productcondition,
    //	'searchtags' => $searchtags,
    'related_product' => '',
    'packInd' => $packInd,
    'friendly_url' => $friendly_url,
    'default_qty' => $default_qty,
    'shop_brand' => $shop_brand,
    'delivery_day' => $delivery_day,
    'added_by' => $_SESSION['master_user_id'],
    'added_type' => $_SESSION['type'],
    'create_date' => $date,
    'brand' => $brand
)) or die(mysql_error());

$res          = $db->getResult();
$se           = "SELECT product_id FROM catalog_product ORDER BY product_id DESC limit 0,1";
$rr           = mysql_query($se);
$my_last_id   = mysql_fetch_array($rr);
$refrenceCode = $refrenceCode + 1;
mysql_query("update volumetric_cal set divid_val=" . $refrenceCode . " where  types='SKU'  ");
foreach (explode(',', $searchtags) as $searchtags_ami) {
    $rr = "INSERT INTO `search_tag` (`tag_name`, `product_id`) VALUES ('" . $searchtags_ami . "', '" . $my_last_id['product_id'] . "')";
    mysql_query($rr);
}
$product_id  = $res[0];
//for association
$category_id = $_POST['category_id'];
for ($i = 0; $i < count($category_id); $i++) {
    $db->insert('product_category_mapping', array(
        'productid' => $product_id,
        'categoryid' => $category_id[$i]
    )) or die(mysql_error());
}
// for association
// for image resizing
/*
 * $imageTransform = new imageTransform;
 *
 * $imageTransform->setQuality(80);
 *
 * $imageTransform->resize($pathpp,600,450,$pathpp);
 */
// for create directory
if (!file_exists("../upload/product/" . $product_id)) {
    mkdir("../upload/product/" . $product_id, 0777, true);
}
$pathp         = "../upload/product/" . $product_id . "/";
$pathpo        = "upload/product/" . $product_id . "/";
$valid_formats = array(
    "jpg",
    "png",
    "JPEG",
    "JPG",
    "bmp"
);
$max_file_size = 1024 * 1024; // 100 kb
$path          = $pathp; // Upload directory
$count         = 0;
if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {
    // Loop $_FILES to execute all files 
    $i = 1;
    foreach (@$_FILES['files']['name'] as $f => $name) {
        if ($_FILES['files']['error'][$f] == 4) {
            continue; // Skip file if any error found
        }
        if ($_FILES['files']['error'][$f] == 0) {
            $allowed  = array(
                'gif',
                'png',
                'jpg'
            );
            $filename = $_FILES["files"]["name"][$f];
            $ext      = pathinfo($filename, PATHINFO_EXTENSION);
            if (in_array($ext, $allowed)) {
                $current_image1 = $_FILES["files"]["name"][$f];
                $extension      = substr(strrchr($current_image1, '.'), 1);
                $time           = date("fYhis");
                $new_image1     = $time . $i . "E" . "." . $extension;
                if (!in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats)) {
                    echo "img is not of valid format";
                    $message[] = "$name is not a valid format";
                    continue; // Skip invalid file formats
                } else { // No error found! Move uploaded files
                    if (move_uploaded_file($_FILES["files"]["tmp_name"][$f], $path . $product_id . "_" . $new_image1)) {
                        $name_pro = explode('.', $name);
                        $db->insert('product_image', array(
                            'productid' => $product_id,
                            'displayname' => $name_pro[0],
                            'filename' => $product_id . "_" . $new_image1
                        ));
                        $db->getResult();
                        $count++; // Number of successfully uploaded files
                    }
                }
            }
        }
        $i++;
    }
}
$product_id;
?>   
<script language="javascript">location.href="catalog_add_product.php?product_id=<?
echo $product_id;
?>"</script> 