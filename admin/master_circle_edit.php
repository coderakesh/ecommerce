<?php include('database.php');

include('functions.php');

include('session.php');
?>

<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
 <head>
 <? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> Master Circle </h1>
        <h4>Add new Circles</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">circles</li>
      </ul>
    </div>
    <div class="row  ">
      <div class="col-md-6">
        <div class="box box-green">
          <div class="box-title">
            <h3><i class="fa fa-bars"></i>Circles </h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
          </div>
          <div class="box-content">
		  <?php 
		  
		   $circle_id = $_REQUEST['circle_id'];
		  
		   $db->select('master_circle','*','',"circle_id=$circle_id",'',''); 
		   $res = $db->getResult(); 
		   $res = $res[0]; 
		   //print_r($res);
		   	?>
            <form  class="form-horizontal" action="master_circle_edit1.php" method="post" enctype="multipart/form-data" id="circle" >
              <div class="row">
                <div class="col-md-12 ">
                  <!-- BEGIN Left Side -->
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Circle Name</label>
                    <div class="col-sm-9 col-lg-10 controls">
					  <input type="hidden" value="<?php echo $circle_id; ?>" name="circle_id" />
                      <input type="text" name="circle_name" id="circle_name" placeholder="Circle Name" class="form-control" value="<?php echo $res['circle_name']; ?>">
                    </div>
                  </div>
             
                </div>
                <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                     <button type="button" class="btn btn-primary" name="change_password" id="bootbox-confirm" > <i class="fa fa-check"></i>Save</button>
                    <button type="button" class="btn">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
	  
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container --> <? include('bottom_link.php'); ?>

</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
