<?php include('database.php');

include('functions.php');

include('session.php');
?>
<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
<head>
<? include('links.php'); ?>
 </head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Promo Code Master</h1>
        <h4>Add new Promo Code</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Promo Code</li>
      </ul>
    </div>
    <div class="row  ">
      <div class="col-md-6">
        <div class="box box-green">
          <div class="box-title">
            <h3><i class="fa fa-bars"></i>Promo Code</h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
          </div>
          <div class="box-content">
            <form  class="form-horizontal" action="master_promo_code1.php" method="post" enctype="multipart/form-data" id="circle" >
              <div class="row">
                <div class="col-md-12 ">
                  <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label">For</label>
                    <div class="col-sm-9 col-lg-10 controls">
                      <label class="radio-inline">
                      <input type="radio" name="code_type" value="rechagre" checked="checked">
                      Recharge </label>
                      <label class="radio-inline">
                      <input type="radio" name="code_type" value="shopping">
                      Shopping</label>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label">Service</label>
                    <div class="col-sm-9 col-lg-10 controls"   style="text-align:left">
                      <?php $db->select('master_service','*',NULL,'status="Y"','service_id DESC');
						  $res = $db->getResult(); ?>
						   <?php foreach($res as $values ){ ?>
						    <label class="control-label" ><input type="radio" value="<?php echo $values['service_id']; ?>" name="service_id" checked="checked"><?php echo $values['service_name']; ?></label>
							<?php } ?>						
					</div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Code Prefix</label>
                    <div class="col-sm-9 col-lg-10 controls">
                      <input type="text" name="code_prefix" id="code_prefix" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Amount</label>
                    <div class="col-sm-9 col-lg-10 controls">
                      <input type="text" name="code_amount" id="" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Minimum<br>
                    (Back)amount</label>
                    <div class="col-sm-9 col-lg-10 controls">
                      <input type="text" name="back_amount" id="back_amount" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Start date</label>
                    <div class="col-sm-9 col-lg-10 controls">
                      <input class="form-control date-picker" id="dp1" size="16" type="text"  name="start_date"  />
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">End date</label>
                    <div class="col-sm-9 col-lg-10 controls">
                      <input class="form-control date-picker" id="dp1" size="16" type="text"  name="end_date" value="" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label">Cashback type</label>
                    <div class="col-sm-9 col-lg-10 controls">
                      <label class="radio-inline">
                      <input type="radio" name="cashback_type" value="wallet" checked="checked">
                      Wallet pay</label>
                      <label class="radio-inline">
                      <input type="radio" name="cashback_type" value="instant" >
                      instant pay </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                    <input type="submit" class="btn btn-primary" name="change_password" value="save" id="bootbox-confirm" />
                    <button type="button" class="btn">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="box">
          <div class="box-title">
            <h3><i class="fa fa-table"></i>Promo Code List</h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a> </div>
          </div>
          <div class="box-content">
            <div class="clearfix"></div>
            <div class="table-responsive" style="border:0">
              <?php $db->select('master_promo_code','*',NULL,'','promo_code_id DESC');

$res = $db->getResult();

//$res = $res[0];
//print_r($res);
 ?>
  <?php $db->select('master_promo_code','*',NULL,'','promo_code_id DESC');

$res = $db->getResult();

//$res = $res[0];
//print_r($res);
 ?> <div id="div_print" style="width:100%; height: 350px; overflow-x: scroll; scrollbar-arrow-color:
blue; scrollbar-face-color: #e7e7e7; scrollbar-3dlight-color: #a0a0a0; scrollbar-darkshadow-color:
#888888; ">              <table class="table table-advance" id="table1">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Service</th>
                    <th class="text-center">Code Prefix</th>
                                        <th class="text-center">Amount</th>

                    <th class="text-center">Minimum
(Back)amount</th>

                    <th class="text-center">Start date</th>
                    <th class="text-center">End date</th>
                    <th class="text-center">Cashback type</th>

                  
                  </tr>
                </thead>
                <tbody>
                  <?php $i=1; foreach($res as $values ){ ?>
                  <tr class="table-flag-blue">
                    <td><?php echo $i; ?></td>
                    <td><?php echo get_service_name($values['service_id']); ?></td>
                    <td><?php echo $values['code_prefix']; ?></td>
                    <td><?php echo $values['code_amount']; ?></td>
                     <td><?php echo $values['back_amount']; ?></td>
                      <td><?php echo $values['start_date']; ?></td>
                       <td><?php echo $values['end_date']; ?></td> 
                       <td><?php echo $values['cashback_type']; ?></td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              </table>
            </div> </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->
<!--basic scripts-->
<script src="../../ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/jquery/jquery-2.1.1.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/jquery-cookie/jquery.cookie.js"></script>
<script type="text/javascript" src="../validation/livevalidation_standalone.compressed.js"></script>
<script>
//for name
var f2 = new LiveValidation('circle_name');
f2.add( Validate.Presence, { failureMessage: "circle Name is Required" } );

</script>
<script type="text/javascript">
  $("#bootbox-confirm").on('click', function () {
	 
	  var valid1 = livevalidatForm();
	  
	   if(valid1==true){
				
	$("#circle").submit();
              
   }     });

</script>
<!--page specific plugin scripts-->
<script type="text/javascript" src="assets/chosen-bootstrap/chosen.jquery.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script type="text/javascript" src="assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="assets/jquery-pwstrength/jquery.pwstrength.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js"></script>
<script type="text/javascript" src="assets/dropzone/downloads/dropzone.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="assets/clockface/js/clockface.js"></script>
<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-switch/static/js/bootstrap-switch.js"></script>
<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/bootstrap3/dataTables.bootstrap.js"></script>
<script src="js/flaty.js"></script>
<script src="js/flaty-demo-codes.js"></script>
</body>
</html>
