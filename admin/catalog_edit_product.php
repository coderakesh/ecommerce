<?php 
include('database.php');

include('functions.php');

include('session.php');

$product_id = $_REQUEST['product_id'];

?>
<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
<head>
<? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> Product </h1>
        <h4>Edit product</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Edit product</li>
      </ul>
    </div>
    <div class="row  ">
      <div class="col-md-12">
        <div class="box box-green">
          <div class="box-title">
            <h3><i class="fa fa-bars"></i>Edit product</h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
          </div>
          <div class="box-content">
            <form  class="form-horizontal" action="catalog_edit_product1.php" method="post" enctype="multipart/form-data" id="operator" >
              <input type="hidden" value="<?php echo $product_id; ?>" name="product_id" />
              <?php  $db->select('catalog_product','*','',"product_id='".$product_id."'",'','');
				   
				   $product = $db->getResult();
				   
				   $product = $product[0];
				   
				   //print_r($product);
				   
				   $checked ='checked="checked"';
				   
				   $selected = 'selected="selected"';
				   
			 ?>
              <div class="row">
                <div class="col-md-12">
                  <div class="tabbable tabs-left" >
                    <ul id="myTab3" class="nav nav-tabs active-red">
                      <li class="active"><a href="#info" data-toggle="tab"> Information</a></li>
                      <li><a href="#price" data-toggle="tab"> Price</a></li>
                      <li><a href="#seo" data-toggle="tab"> SEO</a></li>
                      <li><a href="#associations" data-toggle="tab"> Associations</a></li>
                      <li><a href="#quantity" data-toggle="tab"> Quantity</a></li>
					   <li><a href="#combination" data-toggle="tab">Combinations</a></li>
                      <li><a href="#feature" data-toggle="tab">Features</a></li>
                      <li><a href="#image" data-toggle="tab"> Images</a></li>
                      <li><a href="#supplier" data-toggle="tab"> Supplier</a></li>
                    </ul>
                    <div id="myTabContent3" class="tab-content" style="height:450px;">
                      <div class="tab-pane fade in active" id="info">
                        <div class="row">
                          <div class="col-md-10 ">
                           
						    <div class="form-group">
                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Shop By Brands</label>
                              <div class="col-sm-9 col-lg-5 controls">
                                <input type="radio" value="Yes"  <?php if($product['shop_brand']=='Yes') echo $checked; ?> name="shop_brand" id="textfield1">
                                &nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" value="No" name="shop_brand" id="textfield1" <?php if($product['shop_brand']=='No') echo $checked; ?> >
                                &nbsp;&nbsp;No </div>
                            </div>
							 <div class="form-group">
                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label">New Arrivals</label>
                              <div class="col-sm-9 col-lg-5 controls">
                                <input type="radio" value="Yes" <?php if($product['shop_narrival']=='Yes') echo $checked; ?> name="shop_narrival" id="textfield1">
                                &nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" value="No" name="shop_narrival" id="textfield1"  <?php if($product['shop_narrival']=='No') echo $checked; ?>>
                                &nbsp;&nbsp;No </div>
                            </div>
						   
						   
						    <div class="form-group">
                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Featured product</label>
                              <div class="col-sm-9 col-lg-5 controls">
                                <input type="radio" value="yes" name="featured_product" id="textfield1" <?php if($product['featured_product']=='yes') echo $checked; ?> >
                                &nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" value="no" name="featured_product" id="textfield1" <?php if($product['featured_product']=='no') echo $checked; ?> >
                                &nbsp;&nbsp;No </div>
                            </div>
                            <div class="form-group">
                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Hot product</label>
                              <div class="col-sm-9 col-lg-5 controls">
                                <input type="radio" value="yes"  name="hot_product" id="textfield1" <?php if($product['hot_product']=='yes') echo $checked; ?>>
                                &nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" value="no" name="hot_product" id="textfield1" <?php if($product['hot_product']=='no') echo $checked; ?> >
                                &nbsp;&nbsp;No </div>
                            </div>
                            <div class="form-group">
                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Product name</label>
                              <div class="col-sm-9 col-lg-5 controls">
                                <input type="text" name="product_name"  value="<?php echo $product['name']; ?>" id="textfield1" class="form-control">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Short Description</label>
                              <div class="col-sm-9 col-lg-5 controls">
                                <textarea name="short_description" id="textarea2" rows="5" class="form-control"><?php echo $product['short_description']; ?></textarea>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Description</label>
                              <div class="col-sm-9 col-lg-5 controls">
                                <textarea name="description" id="textarea2" rows="5" class="form-control"><?php echo $product['description']; ?></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="price">
                        <div class="col-md-10 ">
						
						 <div class="form-group">
                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Pre-tax wholesale price</label>
                            <div class="col-sm-9 col-lg-5 controls">
                              <input type="text" name="wholesale_price" id="textfield1" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Pre-tax retail price</label>
                            <div class="col-sm-9 col-lg-5 controls">
                              <input type="text" name="retail_price" id="retail_price" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 col-lg-3 control-label">Tax rule</label>
                            <div class="col-sm-9 col-lg-5 controls">
                              <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="tax_class" id="tax_id">
                                <option value="">Select</option>
                                <?php $db->select('localization_tax','*',NULL,'','tax_id DESC');
	 							 $res = $db->getResult();
								 foreach($res as $feature){
								  ?>
                                <option value="<?php echo $feature['tax_id'].",".$feature['tax_value']; ?>"><?php echo $feature['tax_name']; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Retail price with tax</label>
                            <div class="col-sm-9 col-lg-5 controls">
                              <input type="text" name="retail_with_tax" id="retail_with_tax" class="form-control">
                            </div>
                          </div>
                          
                        </div>
                      </div>
                      <div class="tab-pane fade" id="seo">
                        <div class="col-md-10 ">
                          <div class="form-group">
                            <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Meta title</label>
                            <div class="col-sm-9 col-lg-5 controls">
                              <input type="text" name="meta_title" id="textfield1" class="form-control" value="<?php echo $product['meta_title']; ?>">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Meta keywords</label>
                            <div class="col-sm-9 col-lg-5 controls">
                              <textarea name="meta_keywords" id="textarea2" rows="5" class="form-control"><?php echo $product['meta_keywords']; ?></textarea>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Meta description</label>
                            <div class="col-sm-9 col-lg-5 controls">
                              <textarea name="meta_description" id="textarea2" rows="5" class="form-control"><?php echo $product['meta_description']; ?></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="associations">
                         <div class="col-sm-9 col-lg-2 controls"></div>
						 <div class="col-sm-9 col-lg-9 controls"> <div id="sidetree">
                          <div class="treeheader">&nbsp;</div>
                          <div id="sidetreecontrol"><a href="?#" class="btn btn-primary">Collapse All</a>&nbsp;<a href="?#" class="btn btn-primary">Expand All</a></div>
                          <ul id="tree">
                            <li><a href="?/index.cfm">
                              <input type="checkbox" value="" name="category_id[]">
                              &nbsp;<strong>Home</strong></a>
                              <?php  $cat_checked = array();
							 $db->select('product_category_mapping','*',NULL,"productid='".$product_id."'",'productid DESC');
							 $proCat = $db->getResult();
 							foreach($proCat as $proCats ){ $cat_checked [] = $proCats['categoryid']; }  
							 $qry="SELECT * FROM catalog_category";
 									$result=mysql_query($qry);
 									$arrayCategories = array();
 while($row = mysql_fetch_assoc($result)){ 
 $arrayCategories[$row['category_id']] = array("parent_id" => $row['parent_id'], "name" =>$row['name'],"category_id" =>$row['category_id']);   
  									}  //print_r($arrayCategories); print_r($cat_checked);
									?>
                              <?php if(mysql_num_rows($result)!=0){ createTree_checked($arrayCategories,0,'','',$cat_checked); }	?>
                            </li>
                          </ul>
                        </div> </div>
                      </div>
                      <div class="tab-pane fade" id="quantity">
                        <div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Stock unit</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" name="sku" id="textfield1" class="form-control" value="<?php echo $product['sku']; ?>">
                          </div>
                        </div>
                        <div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Weight</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" name="weight" id="textfield1" class="form-control" value="<?php echo $product['weight']; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">From date </label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <input class="form-control date-picker" id="dp1" size="16" type="text"  name="from_date" value="<?php echo $product['from_date']; ?>" />
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">To date</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <input class="form-control date-picker" id="dp1" size="16" type="text" name="to_date" value="<?php echo $product['to_date']; ?>"/>
                            (product show new between these days) </div>
                        </div>
                      </div>
					  <div class="tab-pane fade" id="combination">
                        <div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Select attribute</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="attribute_id" id="attribute">
                                <option value="">Select</option>
                               <?php $db->select('catalog_product_attribute','*',NULL,'','attribute_id DESC');
	 							 $res = $db->getResult();
								 foreach($res as $feature){ ?>
								<option value="<?php echo $feature['attribute_id']; ?>"><?php echo $feature['aname']; ?></option>
                                <?php } ?>
                              </select>
                          </div>
                        </div>
                        <div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Attribute value</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="attribute_id" id="attribute_value">
                                <option value="">Select</option>
                              </select>               
                          </div>    <button type="button" class="btn btn-primary" id="add_attribute"  style="float:left; margin-left:20px"><i class="fa fa-check"></i> Add feature</button>

                        </div>
                              
						<div class="form-group">
                <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                 </div>
              </div>
         
              <div id="product_attribute_data"></div>
                      </div>
                      <div class="tab-pane fade" id="feature">
					 
                        <div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Select feature</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="attribute_id" id="feature1">
                                <option value="">Select</option>
                               <?php $db->select('catalog_product_features','*',NULL,'','feature_id DESC');
	 							 $res = $db->getResult();
								 foreach($res as $feature){ ?>
								<option value="<?php echo $feature['feature_id']; ?>"><?php echo $feature['feature_name']; ?></option>
                                <?php } ?>
                              </select>
                          </div>
                        </div>
                        <div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Feature value</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="attribute_id" id="feature_value">
                                <option value="">Select</option>
                              </select>
                          </div>
                        </div>
						
						<div class="form-group">
                <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                  <button type="button" class="btn btn-primary" id="add_feature"><i class="fa fa-check"></i> Add feature</button>
                  <button type="button" class="btn">Cancel</button> 
                </div>
              </div><div id="product_feature_data"></div>
						
                      </div>
                      <div class="tab-pane fade" id="image">
                        <input type='button' value='Add Image' id='addButton' class="btn btn-lime">
                        <input type='button' value='Remove Image' id='removeButton' class="btn btn-danger">
                        <br>
                        <br>
                        <div id='TextBoxesGroup'>
                          <div class="col-sm-9 col-lg-10 controls">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                              <div class="input-group">
                                <div class="form-control uneditable-input"> <i class="fa fa-file fileupload-exists"></i> <span class="fileupload-preview"></span> </div>
                                <div class="input-group-btn"> <a class="btn bun-default btn-file"> <span class="fileupload-new">Select file</span> <span class="fileupload-exists">Change</span>
                                  <input type="file" name="files[]" class="file-input">
                                  </a> <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a> </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-10">
                          <div class="panel panel-primary">
                            <div class="panel-heading">
                              <h4 class="panel-title">Old Images</h4>
                            </div>
                            <div class="panel-body">
                              <div class="col-md-12">
                                <div class="box">
                                  <div class="box-content">
                                    <table class="table">
                                      <thead>
                                        <tr>
                                          <th>S.No</th>
                                          <th>Image</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php $images = get_product_images($product_id); //print_r($images); ?>
                                        <?php for($i=0;$i<count($images);$i++){ ?>
                                        <tr>
                                          <td><?php echo $i+1; ?></td>
                                          <td><img src="<?php echo "../".$images[$i]['path']; ?>" width="50px" height="50px;" /></td>
                                          <td><input type='button' value='Remove Image'  class="btn btn-danger"></td>
                                        </tr>
                                        <?php } ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="supplier">
                        <div class="row">
                          <div class="col-md-10 ">
                            <div class="form-group">
                              <label class="col-sm-3 col-lg-2 control-label">Select Manufacturer</label>
                              <div class="col-sm-9 col-lg-5 controls">
                                <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="manufacturer">
                                  <option value="">Select...</option>
                                  <option value="Category 1">Category 1</option>
                                  <option value="Category 2" <?php if($product['manufacturer']=='Category 2') echo $selected; ?>>Category 2</option>
                                  <option value="Category 3">Category 5</option>
                                  <option value="Category 4">Category 4</option>
                                </select>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-3 col-lg-2 control-label">Select color</label>
                              <div class="col-sm-9 col-lg-5 controls">
                                <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="color">
                                  <option value="">Select...</option>
                                  <option value="Category 1" >Category 1</option>
                                  <option value="Category 2" <?php if($product['color']=='Category 2') echo $selected; ?>>Category 2</option>
                                  <option value="Category 3">Category 5</option>
                                  <option value="Category 4">Category 4</option>
                                </select>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-3 col-lg-2 control-label">Select brand</label>
                              <div class="col-sm-9 col-lg-5 controls">
                                <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="brand">
                                  <option value="">Select...</option>
                                  <option value="Category 1" <?php if($product['brand']=='Category 1') echo $selected; ?>>Category 1</option>
                                  <option value="Category 2">Category 2</option>
                                  <option value="Category 3">Category 5</option>
                                  <option value="Category 4">Category 4</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> update</button>
                  <button type="button" class="btn">Cancel</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->
<!--basic scripts-->
<script src="../../ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/jquery/jquery-2.1.1.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/jquery-cookie/jquery.cookie.js"></script>
<!-------------------- for search----------------->
<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript">
$(function() {
            $("#state").autocomplete({
                source: "get_catalog_category.php",
                minLength: 2,
                select: function(event, ui) {
                  
                    $('#abbrev').val(ui.item.abbrev);
					 $('#state_abbrev').val(ui.item.country_id);
                }
            });
            
        });
</script>
<!-------------------- for search----------------->
<!------------------ add tree ---------------------->
<script src="tree/jquery.treeview.js" type="text/javascript"></script>
<script type="text/javascript">
		$(function() {
			$("#tree").treeview({
				collapsed: true,
				animated: "medium",
				control:"#sidetreecontrol",
				persist: "location"
			});
		})

	</script>
<!------------------ add tree ---------------------->
<!--------------------- image upload -------------->
<script type="text/javascript">
 
$(document).ready(function(){
 
    var counter = 2;
 
    $("#addButton").click(function () {
 
	if(counter>5){
            alert("Only 5 images allow");
            return false;
	}   
 
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);
 
	newTextBoxDiv.after().html('<div class="col-sm-9 col-lg-10 controls"><div class="fileupload fileupload-new" data-provides="fileupload"><div class="input-group"><div class="form-control uneditable-input"><i class="fa fa-file fileupload-exists"></i><span class="fileupload-preview"></span></div><div class="input-group-btn"><a class="btn bun-default btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="files[]" class="file-input"></a><a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a></div></div></div></div>');
 
	newTextBoxDiv.appendTo("#TextBoxesGroup");
 
 
	counter++;
     });
 
     $("#removeButton").click(function () {
	if(counter==1){
          alert("No more textbox to remove");
          return false;
       }   
 
	counter--;
 
        $("#TextBoxDiv" + counter).remove();
 
     });
 
     $("#getButtonValue").click(function () {
 
	var msg = '';
	for(i=1; i<counter; i++){
   	  msg += "\n Textbox #" + i + " : " + $('#textbox' + i).val();
	}
    	  alert(msg);
     });
  });
</script>
<!--------------------- image upload -------------->
<!------------------------ see image priview before upload --------------->
<script type="text/javascript">
$(function(){
    Test = {
        UpdatePreview: function(obj){
          // if IE < 10 doesn't support FileReader
          if(!window.FileReader){
             // don't know how to proceed to assign src to image tag
          } else {
             var reader = new FileReader();
             var target = null;
             
             reader.onload = function(e) {
              target =  e.target || e.srcElement;
               $("#myimage").prop("src", target.result);
             };
              reader.readAsDataURL(obj.files[0]);
          }
        }
    };
});
</script>
<!------------------------ see image priview before upload --------------->
<!-------------------- for dependent drop down -------------->
<script type="text/javascript">
$(document).ready(function()
{
$("#attribute").change(function()
{
var attribute_id = $(this).val();
var dataString = 'attribute_id='+attribute_id+'&type=attribute';

$.ajax
({
type: "POST",
url: "get_af_value.php",
data: dataString,
cache: false,
success: function(html){

$("#attribute_value").html(html);

} 
});

});

//for feature

$("#feature1").change(function()
{
var feature_id = $(this).val();
//alert(feature_id);
var dataString = 'feature_id='+feature_id+'&type=feature';
//alert(dataString);
$.ajax
({
type: "POST",
url: "get_af_value.php",
data: dataString,
cache: false,
success: function(html){

$("#feature_value").html(html);
//alert(html);
} 
});

});


});
</script>
<!-------------------- for dependent drop down -------------->
<!------------------ add attribute --------------------->
<script type="text/javascript">
$(document).ready(function(){

$("#add_attribute").click(function(){

var attribute_id = $("#attribute").val();

var attribute_value_id = $("#attribute_value").val();

var dataString = 'product_id='+<?php echo $product_id; ?>+'&af_value_id='+attribute_value_id+'&attribute_id='+attribute_id+'&type=add_attribute';

//alert(dataString);
$.ajax
({
type: "POST",
url: "get_af_value.php",
data: dataString,
cache: false,
success: function(html){

$("#product_attribute_data").load('get_af_value.php?product_attribute_data=a&product_id=<?php echo $product_id;  ?>');


} 
});


});



// for feature

$("#add_feature").click(function(){

var feature_id = $("#feature1").val();

var feature_value_id = $("#feature_value").val();

var dataString = 'product_id='+<?php echo $product_id; ?>+'&af_value_id='+feature_value_id+'&feature_id='+feature_id+'&type=add_feature';

//alert(dataString);
$.ajax
({
type: "POST",
url: "get_af_value.php",
data: dataString,
cache: false,
success: function(html){

$("#product_feature_data").load('get_af_value.php?product_feature_data=a&product_id=<?php echo $product_id;  ?>');


} 
});


});



});

$(document).ready(function(){

$("#product_attribute_data").load('get_af_value.php?product_attribute_data=a&product_id=<?php echo $product_id;  ?>');
$("#product_feature_data").load('get_af_value.php?product_feature_data=a&product_id=<?php echo $product_id;  ?>');
 });
 
 //for remove attribute
function remove_attribute(id,afid,afvid)
 {
var dataString = 'product_id='+<?php echo $product_id; ?>+'&af_value_id='+afvid+'&attribute_id='+afid+'&type=remove_attribute';
$.ajax
({
type: "POST",
url: "get_af_value.php",
data: dataString,
cache: false,
success: function(html){
$("#product_attribute_data").load('get_af_value.php?product_attribute_data=a&product_id=<?php echo $product_id;  ?>');


} 
});
 }
 
 //for remove feature
function remove_feature(id,afid,afvid)
 {
var dataString = 'product_id='+<?php echo $product_id; ?>+'&af_value_id='+afvid+'&attribute_id='+afid+'&type=remove_feature';
$.ajax
({
type: "POST",
url: "get_af_value.php",
data: dataString,
cache: false,
success: function(html){
$("#product_feature_data").load('get_af_value.php?product_feature_data=a&product_id=<?php echo $product_id;  ?>');

} 
});
 }
</script>
<!------------------ add attribute --------------------->
<!--page specific plugin scripts-->
<script type="text/javascript" src="assets/chosen-bootstrap/chosen.jquery.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script type="text/javascript" src="assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="assets/jquery-pwstrength/jquery.pwstrength.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js"></script>
<script type="text/javascript" src="assets/dropzone/downloads/dropzone.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="assets/clockface/js/clockface.js"></script>
<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-switch/static/js/bootstrap-switch.js"></script>
<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/bootstrap3/dataTables.bootstrap.js"></script>
<!--flaty scripts-->
<script src="js/flaty.js"></script>
<script src="js/flaty-demo-codes.js"></script>
</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
