<?php 
include('database.php');
include('functions.php');
include('session.php');
?>
<!DOCTYPE html>
<html>
 <? include('links.php'); ?>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>
<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> Multi Product Upload</h1>
        <h4>Multi Product Upload</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Multi Product Upload</li>
      </ul>
    </div>
    <div class="row  ">
      <div class="col-md-12">
        <div class="box box-green">
          <div class="box-title">
            <h3><i class="fa fa-bars"></i>Multi Product Upload</h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
          </div>
          
          <div class="box-content">
          
            <form  class="form-horizontal" action="multi_product_upload1.php" method="post" enctype="multipart/form-data" id="operator" >
              <div class="row">
              <div class="col-sm-4"><a href="multi_product_upload.csv" download class='btn btn-danger'>Download Sample File</a>
              <br><p style="font-weight:800;color:red"><span style="font-weight:800;color:red;font-size:16px;">Note:</span> Attributes Must Be added after seller column in CSV file</p>
              </div>
                <div class="col-md-8 ">
                  <div class="form-group">
                    <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Select CSV File</label>
                    <div class="col-sm-9 col-lg-5 controls">
                    <input type="file" name="uploadFile" class="form-control" required>
                    </div>
                  </div>                
                  <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                      <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                      <button type="button" class="btn">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>

  <!-- END Content -->

</div>

<!-- END Container -->

<!--basic scripts-->

<script src="../../ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="assets/jquery/jquery-2.1.1.min.js"><\/script>')</script>

<script src="assets/bootstrap/js/bootstrap.min.js"></script>

<script src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<script src="assets/jquery-cookie/jquery.cookie.js"></script>

<!-------------------- for search----------------->

<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>

<script type="text/javascript">

$(function() {

            $("#state").autocomplete({

                source: "get_catalog_category.php",

                minLength: 2,

                select: function(event, ui) {

                  

                    $('#abbrev').val(ui.item.abbrev);

					 $('#state_abbrev').val(ui.item.country_id);

                }

            });

            

        });

</script>

<!-------------------- for search----------------->

<!--page specific plugin scripts-->

<script type="text/javascript" src="assets/chosen-bootstrap/chosen.jquery.min.js"></script>

<script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>

<script type="text/javascript" src="assets/jquery-tags-input/jquery.tagsinput.min.js"></script>

<script type="text/javascript" src="assets/jquery-pwstrength/jquery.pwstrength.min.js"></script>

<script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>

<script type="text/javascript" src="assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js"></script>

<script type="text/javascript" src="assets/dropzone/downloads/dropzone.min.js"></script>

<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<script type="text/javascript" src="assets/clockface/js/clockface.js"></script>

<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>

<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript" src="assets/bootstrap-switch/static/js/bootstrap-switch.js"></script>

<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>

<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>

<script type="text/javascript" src="assets/data-tables/bootstrap3/dataTables.bootstrap.js"></script>

<!--flaty scripts-->

<script src="js/flaty.js"></script>

<script src="js/flaty-demo-codes.js"></script>

</body>



</html>

