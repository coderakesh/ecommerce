<?php 

error_reporting(0);

include('database.php');

include('functions.php');

include('session.php');

?>

<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
 <head>
 <? include('links.php'); ?>
</head>
 
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Supplier</h1>
        <h4>Supplier list</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Supplier list</li>
      </ul>
    </div>
    <div class="row  ">
      
	  <div class="col-md-12">
<div class="box">
<div class="box-title">
<h3><i class="fa fa-table"></i>Supplier List</h3>
<div class="box-tool">
<a class="btn btn-success" href="catalog_product_add_supplier.php"><i class="fa fa-plus"></i> Add new</a>
<a class="btn btn-danger" href="#"><i class="fa fa-times"></i> Delete</a></div>
</div>
<div class="box-content">
<div class="clearfix"></div>
<div class="table-responsive" style="border:0">

<?php $db->select('catalog_product_supplier','*',NULL,'','supplier_id DESC');

$res = $db->getResult();

//$res = $res[0];
//print_r($res);
 ?>

<table class="table table-advance" id="table1">
<thead>
<tr>
<th>S. No</th>
<th>Supplier Name</th>
<th>Supplier Description</th>
<th>Address</th>
<th>Phone</th>
<th>Mobile</th>
<th>City</th>
<th>State</th>
<th>Logo</th>
<th>Meta Title</th>
<th>Meta Keyword</th>
<th>Meta Description</th>
<th>Added Date</th>
<th>Status</th>
</tr>
</thead>
<tbody>
<?php $i=1; foreach($res as $values ){ ?>
<tr class="table-flag-blue">
<td><?php echo $i; ?></td>
<td><?php echo $values['sname']; ?></td>
<td><?php echo $values['sdescription']; ?></td>
<td><?php echo $values['saddress']; ?></td>
<td><?php echo $values['sphone']; ?></td>
<td><?php echo $values['smobile']; ?></td>
<td><?php echo $values['scity']; ?></td>
<td><?php echo $values['sstate']; ?></td>
<td><?php echo $values['slogo']; ?></td>
<td><?php echo $values['smeta_title']; ?></td>
<td><?php echo $values['smeta_keywords']; ?></td>
<td><?php echo $values['smeta_description']; ?></td>
<td><?php echo $values['added_date']; ?></td>
<td><?php echo $values['status']; ?></td>
</tr>
<?php $i++; } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->
 <? include('bottom_link.php'); ?>

</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
