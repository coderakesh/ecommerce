<?php 

error_reporting(0);
header('Access-Control-Allow-Origin: *');
include('database.php');

include('functions.php');

include('session.php');
 
 ?>
 <!DOCTYPE html>
<html>
  <head>
  <? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Order's</h1>
       </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index.php">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Order list</li>
      </ul>
    </div>
    <div class="row  ">
      
	  <div class="col-md-12">
<div class="box">
<div class="box-title">
<h3><i class="fa fa-table"></i>Pickup Request</h3><span style="color:#FFFFFF">  (There show only delivery transit item)</span>
<div class="box-tool">
<!--a class="btn btn-success" href="catalog_add_product.php"><i class="fa fa-plus"></i> Add new</a-->
 </div>
</div>
   <form action="create_pickup.php" method="post"  >
  <div class="col-md-6 ">
  <!-- BEGIN Left Side -->
  <input type="hidden"  value="<? echo $p_location.'&nbsp; ,'.$o_pin; ?>" id="plocation"  name="plocation">
<div class="form-group">
<label for="password1" class="control-label col-lg-3 col-xs-3"> Pickup Date </label>
<div class="col-sm-9 col-lg-9 controls">
<input type="text" id="pdate" name="pdate" value="<?php  if(@$_REQUEST['sdate']!=''){ echo $_REQUEST['sdate']; } else { echo date("Y-m-d"); }?>" class="form-control date-picker">
 </div>
</div> <div class="form-group">
<label for="password1" class="control-label col-lg-3 col-xs-3"> Pickup Address </label>
<div class="col-sm-9 col-lg-9 controls">
<select class="form-control" name="seller_pickup_address">
 <?  	$db->select('default_seller_adr','*',NULL,"1=1",'');
 $default = $db->getResult(); //print_r($cover_img);
  $default =  $default[0]; 
 if($_SESSION['type']=='seller'){
  	$db->select('seller_pickup_address','*',NULL,"seller_id='".$_SESSION['seller_id']."'",'');
 $o_pincod = $db->getResult(); $q=1;
foreach($o_pincod as $o_pincode){ $o_pin =   $o_pincode['pincode'];
 $p_location = $o_pincode['person_name']; ?>
<option value="<? echo $o_pincode['company-name']; ?>"><? echo  $o_pin.' / '.$p_location; ?></option>
<? $q++;} } else{
$o_pin =   $default['pincode'];
$p_location = $default['company_name']; ?>
<option value="<? echo $p_location; ?>"><? echo  $o_pin.' / '.$p_location ; ?></option>
<? } ?>

 </select>
 </div>
</div>
  

  <!-- END Left Side -->
   </div>
   <div class="col-md-6 ">
  <div class="form-group">
                                        <label class="control-label col-md-6">Pickup Time</label>
                                        <div class="col-md-6">
                                            <div class="input-group bootstrap-timepicker">
 <input type="text" id="ptime" class="form-control timepicker-24" name="ptime" requirded >
                                                <span class="input-group-btn">
                                                <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
  <div class="form-group">
<label for="password1" class="control-label col-lg-3 col-xs-3">   </label>
<div class="col-sm-9 col-lg-9 controls">
 <input name="submit" type="submit" class="btn btn-primary pick_up" value="Send Request" />
</div>
</div>
  <!-- END Right Side -->
   </div>
  
<div class="box-content">

 <br><div class="clearfix"></div>
 <div class="table-responsive" style="border:0">
<table class="table table-advance" id="example">
<thead>
<tr>
<th style="width:18px"></th>
<th>Order id</th>
<th>Waybill Id</th>
<th>Product</th>

<th>Pincode</th>
<th>City</th>
<th>Customer</th>
</tr>
</thead>
<tfoot>
<tr>
<th style="width:18px"></th>
<th></th>
<th></th>
<th></th>

<th></th>
<th></th>
<th></th>
</tr>
</tfoot>
<tbody>
<? 
if($_SESSION['type']=='seller'){
 $qu = "SELECT * FROM orders INNER JOIN orderdetail ON orders.oid=orderdetail.oid WHERE orderdetail.seller_id='".$_SESSION['master_user_id']."'  AND sell_type='seller' and   waybill_id != 0     group by   orderdetail.waybill_id   ORDER BY orders.oid DESC";   } else {
	  	  $qu = "SELECT * FROM orders INNER JOIN orderdetail ON orders.oid=orderdetail.oid WHERE   orderdetail.waybill_id !=0       group by   orderdetail.waybill_id  ORDER BY orders.oid DESC";
		  }  
 		  $qry = mysql_query($qu); $i=1;
 while($row = mysql_fetch_array($qry)){
 	$db->select('waybill_master','*',NULL,"id='".$row['waybill_id']."'",'');
 $way_bill = $db->getResult(); //print_r($cover_img);
 $way_bill = $way_bill[0];  
 if( $way_bill['pickup_id']=='0' || $way_bill['pickup_id']=='' ){

  	$db->select('profile_address','*',NULL,"pcaddid='".$row['addressid']."'",'');
 $address = $db->getResult(); //print_r($cover_img);
 $address = $address[0];
?>
<tr class="table-flag-blue">
<td><input type="checkbox" class="checkbox" id="opt<? echo $i; ?>" name="ord[]" value="<? echo $row['odid']; ?>"/>
<input type="hidden" value="<? echo $row['waybill_id']; ?>" name="waybill" >
</td>
<td><? echo $row['order_num']; ?></td>
<td><? echo getwaybill($row['waybill_id']); ?></td>	
<td><ul class="tooltip_marg"><?  	$db->select('orderdetail','pid',NULL,"waybill_id='".$row['waybill_id']."'",'');
 $ord_pro = $db->getResult(); 
 foreach($ord_pro as $ord_pros){ 
  
 	$db->select('catalog_product','coverphoto,weight,vol_weight',NULL,"product_id='".$ord_pros['pid']."'",'');
 $product = $db->getResult(); //print_r($cover_img);
 $product = $product[0];
 $weight = $row['qty']*$product['weight'];
  $vol_weight = $row['qty']*$product['vol_weight'];
 	$db->select('catalog_product','coverphoto,weight,vol_weight',NULL,"product_id='".$ord_pros['pid']."'",'');
 $product = $db->getResult(); //print_r($cover_img);
 $product = $product[0];?>
 

        <li>
          <div class="tool-tip" style="margin-bottom:10px;">
            <i class="tool-tip__icon"><img class="em-product-main-img" src="../<? echo return_main_img($product['coverphoto'],$ord_pros['pid']); ?>" alt='' title="" width="30" height="30" /></i>
            <p class="tool-tip__info">
              <span class="info"><span class="info__title">Quantity:</span>&nbsp;<? echo $row['qty']; ?></span>
              <span><span class="info__title">Price:</span> &nbsp;<? echo $row['price']; ?></span>
   <span class="info"><span class="info__title">Weight(grams):</span>&nbsp; <? echo $weight; ?></span>
              <span><span class="info__title">Volumetric Weight(grams):</span>&nbsp; <? echo $vol_weight; ?></span>
            </p>
          </div>
                    <a href="" title=""><? echo get_product_name($ord_pros['pid']); ?></a>

        </li>
  
<? } ?> </ul></td>

<td> <? echo  $address['pincode']; ?></td>
<td> <? echo getcity($address['city']); ?></td>
<td> <? echo get_user_fulname($row['uid']); ?></td>
</tr>
 <? $i++; } } ?>
 </tbody>
</table>
</div>
</div>
</form>
</div></div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container --> 
 <? include('bottom_link.php'); ?>

</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>

