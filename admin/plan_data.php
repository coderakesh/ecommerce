<link rel="stylesheet" type="text/css" href="validation/livevalid.css" />
<script type="text/javascript" src="validation/livevalidation_standalone.compressed.js"></script>	
<?php 

$service_id = $_POST['service_id'];

if($service_id=='1'){ ?>

<div class="form-group">
  <label class="col-sm-3 col-lg-2 control-label">Select Plan Type</label>
  <div class="col-sm-9 col-lg-10 controls">
    <label class="radio-inline">
    <input type="radio" name="plan_type"  id="full_talktime" value="full_talktime" checked="checked">
    Full Talk Time </label>
    <label class="radio-inline">
    <input type="radio" name="plan_type"  id="special_recharge" value="special_recharge" >
    Special Rechagre </label>
    <label class="radio-inline">
    <input type="radio" name="plan_type"  id="2g_data" value="2g_data">
    2G Data </label>
    <label class="radio-inline">
    <input type="radio" name="plan_type"  id="3g_data" value="3g_data">
    3G Data </label>
    <label class="radio-inline">
    <input type="radio" name="plan_type"  id="roaming" value="roaming">
    Roaming </label>
    <label class="radio-inline">
    <input type="radio" name="plan_type"  id="topup" value="topup">
    Top Up</label>
  </div>
</div>
<div class="form-group " id="validity">
  <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Validity</label>
  <div class="col-sm-9 col-lg-10 controls">
    <input type="text" name="validity" id="validity1" class="form-control ">
  </div>
</div>
<div class="form-group show_hide" id="benifits">
  <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Benifits</label>
  <div class="col-sm-9 col-lg-10 controls">
    <input type="text" name="benifits" id="benifits1"  class="form-control">
  </div>
</div>
<div class="form-group " id="talk_time">
  <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Talk Time</label>
  <div class="col-sm-9 col-lg-10 controls">
    <input type="text" name="talk_time" id="talk_time1"  class="form-control">
  </div>
</div>
<div class="form-group " id="description">
  <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Description</label>
  <div class="col-sm-9 col-lg-10 controls">
    <textarea class="form-control" id="description1" name="description" rows="3"></textarea>
  </div>
</div>
<div class="form-group " id="price">
  <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Price</label>
  <div class="col-sm-9 col-lg-10 controls">
    <input type="text" name="price" id="price4"   class="form-control">
  </div>
</div>
<script type="text/javascript">

var validity1 = new LiveValidation('validity1'); 
				validity1.add( Validate.Presence, { failureMessage: "validity is Required" });
 
/*var benifits1 = new LiveValidation('benifits1'); 
				benifits1.add( Validate.Presence, { failureMessage: "benifits is Required" });
 
 var talk_time1 = new LiveValidation('talk_time1'); 
				talk_time1.add( Validate.Presence, { failureMessage: "Talk Time is Required" });*/
				
var description1 = new LiveValidation('description1'); 
				description1.add( Validate.Presence, { failureMessage: "description1 is Required" });

var price1 = new LiveValidation('price4'); 
price1.add( Validate.Presence, { failureMessage: "Price is Required" } );
</script>

<?php }elseif($service_id=='2'){
?>
<div class="form-group">
  <label class="col-sm-3 col-lg-2 control-label">Select Plan Type</label>
  <div class="col-sm-9 col-lg-10 controls">
    <label class="radio-inline">
    <input type="radio" name="plan_type"  value="6_month_pack" checked="checked"> 6 Month Packs</label>
    <label class="radio-inline">
    <input type="radio" name="plan_type" value="annual_pack" > Annual Packs</label>
    <label class="radio-inline">
    <input type="radio" name="plan_type" value="monthly_pack">Monthly Pack</label>
    <label class="radio-inline">
    <input type="radio" name="plan_type" value="3_month_pack">3 Month Packs</label>
  </div>
</div>
<div class="form-group " id="validity">
  <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Validity</label>
  <div class="col-sm-9 col-lg-10 controls">
    <input type="text" name="validity"  id="validity22" class="form-control ">
  </div>
</div>
<div class="form-group " id="description">
  <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Description</label>
  <div class="col-sm-9 col-lg-10 controls">
    <textarea class="form-control"  id="description22" name="description" rows="3"></textarea>
  </div>
</div>
<div class="form-group" id="price">
  <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Price</label>
  <div class="col-sm-9 col-lg-10 controls">
    <input type="text" name="price"  id="price22" class="form-control">
  </div>
</div>
</div>
<script type="text/javascript">

var validity1 = new LiveValidation('validity22'); 
				validity1.add( Validate.Presence, { failureMessage: "validity is Required" });
 
var description1 = new LiveValidation('description22'); 
				description1.add( Validate.Presence, { failureMessage: "description1 is Required" });

var price1 = new LiveValidation('price22'); 
price1.add( Validate.Presence, { failureMessage: "Price is Required" } );
</script>


<?php 
}


?>


