<?php @session_start(); // start the session , @ for the error supression
 
include("database.php"); 
include("functions.php"); 
$product_id = $_REQUEST['product_id'];
	$db->select('catalog_product','*',NULL,"product_id='".$product_id."'",'');
 $product = $db->getResult(); //print_r($cover_img);
 $product = $product[0];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
 <head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<? include('links.php'); ?>
   </head>
	<body>
	
	<div id="main-content"> 

<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
 
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->


 <div class="alert alert-success hide"  id="success" >
<button class="close" data-dismiss="alert">&times;</button>
<strong>Success!</strong> .</div>  
<div class="row">
<div class="col-md-12 col-lg-12">
<div class="box box-magenta">
<div class="box-title">
<h3><i class="fa fa-bars"></i>Product Details</h3>
 
</div>
 <div class="box-content">
<table class="table table-striped">
<tbody>
<tr>
<th>Name</th>
<td><? echo $product['name'];  ?></td>
<th>Reference Code</th>
<td><?   echo $product['ref_code'];	 ?></td>
 </tr> 
<tr>
<th>Category Name</th>
<td><?   $db->select('product_category_mapping','categoryid',NULL,"productid='".$product['product_id']."'  ",'');
 $cat = $db->getResult();  foreach($cat as $cats){ echo get_category_name($cats['categoryid']).','; }
   ?></td>
<th>Display Mode</th>
<td><?    if($product['displaymode']=='Y'){ echo  'On'; } else { echo  'Off'; } 	 ?></td>
 </tr> 
<tr>
<th>Create Date</th>
<td><? echo $product['create_date'];  ?></td>
<th>Retail price</th>
<td><?   echo $product['retail_price'];	 ?></td>
 </tr> 
 <tr>
<th>Discount</th>
<td><? echo $product['discount'];  ?></td>
<th>Retail price</th>
<td><?   echo $product['retail_price'];	 ?></td>
 </tr>  <tr>
<th>Tax </th>
<td><? echo $product['tax_class'];  ?></td>
<th>Final Price</th>
<td><?   echo $product['retail_price_with_tax'];	 ?></td>
 </tr> 
 <tr>
<th>Tax </th>
<td><? echo $product['tax_class'];  ?></td>
<th>Final Price</th>
<td><?   echo $product['retail_price_with_tax'];	 ?></td>
 </tr>  <tr>
<th>Available Quantity</th>
<td><?   $db->select('product_comb','sum(qty) as final_qty',NULL,"productid='".$product['product_id']."'  ",'');
 $qtys = $db->getResult(); //print_r($cover_img);
 $qtys = $qtys[0]; echo $qtys['final_qty'];  ?></td>
<th>Create By</th>
<td><?   if($values['added_type']=='seller'){ echo get_seller_name($values['added_by']);} else { echo 'Admin'; }?>	  </td>
 </tr> 
 
  </tbody>
</table>
</div>
</div>
 
</div>
</div>
<div class="row">
<div class="col-md-12 col-lg-12">
<div class="box box-magenta">
<div class="box-title">
<h3><i class="fa fa-bars"></i>Combination Details</h3>
 
</div>
 <div class="box-content">
 <table class="table"  >
              <thead>
                <tr>
                <th>Attribute-value pair</th>
                <!--  <th>Impact on price</th>
                  <th>Impact on weight</th>
                  <th>Reference Code  (SKU Code / Style No.) </th>-->
                  <th>Bar Code</th><th>Quantity</th>
                 </tr>
              </thead>
              <tbody>
              <?   $db->select('product_comb','*',NULL,"productid='".$product_id."'",'id DESC');
 $res11 = $db->getResult(); 
$i=1; foreach($res11 as $values1){ ?>
<tr> <?    $db->select('product_comb_att_mapping','*',NULL,"combinationid='".$values1['id']."'",'id DESC');
 $res112 = $db->getResult(); 
$i=1;?>
	   <td><?php foreach($res112 as $values12 ){ echo get_attribute_name($values12['attrid']).'-'.get_attribute_value($values12['attrvalueid']).'&nbsp;'; } ?> </td> <?  ?>
                  <!--<td><? echo $values1['impactpricevalue']; ?></td>
                  <td><? echo $values1['impactweightvalue']; ?></td>
                  <td><? echo $values1['refrencecode']; ?></td>--><td><? echo $values1['barcode']; ?></td>
                  <td><? echo $values1['qty']; ?></td>
                  <input type="hidden" name="comboid_edit" value="<? echo $values1['id']; ?> "  class="combo_edit" id="comboid_<? echo $values1['id']; ?>">
         <? } ?>  </tr>     </tbody>
            </table>
</div>
</div>
 
</div>
</div>


</div>
	
	
<? include('bottom_link.php'); ?>
	  	
	</div></body>
  

</html>
