<?php 

error_reporting(0);

include('database.php');

include('functions.php');
   
include('session.php');
 ?>
 <!DOCTYPE html>
<html>
<head>
 <? include('links.php'); ?>
</head>


<body>
<!-- BEGIN Theme Setting -->
  <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> Seller Status</h1>
        <h4>Seller Status</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Seller list</li>
      </ul>
    </div>
    <div class="row  ">
     
      
	  <div class="col-md-12">
      
<div class="box">
<div class="box-title">
<h3><i class="fa fa-table"></i> Seller List</h3>
<div class="box-tool">
<!--a class="btn btn-success" href="catalog_add_product.php"><i class="fa fa-plus"></i> Add new</a-->
 </div>
</div>
<div class="box-content"> <div class="col-sm-9 col-lg-10 controls" align="center" style="margin-bottom:20px;" >
 <form name="status" id="status" /> 
 <!--label class="radio-inline">
<input type="radio" name="st" onChange="vafun(this.value)" id="no" value="N"> Pending Status </label>
 <label class="radio-inline">
<input type="radio" name="st" onChange="vafun(this.value)" id="yes" value="Y"> Approve Status  </label --> 
 
<div class="form-group">
                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label"><strong> Seller Status </strong> </label>
                              <div class="col-sm-9 col-lg-5 controls">
<select name="st" onChange="vafun(this.value)" class="form-control" style="width:300px;" >
<option value="">-- Select Status --</option>
<option value="N"> Pending </option>
<option value="Y"> Confirm </option>
</select> </div>
                            </div>
</form>  

  </div>
 
<div class="clearfix"></div>
<div class="table-responsive" >


<script> 
function vafun(va){
var dataString = 'status='+ va;
$.ajax
({
type: "POST",
url: "get_status.php",
data: dataString,
cache: false,
success: function(data)
{ //alert(data);
$("#main_tr").html('');
$('#main_tr').html(data);
}
});
		
}
 </script>
<?php

 //$db="SELECT * FROM `seller_registration` ORDER BY id DESC";
 $db= "SELECT p.id, p.company_name, p.email_id, p.mobile_no, p.password, p.pin_code, p.approve, c.pincode, c.f_name, c.l_name, c.salutation, c.city, c.state  FROM seller_registration AS p INNER JOIN seller_details AS c  ON p.id=c.user_id  ORDER BY p.id DESC";
$rr = mysql_query($db);
 ?>
 

<table class="table table-advance dataTable" id="table1" aria-describedby="table1_info">
<thead>
<tr>
<th>S. No</th>
<th>Company Name</th>
<th>Name</th>
<th>Email</th>
<th>Mobile</th>
<th>Password</th>
<th>Pin Code</th>
<th>Status</th>
<th>Action</th>
</tr>
</thead>
<tbody id="main_tr">
<?php $i=1; while($values = mysql_fetch_array($rr)){ // foreach($res as $values ){ ?>
<tr class="table-flag-blue" >
<td><?php echo $i; ?></td>
<td><?php echo $values['company_name']; ?></td>
<td><?php echo $values['salutation']."&nbsp;".$values['f_name']."&nbsp;".$values['l_name']; ?></td>
<td><?php echo $values['email_id']; ?></td>
<td><?php echo $values['mobile_no']; ?></td>
<td><?php echo $values['password']; ?></td>
<td><?php echo $values['pin_code']; ?></td>
<td>
<a href="approve.php?id=<?php echo $values['id']; ?>" <?php if($values['approve'] === 'Y'){ echo"class='btn btn-success'";} else{echo" class='btn btn-danger'";}?>><?php if($values['approve'] === 'Y'){ echo "<i class='fa fa-cloud'></i>"; }    if($values['approve'] == 'N'){ echo "<i class='fa fa-clock-o'></i>"; } ?> </a>
<!--input type="button" name="approve" id="approve" onClick="ajax(<?php //  echo $_SESSION['id']; ?>)" class="btn btn-circle btn-lg" value="<?php //if($values['approve'] === 'N'){ echo 'UnApprove'; } else{echo 'Approve';} ?>"/>
</td-->
</td>
<div id="res"> </div>

<td class="visible-md visible-lg">
<div class="btn-group">
<a class="btn btn-sm show-tooltip" title="" href="seller_edit.php?id=<?php echo $values['id']; ?>" data-original-title="Edit"><i class="fa fa-edit"></i></a>

<!--a class="btn btn-sm show-tooltip" title="" href="panding_saller_view.php?id=<?php // echo $values['id']; ?>" data-original-title="View"><i class="fa fa-eye"></i></a-->	
</div>
</td>
</tr>
<?php $i++; } ?>
</tbody>
</table>


</div>
</div>
</div>
</div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->
  <? include('bottom_link.php'); ?>

</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
