<?php 
include('database.php');

include('functions.php');

include('session.php');

?>

<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
<head>
<? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Product feature</h1>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Product feature</li>
      </ul>
    </div>
    <div class="row  ">
      
	  <div class="col-md-12">
<div class="row">

<div class="col-md-12">
<div class="panel panel-primary">
<div class="panel-heading">
<h4 class="panel-title">Add feature of the product</h4>
</div>
<div class="panel-body">
 <form  class="form-horizontal" action="catalog_product_features1.php" method="post" enctype="multipart/form-data" id="operator" >
						<div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Name</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" name="feature_name" id="textfield1" class="form-control">
                          </div>
                        </div>
						<!--<div class="form-group" >
                          <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Public name</label>
                          <div class="col-sm-9 col-lg-5 controls">
                            <input type="text" name="apublic_name" id="textfield1" class="form-control">
                          </div>
                        </div>-->
						
<div class="form-group">
                <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                  <button type="button" class="btn">Cancel</button>
                </div>
              </div>
</form>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-10">
  <!-- <a href="catalog_product_features_value.php" class="btn btn-primary"><i class="fa fa-check"></i> Add feature value</a><br><br>-->

</div>
<div class="col-md-12">
<div class="box box-magenta">
<div class="box-title">
<h3><i class="fa fa-table"></i> feature list</h3>
<div class="box-tool">
<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
<a data-action="close" href="#"><i class="fa fa-times"></i></a>
</div>
</div>
<div class="box-content">
<?php $db->select('catalog_product_features','*',NULL,'','feature_id DESC');
	  $res = $db->getResult();

	  //$res = $res[0];
	  //print_r($res);
 ?>
<table class="table table-bordered">
<thead>
<tr>
<th>S.no</th>
<th>Feature Type</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php $i=1; foreach($res as $values ){ ?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $values['feature_name'] ?></td>
<td><a class="btn btn-primary btn-sm" href="catalog_product_features_edit.php?f_id=<?php echo $values['feature_id'] ?>"><i class="fa fa-edit"></i> Edit</a></td>
</tr>
<?php $i++; } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>


</div>

</div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container --> 
<? include('bottom_link.php'); ?>

</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
