<?php 
include('database.php');

include('functions.php');

include('session.php');

?>
<!DOCTYPE html>
<html>
<head>
<? include('links.php'); ?>
</head>
 
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
 		    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i> Mailer </h1>
        <h4> Mail Content </h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active"> Mail Content </li>
      </ul>
    </div>
    <div class="row  ">
      <div class="col-md-12 col-sm-12 col-lg-12">
        <div class="box box-green">
          <div class="box-title">
            <h3><i class="fa fa-bars"></i> Content</h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a></div>
          </div>
          
          
          <div class="box-content">
            <form  class="form-horizontal" action="" method="post" enctype="multipart/form-data" id="operator" >
              <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12"> 
                
               
                      <div class="tab-pane fade in active">
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-lg-12">
						
              		
                    
                    <div class="form-group">
                      <label for="textfield1" class="col-xs-3 col-lg-3 control-label ">Subject name </label>
                     <div class="col-sm-9 col-lg-5 controls">
                        <input type="text" name="subject" id="subject" required class="form-control" placeholder="Name of subject" value="">
                      </div>
                    </div>       
                            
                            
                    <div class="form-group">
                      <label for="textfield1" class="col-xs-3 col-lg-3 control-label ">Mail Type </label>
                     <div class="col-sm-9 col-lg-5 controls">
                   
                   <select name="mail_type" id="mail_type" class="form-control">
   <option>--Select--</option> 
<?  $db->select('mail_type','*',NULL,'','id DESC');
  $mails = $db->getResult();
    foreach($mails as $values ){ ?>
<option><? echo $values['name']; ?></option>
<? } ?>     </select>
                       </div>
                    </div>       
                             
                          	<div class="form-group">
                              <label for="textfield1" class="col-xs-3 col-lg-3 control-label">Mail Content</label>
                              <div class="col-sm-9 col-md-9 col-lg-9 controls">
                                <textarea name="content" id="content" rows="5" class="form-control ckeditor"></textarea>
                              </div>
                            </div>
                             
                          </div>
                        </div>
        
                      </div>
                   
                  </div>
                </div>
             
              <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                  <!--button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button-->
                  
                  <input type="submit" name="submit" value="Save" class="fa fa-check btn btn-primary" />
                  
                  <button type="button" class="btn">Cancel</button>
                </div>
              </div>
            </form>
          </div>

          <? if(isset($_POST['submit'])){
		$insert = "INSERT INTO `sms_email_template` (`sms_email_data`, `sms_email_type`, `sms_name`) VALUES ('".$_REQUEST['content']."', '".$_REQUEST['mail_type']."','".$_REQUEST['subject']."')";
				$rr = mysql_query($insert);
				}
		   ?>
          
        </div>
      </div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container --> <? include('bottom_link.php'); ?>

</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
