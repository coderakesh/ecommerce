<?php 

error_reporting(0);

include('database.php');

include('functions.php');

include('session.php');

?>

<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
<head>
  <meta name="description" content="">
 <? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>
<!-- END Theme Setting --> 
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar --> 
<!-- BEGIN Container -->
<div class="container" id="main-container"> 
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar --> 
  <!-- BEGIN Content -->
  <div id="main-content"> 
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Mobile Recharge List</h1>
        <h4>Mobile Recharge List</h4>
      </div>
    </div>
    <!-- END Page Title --> 
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Mobile Recharge List</li>
      </ul>
    </div>
    <div class="row  ">
      <div class="col-md-12">
        <div class="box">
          <div class="box-title">
            <h3><i class="fa fa-table"></i>Mobile Recharge List</h3>
            <div class="box-tool"> 
              <!---<a class="btn btn-success" href="catalog_product_add_manuf.php"><i class="fa fa-plus"></i> Add new</a>--> 
            </div>
          </div>
          <div class="box-content">
            <div class="clearfix"></div>
            <div class="table-responsive" style="border:0">
              <?php 
			  $cdate = date('y-m-d');
$sql1="select * from recharge_mobile where cstatus!='' or payment_status !='' order by rmobid desc";

$db->sql($sql1);

$res = $db->getResult();

//$res = $res[0];
//print_r($res);
 ?>
              <table class="table table-advance" id="table1">
                <thead>
                  <tr>
                    <th>S.no.</th>
                    <th>Email</th>
                    <th>Mobile No</th>
                    <th>Operator Id</th>
                    <th>Circle Id</th>
                    <th>Amount</th>
                    <th>Date</th>
<!--    					<th>Time</th>
                <th>Operator Type</th>
-->                    <th>Recharge Status</th>
                    <th>Payment Status</th>
                    <th>Payment Mode</th>
					<th>Recharge Details</th>
					<th></th>
                   </tr>
                </thead>
                <tbody>
                  <?php $i=1; foreach($res as $values ){ ?>
                  <tr class="table-flag-blue">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $values['email'];?></td>
                    <td><?php echo $values['mobile_number']; ?></td>
                    <td><?php echo get_operator_name($values['operator_id']); ?></td>
                    <td><?php echo get_circle_name($values['circle_id']); ?></td>
                    <td>Rs. <?php echo $values['amount']; ?></td>
                    <td><?php echo $values['cdate']; ?></td>
<!--                          <td><?php echo $values['ctime']; ?></td>
              <td><?php echo $values['operator_type']; ?></td>
-->                    <td><?php echo $values['cstatus']; ?></td>
                    <td><?php echo $values['payment_status']; ?></td>
                    <?php
					if($values['payment_mode']==0)
					{
						$p_status="Wallet";
					}
					else
					{
						$p_status="Gateway";
					}?>
                    <td><?php echo $p_status?></td>
 					<td><a href="recharge_details.php?uid=<? echo $values['user_id']; ?>" class="btn btn-success" > View</a></td>
 					<td><a href="add_wallet.php?uid=<? echo $values['user_id']; ?>" >Add Wallet Amount</a></td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content --> 
</div>
<!-- END Container --> 
<!--basic scripts--> 
<script src="../../ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="assets/jquery/jquery-2.1.1.min.js"><\/script>')</script> 
<script src="assets/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script> 
<script src="assets/jquery-cookie/jquery.cookie.js"></script> 
<script type="text/javascript">




/*$(document).ready(function () {
	$("#enab").on('click',(function() {
		var v = $(this).val();
		if(v=="Disabled")
		{
			$(this).val("Enabled");
			 $('#stat').html('N');
		}
		else{
			$(this).val("Disabled");
			$('#stat').html('Y');
		}
	}));});*/
	function changestate(ide,st){
		$.ajax({
			url:"changestatus.php",
			type:"POST",
			data:{'uid':ide,'state':st},
			success:function(rtext){
				//alert(rtext);
				var $response =$(rtext);
				var status = $response.filter('#div1').html();
				$("#state"+ide).html(status);
				//alert(status);
				var butt = $response.filter('#div2').html();
				$("#btndis"+ide).html(butt);
				}
			});
		
		}

</script> 

<!--page specific plugin scripts--> 
<script type="text/javascript" src="assets/chosen-bootstrap/chosen.jquery.min.js"></script> 
<script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script> 
<script type="text/javascript" src="assets/jquery-tags-input/jquery.tagsinput.min.js"></script> 
<script type="text/javascript" src="assets/jquery-pwstrength/jquery.pwstrength.min.js"></script> 
<script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script> 
<script type="text/javascript" src="assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js"></script> 
<script type="text/javascript" src="assets/dropzone/downloads/dropzone.min.js"></script> 
<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
<script type="text/javascript" src="assets/clockface/js/clockface.js"></script> 
<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script> 
<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script type="text/javascript" src="assets/bootstrap-switch/static/js/bootstrap-switch.js"></script> 
<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script> 
<script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script> 
<script type="text/javascript" src="assets/data-tables/bootstrap3/dataTables.bootstrap.js"></script> 
<!--flaty scripts--> 
<script src="js/flaty.js"></script> 
<script src="js/flaty-demo-codes.js"></script>
</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>
