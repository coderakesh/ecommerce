<?php include('database.php');

include('functions.php');

include('session.php');
?>
<!DOCTYPE html>
<html>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
 <head>
 <? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Add promo amount</h1>
        <h4>Add Promo amount to wallet</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Add promo amount</li>
      </ul>
    </div>
    <div class="row  ">
     
      <div class="col-md-12">
        <div class="box">
          <div class="box-title">
            <h3><i class="fa fa-table"></i>users list to provide promo amount</h3>
            <div class="box-tool"> <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a> <a data-action="close" href="#"><i class="fa fa-times"></i></a> </div>
          </div>
          <div class="box-content">
            <div class="clearfix"></div>
            <div class="table-responsive" style="border:0">
              <?php 
			  $sql = "SELECT a.promo_code_id, a.cashback_type, b.user_id, b.promo_amount
        FROM master_promo_code a, promo_code_history b
        WHERE a.promo_code_id = b.promo_code_id and a.cashback_type='wallet' and b.wallet_amount = b.new_wallet_amount";
			  
          $result = mysql_query($sql) or die(mysql_error());
		  $rowc = mysql_num_rows($result);
            
           ?> <div id="div_print" style="width:100%; height: 350px; overflow-x: scroll; scrollbar-arrow-color:
blue; scrollbar-face-color: #e7e7e7; scrollbar-3dlight-color: #a0a0a0; scrollbar-darkshadow-color:
#888888; ">              <table class="table table-advance" id="table1">
                <thead>
                  <tr>
                    <th>Promo Id</th>
                    <th>Cash back type</th>
                    <th >User Name</th>
                    <th >Amount</th>
                    <th >Provide</th>

                  
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; while($rowc>0){ 
				      $row = mysql_fetch_assoc($result);
					   extract($row);
				  ?>
                  <tr id="<?php echo $i; ?>" class="table-flag-blue">
                 
                    <td><?php echo $row['promo_code_id']; ?></td>
                    <td><?php echo $row['cashback_type']; ?></td>
                    <?php $sql2 = "select first_name, last_name from users where user_id = $user_id"; 
					        $result2 = mysql_query($sql2);
							$row2 = mysql_fetch_assoc($result2);
					?>
                     <td><?php echo $row2['first_name']." ".$row2['last_name'] ?></td>
                      <td><?php echo $row['promo_amount']; ?></td>
                       <td><span class="spn1" onClick="provide_wallet_amount(<?php echo $i; ?>,<?php echo  $row['user_id']?>,<?php echo $row['promo_amount']; ?>)">provide</span></td> 
                  </tr>
                  <?php $rowc--; $i++; } ?>
                </tbody>
              </table>
            </div> </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->
<!--basic scripts-->
<script src="../../ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/jquery/jquery-2.1.1.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/jquery-cookie/jquery.cookie.js"></script>
<script type="text/javascript" src="../validation/livevalidation_standalone.compressed.js"></script>
<script>
//for name
var f2 = new LiveValidation('circle_name');
f2.add( Validate.Presence, { failureMessage: "circle Name is Required" } );

</script>
<script type="text/javascript">
  $("#bootbox-confirm").on('click', function () {
	 
	  var valid1 = livevalidatForm();
	  
	   if(valid1==true){
				
	$("#circle").submit();
              
   }     });
   
   function provide_wallet_amount(trid,user_id,amt)
   {
	   alert(trid);
	   $.ajax({
		   type:"POST",
		   url:"add_promo_to_wallet.php",
		   data:{'uid':user_id,'amount':amt},
		   success:function(rtext){
			     alert(rtext);
				 $('table#table1 tr#'+trid).remove();
			   }
		   });
   }

</script>
<!--page specific plugin scripts-->
<script type="text/javascript" src="assets/chosen-bootstrap/chosen.jquery.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script type="text/javascript" src="assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="assets/jquery-pwstrength/jquery.pwstrength.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js"></script>
<script type="text/javascript" src="assets/dropzone/downloads/dropzone.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="assets/clockface/js/clockface.js"></script>
<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-switch/static/js/bootstrap-switch.js"></script>
<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/bootstrap3/dataTables.bootstrap.js"></script>
<script src="js/flaty.js"></script>
<script src="js/flaty-demo-codes.js"></script>
</body>
</html>
