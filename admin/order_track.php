<?php 

error_reporting(0);
header('Access-Control-Allow-Origin: *');
include('database.php');

include('functions.php');
 include('session.php');
  ?>
 <!DOCTYPE html>
<html>
 <head>
 <? include('links.php'); ?>
</head>
 
<body>
<!-- BEGIN Theme Setting -->
 <? include('right_bar.php'); ?>

<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Order Track</h1>
       </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index.php">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Order Track</li>
      </ul>
    </div>
    <div class="row  ">
      
	  <div class="col-md-12" style="background-color:#FFFFFF">
<div class="box">
<div class="box-title">
<h3><i class="fa fa-table"></i>Order Track</h3><span style="color:#FFFFFF"> </span>
<div class="box-tool">
<!--a class="btn btn-success" href="catalog_add_product.php"><i class="fa fa-plus"></i> Add new</a-->
 </div>
</div>
    <form action="" class="form-horizontal" method="post" id="enquiry_form" enctype="multipart/form-data" name="a" >
<div class="row">
   <div class="col-md-12 ">
 	 <div class="form-group"><!--
<label for="password1" class="control-label col-lg-3 col-xs-3">Search Customer</label>-->
<div class="col-sm-9 col-lg-9 controls" style="text-align:center; float:left">
<input type="text" name="ord_num" id="ord_num" class="form-control blank"   alt="Search Criteria" autocomplete="off"  autofocus="autofocus" placeholder ="Type order number to check status"  />
<img src="../images/product_load.gif" id="img_load" style="display:none; ">
 <input type="hidden" name="ordid" id="ordid"  style="width:5mm" readonly="true" value="0"  /> 
 	   </div><div class="col-sm-3 col-lg-3 scontrols" style="text-align:center;">
  
   
   <input type="button" value="Search Order" class="btn btn-primary search_ord " >    
</div>
</div>
</div>
   <div id="layer1"> </div>
   <!-- END Left Side -->
 </div>
    
</div>
 </form>
</div></div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->

<? include('bottom_link.php'); ?>



<script src="../js/jquery-ui-min.js"></script>

 <script type="text/javascript"> 
 
$(function() {
	$('.search_ord').click(function(){ 
	$('#layer1').html(''); 

ord_num = $('#ord_num').val();    
 $('#img_load').css('display','inline');
 	 $.ajax
({
type: "POST",
  url: "ord_search.php",
   data: 'term=' + ord_num,
    dataType: "json",
	success: function(data)
{     	  
  getdetails(data[0]);
  $('#img_load').css('display','none');

}
});
 
 	});		
            
        }); 
		function getdetails(str)
 {
 $.ajax
({
type: "POST",
url: "order_status.php?order_id="+str,
 cache: false,
success: function(data)
{    	// alert(data);
var data = $.trim(data);
$('#layer1').html(data); 
}
});
 }
</script>
</body>
<!-- Mirrored from themes.shamsoft.net/flaty/form_layout.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 06 Dec 2014 05:01:23 GMT -->
</html>

