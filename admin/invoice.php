<? include('database.php');
include('functions.php');

 function convertDate($date) {
       // EN-Date to GE-Date
       if (strstr($date, "-") || strstr($date, "/"))   {
               $date = preg_split("/[\/]|[-]+/", $date);
               $date = $date[2]."-".$date[1]."-".$date[0];
               return $date;
       }
       // GE-Date to EN-Date
       else if (strstr($date, ".")) {
               $date = preg_split("[.]", $date);
               $date = $date[2]."-".$date[1]."-".$date[0];
               return $date;
       }
       return false;
}

function convert_number_to_words($number) {

$number =  (int)$number ;

    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'Negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'Zero',
        1                   => 'One',
        2                   => 'Two',
        3                   => 'Three',
        4                   => 'Four',
        5                   => 'Five',
        6                   => 'Six',
        7                   => 'Seven',
        8                   => 'Eight',
        9                   => 'Nine',
        10                  => 'Ten',
        11                  => 'Eleven',
        12                  => 'Twelve',
        13                  => 'Thirteen',
        14                  => 'Fourteen',
        15                  => 'Fifteen',
        16                  => 'Sixteen',
        17                  => 'Seventeen',
        18                  => 'Eighteen',
        19                  => 'Nineteen',
        20                  => 'Twenty',
        30                  => 'Thirty',
        40                  => 'Fourty',
        50                  => 'Fifty',
        60                  => 'Sixty',
        70                  => 'Seventy',
        80                  => 'Eighty',
        90                  => 'Ninety',
        100                 => 'Hundred',
        1000                => 'Thousand',
        1000000             => 'Million',
        1000000000          => 'Billion',
        1000000000000       => 'Trillion',
        1000000000000000    => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    } 
	     if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
} include('session.php'); 
$order_id = $_REQUEST['order_id'];
  $db->select('orderdetail','seller_id,sell_type,oid',NULL,'oid="'.$order_id.'"','',0,1);
 $pro_dels = $db->getResult();   $pro_dels = $pro_dels[0]; 
 $db->select('orders','uid,addressid,after_coupon,shipping,instl_charge,invoice,paymenttype,odate',NULL,'oid="'.$pro_dels['oid'].'"','',0,1);
 $ord_dels = $db->getResult(); 
 $ord_dels = $ord_dels[0];  
 if($pro_dels['sell_type']=='admin'){ 
	  $db->select('default_seller_adr','*',NULL,'','',0,1);
   $add_dels = $db->getResult();   $add_dels = $add_dels[0];  $scompany = $add_dels['company_name'];
  $saddress = $add_dels['pickup_add']; $smobile = $add_dels['mobile_no']; $scity = $add_dels['city'];
   $spincode = $add_dels['pincode']; $sstate = $add_dels['state'];
   } else { 
 	  $db->select('seller_details','*',NULL,'user_id=1','',0,1);//"'.$pro_dels['seller_id'].'"
  $add_dels = $db->getResult();  $add_dels = $add_dels[0];
  	  $db->select('bank_details','*',NULL,'user_id=1','',0,1);//"'.$pro_dels['seller_id'].'"
  $bank_dels = $db->getResult(); 
  $bank_dels = $bank_dels[0];  $scompany = $add_dels['comp_display_name'];
  $saddress = $add_dels['bill_pickup_add']; $sstate = $add_dels['bill_state'];  $scity = $add_dels['bill_city']; 
  $smobile = $add_dels['mobile_no'];    $spincode = $add_dels['bill_pincode']; 
    } $db->select('profile_address','*',NULL,'pcaddid="'.$ord_dels['addressid'].'"','',0,1);
  $user_dels = $db->getResult();  $user_dels = $user_dels[0];  
 $db->select('prefix_name','*',NULL, "types='seller_prefix'",'',0,1);
 $prefix = $db->getResult();  $prefix = $prefix[0];     
   $session = $db->getResult();  $session = $session[0];   $from = $session['from_date']; $to = $session['to_date']; 
  $uname = $user_dels['fullname'];  $uaddress = $user_dels['cadd1']; $ucity = getcity($user_dels['city']); $umob = $user_dels['cmobno'];
  $ustate = getstate($user_dels['cstate']); $uccountry = $user_dels['ccountry']; $umobile = $user_dels['cmobno']; $uname = $user_dels['fullname']; $upincode = $user_dels['pincode'];
 ?>    
<html lang="en">
<head>
 <link href="css/invoice.css" rel="stylesheet">
<style>
body{    font-family: Lato,Helvetica Neue, Helvetica, Arial, sans-serif; }
page[size="A4"] { background: white;
       width: 20cm;
    height: 18cm;
    display: block;
    margin: 0 auto;
    margin-bottom: 0.5cm;
    box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5); }
 @media print { body, page[size="A4"] {
   margin: 0;
   box-shadow: 0;
 } }
 .CSSTableGenerator td {
 text-align:center !important;
 }
</style>
</head>

<body>
<page size="A4">
<div class="CSSTableGenerator">   
<table>
<tbody>  <tr style="background:#FFFFFF"><td colspan="4"> <img src="../img/logo/logo.png"  style="float:left" ></td>
<td colspan="4" >  E-Mail id : info@jhoomarwala.in <br>Phone No. : 919893273135  </td>
</tr>   <tr><td colspan="8"> </td>
</tr> 
 
 <tr><td colspan="8"> Retail Invoice</td>
</tr>   <tr>
    <td colspan="4">Invoice Number :  <?  echo $ord_dels['invoice']; ?>    </td>
    <td colspan="4">Invoice Date : <?  $date = strtotime($ord_dels['odate']); 
echo $new_date = date('d-m-Y', $date); ?></td>
  </tr>
  <tr  class="tr-color" >
    <td colspan="4"style="text-align:left;">Seller</td>
    <td colspan="4" style="text-align:left;">buyer</td>
  </tr>
  <tr>
  <td colspan="4"><strong><? echo $scompany; ?> </strong><br><? echo $saddress; ?>,  
 <? echo $smobile; ?>, <br>
 <strong>City : </strong> <? echo getcitysel($scity); ?> /<strong> State :</strong> <? echo getstatesel($sstate);; ?>       <br><strong>pin</strong> <? echo $spincode; ?>  </td>
 <td colspan="4"><strong><? echo $uname; ?></strong><br>  <? echo $uaddress.' , '.$umob; ?> <br>
<strong>City : </strong> <? echo  ($ucity);?>  /<strong> State : </strong><? echo ($ustate); ?>  /<strong> Country :</strong> <? echo getcountry($uccountry);; ?>  <br><strong>pin</strong> <? echo $upincode; ?>  </td>   </tr>
<!--  <tr class="red">
    <td colspan="4"><strong>Dispatched via : </strong>  <?  echo getCourierName(); ?></td>
    <td colspan="4"><strong>Dispatched Doc. no.(awb) : </strong><? echo $waybill; ?>  </td>
  </tr>-->  <tr class="red">
    <td colspan="4"><? if($bank_dels['vat_no']!=''){ ?><strong>GSTIN. : </strong>  <?  echo $bank_dels['vat_no']; } ?></td>
    <td colspan="4"><? if($bank_dels['cst']!=''){ ?><strong>CST : </strong><? echo $bank_dels['cst']; } ?>  </td>
  </tr>
   <tr class="tr-color">
 <? $db->select('orderdetail','*',NULL,'oid="'.$order_id.'"','',0,1);
 $pro_del1 = $db->getResult();$pro_del1 = $pro_del1[0];
    $db->select('catalog_product','tax_class',NULL,'product_id="'.$pro_del1['pid'].'"','',0,1);
 $tax_del1 = $db->getResult();  $tax_del1 = $tax_del1[0]; ?>    <td>s.no </td>
    <td colspan="2"> Item description </td>		
    <td>HSN Code</td>
    <td> qty </td>
    <td> rate </td>
    <td><?  if($tax_del1['tax_class']!=''){ echo 'IGST'; }?></td>
    <td> amount </td>
  </tr><? $db->select('orderdetail','*',NULL,'oid="'.$order_id.'"','',0,1);
 $pro_del = $db->getResult(); $i=1; $tot=0;
 foreach($pro_del as $pro_dels){   
   $db->select('catalog_product','tax_class,ref_code',NULL,'product_id="'.$pro_dels['pid'].'"','',0,1);
 $tax_del = $db->getResult();  $tax_del = $tax_del[0];  
 if($tax_del['tax_class']!=''){
 $real_rate  = round(($pro_dels['price']*100)/(100+$tax_del['tax_class']), 0);
 } else { $real_rate  = $pro_dels['price']; } $real_rate1  = $pro_dels['price']; 
 $tot =$tot+$real_rate1;
   $db->select('product_category_mapping','categoryid',NULL,'productid="'.$pro_dels['pid'].'"','',0,1);
 $cat_del = $db->getResult();  $cat_del = $cat_del[0]; 
    $db->select('catalog_category','hsncode',NULL,'category_id="'.$cat_del['categoryid'].'"','',0,1);
 $hsn_del = $db->getResult();  $hsn_del = $hsn_del[0];     ?> <tr style="background: #fff;">
    <td><? echo $i; ?></td>
    <td align="center" colspan="2"><? echo get_product_name($pro_dels['pid']).' ( '.$tax_del['ref_code'].' ) ';
	 ?> </td>		
     <td align="center" ><? echo $hsn_del['hsncode']; ?></td>
    <td align="center" ><? echo $pro_dels['qty']; ?></td>
    <td align="center" ><? echo $real_rate; ?></td>
    <td  align="center" ><? if($tax_del['tax_class']!=''){ echo $tax_del['tax_class'].'%'; } ?></td>
    <td align="center" > <? echo $pro_dels['price']; ?></td>
  </tr>  <? } ?>
    <? if(($ord_dels['shipping']!='') && ($ord_dels['shipping']!=0)) { ?>
 <tr>
     <td colspan="7" style="text-align:center !important">Shipping Charges</td>	
  <td>Rs. <? echo $ord_dels['shipping']; ?></td>
 </tr>  <? } ?>
   <? if(($ord_dels['instl_charge']!='') && ($ord_dels['instl_charge']!=0)) { ?>
   <tr>
     <td colspan="7" style="text-align:center !important">Instalation Charges</td>	
  <td>Rs. <? echo $ord_dels['instl_charge']; ?></td>
 </tr>  <? } ?>
  <tr> 
    <td colspan="7" style="text-align:center !important">Payment Mode</td>	
  <td><? echo $ord_dels['paymenttype']; ?></td>
 </tr>   <tr> 
    <td colspan="7" style="text-align:center !important">Total</td>	
  <td>Rs. <? echo $ord_dels['after_coupon']; ?></td>
 </tr>   <tr class="red">
   <td colspan="8"><h4>Amount in words : Indian Rupees <? echo convert_number_to_words($ord_dels['after_coupon']); ?> Only</h4> </td> </tr>
<tr class="red" >
   <td colspan="8"><h5 style="text-align:right">Thanks to visit jhoomarwala.in</h5>   </td> </tr>
   </tbody>
</table>
 
</div>

 </page>
</body>
</html>
