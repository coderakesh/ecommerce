<?php 
include('database.php');

include('functions.php');

include('session.php');

?>

<!DOCTYPE html>
<html>
<head>
<? include('links.php'); ?>
</head>
<body>
<!-- BEGIN Theme Setting -->
<? include('right_bar.php'); ?>
<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<?php  include('header.php'); ?>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <?php include('leftmenu.php'); ?>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
        <h1><i class="fa fa-file-o"></i>Category</h1>
        <h4>Category list</h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="index-2.html">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active">Category list</li>
      </ul>
    </div>
    <div class="row">
      
	  <div class="col-md-12">
<div class="box">
<div class="box-title">
<h3><i class="fa fa-table"></i>Category List</h3>
<div class="box-tool">
<a class="btn btn-success" href="catalog_add_category.php"><i class="fa fa-plus"></i> Add new</a>
 </div>
</div>
<div class="box-content">
<div class="clearfix"></div>
<div class="table-responsive" style="border:0">

<?php $db->select('catalog_category','*',NULL,'','category_id ASC');

$res = $db->getResult();

//$res = $res[0];
//print_r($res);
 ?>

<table class="table table-advance" id="table1">
<thead>
<tr>
<th>>S. No.</th>
<th>Category Name</th>
 <th>Action</th>
</tr>
</thead>
<tbody>
<?php $i =1 ; foreach($res as $values ){ ?>
<tr class="table-flag-blue">
<td><? echo $i; ?></td>
<td><?php echo get_nested_category($values['category_id']); ?></td>
 <td class="visible-md visible-lg">
<div class="btn-group">
<a class="btn btn-sm show-tooltip" title="" href="catalog_edit_category.php?category_id=<?php echo $values['category_id']; ?>" data-original-title="Edit"><i class="fa fa-edit"></i></a>
<!--<a class="btn btn-sm btn-danger show-tooltip" title="" href="#" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>-->
</div>
</td>
</tr>
<?php $i++; } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
    </div>
    <!-- END Main Content -->
    <?php include('footer.php'); ?>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a></div>
  <!-- END Content -->
</div>
<!-- END Container -->
<!--basic scripts-->
 
<? include('bottom_link.php'); ?>
</body>
 </html>
