<!DOCTYPE html>
<html lang="en">
<head>
<? include('link.php'); ?>
<!------------ for bootstrap ------------------->
<link href="css/bootstrap2.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/livevalid.css" />
<script type="text/javascript" src="js/livevalidation_standalone.compressed.js"></script>
<!-------------- for alert ------------------>
<style type="text/css">



.LV_validation_message {
	position: absolute;
	left: 15px;
	top: 52px;
}
.form-group {
	padding:2px !important;
}
.company_details {
	display:none;
}
.show_image {
	display:none;
}
.formprocessgif {
	display: none;
}
.formprocessgif1 {
	display: none;
}
.hideform {
	display:none;
}
</style>
<script type="text/javascript">

	function goBack() { 

   window.history.back();

}</script>
</head>
<body>
<div class="container">
<?php if($_REQUEST['forgot']!=''){ ?>
<style type="text/css">



.LV_validation_message {

  position: absolute;

  left: 0;

  top: 75px;

  }



</style>
<div class="row">
  <div id="forgot_password" class="frgt-pop">
    <div class="col-md-12">
      <h3>Forgot Password</h3>
      <form class="form-horizontal form-pop"  action="forgot_password.php" method="post">
        <div class="">
          <div class="">
            <label>Enter your E-Mail address or Mobile Number and we will send you a confirmation code.</label>
            <br>
            <input type="text" class="form-control" name="email_mobile"  style="height:45px;" placeholder="E-mail or Mobile No.">
            <br>
            <br>
          </div>
        </div>
        <div class="">
          <div class="">
            <input type="submit" class="btn btn-info btn-lg btn-block" value="Get Password">
          </div>
        </div>
      </form>
    </div>
    <div class="frgt-pswd"> <a href="login_signup.php" id="back" style="font: 500 14px/1.55 Lato,Helvetica Neue, Helvetica, Arial, sans-serif;"> <i class="fa fa-arrow-left"></i> Go Back</a></div>
  </div>
</div>
<script type="text/javascript">

var email_mobile1 = new LiveValidation('email_mobile22');

				email_mobile1.add(Validate.Presence);

 </script>
<?php }elseif($_REQUEST['onetime_password']!=''){ ?>
<style type="text/css">



.LV_validation_message {

  position: absolute;

  left: 0;

  top: 75px;

  }



</style>
<div class="row">
  <div id="forgot_password" class="row">
    <div class="col-md-12 col-sm-12 text-right"> <strong><br>
      <a href="#" id="back" onClick="goBack();"></a></strong> </div>
    <div class="col-sm-12">
      <h3>Enter one time password (OTP)</h3>
      <br>
      One time password has been sent your mobile ******4565, please enter it here to verify your mobile <br>
      <br>
      <br>
    </div>
    <div class="col-md-12 col-sm-6 ">
      <form class="form-horizontal"  action="verify_otp.php" method="post">
        <div class="form-group">
          <div class="col-sm-12">
            <label>One time password (OTP)</label>
            <br>
            <input type="password" class="form-control" name="otp" id="one_time1" style="height:45px;">
            <input type="hidden" value="<?php echo $_REQUEST['email_mobile']; ?>" name="email_mobile" />
            <input type="hidden" name="user_id" id="user_id" value="<?php echo $_REQUEST['user_id']; ?>" />
            <br>
            <br>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <input type="submit" class="btn btn-info btn-lg btn-block" name="Verify" value="Verify" />
          </div>
        </div>
      </form>
    </div>
    <div class="col-md-12 col-sm-6" style="padding-top:20px;"> If you not recive the otp<br>
      in one minute click to <a href="#" class="otp_resend">Resend OTP</a> </div>
    <div class="col-md-12 col-sm-12" style="padding-top:10px;"> </div>
  </div>
</div>
<script type="text/javascript">

var email_mobile1 = new LiveValidation('one_time1');

				email_mobile1.add(Validate.Presence);



</script>
<?php }elseif($_REQUEST['reset_password']!=''){ ?>
<style type="text/css">



.LV_validation_message {

  position: absolute;

  left: 0;

  top: 75px;

  }



</style>
<div class="row">
  <div id="forgot_password" class="row">
    <div class="col-md-12 col-sm-6 ">
      <h3>Reset Password</h3>
      <form class="form-horizontal" style="padding-top:60px;" action="reset_password.php" method="post">
        <div class="form-group">
          <div class="col-sm-12">
            <label>New password</label>
            <br>
            <input type="password" class="form-control" name="new_password" id="myPasswordField" style="height:45px;">
            <input type="hidden" value="<?php echo $_REQUEST['mobile']; ?>" name="mobile" />
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <label>Confirm password</label>
            <br>
            <input type="password" class="form-control" name="confrim_password" id="f19" style="height:45px;">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <input type="submit" class="btn btn-info btn-lg btn-block" value="Get password">
          </div>
        </div>
      </form>
    </div>
    <div class="col-md-12 col-sm-6 text-right"> <strong><br>
      <br>
      <a href="#" id="back" onClick="goBack();">BACK</a></strong> </div>
  </div>
</div>
<script type="text/javascript">

var f19 = new LiveValidation('f19');

f19.add( Validate.Confirmation, { match: 'myPasswordField' } );

</script>
<?php }else{?>
<div class="row" id="login_signup">
  <div class="col-md-12"><br>
  </div>
  <div class="">
    <div class="col-md-12 col-sm-12"  >
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#login">Login</a></li>
        <li><a data-toggle="tab" href="#signup">Sign Up</a></li>
      </ul>
      <br />
    </div>
    <div class="tab-content" style="border: 0px solid #ddd;">
      <div id="login" class="tab-pane fade in active"> <br>
        <div class="col-sm-7">
          <h2 class="signin-line">Sign In with your Shoooz account</h2>
          <h4 class="signin-line-sub">Welcome back! Enter your password to login!</h4>
          <form class="form-horizontal" id="login_form">
            <div class="form-group">
              <div class="col-sm-12">
                <input type="text" class="custom-input" id="email_mobile1ww" name="email_mobile" placeholder="Mobile or email address" autofocus   required >
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <input type="password" class="custom-input" name="password" id="password1ww" placeholder="Password" required >
              </div>
            </div>
            <div class="" >
              <div class="col-sm-12">
                <p class="forget"> <a href="login_signup.php?forgot=forgot" id="create_password">Forgot your password?</a></p>
              </div>
            </div>
            <div class="">
              <div class="col-sm-12">
                <p class="term-cond">By Logging In, you agree to our <a href="#">T&C </a> and that you have read our <a href="#">Privacy policy</a></p>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <button type="button" id="login_submit" class="btn btn-info btn-lg btn-block">Login</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-sm-5">
          <div class="social-login-separator-popup"></div>
          <div class="login-social-popup"> <a href="login-facebook.php"><img src="img/social/face_checkout.png" /></a> <a href="#"><img src="img/social/google_checkout.png" style="display:none" /></a> </div>
        </div>
      </div>
      <div id="signup" class="tab-pane fade"><br />
        <div class="col-sm-7">
          <form class="form-horizontal" id="user_form" >
            <div class="form-group">
              <div class="col-sm-12">
                <div class="input-group">
                  <div class="input-group-addon"><strong>+91</strong></div>
                  <input type="text" class="custom-input" id="mobile_number"  placeholder="Mobile number" name="mobile_number">
                </div>
                <div id="mobile_error"></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <input type="text" class="custom-input" id="email_address" placeholder="Email address" name="email_address" >
                <div id="email_error" ></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <input type="password" class="custom-input" id="password" placeholder="Password" name="password" >
              </div>
            </div>
            <div class="form-group" >
              <div class="col-sm-12">
                <div class="field">
                  <div class="formprocessgif"><img src="images/ajax-loader.gif" align="center"> We are processing your submssion. Please wait...</div>
                </div>
              </div>
            </div>
            <div class="form-group" style="font-size:12px;" >
              <div class="col-sm-12">
                <p class="term-cond">By Signing Up, you agree to our <a href="#">T&C </a> and that you have read our <a href="#">Privacy policy</a> Your Semi Closed Wallet will be created.</p>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <button type="button" id="submit" class="btn btn-info btn-lg btn-block">Continue</button>
              </div>
            </div>
          </form>
          <form class="form-horizontal" id="otp_form"  method="post">
            <input type="hidden" name="user_id" id="user_id" value="" />
            <div class="form-group">
              <div class="col-sm-12">
                <input type="text" class="form-control" id="inputEmail3" placeholder="One time password (OTP)"  name="otp_number" style="height:40px;">
                <span style="font-size:12px;" ><a href="#" class="otp_resend">Resend OTP</a></span>
                <div class="formprocessgif1"><img src="images/ajax-loader.gif" align="center">&nbsp;&nbsp;Please wait sending OTP again.</div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <input type="text" class="form-control" id="fname" name="first_name" placeholder="First name" style="height:40px;">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <input type="text" class="form-control" id="inputPassword3" name="last_name" placeholder="Last name" style="height:40px;">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <div class="col-sm-6">
                  <input type="radio" name="sex" checked="checked"   value="Male" >
                  &nbsp;Male</div>
                <div class="col-sm-6">
                  <input type="radio" name="sex"  value="Female">
                  &nbsp;Female <br />
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <button type="button" id="otp_submit"  class="btn btn-info btn-lg btn-block">Signup</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-sm-5">
          <div class="social-login-separator-popup"></div>
          <div class="login-social-popup"> <a href="#"><img src="img/social/face_checkout.png" /></a> <a href="#"><img src="img/social/google_checkout.png" /></a> </div>
        </div>
      </div>
    </div>
    <!-------------- for sign up ------------------>
    <!-------------- for sign up ------------------>
  </div>
</div>
<?php } ?>
<!---------------- end from --------------->
<script type="text/javascript">



function goBack(){ window.history.back(); }



</script>
  <?php include('script.php'); ?>

<script type="text/javascript">

/*

// FOR FORGOT PASSWORD START    

$(document).click(".forg",function () {    



var mib = ('#email_mobile22').val();

alert(mib);





});

// FOR FORGOT PASSWORD END

*/





// for mobile



				

var mob = new LiveValidation('mobile_number');

				mob.add(Validate.Presence);

				mob.add( Validate.Numericality );

				mob.add( Validate.Length, { is: 10 } ); 

				mob.add( Validate.Presence, { failureMessage: "Mobile is required" } );

 

 // for email address	



				  var email = new LiveValidation('email_address');

				  email.add(Validate.Email); 

				  email.add(Validate.Presence);

				  

var password = new LiveValidation('password');

password.add( Validate.Presence, { failureMessage: "password is required" } );





</script>
<script type="text/javascript" language="javascript">

  $(document).ready(function() {

  

   $('#otp_form').addClass("hideform");

  

 /* $("#mobile_number").blur(function(){ */

   $(document).on('blur keyup',"#mobile_number",function () {      

  var msg = "";

   $('#mobile_error').text(msg);

   var mobile = $("#mobile_number").val();

 

  var version_id = $("#version_id").val();

 

 

  if(mobile!=''){

  

 $.ajax({

		url: "check.php",

		

		type: "POST",

		

 		

		data: { 'mobile_number': mobile,'data_type': 'mobile' },



 		

		success: function(data) {

 		if(data==2){ 

		

		var msg = "";

		

		$('#mobile_error').text(msg);

		

		 }else{

		

		if(data==1){

		

		var msg = "Your Mobile Number is already exists";

		

		$('#mobile_error').text(msg);

		

		}else{

		

		var msg = "";

		

		$('#mobile_error').text(msg);

		

		}

		

		}

		

		},

	});

  

  }

 

 });

  

  /*$("#email_address").blur(function(){*/

  $(document).on('blur keyup',"#email_address",function () {      

   var msg = "";

  

  $('#email_address').text(msg);

  

  

  var email = $("#email_address").val();

  

 if(email!=''){

  

 $.ajax({

		url: "check.php",

		

		type: "POST",

		

		async: true,

		

		data: { 'email_address': email,'data_type1': 'email' },



		dataType: "html",

		

		success: function(data) {



		//alert(data);

		

		if(data==1){

		

		var msg = "Your email address is already exists";

		

		$('#email_error').text(msg);

		

		

		}else{

		

		var msg = "";

		

		$('#email_error').text(msg);

		

		}

		},

	});

  }

 });

     

  

      $("#submit").click(function(event){

	  

	  var valid1 = livevalidatFormEdit();

	  

	  

//console.log("Value clicked was: "+ valid1);

if(valid1==true){







	  var mobile = $("#mobile_number").val();

  

 	  var email = $("#email_address").val();

	  

	  if((mobile!='')&&(email!='')){ 

	  

	  var m_error = $('#mobile_error').text();



	  var e_error = $('#email_error').text();

	  

	  if((m_error=='')&&(e_error=='')){ 

	

	 $("input[type=submit]").attr('disabled','disabled');

	 

	

           $(".formprocessgif").css("display","block");

   

         

		  $.post( 

		  

          	   "add_user.php",

            	

			   $("#user_form").serialize(),

	          

			   function(data){

          

              

		  	   $('#otp_form').removeClass("hideform");

			   

			   $('#user_form').addClass("hideform");

			   

			 $('#user_id').val(data);

			   

			   /*   $('#mymsg').html("A 6 digit number has been sent on your mobile number.If pin is not recive 30 sec  please click on resend button.&nbsp;&nbsp;<button class='btn btn-mini otp_resend'>Resend OTP</button>");*/

			   

			 

			  

               }

          );

        

		}else{

		

 		}

		

		}else{

		

		//alert('Email and mobile number is rquired');

		

		}

	}	 

      });

	 

	  

	   $("#otp_submit").click(function(event){

	   var user_id = $("#user_id").val();

	       $.post( 

          	   "otp.php",

			   $("#otp_form").serialize(),

			   function(data1){

			   data1 = $.trim(data1); 

			    //alert(data1);   

 				if(data1==1){

				var parentURL = window.parent.location.href

				window.open(parentURL,'_parent');

				parent.$.fancybox.close();

				}else{

	             $.alert.open('error', 'Sorry ! Your OTP does not match Try again');

				}

			   }

          );

     });

	 

////// OTP Resend



   $(document).on('click',".otp_resend",function () {        

        

	var usr_id = $("#user_id").val();

	//alert('polo');



	  $(".formprocessgif1").css("display","block");

		

 $.ajax({

		url: "send_otp2.php",

		

		type: "POST",

		

		async: true,

		

		data: { 'user_id': usr_id},



		dataType: "html",

		

		success: function(data) {

 	// alert(data);

 		$(".formprocessgif1").css("display","none");

	

		},

	});

		

    

	});	 

	  

	  

   });

   

////// for login 

/*var email_mobile1ww = new LiveValidation('email_mobile1ww');

				email_mobile1ww.add(Validate.Presence);



var password1ww = new LiveValidation('password1ww');

				password1ww.add(Validate.Presence);*/





function submitLogin() { 





	   

	   

// 	    var valid1sss = livevalidatFormWizard();

	 	    var valid1sss = true;

	

		//console.log(valid1sss);

if(valid1sss==true){   

	   	       

			   	$.ajax({

				

				url:  "login_signup1.php",



				type: "POST",



				async: true,



				data: $("#login_form").serialize(),

          	  

 				success: function(data1) {

         //alert(data1);

 			  	if(data1==1){

				

				var parentURL = window.parent.location.href

				

				window.open(parentURL,'_parent');

				

				parent.$.fancybox.close();

				

				}else if(data1==2){



                    $.alert.open('error', 'Entered password wrong');

				

				}else if(data1==3){



                    $.alert.open('error', 'Username does not exists');

				

				}

			   

			   }

          });

         

		}

   

   

      

}	 



$('#password1ww').keypress(function(e) {

    if (e.which == '13') {

        submitLogin();

    }

});



$('#login_submit').click(function(){

    submitLogin();

});

   

   /* $("#create_password").click(function(event){

	  

	  $('#forgot_password').removeClass("hideform");

			   

	  $('#login_signup').addClass("hideform");

	 

    });

	

   $("#back").click(function(event){

	  

	  $('#login_signup').removeClass("hideform");

			   

	  $('#forgot_password').addClass("hideform");

	 

    });*/

	

	/* for go back */

	



   </script>
  

</html>
