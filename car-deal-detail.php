<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<!-- Basic page needs
    ============================================ -->
<title>Spare parts, Apex Marketing</title>
<meta charset="utf-8">
<link rel="shortcut icon" type="image/png" href="ico/favicon.ico"/>
<!-- Mobile specific metas
    ============================================ -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<?php include('link.php'); ?>
<style type="text/css">
html {
	scroll-behavior: smooth;
}
body {
	font-family:'Roboto', sans-serif
}
.our-member {
	padding-top: 0;
}
.content-box {
	overflow: hidden;
	line-height: 20px;
	width: 100%;
	padding: 0;
	margin: 0;
	text-align: center;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
	position: relative;
	margin-bottom: 20px;
}
.image-cat {
	overflow: hidden;
}
.content-box img {
	width: 100%;
	display: block;
	max-width: 100%;
	box-shadow: #000 0em 0em 0em;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	-ms-transition: all 0.2s linear;
	transition: all 0.2s linear;
}
.content-box .cat-title {
	margin-top: 15px;
	margin-bottom: 15px;
	padding-top: 0;
}
.content-box .cat-title a {
	text-transform: uppercase;
	color: #007ab4;
	font-weight: 600;
	font-size: 16px;
}
#main-content, #content {
	margin-bottom: 10px;
}
</style>
</head>
<body class="res layout-1">
<div id="wrapper" class="wrapper-fluid banners-effect-5">
  <!-- Header Container  -->
  <?php include('header.php'); ?>
  <!-- //Header Container  -->
  <section style="background: #ececec;margin-bottom:30px;">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="contact.php">Car We Deal</a></li>
      </ul>
    </div>
  </section>
  <!-- Main Container  -->
  <div class="main-container container">
    <div class="row">
      <div id="honda" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Honda </h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <img src="image/catalog/Honda/CITY.jpg" title="City" alt="City"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="City" target="_self"> City </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="CRV" target="_self"> <img src="image/catalog/Honda/CRV.jpg" title="CRV" alt="CRV"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="CRV" target="_self"> CRV </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Civic" target="_self"> <img src="image/catalog/Honda/CIVIC.jpg" title="Civic" alt="Civic"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Civic" target="_self"> Civic </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Jazz" target="_self"> <img src="image/catalog/Honda/JAZZ.jpg" title="Jazz" alt="Jazz"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Jazz" target="_self"> Jazz</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Accord " target="_self"> <img src="image/catalog/Honda/ACCORD.jpg" title="Accord" alt="Accord "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Accord " target="_self"> Accord </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Brio" target="_self"> <img src="image/catalog/Honda/BRIO.jpg" title="Brio" alt="Brio"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Brio" target="_self"> Brio </a> </div>
                    </div>
                  </div>
                </div>
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Amaze " target="_self"> <img src="image/catalog/Honda/AMAZE.jpg" title="Amaze " alt="Amaze "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Amaze " target="_self"> Amaze </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="BRV" target="_self"> <img src="image/catalog/Honda/BRV.jpg" title="BRV " alt="BRV "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="BRV" target="_self"> BRV </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="WRV" target="_self"> <img src="image/catalog/Honda/WRV.jpg" title="WRV " alt="WRV "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="WRV " target="_self"> WRV </a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Toyota" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Toyota </h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Innova " target="_self"> <img src="image/catalog/Toyota/INNOVA.jpg" title="Innova " alt="Innova "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Innova " target="_self"> Innova </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Crysta" target="_self"> <img src="image/catalog/Toyota/CRYSTA.jpg" title="Crysta " alt="Crysta "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Crysta " target="_self"> Crysta </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Corolla" target="_self"> <img src="image/catalog/Toyota/COROLLA.jpg" title="Corolla" alt="Corolla"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Corolla" target="_self"> Corolla </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Altis" target="_self"> <img src="image/catalog/Toyota/ALTIS.jpg" title="Altis" alt="Altis"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Altis" target="_self"> Altis</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Etios" target="_self"> <img src="image/catalog/Toyota/ETIOS.jpg" title="Etios" alt="Etios"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Etios" target="_self"> Etios</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Liva" target="_self"> <img src="image/catalog/Toyota/LIVA.jpg" title="Liva" alt="Liva"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Liva" target="_self"> Liva</a> </div>
                    </div>
                  </div>
                </div>
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Fortuner " target="_self"> <img src="image/catalog/Toyota/FORTUNER.jpg" title="Fortuner " alt="Fortuner "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Fortuner " target="_self"> Fortuner </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Yaris " target="_self"> <img src="image/catalog/Toyota/YARIS.jpg" title="Yaris" alt="Yaris"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Yaris" target="_self"> Yaris</a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Ford" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Ford </h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Fiesta " target="_self"> <img src="image/catalog/all.jpg" title="Fiesta " alt="Fiesta "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Fiesta " target="_self"> Fiesta </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Figo" target="_self"> <img src="image/catalog/all.jpg" title="Figo" alt="Figo"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Figo" target="_self"> Figo</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Ikon" target="_self"> <img src="image/catalog/all.jpg" title="Ikon" alt="Ikon"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Ikon" target="_self"> Ikon</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Endeavour " target="_self"> <img src="image/catalog/all.jpg" title="Endeavour " alt="Endeavour "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Endeavour " target="_self"> Endeavour </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Ecosports" target="_self"> <img src="image/catalog/all.jpg" title="Ecosports" alt="Ecosports"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Ecosports" target="_self"> Ecosports</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Aspire" target="_self"> <img src="image/catalog/all.jpg" title="Aspire" alt="Aspire"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Aspire" target="_self"> Aspire</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Freestyle" target="_self"> <img src="image/catalog/all.jpg" title="Freestyle" alt="Freestyle"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Freestyle" target="_self"> Freestyle</a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Chevrolet" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Chevrolet </h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Beat" target="_self"> <img src="image/catalog/Chevrolet/beat.jpg" title="Beat" alt="Beat"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Beat" target="_self"> Beat </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Spark " target="_self"> <img src="image/catalog/Chevrolet/spark.jpg" title="Spark " alt="Spark "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Spark " target="_self"> Spark </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Sail" target="_self"> <img src="image/catalog/Chevrolet/sail.jpg" title="Sail" alt="Sail"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Sail" target="_self"> Sail</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Optra " target="_self"> <img src="image/catalog/Chevrolet/optra.jpg" title="Optra " alt="Optra "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Optra " target="_self"> Optra </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Captiva " target="_self"> <img src="image/catalog/Chevrolet/captiva.jpg" title="Captiva " alt="Captiva "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Captiva " target="_self"> Captiva </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Tavera" target="_self"> <img src="image/catalog/Chevrolet/tavera.jpg" title="Tavera" alt="Tavera"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Tavera" target="_self"> Tavera</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Enjoy " target="_self"> <img src="image/catalog/Chevrolet/enjoy.jpg" title="Enjoy " alt="Enjoy "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Enjoy " target="_self"> Enjoy </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Cruze " target="_self"> <img src="image/catalog/Chevrolet/cruze.jpg" title="Cruze " alt="Cruze "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Cruze " target="_self"> Cruze </a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Renault" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Renault </h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Duster" target="_self"> <img src="image/catalog/all.jpg" title="Duster" alt="Duster"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Duster" target="_self"> Duster</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Pulse" target="_self"> <img src="image/catalog/all.jpg" title="Pulse" alt="Pulse"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Pulse" target="_self"> Pulse</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Scala" target="_self"> <img src="image/catalog/all.jpg" title="Scala" alt="Scala"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Scala" target="_self"> Scala</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Fluence" target="_self"> <img src="image/catalog/all.jpg" title="Fluence " alt="Fluence "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Fluence " target="_self"> Fluence </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="koleos" target="_self"> <img src="image/catalog/all.jpg" title="koleos" alt="koleos"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="koleos" target="_self"> koleos</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="kwid" target="_self"> <img src="image/catalog/all.jpg" title="kwid" alt="kwid"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="kwid" target="_self"> kwid</a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Nissan" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Nissan</h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Micra" target="_self"> <img src="image/catalog/all.jpg" title="Micra" alt="Micra"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Micra" target="_self"> Micra</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Sunny" target="_self"> <img src="image/catalog/all.jpg" title="Sunny" alt="Sunny"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Sunny" target="_self"> Sunny</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Terrano" target="_self"> <img src="image/catalog/all.jpg" title="Terrano" alt="Terrano"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Terrano" target="_self"> Terrano</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Datson" target="_self"> <img src="image/catalog/all.jpg" title="Datson " alt="Datson "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Datson " target="_self"> Datson </a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Skoda" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Skoda</h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Fabia" target="_self"> <img src="image/catalog/Skoda/FABIA.jpg" title="Fabia" alt="Fabia"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Fabia" target="_self"> Fabia </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Laura" target="_self"> <img src="image/catalog/Skoda/LAURA.jpg" title="Laura" alt="Laura"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Laura" target="_self"> Laura</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Rapid" target="_self"> <img src="image/catalog/Skoda/RAPID.jpg" title="Rapid" alt="Rapid"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Rapid" target="_self"> Rapid</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Superb" target="_self"> <img src="image/catalog/Skoda/SUPERB.jpg" title="Superb " alt="Superb "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Superb " target="_self"> Superb </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Octavia" target="_self"> <img src="image/catalog/Skoda/OCTAVIA.jpg" title="Octavia " alt="Octavia "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Octavia" target="_self"> Octavia </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Yeti" target="_self"> <img src="image/catalog/Skoda/YETI.jpg" title="Yeti " alt="Yeti "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Yeti " target="_self"> Yeti </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Kodiaq" target="_self"> <img src="image/catalog/Skoda/KODIAQ.jpg" title="Kodiaq " alt="Kodiaq "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Kodiaq " target="_self"> Kodiaq </a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Hyundai" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Hyundai </h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Santro" target="_self"> <img src="image/catalog/Hyundai/Santro.jpg" title="Santro" alt="Santro"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Santro" target="_self"> Santro </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Eon" target="_self"> <img src="image/catalog/Hyundai/EON.jpg" title="Eon" alt="Eon"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Eon" target="_self"> Eon </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="I-10" target="_self"> <img src="image/catalog/Hyundai/i10.jpg" title="I-10" alt="I-10"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="I-10" target="_self"> I-10 </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="I-20" target="_self"> <img src="image/catalog/Hyundai/i20.jpg" title="I-20" alt="I-20"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="I-20" target="_self"> I-20</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="I-10 GRAND" target="_self"> <img src="image/catalog/Hyundai/Grand-i10.jpg" title="Fashion" alt="I-10 GRAND"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="I-10 GRAND" target="_self"> I-10 GRAND</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Xcent" target="_self"> <img src="image/catalog/Hyundai/Xcent.jpg" title="Xcent" alt="Xcent"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Xcent" target="_self"> Xcent </a> </div>
                    </div>
                  </div>
                </div>
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Verna" target="_self"> <img src="image/catalog/Hyundai/Verna.jpg" title="Verna" alt="Verna"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Verna" target="_self"> Verna </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Elentra" target="_self"> <img src="image/catalog/Hyundai/Elantra.jpg" title="Elentra " alt="Elentra "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Elentra" target="_self"> Elentra </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Tucson" target="_self"> <img src="image/catalog/Hyundai/Tucson.jpg" title="Tucson " alt="Tucson "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Tucson " target="_self"> Tucson </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Accent " target="_self"> <img src="image/catalog/Hyundai/Accent.jpg" title="Accent " alt="Accent "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Accent" target="_self"> Accent</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Getz" target="_self"> <img src="image/catalog/Hyundai/Getz.jpg" title="Getz" alt="Getz"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Getz" target="_self"> Getz</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Creta" target="_self"> <img src="image/catalog/Hyundai/Creta.jpg" title="Creta" alt="Creta"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Creta" target="_self"> Creta </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Santa Fe" target="_self"> <img src="image/catalog/Hyundai/Santa-Fe.jpg" title="Santa Fe" alt="Santa Fe"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Santa Fe" target="_self"> Santa Fe</a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Audi" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>AUDI</h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="A3" target="_self"> <img src="image/catalog/all.jpg" title="A3" alt="A3"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="A3" target="_self"> A3</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="A4" target="_self"> <img src="image/catalog/all.jpg" title="A4" alt="A4"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="A4" target="_self"> A4</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="A5" target="_self"> <img src="image/catalog/all.jpg" title="A5" alt="A5"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="A5" target="_self"> A5</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="A6" target="_self"> <img src="image/catalog/all.jpg" title="A6" alt="A6"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="A6" target="_self"> A6</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Q3" target="_self"> <img src="image/catalog/all.jpg" title="Q3" alt="Q3"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Q3" target="_self"> Q3</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Q5" target="_self"> <img src="image/catalog/all.jpg" title="Q5" alt="Q5"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Q5" target="_self"> Q5</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Q7" target="_self"> <img src="image/catalog/all.jpg" title="Q7" alt="Q7"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Q7" target="_self"> Q7</a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Mercedes" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Mercedes</h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="A180" target="_self"> <img src="image/catalog/all.jpg" title="A180" alt="A180"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="A180" target="_self"> A180</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="C-CLASS " target="_self"> <img src="image/catalog/all.jpg" title="C-CLASS " alt="C-CLASS "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="C-CLASS " target="_self"> C-CLASS </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="E-CLASS " target="_self"> <img src="image/catalog/all.jpg" title="E-CLASS " alt="E-CLASS "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="E-CLASS " target="_self"> E-CLASS </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="CLA " target="_self"> <img src="image/catalog/all.jpg" title="CLA " alt="CLA "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="CLA " target="_self"> CLA </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="GLA " target="_self"> <img src="image/catalog/all.jpg" title="GLA " alt="GLA "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="GLA " target="_self"> GLA </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="GLC" target="_self"> <img src="image/catalog/all.jpg" title="GLC" alt="GLC"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="GLC" target="_self"> GLC </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="GLE " target="_self"> <img src="image/catalog/all.jpg" title="GLE " alt="GLE "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="GLE " target="_self"> GLE </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="GLS" target="_self"> <img src="image/catalog/all.jpg" title="GLS" alt="GLS"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="GLS" target="_self">GLS</a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="BMW" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>BMW</h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="X1" target="_self"> <img src="image/catalog/all.jpg" title="X1" alt="X1"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="X1" target="_self"> X1</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="X3" target="_self"> <img src="image/catalog/all.jpg" title="X3" alt="X3"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="X3" target="_self"> X3</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="X5" target="_self"> <img src="image/catalog/all.jpg" title="X5" alt="X5"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="X5" target="_self"> X5</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="1-SERIES " target="_self"> <img src="image/catalog/all.jpg" title="1-SERIES " alt="1-SERIES "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="1-SERIES " target="_self"> 1-SERIES </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="3-SERIES " target="_self"> <img src="image/catalog/all.jpg" title="3-SERIES " alt="3-SERIES "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="3-SERIES " target="_self"> 3-SERIES </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="5-SERIES " target="_self"> <img src="image/catalog/all.jpg" title="5-SERIES " alt="5-SERIES "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="5-SERIES " target="_self"> 5-SERIES </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="6-SERIES " target="_self"> <img src="image/catalog/all.jpg" title="6-SERIES " alt="6-SERIES "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="6-SERIES " target="_self"> 6-SERIES </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="7-SERIES " target="_self"> <img src="image/catalog/all.jpg" title="7-SERIES " alt="7-SERIES "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="7-SERIES " target="_self">7-SERIES </a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Fiat" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>FIAT </h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Punto " target="_self"> <img src="image/catalog/all.jpg" title="Punto " alt="Punto "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Punto " target="_self"> Punto </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Linea" target="_self"> <img src="image/catalog/all.jpg" title="Linea" alt="Linea"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Linea" target="_self"> Linea</a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Mahindra" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Mahindra </h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Bolero " target="_self"> <img src="image/catalog/all.jpg" title="Bolero " alt="Bolero "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Bolero " target="_self"> Bolero </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="scorpio" target="_self"> <img src="image/catalog/all.jpg" title="scorpio" alt="scorpio"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="scorpio" target="_self"> scorpio</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Logan" target="_self"> <img src="image/catalog/all.jpg" title="Logan" alt="Logan"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Logan" target="_self"> Logan</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Xylo " target="_self"> <img src="image/catalog/all.jpg" title="Xylo " alt="Xylo "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Xylo " target="_self"> Xylo </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Quanto" target="_self"> <img src="image/catalog/all.jpg" title="Quanto" alt="Quanto"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Quanto" target="_self"> Quanto </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="XUV 500 " target="_self"> <img src="image/catalog/all.jpg" title="XUV 500 " alt="XUV 500 "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="XUV 500 " target="_self"> XUV 500 </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Verito" target="_self"> <img src="image/catalog/all.jpg" title="Verito" alt="Verito"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Verito" target="_self"> Verito </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Alturas" target="_self"> <img src="image/catalog/all.jpg" title="Alturas" alt="Alturas"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Alturas" target="_self"> Alturas </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="TUV 300 " target="_self"> <img src="image/catalog/all.jpg" title="TUV 300 " alt="TUV 300 "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="TUV 300 " target="_self"> TUV 300 </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="KUV 100" target="_self"> <img src="image/catalog/all.jpg" title="KUV 100" alt="KUV 100"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="KUV 100" target="_self"> KUV 100 </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Thar" target="_self"> <img src="image/catalog/all.jpg" title="Thar" alt="Thar"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Thar" target="_self"> Thar </a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Tata" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Tata </h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Indigo " target="_self"> <img src="image/catalog/all.jpg" title="Indigo " alt="Indigo "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Indigo " target="_self"> Indigo </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Indica" target="_self"> <img src="image/catalog/all.jpg" title="Indica" alt="Indica"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Indica" target="_self"> Indica</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Safari Dicor " target="_self"> <img src="image/catalog/all.jpg" title="Safari Dicor " alt="Safari Dicor "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Safari Dicor " target="_self"> Safari Dicor </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Storme" target="_self"> <img src="image/catalog/all.jpg" title="Storme" alt="Storme"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Storme" target="_self"> Storme </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Vista" target="_self"> <img src="image/catalog/all.jpg" title="Vista" alt="Vista"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Vista" target="_self"> Vista </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Manza" target="_self"> <img src="image/catalog/all.jpg" title="Manza" alt="Manza"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Manza" target="_self"> Manza</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Nano" target="_self"> <img src="image/catalog/all.jpg" title="Nano" alt="Nano"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Nano" target="_self"> Nano </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Tigor" target="_self"> <img src="image/catalog/all.jpg" title="Tigor" alt="Tigor"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Tigor" target="_self"> Tigor </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Tiago" target="_self"> <img src="image/catalog/all.jpg" title="Tiago" alt="Tiago"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Tiago" target="_self"> Tiago</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Zest " target="_self"> <img src="image/catalog/all.jpg" title="Zest " alt="Zest "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Zest " target="_self"> Zest </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Nexon" target="_self"> <img src="image/catalog/all.jpg" title="Nexon" alt="Nexon"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Nexon" target="_self"> Nexon </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Hexa" target="_self"> <img src="image/catalog/all.jpg" title="Hexa" alt="Hexa"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Hexa" target="_self"> Hexa </a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Maruti" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Maruti</h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Alto" target="_self"> <img src="image/catalog/Maruti/ALTO.jpg" title="Alto" alt="Alto"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Alto" target="_self"> Alto</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Wagon R" target="_self"> <img src="image/catalog/Maruti/WAGON-R.jpg" title="Wagon R" alt="Wagon R"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Wagon R" target="_self"> Wagon R</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Alto k-10 " target="_self"> <img src="image/catalog/Maruti/ALTO-K-10.jpg" title="Alto k-10 " alt="Alto k-10 "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Alto k-10 " target="_self"> Alto k-10</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="800" target="_self"> <img src="image/catalog/Maruti/MARUTI-800.jpg" title="800" alt="800"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="800" target="_self"> 800</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Omni" target="_self"> <img src="image/catalog/Maruti/OMNI.jpg" title="Omni" alt="Omni"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Omni" target="_self"> Omni </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Swift" target="_self"> <img src="image/catalog/Maruti/SWIFT.jpg" title="Swift" alt="Swift"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="XSwift" target="_self"> Swift</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Ertiga" target="_self"> <img src="image/catalog/Maruti/ERTIGA.jpg" title="Ertiga" alt="Ertiga"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Ertiga" target="_self"> Ertiga </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Estilo" target="_self"> <img src="image/catalog/Maruti/ESTILO.jpg" title="Estilo" alt="Estilo"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Estilo" target="_self"> Estilo </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Baleno" target="_self"> <img src="image/catalog/Maruti/BALENO.jpg" title="Baleno" alt="Baleno"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Baleno" target="_self"> Baleno</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Sx4" target="_self"> <img src="image/catalog/Maruti/SX4.jpg" title="Sx4" alt="Sx4"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Sx4" target="_self"> Sx4 </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Gypsy" target="_self"> <img src="image/catalog/Maruti/GYPSY.jpg" title="Gypsy" alt="Gypsy"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Gypsy" target="_self"> Gypsy </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Esteem" target="_self"> <img src="image/catalog/Maruti/ESTEEM.jpg" title="Esteem" alt="Esteem"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Esteem" target="_self"> Esteem </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Zen" target="_self"> <img src="image/catalog/Maruti/ZEN.jpg" title="Zen" alt="Zen"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Zen" target="_self"> Zen </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="brezza" target="_self"> <img src="image/catalog/Maruti/BREZZA.jpg" title="brezza" alt="brezza"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="brezza" target="_self"> brezza </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Ciaz" target="_self"> <img src="image/catalog/Maruti/CIAZ.jpg" title="Ciaz" alt="Ciaz"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Ciaz" target="_self"> Ciaz </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="A-star" target="_self"> <img src="image/catalog/Maruti/A-STAR.jpg" title="A-star" alt="A-star"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="A-star" target="_self"> A-star </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Ritz" target="_self"> <img src="image/catalog/Maruti/RITZ.jpg" title="Ritz" alt="Ritz"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Ritz" target="_self"> Ritz </a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Volkswagen" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Volkswagen</h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Polo " target="_self"> <img src="image/catalog/Volkswagen/POLO.jpg" title="Polo " alt="Polo "> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Polo " target="_self"> Polo </a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Vento" target="_self"> <img src="image/catalog/Volkswagen/VENTO.jpg" title="Vento" alt="Vento"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Vento" target="_self"> Vento</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Jetta" target="_self"> <img src="image/catalog/Volkswagen/JETTA.jpg" title="Jetta" alt="Jetta"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Jetta" target="_self"> Jetta</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Passat" target="_self"> <img src="image/catalog/Volkswagen/PASSAST.jpg" title="Passat" alt="Passat"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Passat" target="_self"> Passat</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Ameo" target="_self"> <img src="image/catalog/Volkswagen/AMEO.jpg" title="Ameo" alt="Ameo"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Ameo" target="_self"> Ameo</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Tiguan" target="_self"> <img src="image/catalog/Volkswagen/TIGUAN.jpg" title="Tiguan" alt="Tiguan"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Tiguan" target="_self"> Tiguan</a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="Mitsubishi" class="col-sm-12 item-article">
        <div class=" box-1-about">
          <div class="col-md-12 our-member">
            <div class="title-about-us">
              <h2>Mitsubishi</h2>
            </div>
            <div class="overflow-owl-slider1">
              <div class="wrapper-owl-slider1">
                <div class="row slider-ourmember">
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Lancer" target="_self"> <img src="image/catalog/all.jpg" title="Lancer" alt="Lancer"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Lancer" target="_self"> Lancer</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Cedia" target="_self"> <img src="image/catalog/all.jpg" title="Cedia" alt="Cedia"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Cedia" target="_self"> Cedia</a> </div>
                    </div>
                  </div>
                  <div class="item-about col-lg-2 col-md-2 col-sm-2">
                    <div class="content-box">
                      <div class="image-cat"> <a href="contact.php" title="Pajero" target="_self"> <img src="image/catalog/all.jpg" title="Pajero" alt="Pajero"> </a> </div>
                      <div class="cat-title"> <a href="contact.php" title="Pajero" target="_self"> Pajero</a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- //Main Container -->
  <!-- Footer Container -->
  <?php include('footer.php'); ?>
  <!-- //end Footer Container -->
</div>
<!-- Include Libs & Plugins
	============================================ -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="js/themejs/libs.js"></script>
<script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
<script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
<script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
<script type="text/javascript" src="js/datetimepicker/moment.js"></script>
<script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
<!-- Theme files
	============================================ -->
<script type="text/javascript" src="js/themejs/application.js"></script>
<script type="text/javascript" src="js/themejs/homepage.js"></script>
<script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
<script type="text/javascript" src="js/themejs/addtocart.js"></script>
<script type="text/javascript" src="js/themejs/application.js"></script>
</body>
</html>
