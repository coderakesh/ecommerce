(function($) {
                if (typeof EM == 'undefined') EM = {};
                if (typeof EM.SETTING == 'undefined') EM.SETTING = {};

                function setColumnCountGridMode() {
                    var wWin = $(window).width();
                    if (EM.SETTING.DISABLE_RESPONSIVE == 1) {
                        var sDesktop = 'emcatalog-desktop-';
                        var sDesktopSmall = 'emcatalog-desktop-small-';
                        var sTablet = 'emcatalog-tablet-';
                        var sMobile = 'emcatalog-mobile-';
                        var sGrid = $('#em-grid-mode');
                        if (wWin > 1200) {
                            sGrid.removeClass().addClass(sDesktop + '4');
                        } else {
                            if (wWin <= 1200 && wWin > 768) {
                                sGrid.removeClass().addClass(sDesktopSmall + '3');
                            } else {
                                if (wWin <= 768 && wWin > 480) {
                                    sGrid.removeClass().addClass(sTablet + '3');
                                } else {
                                    sGrid.removeClass().addClass(sMobile + '2');
                                }
                            }
                        }
                    } else {
                        var sDesktop = 'emcatalog-desktop-';
                        var sGrid = $('#em-grid-mode');
                        sGrid.removeClass().addClass(sDesktop + '3');
                    }
                };


        //Ready Function
               $(document).ready(function() {
        setColumnCountGridMode();
               });

             
                $(window).resize($.throttle(300, function() {
                    setColumnCountGridMode();
                }));
            })(jQuery);
 
 function show_product(){ 
	cache:false; 
	 globalNext=12;
$.ajax({
            type: 'post',
            url: 'product_load_discount.php?short_id=lh',
            data: $('#hidden_txt').serialize(),
            success: function (response) {  // alert(response);
    			$('#product_load').css('display','none');
				$('#loadpage').append($(response).fadeIn('slow'));
            }
          });
  	}	
	
	function price_low_high_load()
	{
	cache:false;
	 globalNext=12;
		 $.ajax({
            type: 'post',
            url: 'product_load_discount.php?short_id=lh&show_more_post=0',
		    data: $('#hidden_txt').serialize(),
            success: function (response) { 
			
			// alert(response);
			$("#loadpage").empty();
    			$('#product_load').css('display','none');
				$('#loadpage').append($(response).fadeIn('slow'));
            }
          });
		
		
		}
	function price_high_low_load()
	{
	cache:false;
	 globalNext=12;
		 $.ajax({
            type: 'post',
            url: 'product_load_discount.php?short_id=hl&show_more_post=0',
		    data: $('#hidden_txt').serialize(),
            success: function (response) { 
			
			// alert(response);
			$("#loadpage").empty();
    			$('#product_load').css('display','none');
				$('#loadpage').append($(response).fadeIn('slow'));
            }
          });
		
		}
	 
	 	function price_discount_load()
	{
		 globalNext=12;
		 $.ajax({
            type: 'post',
            url: 'product_load_discount.php?short_id=dsc&show_more_post=0',
      data: $('#hidden_txt').serialize(),
            success: function (response) { 
			
			// alert(response);
			$("#loadpage").empty();
    			$('#product_load').css('display','none');
				$('#loadpage').append($(response).fadeIn('slow'));
            }
          });
		
		
		}
	 
	 	 	function fresh_arrival_load()
	{
		cache:false;
 globalNext=12;
		 $.ajax({
            type: 'post',
            url: 'product_load_discount.php?short_id=fresh&show_more_post=0',
		    data: $('#hidden_txt').serialize(),
            success: function (response) { 
			
			// alert(response);
			$("#loadpage").empty();
    			$('#product_load').css('display','none');
				$('#loadpage').append($(response).fadeIn('slow'));
            }
          });
		
		
		}
		
function min_max_price(){ 
cache:false;
	 var catid = $("#catid").val();;
			var file = 'product_price_range';
 $.post(file+".php?&catid="+catid, {
 			}, function(response){  
   			 	$('#min_max_price').append($(response));
					   price_manage();

 				});
	}			

 function filterSystem(minPrice, maxPrice) {
	 
 $('#min_price').val(minPrice);
 $('#max_price').val(maxPrice); 
	show_product();
 
}
 
function price_manage(){               	   
 	var min_price =  parseInt($('#min_price').val()) ; 
 			var max_price = parseInt($('#max_price').val());  	 
        $('#slider-range').slider({
            range: true,
            min: min_price,
            max: max_price,
            values: [min_price, max_price],
            stop: function(event, ui) {  
			$('#loadpage').html('');
                $( "#min_amount" ).text( "" + ui.values[ 0 ]);
                $( "#max_amount" ).text( "" + ui.values[ 1 ]);   
                var mi = ui.values[ 0 ];
                var mx = ui.values[ 1 ];  
            filterSystem(mi, mx);
 			  
            }
        });
     /* $( "#amount" ).val( "Rs." + $( "#slider-range" ).slider( "values", 0 ) +
      " - Rs." + $( "#slider-range" ).slider( "values", 1 ) );*/
}
<!-- for scroller -->
var globalNext = 12;
var scroll_var = 'true';
var scrollListener = function () { 
    $(window).one("scroll", function () {
 var checkScrollFlag = $($(".checkscroll")[$(".checkscroll").length - 1]).val();
 if((checkScrollFlag=='true') && (scroll_var=='true')){
 scroll_var= 'false'; cache:false;
  var catid = $("#catid").val();;
  var short_id = $("#short_id").val();;
  var filter_val = $("#filter_val").val();;
	 				var file = 'product_load_discount';
	var min_price =  parseInt($('#min_price').val()) ; 
 			var max_price = parseInt($('#max_price').val());  	  
 			  if($(window).scrollTop() + $(window).height() <= $(document).height()-20){ 
     $(".formprocessgif").css("display","block");
 $.post(file+".php?show_more_post="+globalNext+'&short_id='+short_id, {
 			}, function(response){   
                            $(".formprocessgif").css("display","none");
  		//	$('#bottomMoreButton').remove();
				//checkScroll = response.value; --- assing value from request to the global variable, alos in the data base check if there are next rows or not if there are bext rows pass value as true or send as false
 			globalNext = globalNext + 12;
	$('#loadpage').append($(response).fadeIn('slow'));
 scroll_var = 'true';
 
			});
 }
 }
 		//unbinds itself every time it fires
      
        setTimeout(scrollListener, 150); //rebinds itself after 200ms
    });
};
  $(document).ready(function() {
 min_max_price();
show_product();
   
      scrollListener();

 <!-- for scroller -->
 		 <!-- for scroller -->
  });
 <!---- for filter checkbox --->
			
 function dumpInArray(){
            var arr = [];
           $('.modal-choices input[type="checkbox"]:checked').each(function(){
              arr.push($(this).val());
           }); cache:false;
           return arr; 
        }

        $('.select-roles').click(function () { 	
	 globalNext=12;

           $('#filter_val').val(dumpInArray().join(","));
$.ajax({
            type: 'post',
            url: 'product_load_discount.php?short_id=lh',
            data: $('#hidden_txt').serialize(),
            success: function (response) {   //alert(response);
    					$('#loadpage').empty();    				
	$('#product_load').css('display','none');
				$('#loadpage').append($(response).fadeIn('slow'));
            }
          }); });
  <!---- for filter checkbox --->

function add_remove_attr(){
var ssss =	$('#filter_val').val();
	alert(ssss);
	}
 