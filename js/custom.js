var index = 0,
    count = 0,
    arr = new Array;

function shok() { 
    createCookie("did", 1, 1), null != readCookie("cart") && "" != readCookie("cart") && (0 != (arr = JSON.parse(readCookie("cart"))).length ? cartIn(arr) : $(".proceed").text("Your cart is empty"))
}

function cartIn(e) {
    for (var t = 0, i = 0; i < e.length; i++) {
        var a = e[i].split("#");
        t += Number(a[2]), index += Number(a[4]), count++
    }
	
    for ($(".items span").text(index), $(".total").text(t), $("#CartItems").html(""), i = 0; i < e.length; i++) a = e[i].split("#"), $('<tr id="cart' + a[0] + '" class="item-in-cart clearfix"><td class="text-center image" style="width:70px"><a href="product.php"><img src="' + a[1] + '" style="width:80px" alt="Coil Over Kit" title="Coil Over Kit" class="preview"></a></td><td class="text-left"> <a class="cart_product_name" href="product.php">' + a[3] + '</a></td><td class="text-center">x' + a[4] + '<input type="hidden" value=' + a[8] + ' id="comb_id" ><input type="hidden" value=' + a[9] + ' class="final_weight' + a[0] + '" ><input type="hidden" value=' + a[10] + ' class="final_weight_outside' + a[0] + '" ></td><td class="text-center"><span class=" fa fa-inr">' + a[2] + '</span></td><td class="text-right"><a id="' + a[0] + "#" + a[5] + '"  onClick="removeItems(this.id)"  class="fa fa-times fa-delete"></a></td></tr>').appendTo("#CartItems");
    $(".proceed").html(""), $(".proceed").html('<a href="mycart.php" style="" class="btn mybutton pull-right bold   fancybox fancybox.iframe ">CHECKOUT <i class="icon-shopping-cart"></i></a>')
}

function changeCart(e) {  
    for (var t = $(".final_combo").val(), i = $("#comb_id").val(), a = $(".weight").val(), r = $(".vol_weight").val(), n = $(".weight_outside").val(), o = 0, s = 0; s < arr.length; s++) {
        if ((p = arr[s].split("#"))[0] == e) {
            var l = Number(p[2]),
                c = $("#" + e + "price").text();
            l += Number(c);
            var d = Number(p[4]);
            d += 1, a < r ? (final_weight = r, final_weight_outside = r) : (final_weight = a, final_weight_outside = n), arr[s] = p[0] + "#" + p[1] + "#" + l.toString() + "#" + p[3] + "#" + d + "#1#1#" + t + "#" + i + "#" + final_weight + "#" + final_weight_outside, o = 1, index += 1, $(".items span").text(index)
        }
    }
    if (0 == o) {
        a < r ? (final_weight = r, final_weight_outside = r) : (final_weight = a, final_weight_outside = n);
        var m = e;
        m += "#" + $("#" + e + "img").attr("src"), m += "#" + $("#" + e + "price").text(), m += "#" + $("#" + e + "name").text(), m += "#1", m += "#2", m += "#3", m += "#" + t, m += "#" + i, m += "#" + final_weight, m += "#" + final_weight_outside, arr[count] = m, ++index, count++, $(".items span").text(index)
    }
    var u = 0;
    for (s = 0; s < arr.length; s++) {
        var p = arr[s].split("#");
        u += Number(p[2])
    }
    for ($(".total").text(u), $("#CartItems").html(""), s = 0; s < arr.length; s++) p = arr[s].split("#"), $('<tr id="cart' + p[0] + '" class="item-in-cart clearfix"><td class="text-center image" style="width:70px"><a href="product.php"><img src="' + p[1] + '" style="width:80px" alt="Coil Over Kit" title="Coil Over Kit" class="preview"></a></td><td class="text-left"> <a class="cart_product_name" href="product.php">' + p[3] + '</a></td><td class="text-center">x' + p[4] + '<input type="hidden" value=' + p[8] + ' id="comb_id" ><input type="hidden" value=' + p[9] + ' class="final_weight' + p[0] + '" ><input type="hidden" value=' + p[10] + ' class="final_weight_outside' + p[0] + '" ></td><td class="text-center"><span class=" fa fa-inr">' + p[2] + '</span></td><td class="text-right"><a id="' + p[0] + "#" + p[5] + '"  onClick="removeItems(this.id)"  class="fa fa-times fa-delete"></a></td></tr>').appendTo("#CartItems");
    $(".proceed").html(""), $(".proceed").html('<a href="mycart.php" class="btn  btn-primary pull-right bold higher">CHECKOUT <i class="icon-shopping-cart"></i></a>'), $(window).scrollTop(0);
    var h = JSON.stringify(arr); 
    createCookie("cart", h, 1)
}

function changeCartWish(e) {
    for (var t = $(".final_combo" + e).val(), i = $("#comb_id" + e).val(), a = 0, r = 0; r < arr.length; r++) {
        if ((d = arr[r].split("#"))[0] == e) {
            var n = Number(d[2]),
                o = $("#" + e + "price").text();
            n += Number(o);
            var s = Number(d[4]);
            s += 1, arr[r] = d[0] + "#" + d[1] + "#" + n.toString() + "#" + d[3] + "#" + s + "#1#1#" + t + "#" + i, a = 1, index += 1, $(".items span").text(index)
        }
    }
    if (0 == a) {
        var l = e;
        l += "#" + $("#" + e + "img").attr("src"), l += "#" + $("#" + e + "price").text(), l += "#" + $("#" + e + "name").text(), l += "#1", l += "#2", l += "#3", l += "#" + t, arr[count] = l, ++index, count++, $(".items span").text(index)
    }
    var c = 0;
    for (r = 0; r < arr.length; r++) {
        var d = arr[r].split("#");
        c += Number(d[2])
    }
    for ($(".total").text(c), $("#CartItems").html(""), r = 0; r < arr.length; r++) d = arr[r].split("#"), $('<div id="cart' + d[0] + '" class="item-in-cart clearfix"><div class="image"><img src="' + d[1] + '" width="60" height="60" alt="cart item" /></div><div class="desc detail"><a href="product.html">' + d[3] + '</a></div> <div class="amt-detail"><div class="quantity"><span class="light-clr qty"> Qty: ' + d[4] + ' &nbsp; <a id="' + d[0] + "#" + d[5] + '" href="#" onclick="removeItems(this.id)" class="icon-remove-sign" title="Remove Item"></a></span></div>  <div class="price"> <strong>Rs ' + d[2] + "</strong></div></div></div>").appendTo("#CartItems");
    $(".proceed").html(""), $(".proceed").html('<a href="mycart.php" class="btn  btn-primary pull-right bold higher">CHECKOUT <i class="icon-shopping-cart"></i></a>');
    var m = JSON.stringify(arr);
    createCookie("cart", m, 1), removeWishItems(e)
}

function removeItems(e) {
    for (var t = e.split("#"), i = 1e3, a = 0, r = 0; r < arr.length; r++) {
        var n = arr[r].split("#");
        a += Number(n[2]), n[0] == t[0] && n[5] == t[1] && (i = r, index -= Number(n[4]))
    }
    if (1e3 != i) {
        arr[i];
        n = arr[i].split("#"), $(".items span").text(index);
        var o = Number(n[2]);
        $(".total").text(a - o), $("#cart" + n[0]).css("display", "none"), arr.splice(i, 1);
        arr.length;
        count--, 0 == arr.length && ($(".total").text(0), $(".items span").text(0), $(".proceed").html("Your cart is empty"), $("#cart" + n[0]).css("display", "none"), index = 0, count = 0)
    }
    var s = JSON.stringify(arr);
    createCookie("cart", s, 1)
}

function removeWishItems(e) {
    $.ajax({
        method: "POST",
        url: "delete_wishitem.php?pid=" + e
    }).done(function(t) {
        $("#wish_" + e).css("display", "none")
    })
}
$(document).ready(function() {
    shok()
});