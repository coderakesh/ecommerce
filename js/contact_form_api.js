
    
  $(document).ready(function(){
    
    $('#name').val('');
    $('#email').val('');
    $('#mobile').val('');
    $('#message').val('');
    $('#captcha').val('');

    $(".imgcaptcha").attr("src","demo_captcha.php?_="+((new Date()).getTime()));
    $(".refresh").click(function () {
        $(".imgcaptcha").attr("src","demo_captcha.php?_="+((new Date()).getTime()));
    });

   /* =================== Start onkeyup ====================== */
	
        $('#name').keyup(function(){
        if(!$('#name').val().match('^([a-zA-Z]+([ ]?[a-zA-Z]+)*)+$'))  {
                    $("#errorname").html('Please enter valid name.');
                }
          else{
              $("#errorname").html('');
          }
      });

       $('#email').keyup(function(){
        if(!$('#email').val().match('^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'))  {
                    $("#erroremail").html('Please enter valid email.');
                }
          else{
              $("#erroremail").html('');
          }
      });
      
     

       $('#mobile').keyup(function(){
        if(!$('#mobile').val().match('[7-9]{1,1}[0-9]{9,9}')){
                  $("#errormobile").html('Please enter valid mobile number.');
                }
          else{
               if($('#mobile').val().length > 10) 
                   {
                           $("#errormobile").html('Please enter 10 digit mobile number.');
                   }
                   else
                   {
                        $("#errormobile").html('');
                    }
          }
      });
      
        $("#message").keyup(function(){ 
         if(!$("#message").val().match('^[a-zA-Z0-9]+.*[^ ]+$'))  {
            $("#errormessage").html('Please enter valid message.');
       }
       else{
           if($("#message").val().length > 300)
           {
             $("#errormessage").html('Please enter maximum 300 word.');
           }
           else
           {
               $("#errormessage").html('');
           }
       }
   });
   

  /* ========================== onkeyup end ========================== */

   /* =================== Start blur ====================== */
	
        $('#name').blur(function(){
        if(!$('#name').val().match('^([a-zA-Z]+([ ]?[a-zA-Z]+)*)+$'))  {
                    $("#errorname").html('Please enter valid name.');
                }
          else{
              $("#errorname").html('');
          }
      });

       $('#email').blur(function(){
        if(!$('#email').val().match('^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'))  {
                    $("#erroremail").html('Please enter valid email.');
                }
          else{
              $("#erroremail").html('');
          }
      });
      
     

       $('#mobile').blur(function(){
        if(!$('#mobile').val().match('[7-9]{1,1}[0-9]{9,9}')){
                  $("#errormobile").html('Please enter valid mobile number.');
                }
          else{
               if($('#mobile').val().length > 10) 
                   {
                           $("#errormobile").html('Please enter 10 digit mobile number.');
                   }
                   else
                   {
                        $("#errormobile").html('');
                    }
          }
      });
      
       $("#message").blur(function(){ 
         if(!$("#message").val().match('^[a-zA-Z0-9]+.*[^ ]+$'))  {
            $("#errormessage").html('Please enter valid message.');
       }
       else{
           if($("#message").val().length > 300)
           {
             $("#errormessage").html('Please enter maximum 300 word.');
           }
           else
           {
               $("#errormessage").html('');
           }
       }
   });
   

  /* ==========================  blur end ========================== */
 
}); // document reddy  close	
 
 $('#subbtn').click(function() {
     var flag='true';        
  
      if(!$('#name').val().match('^([a-zA-Z]+([ ]?[a-zA-Z]+)*)+$'))  {
                $("#errorname").html('Please enter valid name');
                flag='false';
            }
      else
      {
          $("#errorname").html('');
      }
      
       if(!$('#email').val().match('^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'))  {
                $("#erroremail").html('Please enter valid email.');
                flag='false';
            }
      else
      {
          $("#erroremail").html('');
      }
      
       
      if(!$('#mobile').val().match('[7-9]{1}[0-9]{9}'))  {
              $("#errormobile").html('Please enter valid mobile number.');
               flag='false';
       }  
       else
       {
           if($('#mobile').val().length > 10) 
               {
                       $("#errormobile").html('Please enter 10 digit mobile number.');
                       flag='false';
               }
               else
               {
                    $("#errormobile").html('');
                }
       }
       
       
     
         if(!$("#message").val().match('^[a-zA-Z0-9]+.*[^ ]+$'))  {
            $("#errormessage").html('Please enter valid message.');
       }
       else{
           if($("#message").val().length > 300)
           {
             $("#errormessage").html('Please enter maximum 300 word.');
           }
           else
           {
               $("#errormessage").html('');
           }
       }
  
   
     
                        
          if(flag=='true')                                                                                     
           { 
               $('#errorcaptcha').html('');
             $("#load").css("display", "block");  
             $.post("submit_demo_captcha.php",
               {
                   name : $('#name').val(),
                   email : $('#email').val(),
                   mobile : $('#mobile').val(),
                   message : $('#message').val(),
                   captcha : $('#captcha').val()
                   
               },
               function(response)
               {
                    if(response!=0)
                    {
                        $('#errorcaptcha').html('');
                        $("#load").css("display", "none");
                        alert("Thanks! for contacting we will contact you soon.");
 			
                        
                        $('#name').val('');
                        $('#email').val('');
                        $('#mobile').val('');
                        $('#message').val('');
                        $('#captcha').val('');
			$(".imgcaptcha").attr("src","demo_captcha.php?_="+((new Date()).getTime()));
                          
                    }else
                    {
                        $("#load").css("display", "none");
                        $('#errorcaptcha').html('wrong captcha code!');
                    }
                });      // Ajax close              
            }   
           
 
    }); // subbtn close   */



    

