/*
 * Galathemes
 *
 * @license commercial software
 * @copyright (c) 2014 Codespot Software JSC - Galathemes.com. (http://www.galathemes.com)
 */
/*
(function($) {
    if (typeof EM == 'undefined') EM = {};
    if (typeof EM.SETTING == 'undefined') EM.SETTING = {};
    var domLoaded = false;
    // details tabs
    function decorateProductCollateralTabs() {
        var sTab = $('.em-details-tabs');
        if (sTab.length) {
            sTab.each(function(i) {
                $(this).prepend("<ul class='tabs-control'></ul>");
                var controlTab = $('.tabs-control', this);
                $('.box-collateral', $(this)).each(function(j) {
                    id = 'em-details-tab-' + i + '-' + j;
                    $(this).attr('id', id);
                    controlTab.append('<li><a href="#' + id + '">' + $('h2', this).html() + '</a></li>');
                });
                $(this).responsiveTabs({
                    animation: 'fade',
                    startCollapsed:'accordion', 
                });
            });
        }
    };

    //scroll review
    function ScrollToElement(e) {
        if ($(e).size()) {
            $('html, body').animate({
                scrollTop: $(e).offset().top
            }, 500);
        } else {
            return false;
        }
        return true;
    };

    function ScrollToReview() {
        var sReview = $('.product-view .product-essential ');
        var sClick1 = $('.link_review_list', sReview);
        if(sClick1.length){
            sClick1.click(function(e) {
                if (ScrollToElement('#customer_review_list')) {
                    e.preventDefault();
                }
            });
        }
        
        var sClick2 = $('.link_review_form', sReview);
        if(sClick2.length){
            sClick2.click(function(e) {
                if (ScrollToElement('#customer_review_form')) {
                    e.preventDefault();
                }
            });
        }
    };
    
    function emFixedProductInfo() {
        var sInfo = $('.em-product-view-secondary').find('.product-shop');
        var $_colmain = $('.em-col-main');
        var $_primary = $('.em-product-view-primary');
        var sInfoW = $_colmain.width() - $_primary.outerWidth();
        if (EM.SETTING.STICKY_MENU_SEARCH_CART != 0) {
            var $_hmenu = 70;
        }else{
            var $_hmenu = 0;
        }
        var $_hButton = $('.product-options-bottom').height(); 
        if (sInfo.length) {            
            var sticky_info = function() {
                var wWindow = $(window).width();
                var scroll_top = $(window).scrollTop();
                var posTop = $('#em-product-shop-pos-top').offset().top;
                var posBottom = $('#em-product-shop-pos-bottom').offset().top - $(window).height() + 50;
                if(wWindow>1024 && !isMobile){
                    if ( (scroll_top > posTop) && (scroll_top < posBottom) ) {
                        if (!sInfo.hasClass('product-shop-fixed-top')) {  
                            sInfo.addClass('product-shop-fixed-top'); 
                            if($('#product-options-wrapper').length){
                                $_hProductOption = $(window).height() - $('#em-product-info-basic').height() - $_hButton - 40 - $_hmenu;
                                $('#product-options-wrapper').css({
                                   'height' : $_hProductOption + 'px',
                                   'overflow-y' : 'auto',
                                });
                            }   
                            sInfo.width(sInfoW);                        
                        }
                    } else {
                        if (sInfo.hasClass('product-shop-fixed-top')) { 
                            sInfo.removeClass('product-shop-fixed-top');
                            if($('#product-options-wrapper').length){                            
                                $('#product-options-wrapper').css({
                                   'height' : '',
                                   'overflow-y' : 'hidden',
                                });
                            }
                            sInfo.css('width',''); 
                        }
                    }
                }else{
                    if (sInfo.hasClass('product-shop-fixed-top')) { 
                        sInfo.removeClass('product-shop-fixed-top');
                        if($('#product-options-wrapper').length){                            
                            $('#product-options-wrapper').css({
                               'height' : '',
                               'overflow-y' : 'hidden',
                            });
                        } 
                        sInfo.css('width','');
                    }
                }
                
            };
            $(window).scroll(function() {
                sticky_info();
            });   
            sticky_info();
        }        
    };
    
    //Ready Function
    $(document).ready(function() {
        domLoaded = true;
        var wWindow = $(window).width();
        // details
        if (EM.SETTING.USE_TAB != 0) {
            decorateProductCollateralTabs();
        }
        ScrollToReview();
              
    });
    $(window).load(function() {
        if(layout=='1column'){
            emFixedProductInfo();
        }
        setEqualElement('#em-wrapper-related','.product-name');       
        setEqualElement('#em-wrapper-upsell','.product-name'); 
    });
    var tmresize;
    $(window).resize($.throttle(300,function(){        
        clearTimeout(tmresize);
        tmresize = setTimeout(function(){
            if(layout=='1column'){
                emFixedProductInfo();
            }
        },200);                	   
    })); 
})(jQuery);  */        
            jQuery('.cloud-zoom-gallery').click(function() {
                jQuery('#zoom-btn').attr('href', this.href);
            });
            function doSliderMoreview() {
                var owl = $("ul.em-moreviews-slider");
                if (owl.length) {
                    owl.owlCarousel({
                        // Navigation
                        navigation: true,
                        navigationText: ["Previous", "Next"],
                        pagination: false,
                        paginationNumbers: false,


                        // Responsive
                        responsive: true,
                        items: 7,
                        /*items above 1200px browser width*/
                        itemsDesktop: [1200, 4],
                        /*//items between 1199px and 981px*/
                        itemsDesktopSmall: [992, 7],
                        itemsTablet: [768, 3],
                        itemsMobile: [480, 2],
                        // CSS Styles
                        baseClass: "owl-carousel",
                        theme: "owl-theme",
                        transitionStyle: false,
                        addClassActive: true,
                    });
                }
            }

            function changeQty(increase) {
                var qty = parseInt($('#qty').val());
                if (!isNaN(qty)) {
                    console.log(qty)
                    qty = increase ? qty + 1 : (qty > 1 ? qty - 1 : 1);
                    $('#qty').val(qty);
                }
            }

            /* Related Product */
            var relatedProductsCheckFlag = false;

            function selectAllRelated(txt) {
                if (relatedProductsCheckFlag == false) {
                    $('.related-checkbox').each(function(elem) {
 elem.checked = true;                     });
                    relatedProductsCheckFlag = true;
                    txt.innerHTML = "unselect all";
                } else {
                    $('.related-checkbox').each(function(elem) {
                        elem.checked = false;
                    });
                    relatedProductsCheckFlag = false;
                    txt.innerHTML = "select all";
                }
                addRelatedToProduct();
            };
            function addRelatedToProduct() {
                var checkboxes = $('.related-checkbox');
                var values = [];
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].checked) values.push(checkboxes[i].value);
                }
                if ($('related-products-field')) {
                    $('related-products-field').value = values.join(',');
                }
            };

            (function($) {
                $(window).load(function() {
                    if (!isPhone) {
                        $('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
                    }
                    doSliderMoreview();

                    /* Related Slider */
                    var owl = $('#em-related').find('.em-related-slider');
                    if (owl.length) {
                        owl.owlCarousel({
                            // Navigation
                            navigation: true,
                            navigationText: ["Previous", "Next"],
                            pagination: false,
                            paginationNumbers: false,
                            // Responsive
                            responsive: true,
                            items: 4,
                            /*items above 1200px browser width*/
                            itemsDesktop: [1200, 4],
                            /*//items between 1199px and 981px*/
                            itemsDesktopSmall: [992, 3],
                            itemsTablet: [768, 3],
                            itemsMobile: [480, 2],
                            // CSS Styles
                            baseClass: "owl-carousel",
                            theme: "owl-theme",
                            transitionStyle: false,
                            addClassActive: true,
                            afterMove: function() {
                                var $_img = owl.find('img.em-img-lazy');
                                if ($_img.length) {
                                    var timeout = setTimeout(function() {
                                        $_img.trigger("appear");
                                    }, 200);
                                }
                            },
                        });
                    }

                    /* Related Checkbox */
                    $( ".related-checkbox" ).on( "click", function() {
                        addRelatedToProduct()
                    });
                });
            })(jQuery);
 function get_com_val(id,attr,comb_id){    
 $('#comb_id').val(comb_id);
 $('#attr_'+attr).val(id);
stor_val =  $('#attr_'+attr).val();
			$('#valid_msg').text('');

  $('.add_'+attr).removeClass('borderlist');
   $('#'+stor_val).addClass('borderlist'); //do something
   
   
   	var i; var j =0; var val_id='';
	pro_id = $(".add_cart").attr('id');
	combo_count = $("#combo_count").val(); 
		if(combo_count!=0){
		for(i=1;i<=combo_count; i++){	
	id_arr = $('.checkcombo_'+i).attr('id');
	atr_id= $('.atrval_'+i).text();
	 atr_val = $('#attr_'+atr_id).val();
	 if(atr_val!=0)
	 { 		j++;	
val_id+=atr_val+ ".."; 

	 }								
  	}    val_id =  val_id.slice(0, -2);
	   // alert(j); 
		$('.final_combo').val(val_id); 
  			$('#valid_msg').text();
 		  }
 	}

 $('.buy_now').click( function(){   
	var i; var j =0; var val_id='';
	pro_id = $(".add_cart").attr('id');
	combo_count = $("#combo_count").val(); 
		if(combo_count!=0){
		for(i=1;i<=combo_count; i++){	
	id_arr = $('.checkcombo_'+i).attr('id');
	atr_id= $('.atrval_'+i).text();
	 atr_val = $('#attr_'+atr_id).val(); 
	 if(atr_val!=0)
	 { 		j++;	
val_id+=atr_val+ ".."; 

	 }								
  	}    val_id =  val_id.slice(0, -2);
	 if(combo_count==j){  // alert(j); 
		$('.final_combo').val(val_id);   
	 	$('.add_cart').addClass(' fancybox fancybox.iframe  '); 
 			$('.buy_now').attr('href','mycart.php');
			$('#valid_msg').text();
 	changeCart(pro_id);	
        		} else{
						$('.buy_now').attr('href','javascript:void(0);');
$('#valid_msg').text('Please select  combination');
                    $.alert.open('alert', 'Please select  combination');

 			}
	} else{
	//$('.add_cart').addClass(' fancybox fancybox.iframe  '); 
		changeCart(pro_id);
$('.buy_now').attr('href','mycart.php');
	}
}); 
 
$('.add_cart').click( function(){   
	var i; var j =0; var val_id='';
	pro_id = $(".add_cart").attr('id');
	combo_count = $("#combo_count").val(); 
		if(combo_count!=0){
		for(i=1;i<=combo_count; i++){	
	id_arr = $('.checkcombo_'+i).attr('id');
	atr_id= $('.atrval_'+i).text();
	 atr_val = $('#attr_'+atr_id).val();
	 if(atr_val!=0)
	 { 		j++;	
val_id+=atr_val+ ".."; 

	 }								
  	}    val_id =  val_id.slice(0, -2);
	 if(combo_count==j){  // alert(j); 
		$('.final_combo').val(val_id); 
  			$('#valid_msg').text();
 	changeCart(pro_id);	
		} else{
 $('.add_cart').attr('href','javascript:void(0);');
$('#valid_msg').text('Please select  size');
                     $.alert.open('alert', 'Please select  size');
			}
	} else{
  	changeCart(pro_id);
	}
});

$('#pin_click').click(function(){ 
							    
							  
var pincode = $('#pcode').val();
   $.ajax({
 type:"POST",
 url:'checkout_ajax.php?pincode='+pincode+'&type=check_pincode',
  success: function(result){ 			// alert(result);					  
 if(result!=0){ 
 $("#msg").css("color", "#00EA3A");
 $('#msg').text('Item  available at this location');
 }  else {  $("#msg").css("color", "#FF0000");
 $('#msg').text('Item not available at this location, please try another pincode');
 }  
 
  }

 });
 
							  
							  });
 
 
   function get_com_select(attr,pid){    
  // alert($(".atr_id_"+attr).closest('select').next().find('select'));
  var cls_name = $('#attrdec_'+attr).attr('class');
     var ret1 = cls_name.split("_");
    if($('#atrs'+attr).hasClass( "related_1" ) ) {
     $('.blanck_com').val(''); }
 nums = parseFloat(ret1[1])+parseFloat(1);
 // $('#attr_'+attr).val(id);
var atr_val =$('.ser_'+nums).val();/// $( ".atr_id_"+attr+" option:selected" ).val();
atr_selct = $( ".atr_id_"+attr+" option:selected" ).val();
$('#attr_'+attr).val(atr_selct);

if(atr_val!= undefined ){
	    $(".atr_id_"+atr_val+" option").remove();
 	ats_pass ='';
	 cnt_slct = $('select option[value!=""]:selected').length ; 
 for(var k=1; k<=cnt_slct; k++)
   { atw_val = $('.related_'+k).val();
   if(atw_val!=null)
	ats_pass+=atw_val+ ".."; 
    }  
$.ajax({
 type:"POST",
 url:'get_com_select.php?attr='+atr_val+'&pid='+pid+'&atr_selct='+ats_pass+'&types=combo',
   dataType:'json',
  success: function(e){ 
  var output = [];
 	  output.push('<option value="">--Select--</option>');
 $.each(e, function(i, item) { 
  $('#comb_id').val(item.combinationid);  
 /*$('.price_combo_'+pid).html(item.price); */
 output.push('<option value="'+ item.attrvalueid +'">'+ item.attrvaluename +'</option>');
 });$(".atr_id_"+atr_val).html(output.join(''));
 }

 });
   }
 	
	else {
 	 	ats_pass ='';
	 cnt_slct = $('select option[value!=""]:selected').length ; 
 for(var k=1; k<=cnt_slct; k++)
   { atw_val = $('.related_'+k).val();
   if(atw_val!=null)
	ats_pass+=atw_val+ ".."; 
    }  
	var combs = $('#comb_id').val();  
		  $.ajax({
 type:"POST",
 url:'get_com_select.php?pid='+pid+'&atr_selct='+ats_pass+'&types=price',
   dataType:'json',
  success: function(e){ console.log(e);   $.each(e, function(i, item) { 
   $('#comb_id').val(item.combinationid);  
 $('.price_combo_'+pid).html(item.price); }); }   

 });
		}
stor_val =  $('#attr_'+attr).val();
			$('#valid_msg').text('');

  $('.add_'+attr).removeClass('borderlist');
   $('#'+stor_val).addClass('borderlist'); //do something
   
   
   	var i; var j =0; var val_id='';
	pro_id = $(".add_cart").attr('id');
	combo_count = $("#combo_count").val(); 
		if(combo_count!=0){
		for(i=1;i<=combo_count; i++){	
	id_arr = $('.checkcombo_'+i).attr('id');
	atr_id= $('.atrval_'+i).text();
	 atr_val1= $('#attr_'+atr_id).val();
	 if(atr_val1!=0)
	 { 		j++;	
val_id+=atr_val1+ ".."; 

	 }								
  	}    val_id =  val_id.slice(0, -2);
	   // alert(j); 
		$('.final_combo').val(val_id); 
  			$('#valid_msg').text();
 		  } 
 	}