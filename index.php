      <!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>

    <!-- Basic page needs
    ============================================ -->
    <title>Shoes Online - Buy Shoes & Footwear Online for Men & Women in Bhopal</title>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/png" href="ico/favicon.ico" />
    <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0">

    <?php include('link.php'); ?>
    <style type="text/css">
    body {
        font-family: 'Roboto', sans-serif
    }

    .layout-2.common-home #content .module h3.modtitle span:after {
        display: none;
    }

    .typeheader-1 .header-middle .middle-right {
        padding-top: 20px;
    }

    .font-12 {
        font-size: 12px;
    }

    .product-price a:hover {
        color: #ff9600;
    }

    @media all and (min-width: 480px) and (max-width: 768px) {
        .typeheader-1 .header-middle .middle-right {
            padding-top: 0;
        }

        .block-infos {
            margin-top: 0px;
        }

        .block-infos li .inner {
            text-align: center;
        }

        .inner img {
            margin: 0 auto;
        }
    }

    @media all and (max-width: 480px) {
        .block-infos li {
            width: 49%;
        }
    }
    </style>

</head>

<body class="common-home res layout-2">

    <div id="wrapper" class="wrapper-fluid banners-effect-5">

        <!-- Header Container  -->
        <?php include('header.php'); ?>
        <!-- //Header Container  -->

        <!-- Main Container  -->
        <div class="main-container">
            <div id="content">
                <div class="module sohomepage-slider v-slider">
                    <div class="yt-content-slider" data-rtl="yes" data-autoplay="yes" data-autoheight="no"
                        data-delay="4" data-speed="0.6" data-margin="0" data-items_column0="1" data-items_column1="1"
                        data-items_column2="1" data-items_column3="1" data-items_column4="1" data-arrows="no"
                        data-pagination="yes" data-lazyload="yes" data-loop="yes" data-hoverpause="yes">
                        <div class="yt-content-slide">
                            <a href="#" class="hidden-xs"><img
                                    src="image/catalog/slideshow/home2/slide-1.jpg"
                                    srcset="image/catalog/slideshow/home2/slide-1.jpg 1x, image/catalog/slideshow/home2/slide-1.jpg 2x, image/catalog/slideshow/home2/slide-1.jpg 3x"
                                    alt="slider1" class="img-responsive">
                            </a>
                            <a href="#" class="visible-xs"><img src="image/catalog/slideshow/home2/m-slide-1.jpg"
                                    alt="slider1" class="img-responsive"></a>
                        </div>
                        <div class="yt-content-slide">
                            <a href="#" class="hidden-xs"><img
                                    src="image/catalog/slideshow/home2/slide-2.jpg"
                                    alt="slider3" class="img-responsive"></a>
                            <a href="#" class="visible-xs"><img src="image/catalog/slideshow/home2/m-slide-2.jpg"
                                    alt="slider1" class="img-responsive"></a>
                        </div>
                        <div class="yt-content-slide">
                            <a href="#" class="hidden-xs"><img
                                    src="image/catalog/slideshow/home2/slide-3.jpg"
                                    alt="slider3" class="img-responsive"></a>
                            <a href="#" class="visible-xs"><img src="image/catalog/slideshow/home2/m-slide-3.jpg"
                                    alt="slider1" class="img-responsive"></a>
                        </div>
                        <div class="yt-content-slide">
                            <a href="#" class="hidden-xs"><img
                                    src="image/catalog/slideshow/home2/slide-4.jpg"
                                    alt="slider3" class="img-responsive"></a>
                            <a href="#" class="visible-xs"><img src="image/catalog/slideshow/home2/m-slide-4.jpg"
                                    alt="slider1" class="img-responsive"></a>
                        </div>
                    </div>

                    <div class="loadeding"></div>
                </div>
            </div>

            <div id="content" style="padding: 50px 0;background: #fff;">

                <div class="container">
                    <div class="row sliderimages ">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 "></div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 inner">
                            <div class="slider-images">
                                <div class="yt-content-slider" data-rtl="yes" data-loop="yes" data-autoplay="no"
                                    data-autoheight="no" data-autowidth="no" data-delay="4" data-speed="0.6"
                                    data-margin="10" data-items_column0="1" data-items_column1="1"
                                    data-items_column2="1" data-items_column3="1" data-items_column4="1"
                                    data-arrows="yes" data-pagination="no" data-lazyload="yes" data-hoverpause="yes">
                                    <div class="item">
                                        <a href="#">
                                            <img src="image/under-img/1.jpg" alt="image">
                                        </a>
                                    </div>

                                    <div class="item">
                                        <a href="#">
                                            <img src="image/under-img/3.jpg" alt="image">
                                        </a>
                                    </div>

                                    <div class="item">
                                        <a href="#">
                                            <img src="image/under-img/4.jpg" alt="image">
                                        </a>
                                    </div>

                                    <div class="item">
                                        <a href="#">
                                            <img src="image/under-img/2.jpg" alt="image">
                                        </a>
                                    </div>

                                    <div class="item">
                                        <a href="#">
                                            <img src="image/under-img/5.jpg" alt="image">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                    </div>
                </div>
            </div>


            <div id="content" class="bg-img1">


                <div class="container">

                    <div class="row box-content2 layout-41 layout-4">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 extra-right">
                            <div class="module so-extraslider-ltr border-white">
                                <h3 class="modtitle"><span>Women's</span> Collection</h3>
                                <div class="modcontent">
                                    <div class="so-extraslider">

                                        <div class="yt-content-slider extraslider-inner products-list" data-rtl="yes"
                                            data-pagination="no" data-autoplay="yes" data-delay="4" data-speed="0.8"
                                            data-margin="30" data-items_column0="3" data-items_column1="3"
                                            data-items_column2="3" data-items_column3="2" data-items_column4="2"
                                            data-arrows="yes" data-lazyload="yes" data-loop="yes" data-buttonpage="top">
                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">
                                                                    <div class="box-label">
                                                                        <span class="label label-sale">-13%</span>
                                                                    </div>
                                                                    <a href="#" target="_self" title="Sling Back">
                                                                        <img src="image/Women's-Collection/Sling-Back.jpg"
                                                                            alt="Sling Back" class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkpink">
                                                                <h4><a href="#" target="_self" title="Sling Back">Sling
                                                                        Back</a></h4>

                                                                <p class="price"> <span
                                                                        class="price-new">&#x20b9;66.00</span>
                                                                    <span class="price-old">&#x20b9;76.00</span>
                                                                </p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">

                                                                    <a href="#" target="_self" title="Ankle Boots">
                                                                        <img src="image/Women's-Collection/AnkleBoots.jpg"
                                                                            alt="Ankle Boots" class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkpink">
                                                                <h4><a href="#" target="_self" title="Ankle Boots">Ankle
                                                                        Boots </a></h4>

                                                                <p class="price"> <span
                                                                        class="price-new">&#x20b9;66.00</span>
                                                                    <span class="price-old">&#x20b9;76.00</span>
                                                                </p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">

                                                                    <a href="#" target="_self" title="Ballerinas">
                                                                        <img src="image/Women's-Collection/Ballerinas.jpg"
                                                                            alt="Corem ipsum dolor"
                                                                            class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkpink">
                                                                <h4><a href="#" target="_self"
                                                                        title="Ballerinas">Ballerinas</a></h4>
                                                                <p class="price"> &#x20b9;65
                                                                </p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">
                                                                    <div class="box-label">
                                                                        <span class="label-product label-new">New</span>
                                                                    </div>
                                                                    <a href="#" target="_self" title="Calf Boots">
                                                                        <img src="image/Women's-Collection/Calf-Boots.jpg"
                                                                            alt="Calf Boots" class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkpink">
                                                                <h4><a href="#" target="_self" title="Calf Boots">Calf
                                                                        Boots</a></h4>

                                                                <p class="price">&#x20b9;80.00
                                                                </p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">

                                                                    <a href="#" target="_self" title="Canvas Shoes">
                                                                        <img src="image/Women's-Collection/Canvas-Shoes.jpg"
                                                                            alt="Canvas Shoes" class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkpink">
                                                                <h4><a href="#" target="_self"
                                                                        title="Rumstick shankle">Rumstick shankle</a>
                                                                </h4>

                                                                <p class="price"> &#x20b9;132.00</p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">

                                                                    <a href="#" target="_self" title="Court Shoes">
                                                                        <img src="image/Women's-Collection/Court-Shoes.jpg"
                                                                            alt="Court Shoes" class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkpink">
                                                                <h4><a href="#" target="_self" title="Court Shoes">Court
                                                                        Shoes</a></h4>

                                                                <p class="price"> &#x20b9;132.00</p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">

                                                                    <a href="#" target="_self" title="Trainers Shoes">
                                                                        <img src="image/Women's-Collection/Trainers.jpg"
                                                                            alt="Trainers Shoes" class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkpink">
                                                                <h4><a href="#" target="_self"
                                                                        title="Court Shoes">Trainers Shoes</a></h4>

                                                                <p class="price"> &#x20b9;132.00</p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 extra-right">
                            <div class="module so-extraslider-ltr border-white">
                                <h3 class="modtitle"><span>Men's</span> Collection</h3>
                                <div class="modcontent">
                                    <div class="so-extraslider">

                                        <div class="yt-content-slider extraslider-inner products-list" data-rtl="yes"
                                            data-pagination="no" data-autoplay="yes" data-delay="3" data-speed="0.6"
                                            data-margin="30" data-items_column0="3" data-items_column1="3"
                                            data-items_column2="3" data-items_column3="2" data-items_column4="2"
                                            data-arrows="yes" data-lazyload="yes" data-loop="yes" data-buttonpage="top">
                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">
                                                                    <div class="box-label">
                                                                        <span class="label label-sale">-13%</span>
                                                                    </div>
                                                                    <a href="#" target="_self" title="Brogues">
                                                                        <img src="image/Men's-Collection/Brogues.jpg"
                                                                            alt="Brogues" class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkblue">
                                                                <h4><a href="#" target="_self"
                                                                        title="Brogues">Brogues</a></h4>

                                                                <p class="price"> <span
                                                                        class="price-new">&#x20b9;66.00</span>
                                                                    <span class="price-old">&#x20b9;76.00</span>
                                                                </p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">

                                                                    <a href="#" target="_self" title="product">
                                                                        <img src="image/Men's-Collection/dress-boots.jpg"
                                                                            alt="Mapicola incididuv "
                                                                            class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkblue">
                                                                <h4><a href="#" target="_self"
                                                                        title="dress-boots">dress-boots </a></h4>

                                                                <p class="price"> <span
                                                                        class="price-new">&#x20b9;66.00</span>
                                                                    <span class="price-old">&#x20b9;76.00</span>
                                                                </p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">

                                                                    <a href="#" target="_self" title="product">
                                                                        <img src="image/Men's-Collection/Loafers.jpg"
                                                                            alt="Loafers " class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkblue">
                                                                <h4><a href="#" target="_self" title="Loafers">Loafers
                                                                    </a></h4>

                                                                <p class="price"> &#x20b9;65
                                                                </p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">
                                                                    <div class="box-label">
                                                                        <span class="label-product label-new">New</span>
                                                                    </div>
                                                                    <a href="#" target="_self" title="Formal">
                                                                        <img src="image/Men's-Collection/Oxfords.jpg"
                                                                            alt="Formal" class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkblue">
                                                                <h4><a href="#" target="_self" title="Formal">Formal</a>
                                                                </h4>

                                                                <p class="price">&#x20b9;80.00
                                                                </p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">

                                                                    <a href="#" target="_self" title="Penny Loafer">
                                                                        <img src="image/Men's-Collection/Penny-Loafer.jpg"
                                                                            alt="Penny Loafer" class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkblue">
                                                                <h4><a href="#" target="_self"
                                                                        title="Penny-Loafer">Penny-Loafer</a></h4>

                                                                <p class="price"> &#x20b9;132.00</p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">

                                                                    <a href="#" target="_self" title="Trainers Shoe">
                                                                        <img src="image/Men's-Collection/Trainers.jpg"
                                                                            alt="Trainers Shoe" class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkblue">
                                                                <h4><a href="#" target="_self"
                                                                        title="Trainers Shoe">Trainers Shoe</a></h4>

                                                                <p class="price"> &#x20b9;132.00</p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="item ">
                                                <div class="product-layout product-grid2 style1">
                                                    <div class="product-thumb transition product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container">
                                                                <div class="image">

                                                                    <a href="#" target="_self" title="Sneakers">
                                                                        <img src="image/Men's-Collection/Sneakers.jpg"
                                                                            alt="Sneakers" class="img-responsive">
                                                                    </a>

                                                                </div>
                                                                <!--quickview-->
                                                                <div class="so-quickview">
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                        href="#" title="Quick view"
                                                                        data-fancybox-type="iframe"><i
                                                                            class="fa fa-eye"></i><span>Quick
                                                                            view</span></a>
                                                                </div>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption bg-darkblue">
                                                                <h4><a href="#" target="_self"
                                                                        title="Sneakers  ">Sneakers</a></h4>

                                                                <p class="price"> &#x20b9;132.00</p>
                                                                <div class="button-group">
                                                                    <button class="addToCart" title="Add to Cart"
                                                                        type="button" onclick="cart.add('69');"><i
                                                                            class="fa fa-shopping-cart"></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div id="content">
                <div class="container-fluid" style="padding: 0;">
                    <div class="so-categories module">
                        <div class=" ">
                            <div class="col-lg-4 col-md-4 custom-slidercates" style="background: #f7f7f7;">
                                <div class="modcontent img img img-zoom-in" style="margin: 33px 15px 28px;">
                                    <div data-cover="image/category/1.jpg" data-height="150px 276px 250px 325px 400px"
                                        style="background-image: url('image/category/1.jpg');height: 400px;"></div>
                                    <div class="overlay overlay-show bg-dark"></div>
                                    <div class="overlay-content overlay-show">
                                        <a href="#"
                                            class="card-link h3 text-white font-condensed font-weight-bold stretched-link">NEW
                                            ARRIVALS</a>
                                    </div>


                                </div>
                            </div>

                            <div class="col-lg-8 col-md-8 custom-slidercates"
                                style="background:#f7f7f7;border-left:1px solid #cccccc;">


                                <div class="modcontent" style="padding: 25px 15px 6px;">
                                    <h3 class="modtitle mb-40" style="text-align: left;">
                                        <span style="font-size: 24px;">Women's</span> Category
                                    </h3>
                                    <div class="cat-wrap theme3 font-title">
                                        <div class="row">
                                            <div class="content-box col-lg-2 col-md-3 col-xs-4 ">
                                                <div class="image-cat">
                                                    <a href="#" title="Belly" target="_self">
                                                        <img src="image/category/belly1.jpg" title="Belly" alt="Belly">
                                                    </a>
                                                </div>
                                                <div class="cat-title bg-darkpink">
                                                    <a href="#" title="Belly" target="_self">Belly</a>
                                                </div>
                                            </div>
                                            <div class="content-box col-lg-2 col-md-3 col-xs-4">
                                                <div class="image-cat">
                                                    <a href="#" title="casual-shoes" target="_self">
                                                        <img src="image/category/party-wear.jpg" title="Boots" alt="Boots">
                                                    </a>
                                                </div>
                                                <div class="cat-title bg-darkpink">
                                                    <a href="#" title="Boots" target="_self">Party Wear</a>
                                                </div>
                                            </div>
                                            <div class="content-box col-lg-2 col-md-3 col-xs-4">
                                                <div class="image-cat">
                                                    <a href="#" title="Heels" target="_self">
                                                        <img src="image/category/sports-shoe1.jpg" title="Heels" alt="Heels">
                                                    </a>
                                                </div>
                                                <div class="cat-title bg-darkpink">
                                                    <a href="#" title="Heels" target="_self">Sport Shoe</a>
                                                </div>
                                            </div>
                                            <div class="content-box col-lg-2 col-md-3 col-xs-4">
                                                <div class="image-cat">
                                                    <a href="#" title="Loafer" target="_self">
                                                        <img src="image/category/flats.jpg" title="Loafer"
                                                            alt="Loafer">
                                                    </a>
                                                </div>
                                                <div class="cat-title bg-darkpink">
                                                    <a href="#" title="Loafer" target="_self">Flats</a>
                                                </div>
                                            </div>
                                            <div class="content-box col-lg-2 col-md-3 col-xs-4">
                                                <div class="image-cat">
                                                    <a href="#" title="Formal Shoe" target="_self">
                                                        <img src="image/category/sandals1.jpg" title="Sandals"
                                                            alt="Sandals">
                                                    </a>
                                                </div>
                                                <div class="cat-title bg-darkpink">
                                                    <a href="#" title="Formal Shoe" target="_self">Sandal</a>
                                                </div>
                                            </div>                                            
                                            <div class="content-box col-lg-2 col-md-3 col-xs-4">
                                                <div class="image-cat">
                                                    <a href="#" title="boots" target="_self">
                                                        <img src="image/category/sneaker1.jpg" title="Sneaker"
                                                            alt="Sneaker">
                                                    </a>
                                                </div>
                                                <div class="cat-title bg-darkpink">
                                                    <a href="#" title="Sneaker" target="_self">Sneaker </a>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="modcontent" style="padding: 25px 15px 6px;">
                                    <h3 class="modtitle mb-40" style="text-align: left;">
                                        <span style="font-size: 24px;">Men's</span> Category
                                    </h3>
                                    <div class="cat-wrap theme3 font-title">
                                        <div class="row">
                                            <div class="content-box col-lg-2 col-md-3 col-xs-4">
                                                <div class="image-cat">
                                                    <a href="#" title="Sports Shoes" target="_self">
                                                        <img src="image/category/m-sport-shoe.jpg" title="Sports Shoes"
                                                            alt="Sports Shoes">
                                                    </a>
                                                </div>
                                                <div class="cat-title bg-darkblue">
                                                    <a href="#" title="Sports Shoes " target="_self">Sports Wear</a>
                                                </div>
                                            </div>
                                            <div class="content-box col-lg-2 col-md-3 col-xs-4">
                                                <div class="image-cat">
                                                    <a href="#" title="sneaker" target="_self">
                                                        <img src="image/category/m-sneaker.jpg" title="sneaker"
                                                            alt="sneaker">
                                                    </a>
                                                </div>
                                                <div class="cat-title bg-darkblue">
                                                    <a href="#" title="sneaker" target="_self">Sneaker</a>
                                                </div>
                                            </div>
                                            <div class="content-box col-lg-2 col-md-3 col-xs-4">
                                                <div class="image-cat">
                                                    <a href="#" title="Loafer" target="_self">
                                                        <img src="image/category/flip-flop.jpg" title="Loafer"
                                                            alt="Loafer">
                                                    </a>
                                                </div>
                                                <div class="cat-title bg-darkblue">
                                                    <a href="#" title="Loafer" target="_self">Flip-Flop</a>
                                                </div>
                                            </div>
                                            <div class="content-box col-lg-2 col-md-3 col-xs-4">
                                                <div class="image-cat">
                                                    <a href="#" title="Formal Shoe" target="_self">
                                                        <img src="image/category/m-formal-shoe.jpg" title="Formal Shoe"
                                                            alt="Formal Shoe">
                                                    </a>
                                                </div>
                                                <div class="cat-title bg-darkblue">
                                                    <a href="#" title="Formal Shoe" target="_self">Formal Shoe</a>
                                                </div>
                                            </div>
                                            <div class="content-box col-lg-2 col-md-3 col-xs-4">
                                                <div class="image-cat">
                                                    <a href="#" title="casual-shoes" target="_self">
                                                        <img src="image/category/m-casual-shoes.jpg"
                                                            title="casual-shoes" alt="casual-shoes">
                                                    </a>
                                                </div>
                                                <div class="cat-title bg-darkblue">
                                                    <a href="#" title="casual-shoes" target="_self">casual shoes</a>
                                                </div>
                                            </div>
                                            <div class="content-box col-lg-2 col-md-3 col-xs-4">
                                                <div class="image-cat">
                                                    <a href="#" title="boots" target="_self">
                                                        <img src="image/category/m-boots.jpg" title="boots" alt="boots">
                                                    </a>
                                                </div>
                                                <div class="cat-title bg-darkblue">
                                                    <a href="#" title="boots" target="_self">Floater</a>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div id="content" style="background:#fff;">


                <div class="container">

                    <div class="block-policy2">
                        <ul>
                            <li class="item-1">
                                <div class="item-inner">
                                    <div class="icon icon1"></div>
                                    <div class="content"> <a href="#">Same Day Delivery</a>
                                        <p>Instant delivery in just 4 hours.</p>
                                    </div>
                                </div>
                            </li>
                            <li class="item-2">
                                <div class="item-inner">
                                    <div class="icon icon2"></div>
                                    <div class="content"> <a href="#">Free Shipping</a>
                                        <p>Get 100% doorstep delivery</p>
                                    </div>
                                </div>
                            </li>
                            <li class="item-3">
                                <div class="item-inner">
                                    <div class="icon icon3"></div>
                                    <div class="content"> <a href="#">Cash On Delivery</a>
                                        <p>Pay at your ease.</p>
                                    </div>
                                </div>
                            </li>
                            <li class="item-4">
                                <div class="item-inner">
                                    <div class="icon icon4"></div>
                                    <div class="content"> <a href="#">payment method</a>
                                        <p>100% Security Guaranteed.</p>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <!--Main Container -->

        <!-- Footer Container -->
        <?php include('footer.php'); ?>
        <!-- //end Footer Container -->

    </div>

    <!-- End Color Scheme
============================================ -->

    <?php include('script.php'); ?>

</body>

</html>