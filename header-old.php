    <!-- Header Container  -->

<style>

@-webkit-keyframes animLoadedLogo {
  to {
    -webkit-transform: translate3d(0, 100%, 0) translate3d(0, 50px, 0) scale3d(0.65, 0.65, 1); } }

@keyframes animLoadedLogo {
  to {
    -webkit-transform: translate3d(0, 100%, 0) translate3d(0, 50px, 0) scale3d(0.65, 0.65, 1);
    transform: translate3d(0, 100%, 0) translate3d(0, 50px, 0) scale3d(0.65, 0.65, 1); } }

@-webkit-keyframes animLoadedLoader {
  to {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0) scale3d(0.3, 0.3, 1); } }

@keyframes animLoadedLoader {
  to {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0) scale3d(0.3, 0.3, 1);
    transform: translate3d(0, -100%, 0) scale3d(0.3, 0.3, 1); } }

@-webkit-keyframes animLoadedHeader {
  to {
    -webkit-transform: translate3d(0, -100%, 0); } }

@keyframes animLoadedHeader {
  to {
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0); } }

@-webkit-keyframes animInitialHeader {
  from {
    opacity: 0;
    -webkit-transform: translate3d(0, 800px, 0); } }

@keyframes animInitialHeader {
  from {
    opacity: 0;
    -webkit-transform: translate3d(0, 800px, 0);
    transform: translate3d(0, 800px, 0); } }
/*Dev Custom */
/*====================================================*/
.lib-list-item-product2 {
  border: none; }
  .lib-list-item-product2 .image-dev .list-button-dev {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 10;
    transform: translate3d(-50%, -50%, 0);
    -moz-transform: translate3d(-50%, -50%, 0);
    -webkit-transform: translate3d(-50%, -50%, 0);
    -ms-transform: translate3d(-50%, -50%, 0); }
    .lib-list-item-product2 .image-dev .list-button-dev li {
      display: inline-block;
      float: left;
      height: 40px;
      width: 40px;
      background: #fff;
      position: relative;
      border-width: 0 1px 0 0;
      border-style: solid;
      border-color: #ddd;
      opacity: 0;
      transition: transform 0.2s ease-in-out, opacity 0.2s ease-in-out; }
      .lib-list-item-product2 .image-dev .list-button-dev li:nth-child(1) {
        transform: translateX(40px);
        -moz-transform: translateX(40px);
        -webkit-transform: translateX(40px);
        -ms-transform: translateX(40px);
        z-index: 1;
        transition-delay: 0s; }
      .lib-list-item-product2 .image-dev .list-button-dev li:nth-child(2) {
        transition-delay: 0.2s;
        z-index: 2; }
      .lib-list-item-product2 .image-dev .list-button-dev li:nth-child(3) {
        transition-delay: 0.2s;
        z-index: 2; }
      .lib-list-item-product2 .image-dev .list-button-dev li:nth-child(4) {
        transform: translateX(-40px);
        -moz-transform: translateX(-40px);
        -webkit-transform: translateX(-40px);
        -ms-transform: translateX(-40px);
        z-index: 1;
        transition-delay: 0s; }
      .lib-list-item-product2 .image-dev .list-button-dev li:first-child {
        border-left: 1px solid #ddd; }
      .lib-list-item-product2 .image-dev .list-button-dev li a, .lib-list-item-product2 .image-dev .list-button-dev li button {
        background: none;
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        line-height: 40px;
        text-align: center;
        box-shadow: none;
        border: none;
        color: #555;
        width: 40px;
        padding: 0; }
      .lib-list-item-product2 .image-dev .list-button-dev li:hover {
        background: #ff3c20; }
        .lib-list-item-product2 .image-dev .list-button-dev li:hover a, .lib-list-item-product2 .image-dev .list-button-dev li:hover button {
          color: #fff; }
  .lib-list-item-product2 .caption-dev {
    text-align: center;
    padding: 0 15px; }
    .lib-list-item-product2 .caption-dev .rating-dev {
      margin: 5px 0; }
    .lib-list-item-product2 .caption-dev .rating-dev .fa-stack-2x {
      font-size: 11px; }
    .lib-list-item-product2 .caption-dev .title-dev {
      color: #444;
      font-size: 13px; }
    .lib-list-item-product2 .caption-dev .price-dev .price.product-price {
      font-size: 16px; }
    .lib-list-item-product2 .caption-dev .price-dev .price-new {
      font-size: 16px; }
    .lib-list-item-product2 .caption-dev .price-dev .price-old {
      font-size: 12px; }
    .lib-list-item-product2 .caption-dev .add-cart-dev {
      background: #fff;
      border: 1px solid #ddd;
      font-size: 12px;
      text-transform: uppercase;
      color: #999;
      font-weight: bold;
      box-shadow: none;
      border-radius: 0;
      padding: 6px 20px;
      margin: 0 0 30px;
      transition: all 0.2s ease-in-out;
      -moz-transition: all 0.2s ease-in-out;
      -webkit-transition: all 0.2s ease-in-out; }

.lib-list-item-product-over2 .image-dev .list-button-dev li {
  opacity: 1;
  transition: transform 0.2s ease-in-out, opacity 0.2s ease-in-out; }
  .lib-list-item-product-over2 .image-dev .list-button-dev li:nth-child(1) {
    transform: translateX(0);
    -moz-transform: translateX(0);
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transition-delay: 0.2s; }
  .lib-list-item-product-over2 .image-dev .list-button-dev li:nth-child(2) {
    transition-delay: 0s; }
  .lib-list-item-product-over2 .image-dev .list-button-dev li:nth-child(3) {
    transition-delay: 0s; }
  .lib-list-item-product-over2 .image-dev .list-button-dev li:nth-child(4) {
    transform: translateX(0);
    -moz-transform: translateX(0);
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transition-delay: 0.2s; }
/*EFECT PRODUCT NUMBER*/
.lib-two-img {
  position: relative;
  display: block; }
  .lib-two-img .img-1 {
    opacity: 1;
    position: relative;
    transition: all 0.5s ease-in-out; }
  .lib-two-img .img-2 {
    position: absolute;
    z-index: 0;
    top: 0;
    opacity: 0;
    display: block;
    transition: all 0.5s ease-in-out;
    left: 50%;
    transform: translateX(-50%); }

.lib-two-img-over .img-1 {
  opacity: 0;
  transform-style: inherit;
  transition: all 0.5s ease-in-out; }
  .lib-two-img-over .img-2 {
    opacity: 1;
    transform-style: inherit;
    transition: all 0.5s ease-in-out; }
/*EFFECT SLIDERHOME*/
@keyframes myeffect-slideshow {
  0% {
    opacity: 0;
    transform: translateY(-300px);
    -webkit-transform: translateY(-300px);
    -moz-transform: translateY(-300px);
    -ms-transform: translateY(-300px);
    -o-transform: translateY(-300px); }

  100% {
    opacity: 1;
    transform: translateY(0);
    -moz-transform: translateY(0);
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    -o-transform: translateY(0); } }

@-webkit-keyframes myeffect-slideshow {
  0% {
    opacity: 0;
    transform: translateY(-300px);
    -webkit-transform: translateY(-300px);
    -moz-transform: translateY(-300px);
    -ms-transform: translateY(-300px);
    -o-transform: translateY(-300px); }

  100% {
    opacity: 1;
    transform: translateY(0);
    -moz-transform: translateY(0);
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    -o-transform: translateY(0); } }

@-moz-keyframes myeffect-slideshow {
  0% {
    opacity: 0;
    transform: translateY(-300px);
    -webkit-transform: translateY(-300px);
    -moz-transform: translateY(-300px);
    -ms-transform: translateY(-300px);
    -o-transform: translateY(-300px); }

  100% {
    opacity: 1;
    transform: translateY(0);
    -moz-transform: translateY(0);
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    -o-transform: translateY(0); } }
/*===============================================
 [SASS DIRECTORY ]
 [1] BLOCK HEADER
 ==============================================*/
.typeheader-1 {
  background-color: #444444;
  /*=====SEARCH ======*/
  /*BLOCK CART*/
  /*MENU MEGA*/
  /*-- MEGA MENU VERTICAL --*/
  }
  .typeheader-1.navbar-compact .header-middle {
    padding: 10px 0;
    margin: 0; }
  .typeheader-1.navbar-compact .header-bottom {
    padding: 10px 0; }
  .typeheader-1.navbar-compact #cart {
    margin-top: 3px; }
  .typeheader-1 .header-top {
    border-bottom: 1px solid rgb(66, 66, 66);
    color: #fff;
    font-size: 12px;
    min-height: 40px;
    }
  .typeheader-1 .header-top .module {
    display: table-cell;
    margin-bottom: 0; }
  .typeheader-1 .header-middle {
    clear: both;
    /* background-color: #aac134; */
    padding: 5px 0 4px 0;
    }
  .typeheader-1 .header-middle .middle-right {
    padding-top: 9px;
    -webkit-box-align: stretch;
    -moz-box-align: stretch;
    box-align: stretch;
    -webkit-align-items: stretch;
    -moz-align-items: stretch;
    -ms-align-items: stretch;
    -o-align-items: stretch;
    align-items: stretch;
    -ms-flex-align: stretch;
    display: -webkit-box;
    display: -moz-box;
    display: box;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -moz-box-orient: horizontal;
    box-orient: horizontal;
    -webkit-box-direction: normal;
    -moz-box-direction: normal;
    box-direction: normal;
    -webkit-flex-direction: row;
    -moz-flex-direction: row;
    flex-direction: row;
    -ms-flex-direction: row;
    -webkit-box-pack: end;
    -moz-box-pack: end;
    box-pack: end;
    -webkit-justify-content: flex-end;
    -moz-justify-content: flex-end;
    -ms-justify-content: flex-end;
    -o-justify-content: flex-end;
    justify-content: flex-end;
    -ms-flex-pack: end; }
  .typeheader-1 .header-bottom {
    clear: both;
    padding-top: 5px;
    padding-bottom: 9px;
    /* background: #aac134; */
    }
  .typeheader-1 .telephone ul li {
    display: inline-block;
    position: relative;
    list-style: outside none none;
    padding-left: 18px;
    margin-left: 18px;
    padding-right: 0; }
  .typeheader-1 .telephone ul li:after {
    background-color: rgba(255, 255, 255, 0.4);
    content: "";
    height: 11px;
    position: absolute;
    left: 0;
    top: 5px;
    width: 1px; }
  .typeheader-1 .telephone ul li a {
    color: #fff;
    text-transform: capitalize; }
    .typeheader-1 .telephone ul li a i {
      margin-right: 1px; }
  .typeheader-1 .lang-curr {
    float: right; }
  .typeheader-1 .signin-w {
    padding-left: 30px;
    color: #fff; }
  .typeheader-1 .signin-w ul {
    list-style: none; }
  .typeheader-1 .signin-w ul li {
    float: left; }
  .typeheader-1 .signin-w ul li a {
    color: #fff;
    }
  .typeheader-1 .welcome-msg {
    color: #a8a8a5;
    font-size: 15px;
    line-height: 40px;
    clear: both;
    }
  .typeheader-1 .wishlist-comp {
    float: right;
    margin-right: 20px; }
  .typeheader-1 .wishlist-comp li {
    width: 40px;
    height: 40px;
    list-style: none;
    float: left;
    padding-top: 8px;
    margin-left: 15px; }
  .typeheader-1 .wishlist-comp li a i {
    font-size: 26px;
    color: #ffffff;
    }
  .typeheader-1 .wishlist-comp li a span {
    display: none; }
  .typeheader-1 .wishlist-comp li a.top-link-wishlist {
    width: 40px;
    height: 32px;
    display: block;
    overflow: hidden;
    text-indent: -999em; }
    .typeheader-1 .wishlist-comp li a.top-link-wishlist:before {
      font-family: 'FontAwesome';
      content: "\f004";
      font-size: 26px;
      color: #ffffff;
      display: block;
      text-indent: 0;
      }
  .typeheader-1 ul.top-link {
    list-style: none;
    margin: 0;
    vertical-align: top;
    display: inline-flex; }
  .typeheader-1 ul.top-link .dropdown-menu {
    border-radius: 0; }
  .typeheader-1 ul.top-link > li {
    padding: 0;
    position: relative; }
  .typeheader-1 ul.top-link > li .btn-group .btn-link {
    display: block;
    color: #fff;
    font-size: 12px;
    padding: 10px 0 8px; }
  .typeheader-1 ul.top-link > li > a {
    font-size: 12px;
    color: #fff;
    padding-left: 3px; }
  .typeheader-1 ul.top-link > li.account {
    line-height: 40px; }
  .typeheader-1 ul.top-link > li.account > a {
    padding: 0;
    margin: 0; }
  .typeheader-1 ul.top-link > li.account:before {
    content: "\f007";
    font-family: FontAwesome;
    font-size: 12px; }
  .typeheader-1 ul.top-link > li.account .dropdown-menu {
    left: 0;
    right: auto; }
  .typeheader-1 ul.top-link > li.open .dropdown-menu {
    display: block; }
  .typeheader-1 ul.top-link > li.language {
    margin-left: 20px;
    padding-left: 20px; }
  .typeheader-1 ul.top-link > li.language:before {
    background-color: rgba(255, 255, 255, 0.4);
    content: "";
    height: 11px;
    position: absolute;
    left: 0;
    top: 16px;
    width: 1px; }
  .typeheader-1 #sosearchpro .search {
    position: relative;
    margin: 0;
    width: 100%;
    border-radius: 3px; }
  .typeheader-1 #sosearchpro .search .select_category {
    border: none;
    border-right: 1px solid #d7d7d7;
    background-color: #f5f5f5;
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px;
    width: 22%; }
  .typeheader-1 #sosearchpro .search .select_category select {
    height: 40px;
    line-height: 40px;
    color: #666;
    font-size: 12px;
    padding: 0 15px; }
  .typeheader-1 #sosearchpro .search .select_category select:hover {
    cursor: pointer; }
  .typeheader-1 #sosearchpro .search .select_category.icon-select:after {
    color: #999;
    background-color: #f5f5f5;
    font-size: 16px;
    line-height: 42px;
    right: 0;
    left: auto; }
  .typeheader-1 #sosearchpro .search .input-group-btn {
    width: 0; }
  .typeheader-1 #sosearchpro .search button {
    position: absolute;
    top: 0px;
    right: 0px;
    height: 40px;
    text-align: center;
    line-height: 40px;
    padding: 0 35px;
    font-size: 14px;
    color: #fff;
    border-radius: 0;
    text-transform: upper-case;
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px; }
  .typeheader-1 #sosearchpro .search button i {
    font-size: 12px;
    color: #fff; }
  .typeheader-1 #sosearchpro .search input {
    border: none;
    border-radius: 0px;
    border-bottom-right-radius: 4px;
    border-top-right-radius: 4px;
    background-color: #fff;
    z-index: 0;
    height: 40px;
    padding-left: 20px;
    font-size: 12px;
    width: 100%;
    background-color: #fff;
    color: #999; }
  .typeheader-1 #sosearchpro .search input::-moz-placeholder {
    color: #999;
    opacity: 1; }
  .typeheader-1 #sosearchpro .search input:-ms-input-placeholder {
    color: #999; }
  .typeheader-1 #sosearchpro .search input::-webkit-input-placeholder {
    color: #999; }
  .typeheader-1 #sosearchpro .search input:focus {
    z-index: 0; }
  .typeheader-1 .shopping_cart {
    float: right;
    position: relative; }
  .typeheader-1 .shopping_cart .btn-shopping-cart .top_cart {
    position: relative; }
  .typeheader-1 .shopping_cart .btn-shopping-cart .top_cart .icon-c {
    width: 40px;
    height: 40px;
    border-radius: 50%;
    float: left;
    text-align: center;
    line-height: 40px; }
  .typeheader-1 .shopping_cart .btn-shopping-cart .top_cart .icon-c i {
    font-size: 18px;
    color: #fff; }
	.middle-right1 {
    padding-top: 9px;
    -webkit-box-align: stretch;
    -moz-box-align: stretch;
    box-align: stretch;
    -webkit-align-items: stretch;
    -moz-align-items: stretch;
    -ms-align-items: stretch;
    -o-align-items: stretch;
    align-items: stretch;
    -ms-flex-align: stretch;
    display: -webkit-box;
    display: -moz-box;
    display: box;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -moz-box-orient: horizontal;
    box-orient: horizontal;
    -webkit-box-direction: normal;
    -moz-box-direction: normal;
    box-direction: normal;
    -webkit-flex-direction: row;
    -moz-flex-direction: row;
    flex-direction: row;
    -ms-flex-direction: row;
    -webkit-box-pack: end;
    -moz-box-pack: end;
    box-pack: end;
    -webkit-justify-content: flex-end;
    -moz-justify-content: flex-end;
    -ms-justify-content: flex-end;
    -o-justify-content: flex-end;
    justify-content: flex-end;
    -ms-flex-pack: end;
}
  @media (min-width: 991px) and (-webkit-min-device-pixel-ratio: 0) {
        .typeheader-1 .shopping_cart .btn-shopping-cart .top_cart .icon-c {
          padding-top: 3px; } }
  .typeheader-1 .shopping_cart .btn-shopping-cart .top_cart .shopcart-inner {
    float: left;
    color: #fff;
    margin-left: 15px;
    margin-top: 13px; }
  .typeheader-1 .shopping_cart .btn-shopping-cart .top_cart .shopcart-inner .text-shopping-cart {
    font-size: 14px;
    font-weight: 700;
    margin-top: 0px;
    margin-bottom: 0;
    line-height: 100%;
    text-transform: uppercase;
    float: left;
    padding-right: 5px; }
  .typeheader-1 .shopping_cart .btn-shopping-cart .top_cart .shopcart-inner .total-shopping-cart {
    font-size: 12px;
    float: left;
    line-height: 100%;
    font-weight: 700; }
    .typeheader-1 .shopping_cart .btn-shopping-cart .top_cart .shopcart-inner .total-shopping-cart .items_cart2 {
      display: none; }
    .typeheader-1 .shopping_cart .btn-shopping-cart .top_cart .shopcart-inner .total-shopping-cart .items_cart {
      width: 22px;
      height: 22px;
      line-height: 22px;
      text-align: center;
      border-radius: 100%;
      -moz-border-radius: 100%;
      -webkit-border-radius: 100%;
      position: absolute;
      top: -6px;
      left: 27px;
      font-size: 12px;
     color: #fffffe;
       background-color: #c60605; }
  .typeheader-1 .shopping_cart .dropdown-menu {
    top: 100%;
    right: -15px;
    left: auto;
    color: #666; }
  .typeheader-1 .shopping_cart .shoppingcart-box:before {
    right: 50px; }
  .typeheader-1 .megamenu-style-dev .navbar-default {
    background: transparent; }
  .typeheader-1 .container-megamenu.horizontal ul.megamenu > li {
    margin-right: 30px; }
  .typeheader-1 .container-megamenu.horizontal ul.megamenu > li > a {
    line-height: 100%;
    color: #ffffff;
    font-weight: 600;
    text-transform: uppercase;
    padding: 15px 0;
    }
  .typeheader-1 .container-megamenu.vertical {
    position: relative; }
  .typeheader-1 .container-megamenu.vertical #menuHeading .megamenuToogle-pattern .container:after {
    font-family: 'FontAwesome';
    content: "\f107";
    font-size: 16px;
    color: #fff;
    position: absolute;
    right: 20px;
    top: 0;
    font-weight: 400; }
  @media (min-width: 992px) {
    .typeheader-1 .container-megamenu.vertical .vertical-wrapper {
      visibility: hidden;
      -webkit-opacity: 0;
      -moz-opacity: 0;
      -ms-opacity: 0;
      -o-opacity: 0;
      opacity: 0;
      transition: all 0.2s ease-in-out 0s;
      -moz-transition: all 0.2s ease-in-out 0s;
      -webkit-transition: all 0.2s ease-in-out 0s;
      position: absolute;
      width: 100%;
      top: 60px; }

    .typeheader-1 .container-megamenu.vertical:hover .vertical-wrapper {
      visibility: visible;
      -webkit-opacity: 1;
      -moz-opacity: 1;
      -ms-opacity: 1;
      -o-opacity: 1;
      opacity: 1;
      transition: all 0.2s ease-in-out 0s;
      -moz-transition: all 0.2s ease-in-out 0s;
      -webkit-transition: all 0.2s ease-in-out 0s;
      top: 40px; } }

.common-home .typeheader-1 {
  margin-bottom: 30px; }

@media (min-width: 992px) and (max-width: 1920px) {
  .common-home .typeheader-1 .container-megamenu.vertical {
    position: relative; }
    .common-home .typeheader-1 .container-megamenu.vertical #menuHeading {
      margin: 0; }
      .common-home .typeheader-1 .container-megamenu.vertical #menuHeading .megamenuToogle-pattern {
        position: relative; }
        .common-home .typeheader-1 .container-megamenu.vertical #menuHeading .megamenuToogle-pattern:before {
          background-image: url(../../image/icon/icon-tit.png);
          bottom: -26px;
          content: "";
          height: 32px;
          left: 4px;
          position: absolute;
          width: 5px;
          z-index: 999; }
        .common-home .typeheader-1 .container-megamenu.vertical #menuHeading .megamenuToogle-pattern:after {
          background-image: url(../../image/icon/icon-tit.png);
          bottom: -26px;
          content: "";
          height: 32px;
          right: 4px;
          position: absolute;
          width: 5px;
          z-index: 999; }
    .common-home .typeheader-1 .container-megamenu.vertical .vertical-wrapper {
      visibility: visible;
      -webkit-opacity: 1;
      -moz-opacity: 1;
      -ms-opacity: 1;
      -o-opacity: 1;


      opacity: 1;
      transition: all 0.2s ease-in-out 0s;
      -moz-transition: all 0.2s ease-in-out 0s;
      -webkit-transition: all 0.2s ease-in-out 0s; }
    .common-home .typeheader-1 .container-megamenu.vertical:hover .vertical-wrapper {
      visibility: visible;
      -webkit-opacity: 1;
      -moz-opacity: 1;
      -ms-opacity: 1;
      -o-opacity: 1;
      opacity: 1;
      transition: all 0.2s ease-in-out 0s;
      -moz-transition: all 0.2s ease-in-out 0s;
      -webkit-transition: all 0.2s ease-in-out 0s;
      top: 60px; } }

@media (min-width: 1200px) and (max-width: 1649px) {
  .typeheader-1 .bottom1 {
    width: 21%; }
    .typeheader-1 .bottom2 {
      width: 49%; }
    .typeheader-1 .bottom3 {
      width: 30%; }
    .typeheader-1 .container-megamenu.vertical .vertical-wrapper {
      margin-bottom: 30px; }
      .typeheader-1 .container-megamenu.vertical .vertical-wrapper ul.megamenu > a {
        padding-top: 12px;
        padding-bottom: 12px; }
        .typeheader-1 .container-megamenu.vertical .vertical-wrapper ul.megamenu .loadmore {
          padding-top: 12px;
          padding-bottom: 15px; }
          .typeheader-1 .container-megamenu.vertical .vertical-wrapper ul.megamenu .loadmore i {
            top: 12px; }
    .typeheader-1 #sosearchpro .search .select_category {
      width: 26%; } }

@media (min-width: 1366px) and (max-width: 1649px) {
  .typeheader-1 .telephone ul li:first-child {
    display: none; }
    .typeheader-1 .header-middle {
      position: relative; }
    .typeheader-1 .middle-right {
      position: absolute;
      top: 0;
      right: 30px; }
    .typeheader-1 .main-menu {
      width: 83.3333%;
      } }

@media (min-width: 1200px) and (max-width: 1365px) {
  .typeheader-1 .telephone {
    display: none; }
    .typeheader-1 .navbar-logo {
      width: 20%; }
    .typeheader-1 .header-middle {
      position: relative; }
    .typeheader-1 .middle-right {
      position: absolute;
      top: 0;
      right: 30px; }
    .typeheader-1 .main-menu {
      width: 80%; } }
}

</style>

    <header id="header" class=" typeheader-2">
        <!-- Header Top -->
        <div class="header-top hidden-compact">
            <div class="container">
                <div class="row">
                <div class="navbar-logo col-lg-2 col-md-3 col-sm-12 col-xs-2 visible-xs">
                <div class="navbar-header">
                                        <button type="button" id="show-megamenu" data-toggle="collapse" class="navbar-toggle">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                        </div>
                    </div>

                    <!-- Logo -->
                    <div class="navbar-logo col-lg-2 col-md-3 col-sm-12 col-xs-8">
                        <div class="logo"><a href="index.php"><img src="image/catalog/150x80stroke.svg" class="svg-logo" title="SHoooz - The Shoeroom" alt="Your Store" /></a>
                                                      
                    
                    </div>
                    </div>
                    <!-- //end Logo -->


                    <div class="header-top-left col-lg-6 col-md-8 col-sm-6 col-xs-12">
                    
                    <div class="search-header-w">
                            <div class="icon-search hidden-lg hidden-md hidden-sm hidden-xs"><i class="fa fa-search"></i></div> 
                            <div id="sosearchpro" class="sosearchpro-wrapper so-search ">
                                <form method="GET" action="">
                                    <div id="search0" class="search input-group form-group">
                                      

                                        <input class="autosearch-input form-control" type="text" value="" size="50" autocomplete="off" placeholder="Keyword here..." name="search">
                                        <span class="input-group-btn">
                                        <button type="submit" class="button-search btn btn-primary" name="submit_search"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                    <input type="hidden" name="route" value="product/search" />
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    <div class="pos-cart collapsed-block col-lg-4 col-md-4 col-sm-6 col-xs-2">
                        <ul class="top-link list-inline lang-curr">
                            <li class="hidden-xs"> <?php if(( empty($email))&&( empty($user_id))){ ?>
              <a class="iframe-link link-account link-lg btn-group" href="login_signup.php" title="Login" data-fancybox-type="iframe"><i class="fa fa-lock"></i>Login & Signup</a>
              <? } else{ ?>
              <a class=" link-account"  href="logout.php" title="logout">Logout</a>
              <? } ?>
                                
                            </li>
                            <li class="currency">
                                <div class="btn-group currencies-block">
                                <div class="shopping_cart">
                            <div id="cart" class="btn-shopping-cart">

                                <a data-loading-text="Loading... " class="btn-group top_cart dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                   <div class="shopcart">
                                   <span class="icon-c">
                                   <i class="fa fa-shopping-bag"></i>                              </span>
                                   <div class="shopcart-inner">
                                   <p class="text-shopping-cart">  My cart </p>
                                   <span class="total-shopping-cart cart-total-full">
                                   <span class="items"> <span class="items_cart em-topcart-qty ">0</span></span><span class="items_cart2"> item(s)</span><span class="items_carts fa fa-inr total"> 0 </span>                                            </span>                                        </div>
                                    </div>
                                </a>

                                <ul class="dropdown-menu pull-right shoppingcart-box" role="menu">
                                    <li>
                                        <table class="table table-striped">
                                            <tbody id="CartItems">
                                                <tr>
                                                    <td class="text-center" colspan="6">
                                                     You have no items in your shopping cart.  </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <li>
                                        <div>
                                            <table class="table table-bordered">
                                                <tbody>
                                                 
                                                    <tr>
                                                        <td class="text-left"><strong>Total</strong>                                                        </td>
                                                        <td class="text-right"><span class=" fa fa-inr total" >0</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <p class="text-right"> <a class="btn view-cart" href="mycart.php"><i class="fa fa-shopping-cart"></i>View Cart</a>&nbsp;&nbsp;&nbsp; <a class="btn btn-mega checkout-cart" href="mycart.php"><i class="fa fa-share"></i>Checkout</a>                                            </p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                                </div>
                            </li>   
                            
                            
                        </ul>
                        

                        
                    </div>
                </div>
            </div>
        </div>
        <!-- //Header Top -->

        <!-- Header center -->
        <div class="header-middle">
            <div class="container">
                <div class="row">
                    
                    <!-- Search -->
                    <div class="middle2 col-lg-8 col-lg-offset-2 col-md-8">
                        <div class="responsive so-megamenu megamenu-style-dev">
                            <nav class="navbar-default">
                                <div class=" container-megamenu  horizontal open ">
                                     
                                    
                                    <div class="megamenu-wrapper">
                                        <span id="remove-megamenu" class="fa fa-times"></span>
                                        <div class="megamenu-pattern">
                                            <div class="container-mega">
                                                <ul class="megamenu" data-transition="slide" data-animationtime="250">
                                                    <li class="">
                                                        <a href="#">Home </a>
                                                        
                                                    </li>

                                                    <?php  	  
						  $parent_cats=mysql_query("select * from  catalog_category where status=0 and parent_id='' and display_mode='Y' order by cat_order ASC  ");
                          while($parentcat = mysql_fetch_array($parent_cats))
						  {  	
						  	$menus= mysql_query("select * from  catalog_category where status=0 and parent_id='".$parentcat['category_id']."'  order by category_id ASC  "); 
	                        $num_menu = mysql_num_rows($menus);
	                        if($num_menu!=0)
							{
	                          $i=1; 
							  while($menu = mysql_fetch_array($menus))
							  {
		                        ?>
								  <?php 
                                  $cat_img = ''; if($menu['main_img']!=''){  $cat_img =  $menu['main_img'];}
                                  $icon = ''; if($menu['icon']!=''){ $icon =  $menu['icon'];}
                                  ?>
                                  


                                                    <li class="with-sub-menu hover">
                                                        <p class="close-menu"></p>
                                                        <a href="#" class="clearfix">
                                                            <strong><?php echo $menu['name']; ?></strong>
                                                            
                                                            <b class="caret"></b>
                                                        </a>
                                                        <div class="sub-menu" style="width: 100%; right: auto;">
                                                            <div class="content" >
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="column">
                                                                            
                                                                            <div>
                                                                                <ul class="row-list">





                                                                                <?php   
											$scat_count = 0; $col_limit = 6;	
											$sub_menu_count= mysql_query("select * from  catalog_category where status=0 and parent_id=".$menu['category_id']);
									        $sub_menus= mysql_query("select * from  catalog_category where status=0 and parent_id='".$menu['category_id']."' order by category_id ASC ");

										   $sub_menu_num = mysql_num_rows($sub_menu_count);  
										   if($sub_menu_num!=0)
										   { 
											   while($sub_menu = mysql_fetch_array($sub_menus))
											   { ?>
                                                                                   <li>
                                                  <a href="<?php echo $sub_menu['category_id']//get_cat_url($sub_menu['category_id']); ?>"><?php echo $sub_menu['name']; ?> </a>
                                                </li>
                                                <?php
													$scat_count++;
													if($scat_count==$col_limit)
													{
														echo "</ul></div></div></div></div>";
                                                        echo '<div class="col-md-3"><div class="menu"><ul><li>&nbsp;<ul>';
														$scat_count =0;
													}
												}
											}		
											?>                       

                                                                    
                                                                    <div class="col-md-6">
                                                                        <div class="column">
                                                                        
                                                                            <div>
                                                                                <ul class="row-list">
                                                                                    <li>
                                                                                    <img src="image/catalog/menu/megabanner/women's-wear.jpg" />
                                                                                    </li>
                                                                                
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    



                                                    <?php
								}
							  }	
							}
							?>








                                                   
                                                    
                                                    <li class="">
                                                        <p class="close-menu"></p>
                                                        <a href="#" class="clearfix">
                                                            <strong>Offer Zone</strong>
                                                            <img class="label-hot" src="image/catalog/menu/hot-icon.png" alt="icon items">
                                                        </a>
                                            
                                                    </li>
                                                    
                                                    
                                                    
                                                </ul>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </nav>
                        </div>
                        
                    </div>
                    <!-- //end Search -->
                    
                    
                    </div>

            </div>
        </div>
        <!-- //Header center -->   

    </header>
    <!--Header Container  -->

 