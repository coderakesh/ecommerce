<!doctype html>

<html class="no-js" lang="en">

<head>

    <?php include('link.php');

      @session_start();

      $category_id =  $_REQUEST['catid'];

      $search = $_REQUEST['search'];

      $keyword = $_REQUEST['keyword'];

      $product_id = $_REQUEST['pid'];

      if(@$_REQUEST['country_id']!=''){

      $_SESSION['country_id']=$_REQUEST['country_id'];

      }

  ?>

    <link rel="stylesheet" type="text/css" href="css/livevalid.css" />

    <script type="text/javascript" src="js/livevalidation_standalone.compressed.js"></script>

    <style>
    .em-wrapper-main,
    .em-wrapper-slideshow {

        background: #ffffff;

    }

    /** reveiw order css**/
    .qty-ctl {
        float: left;
        height: auto;
        line-height: normal;
        margin: 0 !important;
        padding: 0 !important;
    }

    .qty-ctl button {
        font-size: 0;
        display: block;
        overflow: hidden;
        vertical-align: middle;
        text-align: left;
        -moz-transition: all .5s ease;
        -webkit-transition: all .5s ease;
        -o-transition: all .5s ease;
        transition: all .5s ease;
        -moz-backface-visibility: hidden;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        margin-bottom: 0;
        cursor: pointer;
        background-color: transparent;
        border: 1px solid;
        text-indent: 0;
        width: 40px;
        height: 40px;
        padding: 0 13px;
        position: relative;
        z-index: 1;
    }

    .opc .step-title .title-box,
    .opc .step-title a {
        -webkit-transition: all .5s ease;
        -o-transition: all .5s ease;
        -moz-backface-visibility: hidden;
        -webkit-backface-visibility: hidden;
    }


    .qty-ctl button.decrease {
        -moz-border-radius: 3px 0 0 3px;
        -webkit-border-radius: 3px 0 0 3px;
        border-radius: 3px 0 0 3px;
    }

    .qty-ctl button.decrease:before {
        content: "-";
        font-size: 20px;
        font-weight: 700;
        line-height: 1;
        padding-left: 4px;
    }

    .qty-ctl button.increase {
        -moz-border-radius: 0 3px 3px 0;
        -webkit-border-radius: 0 3px 3px 0;
        border-radius: 0 3px 3px 0;
    }

    .qty-ctl button.increase:before {
        content: "+";
        font-size: 20px;
        line-height: .5;
    }

    .block-layered-nav .block-content .tree-filter .icon:before,
    .block-progress dd.complete a.sample-link:before,
    .opc .step-title a:before {
        font-family: FontAwesome;
        font-style: normal;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    .qty_cart {
        width: 140px;
        display: inline-block;
    }

    .qty_cart input.qty {
        float: left;
        height: 40px;
        width: 60px !important;
        text-align: center;
        margin: 0 -1px;
        -moz-border-radius: 0;
        -webkit-border-radius: 0;
        border-radius: 0;
    }

    .qty_cart input.qty:focus,
    .qty_cart input.qty:hover {
        position: relative;
        z-index: 2;
    }


    table {
        border-collapse: collapse;
        border-spacing: 0;
    }

    table {
        border-collapse: collapse;
        border-spacing: 0;
        empty-cells: show;
        font-size: 100%;
        scrollbar-face-color: expression(runtimeStyle.scrollbarFaceColor '#fff', cellSpacing 0);
    }

    user agent stylesheet table {
        border-collapse: separate;
        border-spacing: 2px;
    }

    .cart-table tr td {
        padding: 18px 15px 10px;
    }

    .data-table thead th {
        color: #FFF;
        background: #DABF5B;
    }

    .data-table th,
    .data-table thead th,
    .data-table tr td {
        border-width: 1px;
        border-style: solid;
    }

    .data-table th,
    .data-table tr td {
        color: #434343;
        font: 500 14px/1.35 Lato, Helvetica Neue, Helvetica, Arial, sans-serif;
        border-color: #e1e1e1;
    }

    .data-table th {
        white-space: nowrap;
    }

    .data-table th,
    .data-table tr td {
        padding: 14px 20px;
    }

    * {
        /* -webkit-box-sizing: border-box; */
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .data-table th,
    .data-table thead th,
    .data-table tr td {
        border-width: 1px;
        border-style: solid;
    }

    #wishlist-table {
        display: inline-block;
        overflow: inherit;
    }

    .data-table,
    .data-table colgroup,
    table colgroup {
        width: 100%;
    }

    .data-table th {
        white-space: nowrap;
    }

    .data-table tr td em {
        font-style: normal;
    }

    .data-table tr td button.button {
        white-space: nowrap;
    }

    .data-table tbody .label {
        display: table-cell;
        vertical-align: middle;
        text-align: left;
    }

    .customer-account-index .my-account .data-table tr th,
    .sales-order-history .my-account .data-table tr th {
        border-left-width: 0;
        border-right-width: 0;
    }

    .customer-account-index .my-account .data-table tr th:first-child,
    .sales-order-history .my-account .data-table tr th:first-child {
        border-left-width: 1px;
    }

    .customer-account-index .my-account .data-table tr th.last,
    .sales-order-history .my-account .data-table tr th.last {
        border-right-width: 1px;
    }

    .customer-account-index .my-account .data-table tr td,
    .sales-order-history .my-account .data-table tr td {
        border-width: 0;
    }

    .customer-account-index .my-account .data-table tr td:first-child,
    .sales-order-history .my-account .data-table tr td:first-child {
        border-left-width: 1px;
    }

    .customer-account-index .my-account .data-table tr td.last,
    .sales-order-history .my-account .data-table tr td.last {
        border-right-width: 1px;
    }

    .customer-account-index .my-account .data-table tr.last td,
    .sales-order-history .my-account .data-table tr.last td {
        border-bottom-width: 1px;
    }

    #checkout-step-review .data-table tr th {
        border-left-width: 0;
        border-right-width: 0;
    }

    #checkout-step-review .data-table tr th:first-child {
        border-left-width: 1px;
    }

    #checkout-step-review .data-table tr th.last {
        border-right-width: 1px;
    }

    #checkout-step-review .data-table tr td {
        border-width: 0;
    }

    #checkout-step-review .data-table tr td:first-child {
        border-left-width: 1px;
    }

    #checkout-step-review .data-table tr td.last {
        border-right-width: 1px;
    }

    #checkout-step-review .data-table tr.last td {
        border-bottom-width: 1px;
    }

    #checkout-step-review .data-table th,
    #checkout-step-review .data-table tr td {
        padding-left: 10px;
        padding-right: 10px;
    }

    #checkout-step-review .data-table tfoot tr td {
        border: none;
        padding: 0 10px 5px 20px;
    }

    .cart .data-table tr td:first-child,
    .cart .data-table tr th:first-child {
        border-left-width: 1px;
    }

    .cart .data-table tr td.last,
    .cart .data-table tr th.last {
        border-right-width: 1px;
    }

    #checkout-step-review .data-table tfoot tr td.last {
        text-align: left !important;
    }

    #checkout-step-review .data-table tfoot tr td .price {
        display: block;
        margin-top: -10px;
    }

    #checkout-step-review .data-table tfoot tr.first td {
        padding-top: 21px;
    }

    #checkout-step-review .data-table tfoot tr.last td {
        padding-top: 0;
        padding-bottom: 16px;
    }

    #checkout-step-review .data-table tfoot tr.last td .price {
        margin-top: -7px;
    }

    .effect-hover-text .banner-text img,
    .effect-hover-text2 .banner-text img,
    .effect-hover-text3 .banner-text img,
    .effect-hover-text4 .banner-text img,
    .effect-hover-text5 .banner-text img {
        margin: 0;
    }

    .cart .data-table tr th {
        border-bottom: 0;
    }

    .cart .data-table tr td {
        border-width: 1px;
    }

    .effect-line-02:after,
    .effect-line:after {
        border-left: 1px solid #fff;
        border-right: 1px solid #fff;
    }

    .cart .data-table tr.last td {
        border-bottom-width: 1px;
    }


    /** reveiw order css**/
    </style>

</head>

<body class="cms-index-index">

    <div class="wrapper">

        <div class="page one-column">

            <div class="em-wrapper-header">

                <?php include 'header.php';

 $_SESSION['address_id'] = $_REQUEST['id']; 	

             ?>

            </div>

            <section style="background: #ececec;margin-bottom:30px;">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                        <li><a href="#">Checkout Process</a></li>
                    </ul>
                </div>
            </section>

            <!-- /.em-wrapper-header -->

            <div class="em-wrapper-main">

                <div class="main-container container" style="background:#fff;padding-bottom: 3rem;">

                    <div class="em-inner-main">

                        <div class="em-wrapper-area02"></div>

                        <div class="em-main-container em-col1-layout">

                            <div class="row">

                                <div class="em-col-main col-sm-12">

                                    <div class="account-login">

                                        <input name="form_key" type="hidden" value="" />

                                        <div class="col2-set">

                                            <div class="col-1 new-users">

                                                <div class="content"> <a href="checkout_process.php">

                                                        <h2><span class="login-icons"><i
                                                                    class="fa fa-sign-in"></i></span>Login / Register
                                                        </h2>

                                                    </a> <a href="checkout_step_2.php">

                                                        <h2><span class="login-icons"><i
                                                                    class="fa fa-map-marker"></i></span>Confirm Address
                                                        </h2>

                                                    </a>

                                                    <h2 class="active"><a href="#rew1"><span class="login-icons"><i
                                                                    class="fa fa-thumbs-up"></i></span>Review Order</a>
                                                    </h2>

                                                    <!--  </span>Coupon Code</h2>-->

                                                    <h2><span class="login-icons"><i class="fa fa-money"></i></span>Make
                                                        Payment</h2>

                                                </div>

                                            </div>

                                            <div class="col-2 registered-users">

                                                <div class="content" id="rew1">

                                                    <div class="content-form">

                                                        <table id="shopping-cart-table" class="data-table cart-table">

                                                            <thead>

                                                                <tr class="em-block-title">

                                                                    <th><span class="nobr">Product Name</span> </th>

                                                                    <th>&nbsp;</th>

                                                                    <th class="a-center" colspan="1"><span
                                                                            class="nobr">Unit Price</span> </th>

                                                                    <th class="a-center">Qty</th>

                                                                    <th class="a-center last" colspan="1">Subtotal</th>

                                                                </tr>

                                                            </thead>

                                                            <tfoot>



                                                                <tr>

                                                                    <td colspan="6" style="text-align: right;"><a
                                                                            href="make_payment.php"
                                                                            class="btn1 btn-4 btn-4c pay_review icon-arrow-right hide">Proceed
                                                                            To Payment </a></td>



                                                                </tr>

                                                            </tfoot>

                                                            <tbody class="cart-items-list clearfix" id="list">

                                                                <?  	$db->select('master_shipment','*',NULL,"id = '".$_SESSION['ship']."'",'');

                                                                    $ship = $db->getResult(); //print_r($cover_img);

                                                                    $ship = $ship[0]; //echo $ship['courier_type'].'ffff'; 

                                                                    $country = $ship['country'];  $courier_type = $ship['courier_type']; $ship_amt_kg = $ship['amount']; 

                                                                            if($country==101){

                                                                            $country_type=0;

                                                                            }

                                                                            else{ $country_type=1; }

                                                                            $db->select('master_installation','*',NULL,"id = '".$_SESSION['instal_amt']."'",'');

                                                                    $instal = $db->getResult(); //print_r($cover_img);

                                                                    $instal = $instal[0]; $int_area = $instal['area'];  ?>
                                                                <tr>



                                                                    <td colspan="4" class="re-sumry-right"> Subtotal
                                                                    </td>

                                                                    <td class="re-sumry-center"><span
                                                                            class="ntotal"></span> </td>

                                                                </tr>

                                                                <tr>

                                                                    <td colspan="4" class="re-sumry-right"> Shippment In
                                                                    </td>

                                                                    <td class="re-sumry-center"><input type="radio" <?
                                                                            if($country_type==0){ ?> checked="checked"
                                                                        <? } ?> value="0" name="country" class="country"
                                                                        onclick="select_country();" />

                                                                        Within India

                                                                        <br />

                                                                        <p> <input type="radio" value="1" <?
                                                                                if($country_type==1){ ?> checked
                                                                            <? } ?> name="country" class="country"
                                                                            onclick="select_country();"/>

                                                                            Outside From India(All Other Countries)</p>
                                                                    </td>

                                                                </tr>

                                                                <tr>



                                                                    <td colspan="4" class="re-sumry-right"> Courier Time
                                                                    </td>

                                                                    <td class="re-sumry-center"><input type="radio" <?
                                                                            if($courier_type==0){ ?> checked
                                                                        <? } ?> value="0" name="courier_type"
                                                                        class="courier_type" onclick="ship_manage(0,1);"
                                                                        />

                                                                        Surface Shipment ( 7 to 10 days)

                                                                        <br />

                                                                        <input type="radio" value="1" <?
                                                                            if($courier_type==1){ ?> checked
                                                                        <? } ?> name="courier_type" class="courier_type"
                                                                        onClick="ship_manage(0,1);" />

                                                                        Air Shipment ( 5 to 7 days) </td>

                                                                </tr>



                                                                <tr>

                                                                    <td colspan="4" class="re-sumry-right"> Select
                                                                        Country</td>

                                                                    <td class="country_id re-sumry-center">

                                                                        <select name="countr" id="countr"
                                                                            class="  form-control" style="width:100%">

                                                                            <?php 

 				 $ct =  "SELECT * FROM all_countries where id=".$_SESSION['country_id']; 

		 	$rrs= mysql_query($ct);

				while($coun = mysql_fetch_array($rrs)){

				?>

                                                                            <option value="<?php echo $coun['id']?>">
                                                                                <?php echo $coun['name']; ?></option>

                                                                            <?php } ?>

                                                                        </select> </td>

                                                                </tr>

                                                                <tr>



                                                                    <td colspan="4" class="re-sumry-right"> Shipping
                                                                        Charges</td>

                                                                    <td class="re-sumry-center"><span
                                                                            class="total_weight"
                                                                            style="display:none">0</span>

                                                                        <span class="total_weight_outside"
                                                                            style="display:none">0</span> <span
                                                                            class="total_ship">0</span><span
                                                                            class="price"></span> </td>

                                                                </tr>

                                                                <tr>



                                                                    <td colspan="4" class="re-sumry-right"> Installation
                                                                        Area</td>

                                                                    <td class="re-sumry-center"><select name="area"
                                                                            id="directions" class="  form-control"
                                                                            onchange="instal_manage();">
                                                                            <option value="NO">No Needs</option>

                                                                            <? 	$db->select('master_installation','*',NULL,"",'');

$instl = $db->getResult(); foreach($instl as $instl1){ ?>

                                                                            <option value="<? echo $instl1['area']; ?>"
                                                                                <? if($int_area==$instl1['area']){ ?>
                                                                                selected
                                                                                <? }  ?>>
                                                                                <? echo $instl1['area']; ?>
                                                                            </option>

                                                                            <? } ?>

                                                                        </select>

                                                                    </td>

                                                                </tr>
                                                                <tr>



                                                                    <td colspan="4" class="re-sumry-right"> Installation
                                                                        Charges</td>

                                                                    <td class="re-sumry-center"><span
                                                                            class="instal_amt">0</span> </td>

                                                                </tr>



                                                                <tr style="background:#DABF5B;" class="grand-value">



                                                                    <td colspan="4" class="re-sumry-right half"
                                                                        align="center"> Grand Total </td>

                                                                    <td class="text-center re-sumry-center">
                                                                        <p class="final-cost" align="center"> Rs &nbsp;
                                                                            <span class="ftotal"></span></p>
                                                                    </td>

                                                                </tr>

                                                            </tbody>

                                                        </table>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <!-- /.account-login -->

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <!-- /.em-wrapper-main -->

            <?php include('footer.php'); ?>
            <?php include('script.php'); ?>

        </div>

        <!-- /.page -->

    </div>

    <!-- /.wrapper -->

</body>

<script type="text/javascript" src="js/review_order.js"></script>

<script>
$(document).ready(function() {

    ship_manage( <?= $_SESSION['country_id'] ?> );

    var proceed = $('.ntotal').text();

    proceed = $.trim(proceed);

    if (proceed == '' || proceed == 0) {

        window.location = "index.php";

    }

});
</script>

</html>