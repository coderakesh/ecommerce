<footer class="footer-container typefooter-1">
      
        
        <div class="footer-middle ">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">
                        <div class="infos-footer">
                            <a href="#" class="module"> <h3 class="modtitle">Find A Store </h3></a>
                             
                            <ul class="menu">
                                <li class="adres">
                                131 "Subhag City Centre", Opp. I.C.H., Malviya Nagar, Bhopal (M.P.), India
                                </li>
                                <li class="phone">
                                +91 7552573571, 0755 422 1206
                                </li>
                                <li class="mail">
                                    <a href="#"> mail@example.com</a>
                                </li>
                                
                            </ul>
                        </div>


                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                        <div class="box-service box-footer">
                            <div class="module clearfix">
                                <h3 class="modtitle">Quick Link</h3>
                                <div class="modcontent">
                                    <ul class="menu">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Support</a></li>
                                      
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                        <div class="box-information box-footer">
                            <div class="module clearfix">
                                <h3 class="modtitle">What's in store</h3>
                                <div class="modcontent">
                                    <ul class="menu">
                                        <li><a href="#">Women's Footwear</a></li>
                                      <li><a href="#">Men's Footwear</a></li>
                                      <li><a href="#">New Arrivals</a></li>
                                      <li><a href="#">Offer Zone</a></li>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                        <div class="box-account box-footer">
                            <div class="module clearfix">
                                <h3 class="modtitle">Help & Support</h3>
                               <div class="modcontent">
                                    <ul class="menu">
                                        <li><a href="#">Terms & Conditions</a></li>
                                      <li><a href="#">Privacy Polocy</a></li>
                                      <li><a href="#">Refund & Cancillation</a></li>
                                      <li><a href="#">Shipping Policy</a></li>
                                       
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">



                    <div class="module product-simple">
                    
                    <div class="modcontent">
                        <div class="extraslider">
                            <!-- Begin extraslider-inner -->
                            <div class="extraslider-inner">
                                <div class="item ">
                                    <div class="product-layout item-inner style1 ">
                                        <div class="item-image">
                                            <div class="item-img-info">
                                                 
                                                    <img src="image/icon/1.png" alt="Original">
                                                     
                                            </div>
                                            
                                        </div>
                                        <div class="item-info">
                                            <div class="item-title">
                                                <strong>100% ORIGINAL </strong> guarantee for all products at Shoooz.com  
                                            </div>    
                                        </div>
                                        <!-- End item-info -->
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->
                                    <div class="product-layout item-inner style1 ">
                                        <div class="item-image">
                                            <div class="item-img-info">
                                               
                                                        <img src="image/icon/2.png">
                                                         
                                            </div>
                                            
                                        </div>
                                        <div class="item-info">
                                            <div class="item-title">
                                               
                                                <strong>Return within 30days </strong>of receiving your order
                                                
                                            </div>
                                             
                                            
                                        </div>
                                        <!-- End item-info -->
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->
                                    <div class="product-layout item-inner style1 ">
                                        <div class="item-image">
                                            <div class="item-img-info">
                                                
                                                            <img src="image/icon/3.png" alt="Sausage cowbee">
                                                            
                                            </div>
                                           
                                        </div>
                                        <div class="item-info">
                                            <div class="item-title">
                                              
                                                <strong>Get free delivery </strong> for every order above Rs. 1199
                                                        
                                            </div>
                                             
                                           
                                             
                                        </div>
                                        <!-- End item-info -->
                                        <!-- End item-wrap-inner -->
                                    </div>
                                    <!-- End item-wrap -->
                                      
                                </div>
                            </div>
                            <!--End extraslider-inner -->
                        </div>
                    </div>
                </div>


                        <!-- <div class="module box-footer so-instagram-gallery-ltr">

                            <h3 class="modtitle">Let's Be In Touch</h3>


                          

                            <div class="modcontent">
                                   <iframe src="//www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/shoooztheshoeroom/&amp;width=350&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true"

						          style="border: none; overflow: hidden; width: 100%; height: 220px;"> </iframe>
                            </div>
                            

                          
                        </div> -->
                    </div>

                    
                    <div class="col-lg-12 col-xs-12 text-center hide">
                        <img src="image/catalog/demo/payment/payment.png" alt="imgpayment">
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Bottom Container -->
        <div class="footer-bottom ">
            <div class="container">
                <div class="copyright">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 copyright-w">
                        <div class="copyright">© 2019-20 Shoooz. All Rights Reserved. Designed by <a href="#" target="_blank">TBI Technologies</a>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 payment-w">
                        <img src="image/catalog/demo/payment/payment-method_2dd397.svg" alt="imgpayment">
                    </div>
                </div>
                   
                </div>
            </div>
        </div>
        <!-- /Footer Bottom Container -->
        
        <!--Back To Top-->
        <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
            <!--Back To Top-->
        <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
    </footer>

    

<script >$(function(){$.ui.autocomplete.prototype._renderItem=function(ul,item){item.label=item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("+$.ui.autocomplete.escapeRegex(this.term)+")(?![^<>]*>)(?![^&;]+;)","gi"),"<span style='color:#2B2BFF'>$1</span>");return $("<li></li>").data("item.autocomplete",item).append("<a>"+item.label+"</a>").appendTo(ul);};$('.tt').autocomplete({source:"search_product.php",minLength:2,select:function(event,ui){$("#search_cat").val(ui.item.product_id);$("#search_pro").val(ui.item.product_id);$("#ssss").val(ui.item.form_action);if(ui.item.cat_id!=''){$('.search-product').prop('disabled',false);$("#searchthis").attr('action',ui.item.form_action);$('.search-product').click();}}});});$('#theme-setting > a').click(function(){$(this).next().animate({width:'toggle'},500,function(){if($(this).is(":hidden")){$('#theme-setting > a > i').attr('class','fa fa-gears fa-2x');}else{$('#theme-setting').css("background","green");$('#theme-setting > a > i').attr('class','fa fa-times fa-2x');}});$(this).next().css('display','inline-block');});function add_wishlist(product_id){$.ajax({type:'post',url:'add_wishlist.php',data:'product_id='+product_id,success:function(response){response=$.trim(response);if(response==1){$.alert.open('info','Your Item Has Been Added to Shortlist');}else{}}});}</script>