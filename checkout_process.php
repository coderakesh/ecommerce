<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<!-- Basic page needs
    ============================================ -->
<title>Spare parts, Apex Marketing</title>
<meta charset="utf-8">
<link rel="shortcut icon" type="image/png" href="ico/favicon.ico"/>
<!-- Mobile specific metas
    ============================================ -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<?php include('link.php'); ?>
<style type="text/css">
body {
	font-family:'Roboto', sans-serif
}
.typeheader-1 .header-middle .middle-right {
	padding-top: 20px;
}
.font-12 {
	font-size: 12px;
}
.typefooter-01 {
	margin-top: 0px;
	background: #ffffff !important;
}
.info-contact {
	margin-bottom: 25px;
}
.typefooter-1 .infos-footer ul {
	margin-top: 20px;
}
</style>
<link rel="stylesheet" type="text/css" href="css/livevalid.css" />
<script type="text/javascript" src="js/livevalidation_standalone.compressed.js"></script>
<style type="text/css">
.LV_valid_field, input.LV_valid_field:hover, input.LV_valid_field:active, textarea.LV_valid_field:hover, textarea.LV_valid_field:active {
	border: none;
}
.LV_invalid_field, input.LV_invalid_field:hover, input.LV_invalid_field:active, textarea.LV_invalid_field:hover, textarea.LV_invalid_field:active {
	border: none;
}
.LV_validation_message {
	top: 49px;
	position: absolute;
}
a:hover, .btn-link:hover {
	color: #327399;
	text-decoration: none;
}
.content-form {
	box-shadow: 0 0 10px #E0E0E0;
}
 
.account-create .fieldset .legend, .account-login .col2-set h2 {
	
	font-size: 15px;
	margin-bottom: 10px;
	color: #434343;
	font-weight: 400;
	padding: 21px 0;
}
*, :after, :before, fieldset {
	margin:0;
	padding:0
}
abbr, acronym, body, fieldset, html, img, table {
	border:0
}
.v-top, img, sup {
	vertical-align:top
}
.product-name a, input.input-text, select, textarea {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease
}
@font-face {
font-family:FontAwesome;
src:url(fonts/fontawesome-webfont.eot?v=4.1.0);
src:url(fonts/fontawesome-webfont.eot?#iefix&v=4.1.0) format("embedded-opentype"), url(fonts/fontawesome-webfont.woff?v=4.1.0) format("woff"), url(fonts/fontawesome-webfont.ttf?v=4.1.0) format("truetype"), url(fonts/fontawesome-webfont.svg?v=4.1.0#fontawesomeregular) format("svg");
font-weight:400;
font-style:normal
}
@font-face {
font-family:maki;
src:url(fonts/maki.eot?56675205);
src:url(fonts/maki.eot?56675205#iefix) format("embedded-opentype"), url(fonts/maki.woff?56675205) format("woff"), url(fonts/maki.ttf?56675205) format("truetype"), url(fonts/maki.svg?56675205#maki) format("svg");
font-weight:400;
font-style:normal
}
html {
	-webkit-tap-highlight-color:black(0);
	-webkit-text-size-adjust:100%
}
body {
	-webkit-text-size-adjust:none;
	text-align:left
}
a {
	text-decoration:none
}
.underline, a:hover {
	text-decoration:underline
}
:focus {
	outline:0
}
a:focus {
	outline:0!important
}
.ie10 #hack:focus {
	background:0 0
}
@media all and (min-width:0) {
:focus {
background:0 0
}
}
h1, h2, h3, h4, h5, h6 {
	font-size:100%;
	font-weight:400
}
caption, cite, code, td, th {
	font-weight:400;
	font-style:normal;
	text-align:left;
	vertical-align:top
}
.hidden, .no-margin {
	margin:0!important
}
.hidden, .no-padding {
	padding:0!important
}
ol, ul {
	list-style:none
}
form {
	display:inline
}
legend {
	display:none
}
table {
	border-collapse:collapse;
	border-spacing:0;
	empty-cells:show;
	font-size:100%;
scrollbar-face-color:expression(runtimeStyle.scrollbarFaceColor '#fff', cellSpacing 0)
}
strong {
	font-weight:700
}
address, cite {
	font-style:normal
}
blockquote, q {
	quotes:none
}
q:after, q:before {
	content:''
}
big, small {
	font-size:80%
}
sup {
	font-size:1em
}
.em_main {
	min-height:250px
}
.cms-index-index .em_main, .sidebar {
	min-height:1px
}
input:focus, input[type=search]:focus {
	outline-offset:-2px
}
input[type=search] {
	-webkit-appearance:none;
	-moz-box-sizing:border-box;
	-webkit-box-sizing:border-box;
	box-sizing:border-box
}
[data-toggle=tooltip] {
display:inline-block!important
}
.hidden {
	display:block!important;
	border:0!important;
	font-size:0!important;
	line-height:0!important;
	width:0!important;
	height:0!important;
	overflow:hidden!important
}
.nobr {
	white-space:nowrap!important
}
.wrap {
	white-space:normal!important
}
.a-left {
	text-align:left!important
}
.a-center {
	text-align:center!important
}
.a-right {
	text-align:right!important
}
.v-middle, input, select, textarea {
	vertical-align:middle
}
.f-left, .left {
	float:left!important
}
.f-right, .right {
	float:right!important
}
.f-none {
	float:none!important
}
.col3-set .col-1, .col3-set .col-2 {
	float:left;
	width:32%
}
.f-fix {
	float:left;
	width:100%
}
.no-display {
	display:none
}
.no-bg {
	background:0 0!important
}
.em-text-upercase, .em-text-uppercase {
	text-transform:uppercase
}
.img-banner {
	margin-bottom:50px;
	display:block;
	position:relative
}
.img-banner.img-responsive {
	display:inline-block
}
.img-responsive-center {
	display:inline-block;
	max-width:100%;
	height:auto
}
.img-responsive {
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.img-banner-small {
	margin-bottom:20px;
	position:relative
}
.spa-center {
	padding:0 15%;
	text-align:center
}
.page, .page-popup, .validation-advice, p.required {
	text-align:left
}
.em-wrapper-banners {
	margin-bottom:50px
}
.box {
	border:1px solid #e5e5e5;
	padding:2rem;
	margin-bottom:25px
}
.box.f-left {
	margin-right:10px
}
.box.f-right {
	margin-left:10px
}
.line-right {
	border-right-width:1px;
	border-right-style:solid
}
.line-left {
	border-left-width:1px;
	border-left-style:solid
}
.em-wrapper-one-ads {
	margin-bottom:20px
}
.em-clear {
	clear:both
}
.em-clear-padding {
	padding-left:0!important;
	padding-right:0!important
}
.em-clear-margin {
	margin-left:0!important;
	margin-right:0!important
}
.em-clear-radius {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.widget {
	display:block
}
.em-collapsed:after, .non-collapsed:after {
	right:0;
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	font-size:100%;
	top:0
}
.non-collapsed {
	position:relative;
	cursor:pointer
}
.non-collapsed:after {
	content:"\f067";
	position:absolute
}
.em-collapsed {
	position:relative;
	cursor:pointer
}
.em-collapsed:after {
	position:absolute;
	content:"\f068"
}
.em-scroll-vertial::-webkit-scrollbar {
width:10px
}
.em-scroll-vertial::-webkit-scrollbar-track {
background:#ebebeb;
-webkit-border-radius:10px;
border-radius:10px
}
.em-scroll-vertial::-webkit-scrollbar-thumb {
-webkit-border-radius:10px;
border-radius:10px;
background:#1ca586
}
.em-scroll-vertial {
	scrollbar-base-color:#1ca586;
	scrollbar-track-color:#ebebeb;
	scrollbar-arrow-color:#c4c4c4
}
.wrapper {
	background-color:#fff;
	background-repeat:no-repeat;
	height:600px;
	background-size:cover
}
 
.cms-index-index .em-col-main {
	padding-bottom:0
}
.page {
	margin:0 auto;
	padding:0
}
.page-empty, .page-print {
	background:#fff;
	padding:20px;
	text-align:left
}
.col2-set .col-narrow {
	width:33%
}
.col2-set .col-wide {
	width:65%
}
.col3-set .col-2 {
	margin-left:2%
}
.col3-set .col-3 {
	float:right;
	width:32%
}
.col4-set .col-1, .col4-set .col-2, .col4-set .col-3 {
	float:left;
	width:23.5%
}
.col4-set .col-2 {
	margin:0 2%
}
.col4-set .col-4 {
	float:right;
	width:23.5%
}
.col2-set-nospace .col-1 {
	float:left;
	width:50%
}
.col2-set-nospace .col-2 {
	float:right;
	width:50%
}
.em-box-custom .page {
	-webkit-box-shadow:0 0 7px #a8a8a8;
	-moz-box-shadow:0 0 7px #a8a8a8;
	box-shadow:0 0 7px #a8a8a8
}
@media (min-width:1200px) {
.wrapper.em-box-custom .page {
width:1200px
}
}
input, select, textarea {
	font:12px/1.35 Arial, Helvetica, sans-serif
}
input.input-text, select, textarea {
	padding:5px 10px;
	border:1px solid;
	height:40px;
	line-height:28px;
	font-size:13px;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	width:50%
}
#back-top, .product-name a {
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
select {
	
	height:36px;
	line-height:14px
}
select.multiselect {
	height:auto
}
select option {
	padding-right:10px
}
select.multiselect option {
	padding:2px 5px
}
textarea {
	overflow:auto;
	height:100px
}
input.checkbox, input.radio {
	margin-right:3px
}
input.qty {
	width:40px!important
}
button.button::-moz-focus-inner {
padding:0;
border:0
}
p.control input.checkbox, p.control input.radio {
	margin-right:6px;
	float:left;
	margin-top:0
}
.form-list li, .form-list li.fields .fields {
	margin-right:3%
}
.gift-messages-form input.checkbox {
	float:left;
	margin-top:0
}
.form-list li {
	margin-bottom:5px
}
.form-list li.fields {
	margin-right:0
}
.form-list li.control {
	margin:13px 0
}
.form-list label {
	float:left;
	position:relative;
	z-index:0;
	padding:6px 0 5px;
	margin-bottom:0;
	text-transform:capitalize
}
.form-list label.required em {
	float:right;
	font-style:normal;
	position:absolute;
	top:0;
	right:-8px;
	color:#fe0000
}
.error-msg, .fieldset .legend, .note-msg, .notice-msg, .toolbar-dropdown, .validation-advice {
	position:relative
}
.form-list li.control label {
	float:none;
	display:block;
	padding:0;
	text-transform:none
}
.form-list li.control input.checkbox, .form-list li.control input.radio {
	margin-right:6px;
	float:left;
	margin-top:0;
	min-height:17px
}
.form-list .field {
	float:left;
	width:47%;
	margin-right:3%
}
.form-list li.control .input-box {
	clear:none;
	display:inline;
	width:auto
}
.form-list .input-box, .form-list .input-range {
	display:block;
	clear:both
}
.error-msg:before, .validation-advice:before {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	font-size:1em
}
.form-list textarea {
	width:254px;
	height:10em
}
.form-list .input-box, .form-list input.input-text, .form-list li.wide textarea, .form-list select {
	width:100%
}
.form-list li.additional-row {
	border-top:1px solid #ccc;
	margin-top:10px;
	padding-top:7px
}
.form-list li.additional-row .btn-remove {
	float:right;
	margin:5px 0 0
}
.fieldset, .v-fix {
	float:left
}
.fieldset {
	width:49%
}
.validation-advice {
	clear:both;
	margin:3px 0 5px;
	padding:5px 0;
	color:#e70808
}
.validation-advice:before {
	content:"\f176";
	padding-right:10px
}
.error, .success {
	font-weight:700
}
.validation-failed {
	border:1px solid #ffa6a6!important;
	background:#fef4f4!important
}
p.required {
	color:red;
	clear:left;
	padding-top:0
}
.divider, .sub-title, .subtitle {
	clear:both
}
.v-fix .validation-advice {
	display:block;
	position:relative
}
.success {
	color:#3d6611
}
.error {
	color:red
}
.notice {
	color:#ccc
}
.std .messages ul {
	padding:0;
	margin:0
}
.messages, .messages ul {
	list-style:none!important;
	margin:0 0 10px;
	padding:0
}
.messages {
	width:100%;
	overflow:hidden;
	padding-left:0!important
}
.messages>li {
	margin-bottom:5px
}
.messages li li, .messages ul {
	margin:0
}
.error-msg, .note-msg, .notice-msg {
	border-style:solid;
	border-width:1px;
	background-position:10px 9px;
	background-repeat:no-repeat;
	padding:8px 8px 8px 10px
}
.error-msg {
	border-color:#f16048;
	background-color:#ffe3e3;
	color:#df280a
}
.error-msg:before {
	content:"\f071";
	padding-right:10px;
	float:left;
	margin-top:1px
}
.note-msg span:before, .notice-msg span:before, .success-msg li span:before {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	font-size:100%
}
.note-msg, .notice-msg {
	border-color:#fcd344;
	background-color:#fafaec;
	color:#3d6611
}
.note-msg span:before, .notice-msg span:before {
	content:"\f06a";
	padding-right:7px
}
.success-msg {
	border:1px solid;
	padding:2px
}
.success-msg li {
	border:1px solid;
	padding:10px 15px;
	margin:0
}
.success-msg li span:before {
	content:"\f00c";
	padding-right:7px
}
.pager .pages li .next:before, .pager .pages li .previous:before {
	line-height:1;
	font-size:12px;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
.page-title {
	margin:0 0 10px
}
.page-title h1, .page-title h2 {
	margin:0
}
.page-title .separator {
	margin:0 3px
}
.page-title .link-rss {
	float:right
}
.title-buttons {
	text-align:right
}
.title-buttons h1, .title-buttons h2, .title-buttons h3, .title-buttons h4, .title-buttons h5, .title-buttons h6 {
	float:left;
	background:0 0
}
.pager .amount {
	float:left;
	margin:5px 0 0
}
.pager .limiter {
	float:right;
	margin-right:20px
}
.pager .pages {
	margin-top:8px
}
.pager .pages ol {
	margin-bottom:0;
	float:right;
	padding:8px 0
}
.pager .pages li {
	display:inline;
	margin:0 0 0 3px;
	text-align:center
}
.pager .pages li.current {
	padding:5px 11px;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	display:inline-block
}
.pager .pages li>a, .pager .pages li>span {
	background:0 0;
	border:none;
	padding:5px 11px;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px
}
.pager .pages li>a:focus, .pager .pages li>a:hover, .pager .pages li>span:focus, .pager .pages li>span:hover {
	background:0 0
}
.pager .pages li .next:before {
	display:inline-block;
	content:"\f0da"
}
.pager .pages li .next img {
	display:none
}
.pager .pages li .previous:before {
	display:inline-block;
	content:"\f0d9"
}
.pager .pages li .previous img {
	display:none
}
.pager label, .sort-by label, .sorter label {
	margin-right:5px;
	margin-top:6px;
	float:left;
	text-transform:capitalize;
	font-weight:400
}
.sorter .view-mode {
	float:left
}
.sorter .view-mode label {
	margin-top:1px
}
.sorter .sort-by {
	float:right
}
.pager select, .sorter select {
	width:100px;
	padding:5px 10px;
	height:31px;
	line-height:30px;
	float:left
}
.toolbar-title {
	float:left
}
.toolbar-dropdown {
	width:100px;
	float:left
}
.toolbar-dropdown span.current {
	display:block;
	padding:5px 10px;
	cursor:pointer;
	border:1px solid
}
.toolbar-dropdown span.current:after {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	content:"\f0d7";
	font-size:100%;
	float:right;
	margin-left:5px;
	margin-top:5px
}
.std blockquote, tr.summary-details-excluded {
	font-style:italic
}
.toolbar-dropdown ul {
	margin:0;
	border:1px solid;
	position:absolute;
	right:0;
	top:26px;
	width:100px;
	z-index:10
}
.std dl dd, .std ul.disc {
	margin:0 0 10px
}
.toolbar-dropdown li {
	padding:5px;
	background-color:#fff
}
.cart-tax-info, .cart-tax-info .cart-price, .cart-tax-total {
	padding-right:20px
}
.toolbar-dropdown li:hover {
	background-color:#f9efe2
}
.toolbar-dropdown li a {
	text-decoration:none;
	display:block
}
tr.summary-total {
	cursor:pointer
}
tr.summary-total .summary-collapse {
	float:right;
	text-align:right;
	padding-left:20px;
	background:url(../images/bkg_collapse.gif) 0 4px no-repeat;
	cursor:pointer
}
tr.show-details .summary-collapse {
	background-position:0 -53px
}
tr.summary-details td {
	font-size:90%;
	background-color:#dae1e4;
	color:#626465
}
.divider, .rating-box {
	font-size:0;
	text-indent:-999em;
	overflow:hidden
}
tr.summary-details-first td {
	border-top:1px solid #d2d8db
}
.cart-tax-total {
	display:block;
	background:url(../images/bkg_collapse.gif) 100% 4px no-repeat;
	cursor:pointer
}
.cart-tax-info .price, .cart-tax-total .price {
	display:inline!important;
	font-weight:400!important
}
.std b, .std dl dt, .std dt, .std strong {
	font-weight:700
}
.cart-tax-total-expanded {
	background-position:100% -53px
}
.std .subtitle {
	padding:0
}
.std ol.ol {
	list-style:decimal;
	padding-left:1.5em
}
.std ul, .std ul.disc {
	list-style:disc
}
.std ul.disc {
	padding-left:18px
}
.std address, .std blockquote, .std dl, .std ol, .std ul {
	margin:0 0 1em;
	padding:0
}
.std ol, .std ul {
	padding-left:1.5em
}
.std ol {
	list-style:decimal
}
.std blockquote, .std dd {
	padding:0 0 0 1.5em
}
.std ul ul {
	list-style-type:circle
}
.std ol ol, .std ol ul, .std ul ol, .std ul ul {
	margin:.5em 0
}
.std address {
	font-style:normal
}
.product-name, .product-options .options-list label, .product-options dt label {
	font-weight:400
}
.std em, .std i {
	font-style:italic
}
.std ul.none-style {
	list-style:none;
	padding-left:0
}
.links li {
	display:inline;
	padding-right:14px
}
.links li.first {
	padding-left:0
}
.links li.last {
	background:0 0;
	padding-right:0
}
.separator {
	margin:0 3px
}
.divider {
	display:block;
	line-height:0;
	height:1px;
	margin:10px 0;
	background:#ddd
}
.noscript {
	border:1px solid #ddd;
	border-width:0 0 1px;
	background:#ffff90;
	line-height:1.25;
	text-align:center;
	color:#2f2f2f
}
.noscript .noscript-inner {
	width:1000px;
	margin:0 auto;
	padding:12px 0;
	background:url(../images/i_notice.gif) 20px 50% no-repeat
}
.noscript p {
	margin:0
}
.demo-notice {
	margin:0;
	padding:6px 10px;
	background:#d75f07;
	line-height:1.15;
	text-align:center;
	color:#fff
}
.notice-cookie {
	border-bottom:1px solid #cfcfcf;
	background:#ffff90;
	line-height:1.25;
	text-align:center;
	color:#2f2f2f
}
.notice-cookie .notice-inner {
	width:870px;
	margin:0 auto;
	padding:12px 0 12px 80px;
	background:url(../images/i_notice.gif) 20px 25px no-repeat;
	text-align:left
}
.notice-cookie .notice-inner p {
	margin:0 0 10px;
	border:1px dotted #cccc73;
	padding:10px
}
.add-to-links .separator {
	display:none
}
.add-to-box, .add-to-links, .add-to-links li {
	display:inline-block
}
.std .add-to-links {
	padding:0;
	list-style:none;
	margin:0
}
.add-to-links li {
	margin-right:5px;
	vertical-align:top
}
.add-to-box {
	clear:both;
	width:100%
}
.no-rating, .ratings {
	overflow:hidden;
	clear:left
}
.no-rating .rating-box, .ratings .rating-box {
	display:inline-block;
	margin-right:5px;
	margin-top:0
}
.rating-box .rating {
	float:left;
	height:11px;
	background:url(../images/bkg_rating.png) 0 100% repeat-x
}
.rating-box {
	width:60px;
	height:11px;
	line-height:0;
	background:url(../images/bkg_rating.png) repeat-x
}
.product-options .options-list .price, .product-options .price {
	font-size:100%!important
}
.product-view .product-img-box .product-image-zoom {
	position:relative;
	overflow:hidden;
	z-index:9
}
.product-view .product-img-box .product-image-zoom img {
	position:absolute;
	left:0;
	top:0;
	cursor:move
}
.product-view .product-img-box .zoom-notice {
	float:left;
	display:block;
	clear:both;
	width:100%
}
.product-view .product-img-box .zoom {
	position:relative;
	z-index:9;
	height:2px;
	margin:10px auto 20px;
	padding:0 28px;
	cursor:pointer;
	float:left;
	clear:left;
	-webkit-box-sizing:border-box;
	-moz-box-sizing:border-box;
	box-sizing:border-box;
	background:url(../images/slider_bg.gif) 50% 50% no-repeat
}
.product-view .product-img-box .zoom.disabled {
	opacity:.3;
	filter:alpha(opacity=30)
}
.product-view .product-img-box .zoom .ui-slider, .product-view .product-img-box .zoom .ui-slider2 {
	position:relative;
	height:2px
}
.product-view .product-img-box .zoom #handle, .product-view .product-img-box .zoom #handle2 {
	position:absolute;
	left:0;
	top:-10px;
	width:9px;
	height:22px;
	background:url(../images/magnifier_handle.gif) no-repeat
}
.product-view .product-img-box .zoom .btn-zoom-out {
	position:absolute;
	left:0;
	top:-8px
}
.product-view .product-img-box .zoom .btn-zoom-in {
	position:absolute;
	right:0;
	top:-8px
}
.cloud-zoom-lens {
	max-width:100%;
	max-height:100%;
	border:none!important;
	margin:0!important
}
.product-options ul.validation-failed {
	padding:0 7px
}
.product-options-bottom .add-to-cart {
	display:block;
	overflow:hidden;
	clear:both
}
.product-options-bottom {
	display:inline-block;
	width:100%
}
.product-options dt label em {
	color:#fc0b2d;
	margin-right:5px
}
.product-options dt .qty-holder {
	float:right
}
.product-options dt .qty-holder label {
	vertical-align:middle
}
.product-options dt .qty-disabled {
	background:0 0;
	border:0;
	padding:3px;
	color:#000
}
.product-options dd {
	margin:4px 0
}
.product-options dd .qty-holder {
	display:block;
	margin:10px 0 15px
}
.product-options dd .time-picker {
	display:-moz-inline-box;
	display:inline-block;
	padding:2px 0;
	vertical-align:middle
}
.product-options dd input.datetime-picker {
	width:150px
}
.product-options dl {
	margin-bottom:0
}
.product-options .options-list li {
	margin:5px 0
}
.product-options .options-list input.radio {
	float:left;
	margin:2px -18px 0 1px
}
.product-options .options-list input.checkbox {
	float:left;
	margin:2px -20px 0 1px
}
.product-options .options-list .label {
	display:block;
	margin-left:20px;
	text-align:left;
	white-space:normal
}
.grouped-items-table {
	margin-bottom:25px;
	margin-top:25px
}
.grouped-items-table td {
	vertical-align:middle;
	padding:10px
}
.grouped-items-table td input.qty {
	height:30px;
	line-height:30px!important;
	width:50px;
	text-align:center
}
.grouped-items-table th {
	padding:10px;
	text-align:left!important
}
.product-name {
	font-size:1em;
	min-height:25px;
	text-align:left
}
.product-name a {
transition:all .5s ease;
	backface-visibility:hidden
}
.cart-msrp-totals {
	color:red;
	font-size:12px!important;
	font-weight:700;
	margin:10px 10px 0;
	padding:10px;
	text-align:right;
	text-transform:uppercase
}
.map-cart-sidebar-total {
	color:red;
	display:block;
	font-size:10px;
	font-weight:700;
	text-align:left;
	padding:2px 5px;
	text-shadow:0 1px 0 #fff
}
.map-popup {
	background:#fff;
	border:1px solid #aaa;
	margin:12px 0 0;
	position:absolute;
	-webkit-box-shadow:0 0 6px #ccc;
	-moz-box-shadow:0 0 6px #ccc;
	box-shadow:0 0 6px #ccc;
	text-align:left;
	width:300px;
	z-index:100
}
.map-popup-heading {
	background:#d9e5ee;
	border-bottom:1px solid #ccc;
	padding:5px 30px 5px 10px;
	width:298px
}
.map-popup-heading h2 {
	font-size:16px;
	margin:0;
	text-shadow:0 1px 0 #f6f6f6;
	overflow:hidden;
	white-space:nowrap;
	word-wrap:break-word;
	text-align:left;
	text-overflow:ellipsis
}
.map-popup-arrow {
	background:url(../images/map_popup_arrow.gif) no-repeat;
	position:absolute;
	left:50%;
	top:-10px;
	height:10px;
	width:19px
}
.map-popup-close, .tool-tip .btn-close a {
	overflow:hidden;
	height:15px;
	text-indent:-99999px;
	vertical-align:middle
}
.map-popup-close {
	font-size:0;
	display:block;
	text-align:left;
	content:"";
	position:absolute;
	right:10px;
	top:8px;
	width:15px;
	-webkit-box-shadow:0 0 3px #999;
	-moz-box-shadow:0 0 3px #999;
	box-shadow:0 0 3px #999;
	-moz-border-radius:2px;
	-webkit-border-radius:2px;
	border-radius:2px;
	background:url(../images/btn_window_close.gif) no-repeat
}
.map-popup-content {
	border-top:1px solid #eee;
	padding:10px;
	overflow:hidden;
	text-align:left;
	width:280px
}
.map-popup-checkout {
	display:inline;
	float:right;
	text-align:right
}
.map-popup-checkout span {
	display:block;
	padding-right:30px
}
.map-popup-checkout .paypal-logo {
	margin:0 0 5px
}
.map-popup-price .price-box, .map-popup-price .price-box .special-price {
	margin:0;
	padding:0
}
.map-popup-price {
	margin:5px 0 0
}
.map-popup-text {
	clear:right;
	margin:0 10px;
	padding:10px 0;
	text-align:left;
	word-wrap:break-word
}
.map-popup-only-text {
	border-top:1px solid #ddd
}
.map-popup-checkout button.btn-cart {
	float:right
}
.map-info .price {
	margin-right:10px
}
.rss-title h1 {
	background:url(../images/i_rss-big.png) 0 4px no-repeat;
	padding-left:27px
}
.rss-table .link-rss {
	display:block;
	line-height:1.35;
	background-position:0 2px
}
.checkout-multishipping-address-editshipping .topcart-popup .btn-remove, .checkout-multishipping-address-newbilling .topcart-popup .btn-remove, .checkout-multishipping-address-newshipping .topcart-popup .btn-remove, .checkout-multishipping-address-selectbilling .topcart-popup .btn-remove, .checkout-multishipping-addresses .topcart-popup .btn-remove, .checkout-multishipping-billing .topcart-popup .btn-remove, .checkout-multishipping-overview .topcart-popup .btn-remove, .checkout-multishipping-shipping .topcart-popup .btn-remove, .checkout-onepage-index .topcart-popup .btn-remove {
	display:none!important
}
.cart .title-buttons .checkout-types {
	display:none
}
.dd-truncated {
	width:100%;
	margin-top:5px;
	float:left
}
.truncated {
	cursor:help;
	padding-top:0;
	float:left
}
.truncated a.dots {
	cursor:help
}
.truncated a.details {
	cursor:help;
	float:left
}
.truncated .truncated_full_value {
	position:relative;
	z-index:999
}
.truncated .truncated_full_value .item-options {
	position:absolute;
	top:-99999em;
	left:-99999em;
	z-index:999;
	width:250px;
	padding:8px;
	border:1px solid #ddd;
	background-color:#f6f6f6
}
.truncated .truncated_full_value .item-options>p {
	font-weight:700;
	text-transform:uppercase
}
.sp-methods dt, dl.item-options dt {
	font-weight:400
}
.truncated .show .item-options {
	top:15px;
	left:0
}
dl.item-options dd .price {
	font-size:100%
}
.please-wait {
	margin-right:5px;
	margin-top:0;
	display:inline-block
}
.please-wait img {
	vertical-align:middle
}
.cvv-what-is-this {
	cursor:help;
	margin-left:5px;
	line-height:40px
}
.info-set {
	margin-bottom:15px
}
.paypal-shipping-method.box {
	border:none;
	padding:0;
	margin:0
}
.tool-tip, .tool-tip .tool-tip-content {
	padding:5px
}
.sp-methods input.radio {
	margin-top:-1px;
	display:inline-block
}
.sp-methods .checkmo-list address {
	float:left
}
.sp-methods .centinel-logos a {
	margin-right:3px
}
.sp-methods .centinel-logos img {
	vertical-align:middle
}
.sp-methods .release-amounts {
	margin:.5em 0 1em
}
.sp-methods .release-amounts button {
	float:left;
	margin:5px 10px 0 0
}
.tool-tip {
	border:1px solid #ddd;
	background-color:#f6f6f6;
	position:absolute;
	z-index:9999;
	top:200px!important
}
.tool-tip .btn-close {
	text-align:right
}
.tool-tip .btn-close a {
	font-size:0;
	display:block;
	text-align:left;
	width:15px;
	background-position:0 0;
	background-repeat:no-repeat;
	background-image:url(../images/btn_window_close.gif)
}
.gift-messages-form label {
	float:none!important;
	position:static!important;
	display:inline-block
}
.gift-messages-form .item {
	margin:0 0 10px
}
.gift-messages-form .item .product-img-box {
	float:left;
	width:75px
}
.gift-messages-form .item .product-image {
	margin:0 0 7px
}
.gift-messages-form .item .number {
	margin:0;
	font-weight:700;
	text-align:center
}
.gift-messages-form .item .details {
	margin-left:90px
}
.gift-message-link {
	display:block;
	background:url(../images/bkg_collapse.gif) 0 4px no-repeat;
	padding-left:20px
}
.gift-message-link.expanded {
	background-position:0 -53px
}
.gift-message-row .btn-close {
	float:right
}
.opc .gift-messages-form {
	margin:0 0 10px;
	padding:10px 15px;
	position:static
}
.captcha-image, .multiple-checkout {
	position:relative
}
.opc .gift-messages-form .inner-box {
	padding:5px
}
.checkout-agreements .agree {
	padding:6px 0;
	margin-top:10px
}
.checkout-agreements li {
	margin:10px 0
}
.checkout-agreements .agreement-content {
	border:1px solid #ddd;
	background-color:#f6f6f6;
	padding:5px;
	height:10em!important;
	overflow:auto
}
.checkout-agreements .agreement-content input.checkbox {
	margin-top:0;
	margin-right:10px
}
.centinel .authentication {
	border:1px solid #ddd;
	background:#fff
}
.centinel .authentication iframe {
	width:99%;
	height:400px;
	background:0 0!important;
	margin:0!important;
	padding:0!important;
	border:0!important
}
.checkout-progress {
	padding:0 50px;
	margin:10px 0
}
.checkout-progress li {
	border-top:10px solid #999;
	float:left;
	font-weight:700;
	margin:0 3px 0 0;
	padding:2px 0 0;
	text-align:center;
	width:19%
}
.multiple-checkout, .multiple-checkout .col2-set {
	padding-bottom:20px
}
.checkout-progress li.active {
	border-top-color:#E96200;
	color:#E96200
}
.multiple-checkout .title-buttons {
	text-align:left
}
.multiple-checkout .title-buttons h1 {
	width:100%;
	text-align:left
}
.multiple-checkout .title-buttons button.button {
	float:right;
	margin-top:15px;
	margin-right:0
}
.multiple-checkout .tool-tip {
	top:50%;
	margin-top:-120px;
	right:10px
}
.multiple-checkout .grand-total {
	font-size:1.5em;
	text-align:right
}
.multiple-checkout #checkout-review-submit, .multiple-checkout #checkout-review-submit #review-buttons-container {
	padding-top:20px
}
.customer-account-resetpassword .fieldset {
	margin-top:0!important
}
.customer-account-resetpassword .buttons-set {
	width:100%;
	float:left;
	margin-top:10px
}
.customer-account-resetpassword .buttons-set p.required {
	margin-bottom:10px
}
.customer-account-forgotpassword .fieldset {
	width:100%
}
.customer-account-forgotpassword .fieldset .form-list input.input-text {
	width:260px
}
.captcha-image {
	float:left
}
.captcha-img {
	border:1px solid #ccc
}
.captcha-reload {
	position:absolute;
	top:2px;
	right:2px
}
.captcha-reload.refreshing {
	animation:rotate 1.5s infinite linear;
	-webkit-animation:rotate 1.5s infinite linear;
	-moz-animation:rotate 1.5s infinite linear
}
#loader, .emfilter-ajaxblock-loading:before {
	animation:cssload-rotate 1.15s linear infinite
}
@-webkit-keyframes rotate {
0% {
-webkit-transform:rotate(-360deg)
}
}
@-moz-keyframes rotate {
0% {
-moz-transform:rotate(-360deg)
}
}
@keyframes rotate {
0% {
transform:rotate(-360deg)
}
}
.window-overlay {
	background:rgba(0, 0, 0, .35);
	position:absolute;
	top:0;
	left:0;
	height:100%;
	width:100%;
	z-index:990
}
.my-account .title-buttons .link-rss {
	float:none;
	margin:0
}
.my-wishlist .cart-cell button.button {
	margin-bottom:5px
}
.downloadable-customer-products .data-table th {
	white-space:nowrap
}
.sales-order-view .my-account a.link-print, .sales-order-view .my-account a.link-reorder, .sales-order-view .my-account span.separator {
	float:right;
	padding-top:5px;
	padding-bottom:5px;
	margin-top:8px
}
#cart-sidebar-reorder-advice-container {
	margin:1em 0
}
.order-info {
	padding:5px 0
}
.order-details .table-caption, .order-info-box {
	padding:0
}
.order-info dd, .order-info dt, .order-info li, .order-info ul {
	display:inline
}
.order-info dt {
	font-weight:700
}
.order-date, .page-print .order-date {
	margin:10px 0
}
.sales-order-view .my-account .title-buttons h1 {
	text-align:left;
	width:100%
}
.order-items {
	width:100%
}
.order-items #my-orders-table.data-table tfoot td.a-right {
	text-align:right!important
}
.order-additional {
	margin:15px 0
}
.gift-message dt strong {
	font-weight:700;
	color:#666
}
.gift-message dd {
	margin:5px 0 0
}
.order-about dt {
	font-weight:700
}
.order-about dd {
	margin:0 0 7px
}
.tracking-table {
	margin:0 0 15px
}
.tracking-table th {
	font-weight:700;
	white-space:nowrap
}
.tracking-table-popup {
	width:100%
}
.tracking-table-popup th {
	font-weight:700;
	white-space:nowrap
}
.tracking-table-popup td, .tracking-table-popup th {
	padding:1px 8px
}
.page-print .print-head img {
	float:left
}
.page-print .print-head address {
	float:left;
	margin-left:15px
}
.page-print .gift-message-link {
	display:none
}
.page-print .price-excl-tax, .page-print .price-incl-tax {
	display:block;
	white-space:nowrap
}
.footer ul li, .page-print .cart-price, .page-print .price-excl-tax .label, .page-print .price-excl-tax .price, .page-print .price-incl-tax .label, .page-print .price-incl-tax .price {
	display:inline
}
.newsletter-manage-index .fieldset {
	float:none;
	width:100%
}
.newsletter-manage-index .fieldset .legend {
	padding-bottom:10px
}
.wishlist-index-share .my-account .buttons-set p.required {
	width:100%
}
.wishlist-index-share .my-account .buttons-set p.back-link {
	float:left;
	margin-bottom:5px
}
.wishlist-index-share .fieldset {
	float:none;
	width:100%
}
.my-wishlist .truncated {
	clear:both
}
.list-inline {
	margin-left:0
}
#nav:after, .add-to-box:after, .add-to-cart:after, .advanced-search-summary:after, .block .actions:after, .block .block-content:after, .block li.item:after, .block-layered-nav .currently li:after, .block-poll li:after, .box-account .box-head:after, .box-reviews li.item:after, .box-tags li.item:after, .buttons-set:after, .cart .crosssell li.item:after, .cart-collaterals:after, .cart:after, .checkout-progress:after, .clearer:after, .col-main:after, .col2-set:after, .col3-set:after, .col4-set:after, .dashboard .box .box-title:after, .footer-container .bottom-container:after, .footer:after, .form-list .field:after, .form-list li:after, .gift-messages-form .item:after, .group-select li:after, .header .quick-access:after, .header-container .top-container:after, .header-container:after, .header:after, .main:after, .multiple-checkout .place-order:after, .opc .step-title:after, .page-print .print-head:after, .page-title:after, .pager:after, .product-collateral:after, .product-essential:after, .product-options .options-list li:after, .product-options-bottom:after, .product-review:after, .product-view .box-description:after, .product-view .box-tags .form-add:after, .product-view .product-img-box .more-views ul:after, .products-grid:after, .products-list li.item:after, .ratings:after, .search-autocomplete li:after, .send-friend .form-list li p:after, .sorter:after, .tags:after, .widget-static-block:after {
	display:block;
	content:".";
	clear:both;
	font-size:0;
	line-height:0;
	height:0;
	overflow:hidden
}
#back-top a:before, .add-to-links li a.link-compare:before, .add-to-links li a.link-wishlist:before, .button-link.link-cart:before, .em-sidebar a.link-cart:before, .em-sidebar button.button.btn-cart:before, .link-wishlist:before, .quickshop-link-container .quickshop-link:before, button.button.btn-cart:before {
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
.brand-logo, .em-payment-icon, .icon {
	font-size:0;
	display:block;
	text-indent:-99999px;
	overflow:hidden;
	vertical-align:middle;
	text-align:left
}
.banner-center {
	text-align:center;
	margin-bottom:17px
}
.banner-center a {
	margin-bottom:0;
	display:inline-block;
	max-width:100%
}
#back-top {
	z-index:15;
	position:fixed;
	right:15px;
	bottom:20px;
	backface-visibility:hidden;
	display:inline-block
}
#back-top a {
	display:inline-block;
	text-align:center;
	height:52px;
	width:52px;
	border:1px solid;
	position:relative;
	text-transform:uppercase;
	padding-top:25px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
#back-top a:before {
	display:inline-block;
	content:"\f077";
	font-size:12px;
	position:absolute;
	top:5px;
	left:18px
}
#bg_fade {
	z-index:999;
	opacity:.5
}
#overlay {
	opacity:.8;
	filter:alpha(opacity=80)
}
.emfilter-ajaxblock-loading {
	position:relative;
	text-align:center
}
.emfilter-ajaxblock-loading:before {
	display:inline-block;
	border-style:solid;
	border-width:5px;
	border-left-color:#f02222;
	border-radius:974px;
	-o-border-radius:974px;
	-ms-border-radius:974px;
	-webkit-border-radius:974px;
	-moz-border-radius:974px;
	content:"";
	height:40px;
	width:40px;
	margin:3rem auto;
	-o-animation:cssload-rotate 1.15s linear infinite;
	-ms-animation:cssload-rotate 1.15s linear infinite;
	-webkit-animation:cssload-rotate 1.15s linear infinite;
	-moz-animation:cssload-rotate 1.15s linear infinite
}
.em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.none, .em-wrapper-header .navbar-fixed-top {
	-webkit-animation-play-state:running;
	-webkit-animation-fill-mode:forwards;
	-moz-animation-name:anim_titles;
	-moz-animation-direction:normal;
	-moz-animation-play-state:running;
	-moz-animation-fill-mode:forwards
}
#em_quickshop_handler, .button-link, .em-sidebar a.link-cart, .tp-caption a.ss-btn-link, a.goto-cart, a.viewall, button.button {
	border:1px solid;
	overflow:visible;
	width:auto;
	margin:0 10px 10px 0;
	padding:7px 10px;
	cursor:pointer;
	float:left;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	white-space:nowrap
}
#em_quickshop_handler span, .button-link span, .em-sidebar a.link-cart span, .tp-caption a.ss-btn-link span, a.goto-cart span, a.viewall span, button.button span {
	display:inline-block;
	text-align:center;
	text-transform:uppercase;
	padding:0 3px
}
.cart button.btn-checkout span span {
	padding:4px 10px
}
.quickshop-link-container {
	display:inline
}
.quickshop-link-container .quickshop-link {
	font-size:0;
	overflow:hidden;
	text-align:left;
	text-indent:0;
	padding:7px;
	vertical-align:top;
	width:30px;
	float:none;
	display:inline-block;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	border-style:solid;
	border-width:1px
}
.quickshop-link-container .quickshop-link span {
	padding:0
}
.quickshop-link-container .quickshop-link:before {
	display:inline-block;
	content:"\f06e";
	font-size:14px
}
.button-link.link-cart, .em-sidebar a.link-cart, .em-sidebar button.button.btn-cart, button.button.btn-cart {
	overflow:hidden;
	text-align:left;
	text-indent:0;
	padding:7px;
	vertical-align:top;
	width:30px;
	margin-right:5px;
	float:none;
	display:inline;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	font-size:0
}
.button-link.link-cart span, .em-sidebar a.link-cart span, .em-sidebar button.button.btn-cart span, button.button.btn-cart span {
	padding:0
}
.button-link.link-cart:before, .em-sidebar a.link-cart:before, .em-sidebar button.button.btn-cart:before, button.button.btn-cart:before {
	display:inline-block;
	content:"\f07a";
	font-size:14px
}
button.button.btn-cart {
	width:auto
}
.add-to-links li a.link-compare, .add-to-links li a.link-wishlist, .link-wishlist {
	overflow:hidden;
	text-align:left;
	text-indent:0;
	vertical-align:top;
	width:30px;
	border:1px solid;
	-webkit-backface-visibility:hidden
}
button.button.btn-cart span {
	padding:0 3px
}
.em-featured-slider button.button.btn-cart {
	font-size:0
}
.em-featured-slider button.button.btn-cart span {
	padding:0
}
.add-to-links {
	margin-bottom:0
}
.add-to-links li a.link-wishlist, .link-wishlist {
	font-size:0;
	display:block;
	padding:7px;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px
}
.add-to-links li a.link-compare, p.back-link a {
-webkit-transition:all .5s ease;
	-moz-backface-visibility:hidden
}
.add-to-links li a.link-wishlist span, .link-wishlist span {
	padding:0
}
.add-to-links li a.link-wishlist:before, .link-wishlist:before {
	display:inline-block;
	content:"\f08a";
	font-size:14px
}
.add-to-links li a.link-compare {
	font-size:0;
	display:block;
	padding:7px;
-moz-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	backface-visibility:hidden;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px
}
.add-to-links li a.link-compare span {
	padding:0
}
.add-to-links li a.link-compare:before {
	display:inline-block;
	content:"\f079";
	font-size:14px
}
p.back-link a {
	border:1px solid;
	overflow:visible;
	width:auto;
	margin:0 10px 10px 0;
	cursor:pointer;
	float:left;
-moz-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	padding:7px 25px;
	text-transform:uppercase;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px
}
.ajaxcart a.back, .ajaxcart a.next, .load-more-image a.load-more {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.btn-edit:before, .btn-remove2:before, .btn-remove:before {
	font-family:FontAwesome;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	padding:3px 4px;
	background:#000;
	font-style:normal;
	-moz-osx-font-smoothing:grayscale;
	color:#fff
}
p.back-link a small {
	display:none
}
.btn-remove, .btn-remove2 {
	font-size:0;
	display:inline-block;
	border:2px solid #d4d4d4;
	-moz-border-radius:50%;
	-webkit-border-radius:50%;
	border-radius:50%
}
.btn-remove2:before, .btn-remove:before {
	display:inline-block;
	content:"\f00d";
	font-size:11px;
	-moz-border-radius:50%;
	-webkit-border-radius:50%;
	border-radius:50%
}
.btn-edit {
	font-size:0;
	border:2px solid #d4d4d4;
	-moz-border-radius:50%;
	-webkit-border-radius:50%;
	border-radius:50%
}
.btn-edit:before {
	display:inline-block;
	content:"\f040";
	font-size:11px;
	-moz-border-radius:50%;
	-webkit-border-radius:50%;
	border-radius:50%
}
.button-full-width, button.button-full-width {
	margin-right:0;
	width:100%
}
.button-full-width span, button.button-full-width span {
	float:none;
	display:inline-block;
	width:100%
}
.ajaxcart a.back, .ajaxcart a.next {
	border:1px solid;
	overflow:visible;
	width:auto;
	margin:0 10px 10px 0;
	cursor:pointer;
	float:left;
transition:all .5s ease;
	backface-visibility:hidden;
	padding:7px 25px;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px
}
.ajaxcart a.back span, .ajaxcart a.next span {
	float:left;
	text-align:center;
	text-transform:uppercase
}
.load-more-image {
	text-align:center;
	display:inline-block;
	width:100%
}
.load-more-image a.load-more {
	display:inline-block;
	padding:11px 20px;
	min-width:220px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	border:1px solid;
transition:all .5s ease;
	backface-visibility:hidden
}
.data-table th, .data-table tr td {
	padding:14px 20px
}
.data-table th, .data-table thead th, .data-table tr td {
	border-width:1px;
	border-style:solid
}
#wishlist-table {
	display:inline-block;
	overflow:inherit
}
.data-table, .data-table colgroup, table colgroup {
	width:100%
}
.data-table th {
	white-space:nowrap
}
.data-table tr td em {
	font-style:normal
}
.data-table tr td button.button {
	white-space:nowrap
}
.data-table tbody .label {
	display:table-cell;
	vertical-align:middle;
	text-align:left
}
.customer-account-index .my-account .data-table tr th, .sales-order-history .my-account .data-table tr th {
	border-left-width:0;
	border-right-width:0
}
.customer-account-index .my-account .data-table tr th:first-child, .sales-order-history .my-account .data-table tr th:first-child {
	border-left-width:1px
}
.customer-account-index .my-account .data-table tr th.last, .sales-order-history .my-account .data-table tr th.last {
	border-right-width:1px
}
.customer-account-index .my-account .data-table tr td, .sales-order-history .my-account .data-table tr td {
	border-width:0
}
.customer-account-index .my-account .data-table tr td:first-child, .sales-order-history .my-account .data-table tr td:first-child {
	border-left-width:1px
}
.customer-account-index .my-account .data-table tr td.last, .sales-order-history .my-account .data-table tr td.last {
	border-right-width:1px
}
.customer-account-index .my-account .data-table tr.last td, .sales-order-history .my-account .data-table tr.last td {
	border-bottom-width:1px
}
#checkout-step-review .data-table tr th {
	border-left-width:0;
	border-right-width:0
}
#checkout-step-review .data-table tr th:first-child {
	border-left-width:1px
}
#checkout-step-review .data-table tr th.last {
	border-right-width:1px
}
#checkout-step-review .data-table tr td {
	border-width:0
}
#checkout-step-review .data-table tr td:first-child {
	border-left-width:1px
}
#checkout-step-review .data-table tr td.last {
	border-right-width:1px
}
#checkout-step-review .data-table tr.last td {
	border-bottom-width:1px
}
#checkout-step-review .data-table th, #checkout-step-review .data-table tr td {
	padding-left:10px;
	padding-right:10px
}
#checkout-step-review .data-table tfoot tr td {
	border:none;
	padding:0 10px 5px 20px
}
.cart .data-table tr td:first-child, .cart .data-table tr th:first-child {
	border-left-width:1px
}
.cart .data-table tr td.last, .cart .data-table tr th.last {
	border-right-width:1px
}
#checkout-step-review .data-table tfoot tr td.last {
	text-align:left!important
}
#checkout-step-review .data-table tfoot tr td .price {
	display:block;
	margin-top:-10px
}
#checkout-step-review .data-table tfoot tr.first td {
	padding-top:21px
}
#checkout-step-review .data-table tfoot tr.last td {
	padding-top:0;
	padding-bottom:16px
}
#checkout-step-review .data-table tfoot tr.last td .price {
	margin-top:-7px
}
.effect-hover-text .banner-text img, .effect-hover-text2 .banner-text img, .effect-hover-text3 .banner-text img, .effect-hover-text4 .banner-text img, .effect-hover-text5 .banner-text img {
	margin:0
}
.cart .data-table tr th {
	border-bottom:0
}
.cart .data-table tr td {
	border-width:1px
}
.effect-line-02:after, .effect-line:after {
	border-left:1px solid #fff;
	border-right:1px solid #fff
}
.cart .data-table tr.last td {
	border-bottom-width:1px
}
.product-item a.product-image .em-alt-hover, .products-grid.isotope .item a.product-image .em-alt-hover, .products-list .item a.product-image .em-alt-hover {
	position:absolute;
	left:0;
	top:0;
	opacity:0!important;
	filter:alpha(opacity=0)!important;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.product-item:hover a.product-image .em-alt-org, .products-grid.isotope .item:hover a.product-image .em-alt-org, .products-list .item:hover a.product-image .em-alt-org {
	opacity:.5!important;
	filter:alpha(opacity=0)!important;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.product-item:hover a.product-image .em-alt-hover, .products-grid.isotope .item:hover a.product-image .em-alt-hover, .products-list .item:hover a.product-image .em-alt-hover {
	opacity:1!important;
	filter:alpha(opacity=100)!important;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	position:absolute;
	left:0;
	top:0
}
.em-effect06 {
	position:relative;
	z-index:1;
	text-align:center
}
.em-effect06 a {
	position:relative;
	overflow:hidden;
	z-index:0;
	display:inline-block;
	vertical-align:top;
	max-width:100%
}
.em-effect06 .em-eff06-04:before, .em-effect06 a:after {
	content:'';
	position:absolute;
	z-index:1;
	background:rgba(0, 0, 0, .18)
}
.em-effect06 a:after {
	width:100%;
	height:0;
	top:0;
	left:0;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.effect03 a:before, .em-effect06 .em-eff06-04:before {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.em-effect06 .em-eff06-02:after {
	height:100%;
	bottom:0;
	opacity:0;
	filter:alpha(opacity=0)
}
.em-effect06 .em-eff06-02:hover:after {
	opacity:1;
	filter:alpha(opacity=100)
}
.em-effect06 .em-eff06-03:after {
	height:100%;
	bottom:0;
	-webkit-transform:scaleX(0);
	transform:scaleX(0)
}
.em-effect06 .em-eff06-03:hover:after {
	-webkit-transform:scaleX(1);
	transform:scaleX(1)
}
.em-effect06 .em-eff06-04:after {
	width:0;
	height:0;
	top:0;
	left:0
}
.em-effect06 .em-eff06-04:before {
	width:0;
	bottom:0;
	right:0;
	height:0;
transition:all .5s ease;
	backface-visibility:hidden
}
.effect03 a:before, .em-effect06 .em-eff06-04:hover:after, .em-effect06 .em-eff06-04:hover:before {
	width:100%;
	height:100%
}
.effect03 a {
	display:block;
	position:relative
}
.effect03 a:before {
	content:"";
	position:absolute;
	left:0;
transition:all .5s ease;
	backface-visibility:hidden
}
.effect-hover-text, .effect-hover-text .banner-img {
	-webkit-backface-visibility:hidden;
	position:relative
}
.effect03 a:hover:before {
	z-index:2;
	box-shadow:0 0 0 15px #fff inset;
	opacity:.5;
	filter:alpha(opacity=50)
}
.effect-hover-text {
	overflow:hidden;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden
}
.effect-hover-text .banner-img {
	display:block;
	max-width:100%;
	-webkit-transform:translateZ(0);
	transform:translateZ(0);
	backface-visibility:hidden;
	-moz-osx-font-smoothing:grayscale;
	-webkit-transition-property:color;
	transition-property:color;
-webkit-transition-duration:.3s;
transition-duration:.3s
}
.effect-hover-text .banner-img img, .effect-hover-text .banner-text {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.effect-hover-text .banner-img:before {
	content:"";
	position:absolute;
	top:0;
	left:0;
	right:0;
	bottom:0;
	z-index:1;
	background:rgba(0, 0, 0, .48);
	-webkit-transform:scaleY(0);
	transform:scaleY(0);
	-webkit-transform-origin:50% 0;
	transform-origin:50% 0;
	-webkit-transition-property:transform;
	transition-property:transform;
-webkit-transition-duration:.3s;
transition-duration:.3s;
	-webkit-transition-timing-function:ease-out;
	transition-timing-function:ease-out
}
.effect-hover-text:hover .banner-img:before {
	-webkit-transform:scaleY(1);
	transform:scaleY(1)
}
.effect-hover-text .banner-img img {
transition:all .5s ease;
	backface-visibility:hidden;
	-webkit-transform:scale(1);
	-ms-transform:scale(1);
	transform:scale(1)
}
.effect-hover-text .banner-text {
transition:all .5s ease;
	backface-visibility:hidden;
	position:absolute;
	left:0;
	top:0;
	right:0
}
.effect-hover-text:hover .banner-img img {
	-webkit-transform:scale(1.1, 1.1);
	-ms-transform:scale(1.1, 1.1);
	transform:scale(1.1, 1.1)
}
.effect-hover-text:hover .banner-text {
	top:-30px
}
.effect-hover-text2 {
	position:relative;
	overflow:hidden;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.effect-hover-text2 .banner-img:after {
	border-radius:50%;
	background:#000;
	opacity:.4;
	filter:alpha(opacity=40);
	position:absolute;
	width:125%;
	height:165%;
	top:-32.5%;
	left:-12.5%;
	-webkit-transform:scale(0);
	-ms-transform:scale(0);
	transform:scale(0);
	transition:all 250ms linear;
	-webkit-transition:all 250ms linear;
	content:""
}
.effect-hover-text2 .banner-img img, .effect-hover-text2 .banner-text {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.effect-hover-text2:hover .banner-img:after {
	-webkit-transform:scale(1);
	-ms-transform:scale(1);
	transform:scale(1)
}
.effect-hover-text2 .banner-img img {
transition:all .5s ease;
	backface-visibility:hidden;
	-webkit-transform:scale(1);
	-ms-transform:scale(1);
	transform:scale(1)
}
.effect-hover-text2 .banner-text {
transition:all .5s ease;
	backface-visibility:hidden;
	position:absolute;
	left:0;
	top:0;
	right:0
}
.effect-hover-text3, .effect-hover-text3 .banner-img {
	-webkit-backface-visibility:hidden;
	position:relative
}
.effect-hover-text2:hover .banner-img img {
	-webkit-transform:scale(1.1, 1.1);
	-ms-transform:scale(1.1, 1.1);
	transform:scale(1.1, 1.1)
}
.effect-hover-text3 {
	overflow:hidden;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden
}
.effect-hover-text3 .banner-img {
	display:block;
	max-width:100%;
	-webkit-transform:translateZ(0);
	transform:translateZ(0);
	backface-visibility:hidden;
	-moz-osx-font-smoothing:grayscale;
	-webkit-transition-property:color;
	transition-property:color;
-webkit-transition-duration:.3s;
transition-duration:.3s
}
.effect-hover-text3 .banner-img img, .effect-hover-text3 .banner-text {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.effect-hover-text3 .banner-img:before {
	content:"";
	position:absolute;
	top:0;
	left:0;
	right:0;
	bottom:0;
	z-index:1;
	background:rgba(0, 0, 0, .48);
	-webkit-transform:scaleY(0);
	transform:scaleY(0);
	-webkit-transition-property:transform;
	transition-property:transform;
-webkit-transition-duration:.3s;
transition-duration:.3s;
	-webkit-transition-timing-function:ease-out;
	transition-timing-function:ease-out
}
.effect-hover-text3:hover .banner-img:before {
	-webkit-transform:scaleY(1);
	transform:scaleY(1)
}
.effect-hover-text3 .banner-img:before {
	-webkit-transform-origin:50% 100%;
	transform-origin:50% 100%
}
.effect-hover-text3 .banner-img img {
transition:all .5s ease;
	backface-visibility:hidden;
	-webkit-transform:scale(1);
	-ms-transform:scale(1);
	transform:scale(1)
}
.effect-hover-text3 .banner-text {
transition:all .5s ease;
	backface-visibility:hidden;
	position:absolute;
	left:0;
	top:0;
	right:0
}
.effect-hover-text4, .effect-hover-text4 .banner-img:after {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.effect-hover-text3:hover .banner-img img {
	-webkit-transform:scale(1.1, 1.1);
	-ms-transform:scale(1.1, 1.1);
	transform:scale(1.1, 1.1)
}
.effect-hover-text3:hover .banner-text {
	top:-30px
}
.effect-hover-text4 {
	position:relative;
	overflow:hidden;
transition:all .5s ease;
	backface-visibility:hidden
}
.effect-hover-text4 .banner-img {
	position:relative;
	overflow:hidden;
	display:block;
	max-width:100%
}
.effect-hover-text4 .banner-img:after {
	content:'';
	position:absolute;
	width:100%;
	top:0;
	left:0;
transition:all .5s ease;
	backface-visibility:hidden;
	background:rgba(0, 0, 0, .48);
	height:100%;
	bottom:0;
	-webkit-transform:scaleX(0);
	transform:scaleX(0)
}
.effect-hover-text4 .banner-img img, .effect-hover-text4 .banner-text {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.effect-hover-text4:hover .banner-img:after {
	-webkit-transform:scaleX(1);
	transform:scaleX(1)
}
.effect-hover-text4 .banner-img img {
transition:all .5s ease;
	backface-visibility:hidden;
	-webkit-transform:scale(1);
	-ms-transform:scale(1);
	transform:scale(1)
}
.effect-hover-text4 .banner-text {
transition:all .5s ease;
	backface-visibility:hidden;
	position:absolute;
	left:0;
	top:0;
	right:0
}
.effect-hover-text5, .effect-hover-text5 .banner-img {
	-webkit-backface-visibility:hidden;
	position:relative
}
.effect-hover-text4:hover .banner-img img {
	-webkit-transform:scale(1.1, 1.1);
	-ms-transform:scale(1.1, 1.1);
	transform:scale(1.1, 1.1)
}
.effect-hover-text5 {
	overflow:hidden;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden
}
.effect-hover-text5 .banner-img {
	display:block;
	max-width:100%;
	-webkit-transform:translateZ(0);
	transform:translateZ(0);
	backface-visibility:hidden;
	-moz-osx-font-smoothing:grayscale;
	-webkit-transition-property:color;
	transition-property:color;
-webkit-transition-duration:.3s;
transition-duration:.3s
}
.effect-hover-text5 .banner-img img, .effect-hover-text5 .banner-text {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-webkit-backface-visibility:hidden
}
.effect-hover-text5 .banner-img:before {
	content:"";
	position:absolute;
	top:0;
	left:0;
	right:0;
	bottom:0;
	z-index:1;
	background:rgba(0, 0, 0, .48);
	-webkit-transform:scaleY(0);
	transform:scaleY(0);
	-webkit-transition-property:transform;
	transition-property:transform;
-webkit-transition-duration:.3s;
transition-duration:.3s;
	-webkit-transition-timing-function:ease-out;
	transition-timing-function:ease-out
}
.effect-hover-text5:hover .banner-img:before {
	-webkit-transform:scaleY(1);
	transform:scaleY(1)
}
.effect-hover-text5 .banner-img:before {
	-webkit-transform-origin:50% 100%;
	transform-origin:50% 100%
}
.effect-hover-text5 .banner-img img {
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden;
	-webkit-transform:scale(1);
	-ms-transform:scale(1);
	transform:scale(1)
}
.effect-hover-text5 .banner-text {
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden;
	position:absolute;
	left:0;
	top:0;
	right:0
}
.effect-flip-book .banner-img a.effect:after, .effect-flip-book .flip_book_container .flip_book_face img {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-webkit-backface-visibility:hidden
}
.effect-hover-text5:hover .banner-img img {
	-webkit-transform:scale(1.1, 1.1);
	-ms-transform:scale(1.1, 1.1);
	transform:scale(1.1, 1.1)
}
.effect-hover-text5:hover .banner-text {
	top:30px
}
.effect-flip-book .flip_book_container {
	position:relative;
	-webkit-perspective:1000;
	-moz-perspective:1000;
	-o-perspective:1000;
	-ms-perspective:1000;
	perspective:1000
}
.effect-flip-book .flip_book_container .flip_book_face {
	-webkit-transform-style:preserve-3d;
	-moz-transform-style:preserve-3d;
	-o-transform-style:preserve-3d;
	-ms-transform-style:preserve-3d;
	transform-style:preserve-3d
}
.effect-flip-book .flip_book_container .flip_book_face img {
	position:absolute;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden
}
.effect-flip-book .banner-img a.effect:after, .effect-flip-book .banner-img a.effect:before {
	width:0;
	content:'';
	-moz-backface-visibility:hidden;
	background:rgba(0, 0, 0, .18);
	height:0
}
.effect-flip-book .flip_book_container .flip_book_face .flip_book_face_left {
	-webkit-transform-origin:0 0;
	-moz-transform-origin:0 0;
	-o-transform-origin:0 0;
	-ms-transform-origin:0 0;
	transform-origin:0 0;
	z-index:1
}
.effect-flip-book .flip_book_container .flip_book_face .flip_book_face_right {
	-webkit-transform-origin:100% 0;
	-moz-transform-origin:100% 0;
	-o-transform-origin:100% 0;
	-ms-transform-origin:100% 0;
	transform-origin:100% 0;
	z-index:1
}
.effect-flip-book .flip_book_container:hover {
	z-index:1
}
.effect-flip-book .flip_book_container:hover .flip_book_face_left {
	-webkit-transform:rotateY(-86deg);
	-moz-transform:rotateY(-86deg);
	-o-transform:rotateY(-86deg);
	-ms-transform:rotateY(-86deg);
	transform:rotateY(-86deg)
}
.effect-flip-book .flip_book_container:hover .flip_book_face_right {
	-webkit-transform:rotateY(86deg);
	-moz-transform:rotateY(86deg);
	-o-transform:rotateY(86deg);
	-ms-transform:rotateY(86deg);
	transform:rotateY(86deg)
}
.effect-flip-book img {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.effect-flip-book .banner-img {
	position:relative
}
.effect-flip-book .banner-img a.effect {
	position:relative;
	overflow:hidden;
	z-index:0;
	display:block
}
.effect-flip-book .banner-img a.effect:after {
	position:absolute;
	z-index:1;
	top:0;
	left:0;
transition:all .5s ease;
	backface-visibility:hidden
}
.effect-flip-book .banner-img a.effect:before {
	bottom:0;
	right:0;
	position:absolute;
	z-index:1;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.effect-flip-book .banner-img .desc {
	position:absolute;
	left:0;
	right:0;
	bottom:40%;
	margin:0 auto;
	color:#fff;
	cursor:pointer;
	text-align:center
}
.effect-flip-book .banner-img .desc h1, .effect-flip-book .banner-img .desc h2 {
	color:#fff
}
.effect-flip-book .banner-img .desc .button-link {
	float:none;
	display:inline-block;
	height:0;
	padding:0;
	font-size:0;
	opacity:0;
	filter:alpha(opacity=0)
}
.effect-flip-book .banner-img:hover .button-link {
	height:100%;
	padding:10px 25px;
	font-size:100%;
	opacity:1;
	filter:alpha(opacity=100)
}
.effect-flip-book .banner-img:hover a.effect:after, .effect-flip-book .banner-img:hover a.effect:before {
	width:100%;
	height:100%
}
.em-effect-13 {
	vertical-align:middle;
	position:relative;
	overflow:hidden
}
.em-effect-13:before {
	content:"";
	position:absolute;
	left:50%;
	right:50%;
	bottom:0;
	z-index:1;
	background:#2098d1;
	height:4px;
	-webkit-transition-property:left, right;
	transition-property:left, right;
-webkit-transition-duration:.3s;
transition-duration:.3s;
	-webkit-transition-timing-function:ease-out;
	transition-timing-function:ease-out;
	opacity:0;
	filter:alpha(opacity=0)
}
.em-effect-13:hover:before {
	left:0;
	right:0;
	opacity:1;
	filter:alpha(opacity=100)
}
.em-blog-item .hov, .em-effect-13 .hov {
	display:none
}
.products-grid li.item.cat-effect01 .product-item a.product-image {
	position:relative
}
.products-grid li.item.cat-effect01 .product-item a.product-image .bkg-hover {
	margin:0;
	transform:scale(0, 0);
	-moz-transform:scale(0, 0);
	-webkit-transform:scale(0, 0)
}
.products-grid li.item.cat-effect01:hover a.product-image .bkg-hover {
	background:#000;
	width:100%;
	height:100%;
	position:absolute;
	top:0;
	left:0;
	opacity:.5;
	filter:alpha(opacity=50);
	transform:scale(1, 1);
	-moz-transform:scale(1, 1);
	-webkit-transform:scale(1, 1);
transition:transform .5s ease 0s;
-moz-transition:-moz-transform .5s ease 0s;
-ms-transition:-ms-transform .5s ease 0s;
-webkit-transition:-webkit-transform .5s ease 0s
}
.products-grid .item {
	position:relative
}
.products-grid .item a.product-image:after {
	content:"";
	position:absolute;
	left:50%;
	right:50%;
	top:50%;
	bottom:50%;
	z-index:0;
	display:inline-block;
	opacity:0;
	filter:alpha(opacity=0);
transition:all .3s ease-in-out 0s;
-moz-transition:all .3s ease-in-out 0s;
-webkit-transition:all .3s ease-in-out 0s;
	background-color:rgba(0, 0, 0, .8)
}
.products-grid .item:hover a.product-image:after {
	opacity:.6;
	filter:alpha(opacity=60);
	left:-10px;
	right:-10px;
	top:-10px;
	bottom:0
}
.effect {
	position:relative
}
.effect-line, .effect-line:before {
	z-index:1;
	content:"";
	position:absolute
}
.effect-line {
	top:0;
	right:0;
	left:0;
	bottom:0
}
.effect-line:after, .effect-line:before {
	left:10px;
	right:10px;
	top:10px;
	bottom:10px
}
.effect-line:before {
	border-top:1px solid #fff;
	border-bottom:1px solid #fff;
	transform:scale(0, 1);
	-moz-transform:scale(0, 1);
	-webkit-transform:scale(0, 1);
	-ms-transform:scale(0, 1);
transition:all .4s ease-in-out 0s;
-moz-transition:all .4s ease-in-out 0s;
-webkit-transition:all .4s ease-in-out 0s
}
.effect-line-02:before, .effect-line:after {
transition:all .4s ease-in-out 0s;
	z-index:1;
	content:"";
	position:absolute
}
.effect-line:after {
	transform:scale(1, 0);
	-moz-transform:scale(1, 0);
	-webkit-transform:scale(1, 0);
	-ms-transform:scale(0, 1);
-moz-transition:all .4s ease-in-out 0s;
-webkit-transition:all .4s ease-in-out 0s
}
.effect-line:hover:after, .effect-line:hover:before {
	transform:scale(1, 1);
	-moz-transform:scale(1, 1);
	-webkit-transform:scale(1, 1);
	-ms-transform:scale(1, 1)
}
.effect-line-02 {
	position:absolute;
	content:"";
	top:0;
	right:0;
	left:0;
	bottom:0;
	z-index:1
}
.effect-line-02:before {
	left:10px;
	right:10px;
	top:20px;
	bottom:20px;
	border-top:1px solid #fff;
	border-bottom:1px solid #fff;
	transform:scale(0, 1);
	-moz-transform:scale(0, 1);
	-webkit-transform:scale(0, 1);
	-ms-transform:scale(0, 1);
-moz-transition:all .4s ease-in-out 0s;
-webkit-transition:all .4s ease-in-out 0s
}
.effect-line-02:after {
	z-index:1;
	content:"";
	position:absolute;
	left:20px;
	right:20px;
	top:10px;
	bottom:10px;
	transform:scale(1, 0);
	-moz-transform:scale(1, 0);
	-webkit-transform:scale(1, 0);
	-ms-transform:scale(0, 1);
transition:all .4s ease-in-out 0s;
-moz-transition:all .4s ease-in-out 0s;
-webkit-transition:all .4s ease-in-out 0s
}
.effect-line-02:hover:after, .effect-line-02:hover:before {
	transform:scale(1, 1);
	-moz-transform:scale(1, 1);
	-webkit-transform:scale(1, 1);
	-ms-transform:scale(1, 1)
}
.clearfix:after, .configurable-swatch-list:after, .product-view .product-options .swatch-attr:after {
	display:block;
	content:".";
	clear:both;
	font-size:0;
	line-height:0;
	height:0;
	overflow:hidden
}
.product-view .product-options .swatch-attr {
	float:none;
	display:block;
	clear:both;
	border:0
}
.product-view .product-options dt.swatch-attr label {
	display:block;
	line-height:1.3
}
.configurable-swatch-list {
	margin-left:-3px;
	zoom:1;
	clear:both
}
.configurable-swatch-list li {
	float:left;
	zoom:1;
	margin:0 0 0 3px
}
#narrow-by-list dd .configurable-swatch-list li {
	margin:0 0 0 3px;
	width:47%
}
.swatch-link img {
	border-radius:3px
}
.swatch-label, .swatch-link {
	display:block;
	border-radius:3px;
	font-size:14px;
	text-align:center;
	color:#666;
	text-decoration:none;
	box-sizing:content-box
}
.swatch-link {
	border:1px solid #bbb;
	margin:0 0 3px
}
#narrow-by-list dd .swatch-link {
	float:left;
	margin-right:2px;
	padding:0
}
.currently .swatch-link {
	display:inline-block;
	margin:0 0 0 10px
}
.swatch-label {
	border:1px solid #fff;
	margin:0;
	white-space:nowrap;
	background:#efefef
}
.swatch-link:hover {
	cursor:pointer
}
.swatch-link .x {
	display:none;
	text-indent:-999em;
	position:absolute;
	left:0;
	right:0;
	top:0;
	bottom:0;
	background:url(../images/bg_x.png) center no-repeat;
	z-index:10
}
.configurable-swatch-list .not-available .x {
	display:block
}
.configurable-swatch-list .not-available .swatch-link {
	border-color:#e6e6e6;
	position:relative
}
.configurable-swatch-list .not-available .swatch-label {
	color:#aaa;
	background:#fff
}
.swatch-link.has-image .swatch-label {
	position:relative
}
.swatch-link.has-image img {
	position:absolute;
	top:0;
	left:0
}
.configurable-swatch-list .not-available .swatch-link.has-image img {
	opacity:.4;
	filter:alpha(opacity=40)
}
.configurable-swatch-list .hover .swatch-link, .configurable-swatch-list .selected .swatch-link, .swatch-link:hover {
	border-color:#44f
}
.currently .swatch-link:hover {
	cursor:default;
	border-color:#bbb
}
.configurable-swatch-list .wide-swatch .swatch-label {
	padding:0 6px
}
.configurable-swatch-box {
	background:0 0!important
}
.configurable-swatch-box select.swatch-select {
	display:none
}
.configurable-swatch-box .validation-advice {
	margin:0 0 5px;
	background:#D91A00;
	padding:2px 5px!important;
	font-weight:700;
	color:#fff!important;
	float:left;
	display:block;
	border-radius:3px
}
.em-details-tabs.r-tabs .r-tabs-nav .r-tabs-anchor, .products-grid.owl-carousel .owl-item .item, .products-list.owl-carousel .owl-item .item {
	margin-right:0
}
.availability.out-of-stock span {
	color:#333
}
.product-view .add-to-cart button.out-of-stock {
	background-position:-80px -362px;
	cursor:default
}
.product-view .product-options dd .input-box {
	width:auto;
	height:auto
}
.product-options .select-label {
	display:none
}
.product-options dt.swatch-attr .select-label, .product-options dt.swatch-attr label {
	font-size:12px
}
.product-options dt.swatch-attr .select-label {
	display:inline;
	font-weight:400;
	color:#00acb1;
	padding-left:5px
}
.em-details-tabs, .em-tabs {
	display:inline-block;
	width:100%
}
.em-details-tabs ul.em-tabs-control li, .em-tabs ul.em-tabs-control li {
	display:inline-block;
	margin-bottom:-1px
}
.em-details-tabs ul.em-tabs-control li a, .em-tabs ul.em-tabs-control li a {
	position:relative;
	margin-right:0;
	padding:10px 20px
}
.products-grid.owl-carousel .owl-item .em-item-slider .products-grid .item, .products-list.owl-carousel .owl-item .em-item-slider .products-grid .item {
	margin-bottom:2rem
}
.products-grid.owl-carousel .owl-item .em-item-slider .products-grid .item.last, .products-list.owl-carousel .owl-item .em-item-slider .products-grid .item.last {
	margin-bottom:0
}
.owl-carousel .owl-item .item {
	float:none;
	margin-bottom:0;
	width:100%
}
.em-details-tabs.r-tabs, .em-tabs {
	text-align:center
}
.em-details-tabs.r-tabs .em-tabs-control, .em-tabs .em-tabs-control {
	border-bottom-width:1px;
	border-bottom-style:solid
}
.em-details-tabs.r-tabs .r-tabs-nav .r-tabs-anchor, .em-tabs .r-tabs-nav .r-tabs-anchor {
	border-width:1px;
	border-style:solid
}
.em-details-tabs.r-tabs .r-tabs-panel, .em-tabs .r-tabs-panel {
	margin-bottom:0;
	padding-bottom:0
}
.em-details-tabs.r-tabs .em-details-tabs-content, .em-details-tabs.r-tabs .em-tabs-content, .em-tabs .em-details-tabs-content, .em-tabs .em-tabs-content {
	padding:15px 0 0
}
.em-details-tabs.r-tabs .em-details-tabs-content {
	border-width:1px;
	border-style:solid;
	margin-top:-1px;
	padding:2rem;
	-moz-border-radius:0 5px 5px;
	-webkit-border-radius:0 5px 5px 5px;
	border-radius:0 5px 5px
}
.em-details-tabs .tabs-control {
	text-align:left
}
.em-tabs-widget .products-grid li.item {
	width:100%
}
.r-tabs .r-tabs-accordion-title .r-tabs-anchor {
	text-transform:uppercase!important;
	border-width:1px!important;
	border-style:solid!important;
	clear:both;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.owl-theme .owl-controls {
	text-align:center;
	margin-top:0!important
}
.owl-theme .owl-controls .owl-pagination {
	margin-top:20px;
	margin-bottom:1px
}
.slider-icon-bottom .owl-theme .owl-controls {
	position:static
}
.em-tabs-content .owl-theme .owl-controls {
	top:-39px;
	right:11px;
	overflow:initial;
	margin-top:0
}
.owl-theme .owl-controls .owl-buttons div {
	border:1px solid;
	-webkit-box-shadow:none!important;
	-moz-box-shadow:none!important;
	box-shadow:none!important;
	font-size:0!important;
	margin:0!important;
	padding:7px!important;
	width:32px
}
.owl-theme .owl-controls .owl-buttons div.owl-next:before, .owl-theme .owl-controls .owl-buttons div.owl-prev:before {
	display:inline-block;
	font-size:15px;
	-webkit-backface-visibility:hidden;
	line-height:1;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
.owl-theme .owl-controls .owl-buttons div.owl-next:before {
	content:"\f054";
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden;
	margin-left:3px
}
#addtoContainer .row2 a.btn_ajaxaddto, .owl-theme .owl-controls .owl-buttons div.owl-prev:before {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden
}
.owl-theme .owl-controls .owl-buttons div.owl-prev:before {
	content:"\f053";
transition:all .5s ease;
	backface-visibility:hidden;
	margin-right:2px
}
.owl-pagination-numbers.owl-theme .owl-controls .owl-page span.owl-numbers:before, .slider-icon-text .owl-theme .owl-controls .owl-buttons div:before {
	display:none!important
}
.owl-theme .owl-controls .owl-buttons div.owl-prev {
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	position:absolute;
	right:45px;
	top:-70px
}
.owl-theme .owl-controls .owl-buttons div.owl-next {
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	position:absolute;
	right:10px;
	top:-70px
}
.slider-icon-text .owl-theme .owl-controls .owl-buttons div {
	text-transform:uppercase;
	font-size:80%!important;
	margin:0!important;
	padding:0!important;
	width:auto;
	-webkit-box-shadow:none!important;
	-moz-box-shadow:none!important;
	box-shadow:none!important
}
.slider-icon-text .owl-theme .owl-controls .owl-buttons div:first-child {
	border-right:1px solid;
	padding-right:10px!important;
	margin-right:10px!important
}
.slider-icon-text .owl-theme .owl-controls {
	top:-33px
}
.slider-style02 {
	padding:0 50px;
	margin:20px 0
}
.slider-style02 .em-slider-navigation-icon.owl-theme .owl-controls {
	position:static
}
.slider-style02 .em-slider-navigation-icon.owl-theme .owl-controls .owl-buttons div {
	position:absolute;
	top:23%
}
.slider-style02 .em-slider-navigation-icon.owl-theme .owl-controls .owl-buttons div.owl-prev {
	left:-40px;
	right:auto
}
.slider-style02 .em-slider-navigation-icon.owl-theme .owl-controls .owl-buttons div.owl-next {
	left:auto;
	right:-40px
}
.slider-navi-bottom .owl-theme .owl-controls {
	margin-top:20px!important
}
.slider-navi-bottom .owl-theme .owl-controls .owl-buttons div.owl-next, .slider-navi-bottom .owl-theme .owl-controls .owl-buttons div.owl-prev {
	position:static;
	margin:0 3px!important
}
.owl-theme .owl-controls .owl-page.active span:before, .owl-theme .owl-controls .owl-page:hover span:before {
	width:4px!important;
	height:4px!important
}
.owl-theme .owl-controls .owl-page span {
	border:1px solid;
	width:12px!important;
	height:12px!important;
	margin:0 0 0 10px!important;
	-moz-border-radius:50%!important;
	-webkit-border-radius:50%!important;
	border-radius:50%!important;
	opacity:1;
	filter:alpha(opacity=100)
}
.slider-icon-square .owl-theme .owl-controls .owl-page span {
	width:8px!important;
	height:8px!important;
	margin:0 0 0 2px!important;
	-moz-border-radius:0!important;
	-webkit-border-radius:0!important;
	border-radius:0!important;
	opacity:1;
	filter:alpha(opacity=100)
}
.owl-pagination-numbers.owl-theme .owl-controls .owl-page span.owl-numbers {
	width:auto!important;
	height:auto!important;
	margin:0 0 0 1px!important;
	-moz-border-radius:0!important;
	-webkit-border-radius:0!important;
	border-radius:0!important;
	border:1px solid;
	padding:2px 7px!important;
	font-size:80%
}
.owl-pagination-numbers.owl-theme .owl-controls .owl-pagination {
	margin-top:1px
}
.em-rtl .owl-theme .owl-controls .owl-buttons div.owl-prev {
	right:auto;
	left:10px
}
.em-rtl .owl-theme .owl-controls .owl-buttons div.owl-next {
	right:auto;
	left:45px
}
.fancybox-opened .fancybox-skin {
	padding:0!important
}
.fancybox-skin {
	-moz-border-radius:5px!important;
	-webkit-border-radius:5px!important;
	border-radius:5px!important
}
.popup-content {
	background:url(../images/popup_banner.jpg) right bottom no-repeat #f5f4f2;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	display:block;
	width:100%
}
.popup-content .em-account-popup {
	padding:50px
}
.popup-content .em-account-popup .action-forgot {
	padding-top:5px;
	overflow:hidden
}
.popup-content .em-account-popup .action-forgot .actions {
	float:left;
	clear:both
}
.popup-content .em-account-popup .action-forgot .actions .required {
	float:left;
	clear:none;
	margin-top:5px
}
.popup-content .em-account-popup .action-forgot .login_forgotpassword {
	float:left;
	padding-bottom:10px
}
.popup-content .em-account-popup .action-forgot .login_forgotpassword a {
	text-transform:capitalize
}
.popup-content .em-account-popup .action-forgot .login_forgotpassword p {
	margin-bottom:6px
}
.popup-content .em-account-popup .form-list li {
	width:100%;
	margin-right:0
}
.popup-content .em-account-popup .form-list p.required {
	font-size:85%;
	padding:0;
	margin:0
}
#addtoContainer .back, .em-search-icon span {
	font-size:0;
	vertical-align:middle;
	overflow:hidden
}
.popup-content #show_popup {
	margin-right:5px;
	margin-top:-2px
}
.popup-content-block .block-ads-content img {
	-moz-border-radius:0 0 5px 5px;
	-webkit-border-radius:0 0 5px 5px;
	border-radius:0 0 5px 5px
}
.popup-subscribe {
	padding:50px;
	width:75%
}
.popup-subscribe .em-block-title {
	width:100%;
	margin:0
}
.popup-subscribe .em-block-title p.h3 {
	margin-bottom:6px
}
#addtoContainer {
	position:fixed;
	left:37%;
	top:30px;
	width:400px;
	padding:20px;
	background:#fff;
	z-index:9999;
	border:10px solid #a1a1a1;
	text-align:center;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
#addtoContainer .back {
	width:38px;
	height:38px;
	background-image:url(../images/btn_popup-close.png);
	background-position:0 0;
	background-repeat:no-repeat;
	display:block;
	text-indent:-99999px;
	text-align:left;
	position:absolute;
	right:-25px;
	top:-25px
}
.em-search, .em-search-icon, .page-sitemap .links a {
	position:relative
}
#addtoContainer .row2 a.btn_ajaxaddto {
	display:inline-block;
	padding:10px 25px;
	text-transform:uppercase;
	margin:10px 5px 5px;
	border:1px solid;
transition:all .5s ease;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.page-sitemap .links {
	display:block;
	text-align:right;
	margin:0 20px 0 0
}
.page-sitemap .sitemap {
	padding:0 20px;
	margin:15px 0
}
.page-sitemap .sitemap li {
	margin:3px 0
}
.page-sitemap .pager {
	text-align:center
}
.page-sitemap .pager .amount {
	margin-top:0;
	margin-right:10px;
	margin-left:20px
}
.sales-guest-form .form-alt {
	overflow:hidden
}
.sales-guest-form .form-alt .input-box {
	margin-bottom:10px;
	clear:both;
	float:left;
	text-align:left!important;
	width:260px!important
}
.sales-guest-form .form-alt .input-box input {
	width:260px!important
}
.sales-guest-form .form-alt select {
	width:260px
}
.advanced-search {
	width:100%
}
.advanced-search select.multiselect {
	height:auto;
	width:95%
}
.advanced-search .form-list .input-range input.input-text {
	width:46.5%
}
.advanced-search .form-list input.input-text, .advanced-search .form-list li.wide input.input-text {
	width:95%
}
#contactForm .fieldset {
	display:block;
	float:none;
	width:100%
}
.send-friend .fieldset {
	float:none;
	width:100%
}
.form-list select.guest-select {
	width:300px
}
.not-pound-content, .not-pound-content img.img-responsive {
	display:inline-block
}
.not-pound-content .group-button li {
	display:inline-block;
	padding-top:10px
}
.em-search-icon span {
	display:block;
	text-align:left;
	text-indent:0;
	margin-bottom:-30px;
	padding-bottom:30px
}
.em-search-icon span:before {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	content:"\f002";
	font-size:20px
}
.em-search .em-search-style01 #em-search-content-fixed-top, .em-search .em-search-style01 .em-container-js-search {
	position:absolute;
	right:0;
	width:500px;
	top:50px;
	background:#fff;
	padding:20px;
	z-index:999
}
.em-search .em-search-style01 #em-search-content-fixed-top.em-hide, .em-search .em-search-style01 .em-container-js-search.em-hide {
	visibility:hidden;
	width:0;
	height:0;
	opacity:0;
	filter:alpha(opacity=0)
}
.em-search .em-search-style01 #em-search-content-fixed-top.em-show, .em-search .em-search-style01 .em-container-js-search.em-show {
	visibility:visible;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	transform:scale(1);
	-webkit-transform:scale(1)
}
.em-rtl .em-search .em-search-style01 #em-search-content-fixed-top, .em-rtl .em-search .em-search-style01 .em-container-js-search, .em-rtl .em-search-style01 #em-search-content {
	left:0;
	right:auto
}
.em-search-style02 .form-search button.button {
	font-size:0;
	display:block;
	overflow:hidden;
	text-align:left;
	text-indent:0;
	padding:14px;
	vertical-align:top;
	width:44px
}
.em-search-style02 .form-search button.button span {
	padding:0
}
.em-search-style02 .form-search button.button:before {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	content:"\f002";
	font-size:14px
}
.form-search {
	width:100%;
	margin-bottom:0;
	border-style:solid;
	border-width:0;
	position:relative
}
.form-search .text-search {
	float:left;
	position:relative;
	width:70%;
	margin:25px 0 0
}
.navbar-fixed-top .form-search .text-search {
	margin:10px 0 0
}
.form-search label {
	display:none
}
.form-search input.input-text {
	border:none;
	float:left;
	font-size:100%;
	margin:0;
	background:0 0;
	width:100%;
	padding:5px 50px 5px 10px;
	height:42px;
	line-height:34px
}
.form-search button.button {
	float:right;
	position:absolute;
	top:-1px;
	right:-1px;
	margin:0;
	padding:13px 15px;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.form-search iframe#search_autocomplete_iefix {
	top:25px!important;
	left:0!important
}
.input_cat {
	float:left;
	position:relative;
	border-right-width:1px;
	border-right-style:solid;
	width:31%
}
.input_cat select {
	width:100%;
	border:none;
	padding:5px 15px;
	height:39px;
	line-height:39px
}
.input_cat .catsearch-dropdown span.current {
	cursor:default;
	float:left;
	margin-top:0;
	padding:10px 30px 11px 5px;
	min-height:39px;
	text-align:left;
	width:100%;
	position:relative;
	white-space:nowrap;
	text-overflow:ellipsis;
	overflow:hidden
}
.input_cat .catsearch-dropdown span.current:after {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	content:"\f0d7";
	font-size:100%;
	position:absolute;
	right:10px;
	margin-top:5px
}
.input_cat .catsearch-dropdown ul {
	border-style:solid;
	border-width:1px;
	position:absolute;
	left:-1px;
	top:39px;
	width:194px;
	z-index:999;
	max-height:400px;
	overflow:auto;
	-webkit-box-shadow:0 0 5px #e5e5e5;
	-moz-box-shadow:0 0 5px #e5e5e5;
	box-shadow:0 0 5px #e5e5e5
}
.input_cat .catsearch-dropdown ul li {
	padding:5px;
	text-align:left
}
.input_cat .catsearch-dropdown ul li:hover {
	cursor:pointer
}
.input_cat .catsearch-dropdown ul li a {
	text-decoration:none
}
.search-autocomplete {
	z-index:999;
	position:absolute;
	top:20px;
	left:20px
}
.search-autocomplete ul {
	border:1px solid #ddd;
	background-color:#fff
}
.search-autocomplete li {
	border:1px solid #cdcdcd;
	padding:3px 9px;
	cursor:pointer;
	line-height:1.35
}
.search-autocomplete li .amount {
	float:right;
	font-weight:700
}
.em-icon-search-mobile:before, .em-top-search.search-fixed-right .form-search button.button:before {
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	content:"\f002"
}
.search-autocomplete li.selected {
	background:#f7e8dd
}
.em-fixed-sidebar .form-search input.input-text {
	width:75%
}
.em-top-search.search-fixed-right {
	position:fixed;
	right:15px;
	bottom:90px;
	z-index:30;
	width:auto;
	margin:0;
	padding:0
}
.em-top-search.search-fixed-right .em-search-style01 .em-search-icon, .em-top-search.search-fixed-right .input_cat {
	display:none
}
.em-top-search.search-fixed-right .em-search-style01 #em-search-content {
	display:block;
	position:static;
	opacity:1;
	filter:alpha(opacity=100);
	visibility:visible
}
.em-slideshow, .em-slideshow h3 {
	position:relative
}
.em-top-search.search-fixed-right .form-search {
	width:auto;
	border-color:#ff9301;
	border-width:2px
}
.em-top-search.search-fixed-right .form-search .text-search {
	width:100%;
	min-height:50px
}
.em-top-search.search-fixed-right .form-search input.input-text {
	width:0;
	overflow:hidden;
	padding-right:40px;
	padding-left:10px;
	height:50px;
	text-overflow:ellipsis;
	white-space:nowrap;
	transition:none;
	-webkit-transition:none
}
.em-top-search.search-fixed-right .form-search input.input-text:focus {
	width:300px
}
.em-top-search.search-fixed-right .form-search:hover input.input-text {
	width:300px;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-top-search.search-fixed-right .form-search button.button {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	top:0;
	right:0;
	padding:15px;
	min-width:50px;
	height:50px
}
.em-top-search.search-fixed-right .form-search button.button:before {
	display:inline-block;
	font-size:18px
}
.em-top-search.search-fixed-right .form-search button.button span span {
	padding:0;
	font-size:0!important;
	text-indent:-9999px
}
.em-icon-search-mobile {
	float:right;
	cursor:pointer
}
.em-icon-search-mobile:before {
	display:inline-block;
	font-size:14px;
	margin-right:5px
}
.em-multideal-price-box p, .price-box p {
	display:inline-block;
	margin-bottom:0;
	margin-top:0;
	font-size:18px
}
.em-multideal-price-box p:first-child, .price-box p:first-child {
	margin-right:5px;
	margin-top:0
}
.em-multideal-price-box p.price-to, .price-box p.price-to {
	margin-top:0
}
.em-multideal-price-box p.old-price .price-label, .em-multideal-price-box p.special-price .price-label, .price-box p.old-price .price-label, .price-box p.special-price .price-label {
	display:none
}
.em-multideal-price-box .price-label, .em-multideal-price-box span.price, .price-box .price-label, .price-box span.price {
	display:inline-block
}
.products-list .price-box p, .products-list .price-box p.special-price {
	margin-top:0
}
.old-price .price {
	text-decoration:line-through
}
.price-excl-tax, .price-excl-tax .label, .price-excl-tax .price, .price-excluding-tax, .price-incl-tax, .price-incl-tax .label, .price-incl-tax .price, .price-including-tax, .weee {
	display:block
}
@keyframes text_left_fast {
0%, 100%, 90% {
-webkit-transform:translateX(-800px);
-moz-transform:translateX(-800px);
-ms-transform:translateX(-800px);
transform:translateX(-800px);
opacity:0
}
20%, 60% {
-webkit-transform:translateX(0);
-moz-transform:translateX(0);
-ms-transform:translateX(0);
transform:translateX(0);
opacity:1
}
}
@keyframes text_left {
0%, 100%, 95% {
-webkit-transform:translateX(-800px);
-moz-transform:translateX(-800px);
-ms-transform:translateX(-800px);
transform:translateX(-800px);
opacity:0
}
25%, 60% {
-webkit-transform:translateX(0);
-moz-transform:translateX(0);
-ms-transform:translateX(0);
transform:translateX(0);
opacity:1
}
}
@-webkit-keyframes opa {
0%, 25% {
opacity:0
}
50% {
opacity:1
}
}
@keyframes opa {
0%, 25% {
opacity:0
}
50% {
opacity:1
}
}
@-webkit-keyframes fadeInDown {
0% {
opacity:0;
-webkit-transform:translate3d(0, -100%, 0);
transform:translate3d(0, -100%, 0)
}
100% {
opacity:1;
-webkit-transform:none;
transform:none
}
}
@keyframes fadeInDown {
0% {
opacity:0;
-webkit-transform:translate3d(0, -100%, 0);
transform:translate3d(0, -100%, 0)
}
100% {
opacity:1;
-webkit-transform:none;
transform:none
}
}
.em-slideshow {
	z-index:1
}
.container-fluid .col-sm-24 {
	padding-left:0;
	padding-right:0
}
.em-slideshow .img-responsive {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.em-slideshow h1, .em-slideshow h2, .em-slideshow h3, .em-slideshow h4 {
	color:#fff
}
.em-slideshow h3:before {
	height:2px;
	width:50px;
	background-color:#fff;
	content:"";
	position:absolute;
	left:47%;
	bottom:0
}
.em-slideshow h2 {
	display:inline-block;
	position:relative;
	padding:2.5rem 0
}
.em-slideshow h2:after, .em-slideshow h2:before {
	height:2px;
	background-color:#fff;
	content:"";
	position:absolute;
	width:100%;
	left:0
}
.em-slideshow h2:before {
	bottom:0
}
.em-slideshow h2:after {
	top:0
}
.em-slideshow .owl-carousel {
	margin:0
}
.em-slideshow .owl-carousel .owl-item {
	padding:0;
	position:relative;
	overflow:hidden
}
.em-slideshow .owl-carousel .owl-item img {
	width:100%
}
.em-slideshow .owl-carousel .owl-item .em-owlcarousel-des {
	position:absolute;
	left:25%;
	top:17%;
	width:50%;
	text-align:center
}
.em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInDown .button-link, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInDown h1, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInDown h2, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInLeft .button-link, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInLeft h1, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInLeft h2, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInRight .button-link, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInRight h1, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInRight h2, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInUp .button-link, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInUp h1, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInUp h2 {
	-webkit-animation-name:text_left_fast;
	-webkit-animation-duration:5s;
	-webkit-animation-timing-function:linear;
	-webkit-animation-iteration-count:infinite;
	-webkit-animation-direction:normal;
	-webkit-animation-play-state:running;
	-webkit-animation-fill-mode:forwards;
	-moz-animation-name:anim_titles;
	-moz-animation-duration:5s;
	-moz-animation-timing-function:linear;
	-moz-animation-iteration-count:infinite;
	-moz-animation-direction:normal;
	-moz-animation-play-state:running;
	-moz-animation-fill-mode:forwards;
	-webkit-animation-delay:1s;
	-moz-animation-delay:1s
}
.em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInDown h3, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInDown h4, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInLeft h3, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInLeft h4, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInRight h3, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInRight h4, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInUp h3, .em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.fadeInUp h4 {
	-webkit-animation-name:text_left;
	-webkit-animation-duration:5s;
	-webkit-animation-timing-function:linear;
	-webkit-animation-iteration-count:infinite;
	-webkit-animation-direction:normal;
	-webkit-animation-play-state:running;
	-webkit-animation-fill-mode:forwards;
	-moz-animation-name:anim_titles;
	-moz-animation-duration:5s;
	-moz-animation-timing-function:linear;
	-moz-animation-iteration-count:infinite;
	-moz-animation-direction:normal;
	-moz-animation-play-state:running;
	-moz-animation-fill-mode:forwards;
	-webkit-animation-delay:1s;
	-moz-animation-delay:1s
}
.em-slideshow .owl-carousel .owl-item .em-owlcarousel-des.none {
	-webkit-animation-name:opa;
	-webkit-animation-duration:5s;
	-webkit-animation-timing-function:linear;
	-webkit-animation-iteration-count:infinite;
	-webkit-animation-direction:normal;
	-moz-animation-duration:5s;
	-moz-animation-timing-function:linear;
	-moz-animation-iteration-count:infinite;
	-webkit-animation-delay:1s;
	-moz-animation-delay:1s
}
.em-slideshow .owl-theme .owl-controls .owl-buttons div {
	top:38%;
	font-size:0!important;
	margin:0!important;
	padding:19px 7px!important;
	width:58px;
	border:1px solid transparent;
	-webkit-box-shadow:none!important;
	-moz-box-shadow:none!important;
	box-shadow:none!important;
	color:#fff;
	background:rgba(0, 0, 0, .5);
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.em-slideshow .owl-theme .owl-controls .owl-buttons div.owl-next:before, .em-slideshow .owl-theme .owl-controls .owl-buttons div.owl-prev:before {
	font-size:1.9rem;
	-moz-backface-visibility:hidden;
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
.em-slideshow .owl-theme .owl-controls .owl-buttons div.owl-next:before {
	content:"\f054";
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	margin-left:3px
}
.em-slideshow .owl-theme .owl-controls .owl-buttons div.owl-prev:before {
	content:"\f053";
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	margin-right:2px
}
.em-slideshow .owl-theme .owl-controls .owl-buttons div:hover {
	background:#fff;
	color:#a1a1a1;
	border-color:transparent
}
.em-slideshow .owl-theme .owl-controls .owl-buttons div.owl-prev {
	left:0;
	right:auto
}
.em-slideshow .owl-theme .owl-controls .owl-buttons div.owl-next {
	right:0;
	left:auto
}
.em-slideshow .owl-theme .owl-controls .owl-pagination {
	position:absolute;
	left:48%;
	bottom:30px;
	margin:0
}
.em-slideshow .owl-theme .owl-controls .owl-pagination .owl-page span {
	border-width:2px;
	background:0 0;
	border-color:#a3a19e;
	width:15px!important;
	height:15px!important
}
.em-slideshow .owl-theme .owl-controls .owl-pagination .owl-page span:hover, .em-slideshow .owl-theme .owl-controls .owl-pagination .owl-page.active span {
	border-color:#fff
}
.em-slideshow a.button-link {
	float:none;
	display:inline-block;
	padding:13px 20px
}
#progressBar {
	position:absolute;
	z-index:1;
	background:rgba(237, 237, 237, .22)!important
}
#progressBar #bar {
	background:rgba(0, 0, 0, .33)
}
.em_post-item .post-title h2 {
	margin-bottom:10px
}
.em_post-item .post-image img {
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em_post-item .post-header {
	overflow:hidden;
	padding-top:0;
	margin-top:0;
	display:block;
	margin-bottom:5px;
	border-bottom:1px solid #E2E2E2
}
.em_post-items .em_post-item {
	border-top:1px solid #e5e5e5;
	padding-top:2rem
}
.em_post-items .em_post-item:first-child {
	border:none;
	padding-top:0
}
.em_post-items .post-header {
	padding-top:0
}
.em_block-recent-comments li, .em_block-recent-post li {
	border-top:1px solid #e5e5e5;
	padding-top:17px
}
.em_block-recent-comments li:first-child, .em_block-recent-post li:first-child {
	border-top-width:0;
	padding-top:0
}
#allcomments>.comment-item {
	border:1px solid #e5e5e5;
	padding:26px 30px 19px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
#allcomments .comment-by:before {
	display:none
}
#allcomments .time-stamp {
	font-size:85%
}
#allcomments .time-stamp:before {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	content:"\f133";
	font-size:120%;
	margin-right:5px
}
#allcomments .comment-button button.button:before, .em-top-cart .em-container-topcart .em-amount-topcart:after {
	display:inline-block;
	font-size:100%;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
#allcomments .comment-button button.button:before {
	content:"\f112";
	margin-right:7px;
	float:left;
	margin-top:2px
}
.form-comment {
	border:1px solid #e5e5e5;
	padding:23px 30px 19px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.article-comments .toolbar .sort-by, .blog-category-view .toolbar .sort-by, .blog-index-index .toolbar .sort-by, .blog-tag-view .toolbar .sort-by {
	margin-left:0
}
.contacts-index-index #map-canvas {
	margin-bottom:23px
}
.contacts-index-index .em-wrapper-main address {
	margin-bottom:10px
}
.contacts-index-index .em-wrapper-main span.fa {
	margin-right:10px
}
.em-top-cart {
	text-align:left
}
.em-top-cart .em-container-topcart {
	position:relative
}
.em-top-cart .em-container-topcart .em-amount-topcart {
	padding-bottom:30px
}
.em-top-cart .em-container-topcart .em-amount-topcart:after {
	content:"\f0d7";
	margin-left:5px;
-webkit-transition:-webkit-transform .2s ease-in;
-moz-transition:-moz-transform .2s ease-in;
-o-transition:-o-transform .2s ease-in;
transition:transform .2s ease-in
}
.em-top-cart .em-container-topcart:hover .em-amount-topcart:after {
	-moz-transform:rotate(180deg);
	-webkit-transform:rotate(180deg);
	-o-transform:rotate(180deg);
	transform:rotate(180deg)
}
.em-top-cart .topcart-popup {
	padding:15px 5px 0;
	border-style:solid;
	border-width:1px;
	position:absolute;
	right:0;
	width:300px;
	z-index:22;
	top:57px
}
.em-top-cart .topcart-popup .topcart-popup-content .em-block-subtitle {
	text-transform:uppercase;
	display:none
}
.em-top-cart .topcart-popup .topcart-popup-content .amount-content {
	padding:0
}
.em-top-cart .topcart-popup .topcart-popup-content a.goto-cart {
	float:right;
	margin-right:0
}
.topcart-popup-content .em-topcart-list {
	margin:15px 0 0
}
.topcart-popup-content li.item {
	position:relative;
	display:inline-block;
	width:100%;
	margin:0 0 20px;
	border-bottom:1px solid #f0f0f0;
	padding:0 0 20px
}
.topcart-popup-content li.item.last {
	margin-bottom:0
}
.topcart-popup-content li.item a.product-image {
	float:left;
	margin-right:15px;
	margin-bottom:0
}
.topcart-popup-content li.item .product-details {
	margin-left:120px;
	padding-top:0
}
.topcart-popup-content li.item p {
	margin-bottom:5px
}
.topcart-popup-content li.item .btn-edit, .topcart-popup-content li.item .btn-remove {
	display:inline-block;
	text-align:left;
	float:none;
	position:absolute;
	top:0;
	left:0
}
.topcart-popup-content li.item .btn-edit {
	left:45px;
	display:none
}
.topcart-popup-content li.item .truncated .show .item-options {
	top:15px;
	left:-150px
}
.topcart-popup-content .subtotal {
	width:100%;
	float:left;
	text-transform:uppercase;
	margin:1.2rem 0 2rem
}
.topcart-popup-content .subtotal .label {
	float:left;
	margin-right:20px;
	padding-top:6px
}
.topcart-popup-content .subtotal .price {
	float:right
}
.topcart-popup-content .wrapper_bottom_button {
	overflow:hidden;
	clear:both;
	margin:0 -21px -1px;
	padding:15px 20px 5px
}
.topcart-popup-content .wrapper_bottom_button a.goto-cart, .topcart-popup-content .wrapper_bottom_button button.button {
	padding-left:20px;
	padding-right:20px
}
.topcart-popup-content .wrapper_bottom_button a.goto-cart:before {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	content:"\f07a";
	font-size:14px
}
.cart-fixed-right {
	position:fixed!important;
	right:15px!important;
	bottom:150px!important;
	top:auto!important;
	z-index:31;
	width:auto!important;
	padding:0!important;
	margin:0!important
}
.cart-fixed-right .em-summary-topcart {
	min-width:54px;
	padding:0!important;
	margin-right:0
}
.cart-fixed-right .em-summary-topcart span.em-icon {
	font-size:25px
}
.cart-fixed-right .em-summary-topcart span {
	display:none!important
}
.cart-fixed-right .em-container-topcart .em-amount-topcart {
	padding:15px 11px!important;
	display:block!important;
	text-align:center;
	margin:0!important
}
.cart-fixed-right .em-container-topcart .em-amount-topcart:before {
	display:none!important;
	content:""
}
.cart-fixed-right .em-container-topcart .em-amount-topcart:after {
	content:"\f07a"!important;
	margin-left:0;
	padding:0!important;
	width:auto!important;
	float:none!important;
	font-size:130%;
	-moz-transform:rotate(0)!important;
	-webkit-transform:rotate(0)!important;
	-o-transform:rotate(0)!important;
	transform:rotate(0)!important
}
.cart-fixed-right .em-container-topcart .em-amount-topcart:hover:after {
	content:'\f07a';
	font-size:130%;
	-moz-transform:rotate(0)!important;
	-webkit-transform:rotate(0)!important;
	-o-transform:rotate(0)!important;
	transform:rotate(0)!important
}
.cart-fixed-right.em-top-cart .topcart-popup {
	top:auto;
	right:60px;
	bottom:-55px;
	padding-top:18px
}
.cart-fixed-right.em-top-cart .topcart-popup li.item a.product-image {
	width:70px
}
.cart-fixed-right.em-top-cart .topcart-popup li.item .product-details {
	margin-left:80px
}
.qty-ctl {
	float:left;
	height:auto;
	line-height:normal;
	margin:0!important;
	padding:0!important
}
.qty-ctl button {
	font-size:0;
	display:block;
	overflow:hidden;
	vertical-align:middle;
	text-align:left;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	margin-bottom:0;
	cursor:pointer;
	background-color:transparent;
	border:1px solid;
	text-indent:0;
	width:40px;
	height:40px;
	padding:0 13px;
	position:relative;
	z-index:1
}
.opc .step-title .title-box, .opc .step-title a {
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.qty-ctl button.decrease {
	-moz-border-radius:3px 0 0 3px;
	-webkit-border-radius:3px 0 0 3px;
	border-radius:3px 0 0 3px
}
.qty-ctl button.decrease:before {
	content:"-";
	font-size:20px;
	font-weight:700;
	line-height:1;
	padding-left:4px
}
.qty-ctl button.increase {
	-moz-border-radius:0 3px 3px 0;
	-webkit-border-radius:0 3px 3px 0;
	border-radius:0 3px 3px 0
}
.qty-ctl button.increase:before {
	content:"+";
	font-size:20px;
	line-height:.5
}
.block-layered-nav .block-content .tree-filter .icon:before, .block-progress dd.complete a.sample-link:before, .opc .step-title a:before {
	font-family:FontAwesome;
	font-style:normal;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
.qty_cart {
	width:140px;
	display:inline-block
}
.qty_cart input.qty {
	float:left;
	height:40px;
	width:60px!important;
	text-align:center;
	margin:0 -1px;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.qty_cart input.qty:focus, .qty_cart input.qty:hover {
	position:relative;
	z-index:2
}
.cart-table tr td {
	padding:18px 15px 10px
}
.cart-table tr td .cart-product {
	position:relative;
	margin-top:-11px
}
.cart-table tr td .cart-product .btn-remove {
	position:absolute;
	top:0;
	left:0;
	z-index:1
}
.cart-table tr td .cart-price {
	display:inline-block;
	margin-top:-12px
}
.cart-table tr td .link-wishlist {
	display:inline-block;
	margin-top:-6px
}
.cart-table tr td .qty_cart {
	margin-top:-10px
}
.cart-table tr td.last {
	padding-bottom:20px
}
.cart-table tfoot tr td {
	padding:20px 0 10px;
	border-color:transparent
}
.cart-collaterals {
	padding-top:0
}
.cart-collaterals h2 {
	padding:10px 0;
	margin-bottom:10px
}
.cart-collaterals .em-box {
	padding:17px 20px 9px;
	min-height:326px;
	margin-bottom:50px
}
.cart-collaterals .em-box p {
	margin-bottom:10px
}
.cart-collaterals .form-list {
	margin-bottom:0
}
.cart-collaterals .form-list li {
	margin:5px 0 0
}
.cart-collaterals .form-list li:first-child {
	margin-top:0
}
.cart-collaterals input.input-text {
	width:100%
}
.cart-collaterals .buttons-set {
	padding-top:20px
}
.cart-collaterals .sp-methods {
	margin-bottom:0
}
.cart-collaterals .sp-methods dt {
	float:left;
	margin-right:5px
}
.cart-collaterals .totals .em-box {
	padding-top:24px
}
.cart-collaterals .totals table td {
	text-align:left!important
}
.cart-collaterals .totals table td strong {
	font-weight:400
}
.cart-collaterals .totals table td .price {
	display:inline-block;
	margin-top:-12px
}
.cart-collaterals .totals .checkout-types {
	margin-top:9px
}
.cart-collaterals .totals .checkout-types li {
	width:100%;
	display:inline-block;
	margin-bottom:0
}
.cart-collaterals .totals .checkout-types li button.btn-checkout {
	margin-right:0;
	margin-bottom:12px;
	text-align:center;
	display:inline-block
}
.cart-collaterals .totals .checkout-types li button.btn-checkout span {
	float:none;
	display:inline-block;
	width:100%
}
.crosssell {
	margin-top:20px
}
.crosssell h2 {
	margin-bottom:18px;
	text-align:left
}
.crosssell-content {
	overflow:hidden
}
.crosssell-content #em-grid-mode {
	margin:0!important;
	border:1px solid #e5e5e5;
	padding:10px
}
.em-sidebar .block-progress {
	margin-bottom:20px!important
}
.em-sidebar .block-progress .block-content {
	padding-top:9px;
	padding-bottom:9px;
	-moz-border-radius:0 0 5px 5px;
	-webkit-border-radius:0 0 5px 5px;
	border-radius:0 0 5px 5px
}
.block-progress dt {
	padding:6px 0;
	font-weight:500
}
.block-progress dl {
	margin-bottom:0
}
.block-progress dd.complete address {
	margin-bottom:6px
}
.block-progress dd.complete a.sample-link:before {
	display:inline-block;
	font-weight:400;
	content:"\f101";
	font-size:100%;
	padding-right:7px
}
.opc-1sc #opc-login p a {
	font-size:85%;
	text-transform:uppercase;
	font-weight:700
}
.opc-1sc #opc-login p a:hover {
	text-decoration:underline
}
.checkout-onepage-success .em-col-main .print-order, .em-logo .logo {
	text-decoration:none
}
.opc-col>div {
	padding:13px 20px;
	border:1px solid #e8e8e8;
	margin-bottom:40px
}
.opc-col .discount label {
	margin-bottom:8px
}
.opc-col #opc-payment .form-list .v-fix:first-child, .opc-col .sp-methods .form-list .v-fix:first-child {
	margin-right:4%
}
.opc-col #opc-payment .form-list .v-fix, .opc-col .sp-methods .form-list .v-fix {
	width:48%
}
.opc .section.allow:hover .step-title, .opc .section.allow:hover .step-title a {
	cursor:pointer
}
.opc .section.allow.active .step-title, .opc .section.allow.active .step-title a {
	cursor:default
}
.opc .step-title {
	margin-bottom:10px
}
.opc .step-title .title-box {
-moz-transition:all .5s ease;
transition:all .5s ease;
	backface-visibility:hidden;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	padding:1rem 2rem;
	min-height:37px;
	float:left;
	width:100%;
	border:1px solid #e5e5e5
}
.opc .step-title .title-box a {
	font-size:0
}
.opc .step-title .number {
	float:left;
	display:none
}
.opc .step-title h2 {
	float:left
}
.opc .step-title a {
-moz-transition:all .5s ease;
transition:all .5s ease;
	backface-visibility:hidden;
	font-size:0;
	float:right;
	cursor:default
}
.block-layered-nav .block-content .img-filter li a, .em_block-tag-cloud li a, .tags-list li a {
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.opc .step-title a:before {
	display:inline-block;
	font-weight:400;
	content:"\f107";
	font-size:18px;
	margin-top:3px
}
.opc .step {
	padding:30px 20px 19px;
	margin-bottom:1rem;
	margin-top:-13px;
	border:1px solid #e5e5e5;
	border-top:0;
	-moz-border-radius:0 0 3px 3px;
	-webkit-border-radius:0 0 3px 3px;
	border-radius:0 0 3px 3px
}
.opc ul.form-list {
	margin-bottom:14px
}
.opc ul.form-list>li {
	margin-right:0
}
.opc ul.form-list>li.wide {
	margin-right:3%
}
.opc .form-list li.control {
	margin-top:7px;
	margin-bottom:7px
}
.opc .tool-tip {
	top:auto!important;
	bottom:200px
}
.opc #checkout-step-login h3 {
	margin-bottom:14px
}
.opc #checkout-step-login .col-2 h4 {
	float:left;
	margin-bottom:10px;
	margin-right:2px
}
.opc #checkout-step-login .col-2 .form-list {
	width:90%
}
.opc #checkout-step-login .col-1 strong {
	font-weight:400
}
.opc #checkout-step-login ul.ul li {
	padding-bottom:7px
}
.opc #checkout-step-login ul.ul li:before {
	content:"+";
	padding-right:5px
}
.opc #checkout-step-login .buttons-set a {
	margin-top:6px
}
.opc #checkout-step-shipping_method .sp-methods dt {
	float:left;
	margin-right:5px
}
.opc #checkout-review-submit .button.button {
	float:right;
	margin-right:0
}
.opc #checkout-review-submit .please-wait {
	float:right
}
.opc #checkout-review-submit .f-left {
	float:right!important;
	clear:both
}
.sp-methods .form-list input.input-text, .sp-methods .form-list select {
	width:280px
}
.sp-methods .form-list select.month {
	width:133px;
	margin-right:15px
}
.sp-methods .form-list select.year {
	width:132px
}
.checkout-onepage-success .em-col-main .em-box {
	padding:27px 20px 19px
}
.checkout-onepage-success .em-col-main .em-box>p {
	margin-bottom:7px
}
.checkout-onepage-success .em-col-main .buttons-set {
	padding-top:8px
}
.em-block-title {
	text-transform:capitalize
}
.em-block-title .h2 {
	margin-bottom:15px
}
.em-line-01 {
	position:relative;
	margin-bottom:50px
}
.em-line-01 .block-title, .em-line-01 .em-block-title, .em-line-01 .em-widget-title, .em-line-01 .widget-title {
	border-bottom-width:1px;
	border-bottom-style:solid;
	margin:0 0 30px;
	font-size:100%
}
.em-line-01 .widget-title {
	margin:30px 0
}
.em-line-01 .block-title h2, .em-line-01 .block-title h3, .em-line-01 .block-title strong, .em-line-01 .em-block-title h2, .em-line-01 .em-block-title h3, .em-line-01 .em-block-title strong, .em-line-01 .em-widget-title h2, .em-line-01 .em-widget-title h3, .em-line-01 .em-widget-title strong, .em-line-01 .widget-title h2, .em-line-01 .widget-title h3, .em-line-01 .widget-title strong {
	margin:0 0 -1px;
	border-bottom-width:4px;
	border-bottom-style:solid;
	padding-bottom:10px;
	display:inline-block;
	text-transform:uppercase;
	position:relative;
	z-index:1
}
.em-line-01 .block-title small, .em-line-01 .em-block-title small, .em-line-01 .em-widget-title small, .em-line-01 .widget-title small {
	font-size:100%
}
.em-line-01 .em-tabs {
	margin-top:-75px;
	text-align:right
}
.em-line-01 .em-tabs.r-tabs .r-tabs-nav .r-tabs-anchor {
	margin-right:0;
	margin-left:5px;
	padding:9px 20px 15px;
	min-height:45px
}
.em-line-01.em-new-arrivals-tabs {
	margin-bottom:15px
}
.em-line-02 {
	border-bottom:2px solid;
	margin-bottom:20px
}
.em-box-01 {
	border:1px solid;
	padding-top:2px;
	margin-bottom:20px
}
.em-box-01>div {
	border-top:1px solid;
	padding:15px
}
.em-box-01 .widget-title h2, .em-box-01 .widget-title h3 {
	text-transform:uppercase
}
.em-box-02 .title-box {
	padding:0;
	margin:0
}
.em-box-02 .title-box h1, .em-box-02 .title-box h2 {
	margin-bottom:0
}
.em-box-02 .em-block-title p.h3 {
	margin-bottom:5px
}
.em-box-bkg {
	padding:3rem 2rem 1rem;
	margin-bottom:50px
}
.em-box-bkg .em-line-01 {
	margin-bottom:0
}
.em-box-04 {
	padding:20px;
	margin-bottom:20px
}
.em-box-04 .em-line-01 {
	margin-bottom:0
}
.em-sidebar .block, .em-sidebar .multidealpro_recent, .em-sidebar .widget {
	margin-bottom:20px;
	border-style:solid;
	border-width:1px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em-sidebar .block .block-title, .em-sidebar .block .em-block-title, .em-sidebar .block .em-widget-title, .em-sidebar .block .widget-title, .em-sidebar .multidealpro_recent .block-title, .em-sidebar .multidealpro_recent .em-block-title, .em-sidebar .multidealpro_recent .em-widget-title, .em-sidebar .multidealpro_recent .widget-title, .em-sidebar .widget .block-title, .em-sidebar .widget .em-block-title, .em-sidebar .widget .em-widget-title, .em-sidebar .widget .widget-title {
	border-bottom-width:0;
	margin-bottom:0;
	padding:1.4rem 2rem
}
.em-sidebar .block .block-title h2, .em-sidebar .block .block-title h3, .em-sidebar .block .block-title strong, .em-sidebar .block .em-block-title h2, .em-sidebar .block .em-block-title h3, .em-sidebar .block .em-block-title strong, .em-sidebar .block .em-widget-title h2, .em-sidebar .block .em-widget-title h3, .em-sidebar .block .em-widget-title strong, .em-sidebar .block .widget-title h2, .em-sidebar .block .widget-title h3, .em-sidebar .block .widget-title strong, .em-sidebar .multidealpro_recent .block-title h2, .em-sidebar .multidealpro_recent .block-title h3, .em-sidebar .multidealpro_recent .block-title strong, .em-sidebar .multidealpro_recent .em-block-title h2, .em-sidebar .multidealpro_recent .em-block-title h3, .em-sidebar .multidealpro_recent .em-block-title strong, .em-sidebar .multidealpro_recent .em-widget-title h2, .em-sidebar .multidealpro_recent .em-widget-title h3, .em-sidebar .multidealpro_recent .em-widget-title strong, .em-sidebar .multidealpro_recent .widget-title h2, .em-sidebar .multidealpro_recent .widget-title h3, .em-sidebar .multidealpro_recent .widget-title strong, .em-sidebar .widget .block-title h2, .em-sidebar .widget .block-title h3, .em-sidebar .widget .block-title strong, .em-sidebar .widget .em-block-title h2, .em-sidebar .widget .em-block-title h3, .em-sidebar .widget .em-block-title strong, .em-sidebar .widget .em-widget-title h2, .em-sidebar .widget .em-widget-title h3, .em-sidebar .widget .em-widget-title strong, .em-sidebar .widget .widget-title h2, .em-sidebar .widget .widget-title h3, .em-sidebar .widget .widget-title strong {
	border-bottom-width:0;
	padding-bottom:0;
	margin-bottom:0;
	text-transform:uppercase
}
.em-sidebar .block-content, .em-sidebar .widget-products {
	padding:16px 20px
}
.em-sidebar .block-content .actions, .em-sidebar .widget-products .actions {
	padding-bottom:5px
}
.em-sidebar .block-content .products-list .item, .em-sidebar .widget-products .products-list .item {
	margin-bottom:0;
	padding:2rem 0 1rem;
	border-width:1px 0 0;
	border-top-style:dotted
}
.em-sidebar .block-content .products-list .item.first, .em-sidebar .block-content .products-list .item:first-child, .em-sidebar .widget-products .products-list .item.first, .em-sidebar .widget-products .products-list .item:first-child {
	border-top-color:transparent;
	padding-top:0
}
.em-sidebar .block-content .products-list .item a.product-image, .em-sidebar .widget-products .products-list .item a.product-image {
	margin-bottom:1rem
}
.em-sidebar .block-banner {
	margin-top:20px
}
.em-sidebar .line-right {
	padding-right:10px
}
.em-sidebar .line-left {
	padding-left:10px
}
.block-ads img.img-responsive {
	display:inline-block;
	margin-bottom:20px
}
.block-compare .empty {
	padding-top:10px
}
.block-compare .block-content {
	padding-top:0;
	padding-bottom:0
}
.block-compare #compare-items li.item {
	margin-bottom:0;
	padding:2rem 0 1rem;
	border-top-width:1px;
	border-top-style:dotted;
	position:relative
}
.block-compare #compare-items li.item .last, .block-wishlist .block-content .actions {
	padding-bottom:0
}
.block-compare #compare-items li.item:first-child {
	border-top-color:transparent
}
.block-compare #compare-items li.item a.product-image {
	float:left;
	margin-right:2rem;
	margin-bottom:1rem
}
.block-compare #compare-items li.item .product-shop {
	overflow:hidden
}
.block-compare #compare-items li.item .btn-remove {
	position:absolute;
	top:20px;
	left:0;
	z-index:1
}
.block-wishlist p.block-subtitle {
	margin-bottom:0
}
.block-wishlist li.item {
	margin-bottom:10px;
	position:relative;
	border-top-width:1px;
	border-top-style:dotted;
	padding-top:20px
}
.block-wishlist li.item:first-child {
	border-top-width:0;
	padding-top:20px
}
.block-wishlist li.item a.product-image {
	float:left;
	margin-right:20px;
	margin-bottom:10px
}
.block-wishlist li.item .product-details {
	overflow:hidden
}
.block-wishlist li.item .btn-remove {
	position:absolute;
	top:20px;
	left:0;
	z-index:1
}
.block-wishlist li.item a.link-cart {
	float:left;
	margin-top:10px
}
.block-reorder .checkbox {
	float:left;
	margin-right:5px
}
.paypal-logo {
	margin-bottom:20px;
	clear:both;
	padding-top:10px
}
.em-sidebar .paypal-logo {
	display:none
}
.block-poll .block-subtitle {
	margin-bottom:11px
}
.block-poll ul li {
	padding:4px 0
}
.block-poll ul li input.radio {
	display:inline-block
}
.em_block-tag-cloud li, .tags-list li {
	display:inline-block;
padding:0 .5rem .5rem 0
}
.em_block-tag-cloud li a, .tags-list li a {
	display:inline-block;
padding:.5rem 1rem;
	border-style:solid;
	border-width:1px;
	font-size:100%!important;
-moz-transition:all .5s ease;
transition:all .5s ease;
	backface-visibility:hidden
}
.block-recent .widget-products {
	border:1px solid #e5e5e5;
	border-top:0;
	padding:0 20px
}
.block-recent .products-grid li.item {
	width:100%
}
.block-recent li.item {
	border-top-width:1px;
	border-top-style:dotted;
	padding:31px 0
}
.block-recent li.item:first-child {
	border-top:0
}
.block-recent .review-details {
	padding-top:10px
}
.em-fixed-sidebar {
	padding-top:50px
}
.em-fixed-sidebar #em-sidbar-nav-panel {
	padding-bottom:30px;
	display:inline-block;
	width:100%
}
.em-fixed-sidebar .em-header-middle {
	clear:both
}
.block-layered-nav .block-content .block-subtitle {
	padding:0;
	display:none
}
.block-layered-nav .block-content .actions, .block-layered-nav .block-content dt {
	margin-bottom:10px
}
.block-layered-nav .block-content dd {
	margin:0 -20px 20px;
	padding:0 20px 10px;
	border-bottom:1px solid;
	position:relative
}
.block-layered-nav .block-content dd.last {
	border:none;
	margin-bottom:0;
	padding-bottom:0
}
.block-layered-nav .block-content .tree-filter {
	margin:0 -20px
}
.block-layered-nav .block-content .tree-filter .icon {
	margin:17px 20px 0 0;
	display:inline-block;
	background:0 0;
	width:auto;
	height:auto;
	text-indent:0;
	float:right;
	clear:both
}
.block-layered-nav .block-content .tree-filter .icon:before {
	display:inline-block;
	font-weight:400;
	content:"\f196";
	font-size:14px
}
.block-layered-nav .block-content .tree-filter .expanded .icon:before {
	content:"\f147"
}
.block-layered-nav .block-content .tree-filter .collapsed .icon:before {
	content:"\f196"
}
.block-layered-nav .block-content .tree-filter li {
	padding:0
}
.block-layered-nav .block-content .tree-filter li:before {
	content:"";
	padding-right:0;
	padding-left:0;
	display:inline
}
.block-layered-nav .block-content .tree-filter ol ol .label-icon .label:before, .block-layered-nav .block-content dd li:before {
	content:"\f105";
	font-size:14px;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
.block-layered-nav .block-content .tree-filter .label-icon {
	padding:14px 20px
}
.block-layered-nav .block-content .tree-filter li.expanded ol .label-icon {
	padding-left:20px
}
.block-layered-nav .block-content .tree-filter ol {
	border-top-width:1px;
	border-top-style:solid
}
.block-layered-nav .block-content .tree-filter ol ol .icon {
	margin-top:10px
}
.block-layered-nav .block-content .tree-filter ol ol .label-icon {
	padding:7px 20px;
	text-transform:none
}
.block-layered-nav .block-content .tree-filter ol ol .label-icon .label:before {
	display:inline-block;
	padding:0 10px 0 0
}
.block-layered-nav .block-content .tree-filter ol ol ol {
	border:none;
	padding-left:20px
}
.block-layered-nav .block-content .tree-filter>li>ol>li {
	border-bottom-width:1px;
	border-bottom-style:solid
}
.block-layered-nav .block-content .tree-filter>li>ol>li.last, .block-layered-nav .block-content .tree-filter>li>ol>li.selected {
	border-bottom:none
}
.block-layered-nav .block-content .tree-filter>li>ol>li.selected>ol {
	padding-top:11px
}
.block-layered-nav .block-content .tree-filter>li.selected>ol>li>ol {
	padding-top:11px;
	padding-bottom:11px
}
.block-layered-nav .block-content dd li {
	padding:4px 0
}
.block-layered-nav .block-content dd li:before {
	display:inline-block;
	padding-right:10px
}
.block-layered-nav .block-content #narrow-by-list .filter-act {
	margin-top:0;
	position:absolute;
	top:-34px;
	right:15px
}
.block-layered-nav .block-content .rslider {
	margin-top:0;
	padding-top:14px
}
.block-layered-nav .block-content .rslider .ui-slider, .block-layered-nav .block-content .rslider .ui-slider-range {
	background:0 0;
	height:3px
}
.block-layered-nav .block-content .rslider .ui-slider-handle {
	background:#fff;
	width:14px;
	height:14px;
	border:2px solid;
	-moz-border-radius:50%;
	-webkit-border-radius:50%;
	border-radius:50%;
	top:-6px
}
.block-layered-nav .block-content .rslider .ui-slider-handle:before {
	background:#1585c7;
	content:'';
	top:2px;
	left:2px;
	width:6px;
	height:6px;
	position:absolute;
	-moz-border-radius:50%;
	-webkit-border-radius:50%;
	border-radius:50%
}
.block-layered-nav .block-content .rslider .values {
	margin-bottom:20px;
	margin-top:15px
}
.block-layered-nav .block-content .img-filter li {
	border:none;
	padding:2px 1px
}
.block-layered-nav .block-content .img-filter li:before {
	content:"";
	padding-right:0;
	padding-left:0
}
.block-layered-nav .block-content .img-filter li a {
	display:inline-block;
-moz-transition:all .5s ease;
transition:all .5s ease;
	backface-visibility:hidden;
	width:30px;
	height:30px
}
.emcatalog-enable-hover.products-grid li.item .product-item .em-element-display-hover, .products-grid .item .product-shop {
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.block-layered-nav .block-content .img-filter li a img {
	max-width:100%
}
.block-layered-nav .block-content .img-filter li a:hover {
	-webkit-box-shadow:0 0 5px #797979;
	-moz-box-shadow:0 0 5px #797979;
	box-shadow:0 0 5px #797979;
	padding:2px
}
.block-latest .products-list li.item {
	border:0;
	border-top-width:1px;
	border-top-style:dotted;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	padding:20px 0 10px;
	margin:0 10px
}
.block-latest .products-list li.item:first-child {
	padding-top:10px;
	border-top:0
}
.block-latest .box-reviews>.product-list, .block-latest .box-reviews>.products-grid {
	border:1px solid #e5e5e5;
	border-top:0;
	padding:20px;
	margin-bottom:0
}
.block-latest .box-reviews>.products-grid li.item {
	padding:0;
	border:0;
	margin-right:0;
	text-align:left
}
.widget-recentreview-products .widget-products {
	padding:10px
}
.widget-recentreview-products .widget-products .item.last {
	margin-bottom:0
}
.widget-recentreview-products .widget-products .products-grid>.item {
	margin-right:0;
	width:100%
}
.widget-recentreview-products .widget-products .products-grid>.item .price-box {
	margin-bottom:10px
}
.block-em-latest .box-reviews>ul {
	border-top:0;
	padding:10px 20px;
	-moz-border-radius:0 0 5px 5px;
	-webkit-border-radius:0 0 5px 5px;
	border-radius:0 0 5px 5px
}
.block-em-latest .box-reviews>ul.products-grid {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	margin:0
}
.block-em-latest .box-reviews>ul.products-grid .item {
	margin-right:0;
	padding:0;
	border:0;
	text-align:left
}
#recently-viewed-items li.item {
	margin-bottom:0;
	padding:2rem 0 1rem;
	border-top-width:1px;
	border-top-style:dotted;
	position:relative
}
#recently-viewed-items li.item:first-child {
	border-top-color:transparent;
	padding-top:0
}
#recently-viewed-items li.item a.product-image {
	float:left;
	margin-right:20px;
	margin-bottom:1rem
}
#recently-viewed-items li.item .product-shop {
	overflow:hidden
}
#recently-viewed-items li.item.last {
	padding-bottom:0
}
a.product-image {
	position:relative;
	margin-bottom:18px;
	display:block
}
a.product-image img {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.products-grid a.product-image img {
	width:100%;
	height:auto;
	max-height:100%;
	max-width:100%;
	display:block;
	margin:0 auto;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	object-fit:contain
}
.ratings {
	padding:0 0 1rem
}
.ratings .rating-box {
	margin-right:0
}
div.desc {
	margin-bottom:1.55rem
}
.products-grid .item {
	margin:0 20px 10px 0;
	float:left;
	text-align:center
}
.products-grid .item .product-item {
	overflow:hidden;
	width:100%;
	border-width:1px;
	border-style:solid;
	height:350px
}
.products-grid .item img {
	width:100%;
	height:auto;
	-moz-border-radius:5px 5px 0 0;
	-webkit-border-radius:5px 5px 0 0;
	border-radius:5px 5px 0 0
}
.products-grid .item .product-shop {
	display:inline-block;
	width:100%;
-moz-transition:all .5s ease;
transition:all .5s ease;
	backface-visibility:hidden;
	padding:0 5px 10px
}
.products-grid .item .add-to-links li {
	float:none;
	display:inline-block;
	vertical-align:top
}
.em-crosssell-noslider.products-grid li.item, .em-upsell-noslider.products-grid li.item {
	padding:10px!important;
	margin-right:0
}
.category-products .products-grid li.item {
	padding:0 10px;
	margin-right:0;
	height:350px
}
.category-products .item .product-item {
	overflow:hidden;
	width:auto;
	display:block;
	min-height:100%
}
.category-products .item .product-item:hover {
	position:absolute;
	top:0;
	left:10px;
	right:10px;
	z-index:2
}
.button-show01 .products-grid .item .product-shop-top, .category-products .item .product-shop-top, .product-shop-top, .products-grid .item .product-shop-top {
	position:relative
}
.category-products .products-grid .em-btn-addto button.button {
	margin-bottom:0;
	font-size:0
}
.category-products .products-grid .em-btn-addto button.button span {
	padding:0
}
.products-list .product-image {
	float:left;
	margin:0 20px 10px 0
}
.products-list .product-shop {
	overflow:hidden;
	text-align:left
}
.products-list .product-shop .price-box {
	margin-bottom:14px
}
.products-list div.item {
	display:inline-block;
	width:100%;
	margin-bottom:20px
}
.products-list li.item {
	overflow:hidden
}
.em-sidebar .products-list, .em-sidebar .products-list .product-shop .price-box {
	margin-bottom:0
}
.products-list>.item {
	padding:10px;
	border:1px solid transparent;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	margin-bottom:40px
}
.products-list>.item a.product-image {
	margin-bottom:0
}
.products-list>.item .product-shop {
	margin-bottom:10px
}
.emcatalog-enable-hover.products-grid li.item .product-item .em-element-display-hover {
	transform:scale(0, 0);
	-moz-transform:scale(0, 0);
	-webkit-transform:scale(0, 0);
-moz-transition:all .5s ease;
transition:all .5s ease;
	backface-visibility:hidden;
	height:0;
	opacity:0!important
}
.emcatalog-enable-hover.products-grid li.item .product-item:hover .em-element-display-hover {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	transform:scale(1, 1);
	-moz-transform:scale(1, 1);
	-webkit-transform:scale(1, 1);
	height:100%;
	opacity:1!important
}
.product-shop-top .em-btn-addto, .product-shop-top .quickshop-link-container {
	display:inline-block;
	vertical-align:top
}
.button-show01 .products-grid .item .product-shop-top .bottom, .category-products .item .product-shop-top .bottom, .products-grid .item .product-shop-top .bottom {
	position:absolute;
	left:auto;
	bottom:0;
	text-align:center;
	width:100%;
	opacity:0;
	filter:alpha(opacity=0);
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.button-show02 .products-grid .item .em-element-display-hover, .button-show03 .products-grid .item .bottom {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.button-show01 .products-grid .item .product-shop-top .bottom button.button.btn-cart, .category-products .item .product-shop-top .bottom button.button.btn-cart, .products-grid .item .product-shop-top .bottom button.button.btn-cart {
	margin-bottom:0
}
.button-show01 .products-grid .item .product-shop-top .out-of-stock, .category-products .item .product-shop-top .out-of-stock, .products-grid .item .product-shop-top .out-of-stock {
	display:inline-block;
	vertical-align:top;
margin-right:.5rem;
	margin-top:4px
}
.button-show01 .products-grid .item .product-shop-top .em-btn-addto, .button-show01 .products-grid .item .product-shop-top .quickshop-link-container, .category-products .item .product-shop-top .em-btn-addto, .category-products .item .product-shop-top .quickshop-link-container, .products-grid .item .product-shop-top .em-btn-addto, .products-grid .item .product-shop-top .quickshop-link-container {
	display:inline-block;
	vertical-align:top
}
.button-show01 .products-grid .item:hover .bottom, .category-products .item:hover .bottom, .products-grid .item:hover .bottom {
	bottom:50%;
	opacity:1;
	filter:alpha(opacity=100)
}
.button-show02 .products-grid .item .add-to-links {
	width:66.6667%;
	float:left
}
.button-show02 .products-grid .item .add-to-links li {
	width:50%;
	float:left;
	margin:0
}
.button-show02 .products-grid .item .add-to-links li a {
	width:100%;
	text-align:center;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	border:0;
	padding:10px 7px
}
.button-show02 .products-grid .item .em-btn-addto {
	width:75%;
	float:left
}
.button-show02 .products-grid .item button.button.btn-cart {
	font-size:0;
	width:33.3333%;
	float:left;
	border:0;
	margin:0;
	text-align:center;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	padding:10px 7px
}
.button-show02 .products-grid .item button.button.btn-cart span {
	padding:0
}
.button-show02 .products-grid .item .quickshop-link-container {
	width:25%;
	float:left
}
.button-show02 .products-grid .item .quickshop-link-container a {
	width:100%;
	border:0;
	text-align:center;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	padding:10px 7px
}
.button-hide-text button.button.btn-cart span, .button-show03 .products-grid .item button.button.btn-cart span {
	padding:0
}
.button-show02 .products-grid .item .em-element-display-hover {
transition:all .5s ease;
	backface-visibility:hidden;
	opacity:0;
	filter:alpha(opacity=0)
}
.button-show02 .products-grid .item .product-shop-top {
	position:relative;
	border:1px solid transparent;
	overflow:hidden
}
.button-show02 .products-grid .item .product-shop-top .bottom {
	position:absolute;
	bottom:-50px;
	left:0;
	width:100%;
	text-align:center
}
.button-show02 .products-grid .item .product-shop-top .bottom .em-element-cotent {
	display:inline-block;
	width:100%;
	vertical-align:top
}
.button-show02 .products-grid .item:hover .product-shop-top .bottom {
	bottom:18px;
	opacity:1;
	filter:alpha(opacity=100)
}
.button-show03 .products-grid .item .add-to-links {
	width:100%;
	float:left
}
.button-show03 .products-grid .item .add-to-links li {
	width:100%;
	float:left;
	margin-right:0
}
.button-show03 .products-grid .item .add-to-links li a {
	width:100%;
	text-align:center;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	border:0
}
.button-show03 .products-grid .item .em-btn-addto {
	width:100%;
	float:left
}
.button-show03 .products-grid .item button.button.btn-cart {
	font-size:0;
	width:100%;
	float:left;
	border:0;
	margin:0;
	text-align:center;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.button-show03 .products-grid .item .quickshop-link-container {
	width:100%;
	float:left
}
.button-show03 .products-grid .item .quickshop-link-container a {
	width:100%;
	border:0;
	text-align:center;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.button-show03 .products-grid .item .bottom {
transition:all .5s ease;
	backface-visibility:hidden;
	opacity:0;
	filter:alpha(opacity=0)
}
.button-show03 .products-grid .item .product-shop-top {
	position:relative;
	border:1px solid transparent;
	overflow:hidden
}
.button-show03 .products-grid .item .product-shop-top .bottom {
	position:absolute;
	top:10px;
	left:-100px;
	width:30px;
	text-align:center
}
.availability-only, .availability-only a, .em-product-view-secondary, .email-friend {
	position:relative
}
.button-show03 .products-grid .item .product-shop-top .bottom .em-element-cotent {
	display:inline-block;
	width:100%;
	vertical-align:top
}
.button-show03 .products-grid .item:hover .product-shop-top .bottom {
	opacity:1;
	filter:alpha(opacity=100);
	left:10px
}
.button-hide-text button.button.btn-cart {
	font-size:0
}
.brand-logo-detail a {
	display:inline-block;
	max-width:100%
}
.email-friend a:before {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	content:"\f003";
	font-size:90%;
	margin-right:5px
}
.availability-only {
	text-transform:uppercase;
	font-size:100%
}
.availability-only a.expanded:after, .availability-only a:after {
	font-size:14px;
	padding-left:5px;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	display:inline-block
}
.availability-only span {
	position:relative;
	padding-right:100px
}
.availability-only a:after {
	content:"\f13a"
}
.availability-only a.expanded:after {
	content:"\f139"
}
.availability-only span:after, .availability-only span:before {
	height:8px;
	content:"";
	position:absolute;
	top:5px
}
.availability-only span:before {
	width:80px;
	border:1px solid #000;
	right:0
}
.availability-only span:after {
	width:35px;
	background:#000;
	right:45px
}
.product-view-detail select {
	width:100%
}
.product-view-detail .product-img-box {
	margin-bottom:50px
}
.product-view-detail .product-img-box .media-left {
	position:relative;
	z-index:2
}
.product-view-detail .product-img-box .media-left .product-image {
	max-width:100%
}
.product-view-detail .product-img-box .media-left .product-image img {
	max-width:100%;
	border:1px solid #f7f7f7;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.product-view-detail .product-img-box .more-views {
	position:relative;
	display:inline-block;
	width:100%;
	margin-top:10px
}
.product-view-detail .product-img-box .more-views .owl-carousel .owl-controls .owl-buttons div.owl-next {
	top:37%;
	right:10px
}
.product-view-detail .product-img-box .more-views .owl-carousel .owl-controls .owl-buttons div.owl-prev {
	top:37%;
	left:10px
}
.product-view-detail .product-img-box .more-views li.item:hover {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.product-view-detail .product-img-box .more-views .owl-wrapper-outer:hover li.item {
	opacity:.5;
	filter:alpha(opacity=50)
}
.product-view-detail .product-img-box .more-views .owl-wrapper-outer:hover li.item:hover {
	opacity:1;
	filter:alpha(opacity=100)
}
.product-view-detail .product-img-box .more-views img {
	width:100%;
	height:auto;
	border:1px solid #f7f7f7;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.product-view-detail .product-img-box .more-views .em-moreviews-noslider li.item {
	float:left;
	margin:0 10px 10px 0
}
.product-view-detail .product-shop .short-description h2 {
margin-bottom:.2rem;
	text-transform:uppercase
}
.product-view-detail .product-shop .short-description .std {
	margin-bottom:2rem
}
.product-view-detail .product-shop .no-rating {
	position:relative
}
.product-view-detail .product-shop .no-rating a:before {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	content:"\f044";
	font-size:16px;
	margin-right:5px
}
.product-view-detail .product-shop .em-review-email {
	display:inline-block;
	width:100%;
	font-size:85%
}
.product-view-detail .product-shop .add-to-cart label, .product-view-detail .product-shop .product-options-bottom .price-box {
	display:none
}
.product-view-detail .product-shop .em-review-email .no-rating, .product-view-detail .product-shop .em-review-email .ratings {
	float:left;
	padding:0;
margin-bottom:.25rem
}
.product-view-detail .product-shop .em-review-email .email-friend {
	float:right;
	margin-bottom:1.55rem
}
.product-view-detail .product-shop .em-addthis-plug, .product-view-detail .product-shop .product-options {
	margin-bottom:2rem
}
.product-view-detail .product-shop .em-addthis-plug span {
	float:left;
	margin-right:1rem;
	margin-top:1px
}
.product-view-detail .product-shop .price-box {
	margin-bottom:1.7rem
}
.product-view-detail .product-shop .price-box-bundle .price-box {
	font-size:100%;
	margin-bottom:0
}
.product-view-detail .product-shop .price-box-bundle .price-box .price {
	font-size:120%
}
.product-view-detail .product-shop .grouped-items-table .price-box {
	border:0;
	padding:0;
	margin:0;
	font-size:100%
}
.product-view-detail .product-shop .grouped-items-table .price-box .regular-price {
	font-size:100%!important
}
.product-view-detail .product-shop .grouped-items-table .price {
	margin:0
}
.product-view-detail .product-shop .ratings {
	float:left
}
.product-view-detail .product-shop .ratings .rating-box {
	float:left;
	margin-top:2px;
	margin-right:5px
}
.product-view-detail .product-shop .ratings .rating-links {
	float:left;
	margin-bottom:0
}
.product-view-detail .product-shop .em-sku-availability .availability {
	display:block
}
.product-view-detail .product-shop .availability {
	margin-bottom:2rem;
	display:none
}
.product-view-detail .product-shop .sku {
margin-bottom:.25rem
}
.product-view-detail .product-shop .sale_off {
	position:static;
	display:inline-block;
	margin-bottom:10px
}
.product-view-detail .product-shop .show_details {
	margin-top:23px;
	margin-bottom:20px
}
.product-view-detail .product-shop .show_details .title {
	text-align:center
}
.product-view-detail .product-shop .show_details .show_clock {
	padding-bottom:20px
}
.product-view-detail .product-shop .show_details .em-multideal-price-box {
	text-align:center
}
.product-view-detail .product-shop .show_details .em-multideal-price-box span.price {
	font-size:150%!important
}
.product-view-detail .product-shop .show_details .price-box.map-info a {
	font-size:80%;
	padding-bottom:10px
}
.product-view-detail .product-shop .show_details .price-box {
	padding:0;
	margin:0;
	border:none;
	text-align:center
}
.product-view-detail .product-shop .show_details .show_clock .clock {
	text-align:center;
	padding-top:15px
}
.product-view-detail .product-shop .deal_qty {
	text-align:left
}
.product-view-detail .product-shop .add-to-cart {
padding-top:.3rem;
	margin-bottom:1rem
}
.product-view-detail .product-shop .add-to-cart .button_addto {
	display:inline-block;
	width:100%;
padding-top:.3rem
}
.product-view-detail .qty_cart {
	clear:both;
	display:inline-block;
	float:left;
	margin-right:2%;
	width:49%;
	position:relative
}
.product-view-detail .qty_cart button {
	position:absolute;
	top:0
}
.product-view-detail .qty_cart button.decrease {
	left:0
}
.product-view-detail .qty_cart button.increase {
	right:0
}
.product-view-detail .qty_cart input.qty {
	width:100%!important;
	margin:0
}
.product-view-detail button.button {
	width:49%;
	margin-right:2%;
	text-align:center
}
.product-view-detail button.button.btn-cart {
	margin-right:0;
	float:right
}
.product-view-detail button.button.btn-cart:before {
	font-size:100%
}
.product-view-detail .add-to-links {
	width:49%
}
.product-view-detail .add-to-links li {
	margin-right:4%
}
.product-view-detail .add-to-links li a.link-compare, .product-view-detail .add-to-links li a.link-wishlist {
	padding:10px 9px;
	width:38px
}
.product-view-detail .add-to-links li a.link-compare:before, .product-view-detail .add-to-links li a.link-wishlist:before {
	font-size:18px
}
.product-view .em-details-tabs.r-tabs {
	margin-bottom:50px
}
.product-view .em-details-tabs.r-tabs .box-collateral {
	border:none;
	padding:0
}
.product-view .em-details-tabs.r-tabs .box-collateral .em-block-title {
	display:none
}
.product-view .em-details-tabs.r-tabs .box, .product-view .em-details-tabs.r-tabs .box-collateral-content {
	margin-bottom:0;
	border-width:0;
	padding:0
}
.product-view .em-details-tabs .em-details-tabs-content {
	text-align:left
}
.product-view .box {
	margin-bottom:50px
}
.product-view .data-table th, .product-view .data-table tr td {
	padding:10px
}
.add-to-cart label {
	padding:15px 0 6px;
	margin-bottom:0;
	margin-right:8px;
	float:left;
	width:100%
}
.form-add .form-list input.input-text, .form-add .form-list textarea {
	width:100%
}
.box-tags label {
	float:left
}
.box-tags .form-add .input-box {
	clear:both;
	width:50%;
	float:left;
	margin-right:10px;
	margin-bottom:10px
}
.box-tags .form-add .input-box input.input-text {
	width:100%
}
.box-tags button.button {
	margin-bottom:6px;
	float:left;
	padding:11px 20px
}
.product-tags li {
	display:inline-block;
	margin-right:15px;
	text-transform:uppercase;
	font-size:90%
}
.product-view .dc-toggle {
	position:fixed;
	z-index:100;
	top:189px;
	right:25px;
	overflow:hidden;
	float:left;
	font-size:0;
	color:#ed9c10;
	width:18px
}
.product-view .dc-toggle.icon_hide_social:before, .product-view .dc-toggle:before {
	font-size:20px;
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
.product-view .dc-toggle:before {
	content:"\f146"
}
.product-view .dc-toggle.icon_hide_social:before {
	content:"\f0fe"
}
.dcssb-float {
	z-index:99!important
}
.product-img-box #zoom-btn {
	font-size:0;
	content:"";
	position:absolute;
	bottom:20px;
	right:20px;
	z-index:9999
}
.product-img-box #zoom-btn:before {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	content:"\f00e";
	font-size:23px
}
.quickshop-main .product-img-box #zoom-btn {
	display:none
}
#product-review-table {
	width:69%
}
#product-review-table tr td {
	border:none;
	text-align:center;
	vertical-align:top;
	padding-top:6px;
	padding-bottom:8px
}
#product-review-table tr th {
	border:none;
	background:0 0;
	text-transform:none;
	padding-top:8px;
	padding-bottom:8px;
	text-align:center
}
.box-reviews .form-add h3, .box-reviews .reviews .em-box-review dt a {
	text-transform:capitalize
}
#product-review-table tr input.radio {
	display:inline-block;
	min-height:1px;
	margin:0
}
#product-review-table tbody tr th {
	padding-left:0;
	text-align:left
}
.ratings-table {
	margin-bottom:10px
}
.ratings-table th {
	padding:5px 5px 3px 0
}
.ratings-table td {
	padding:9px 5px 3px
}
.box-reviews .reviews {
	margin-bottom:50px
}
.box-reviews .reviews .em-box-review {
	margin:0;
	overflow:hidden
}
.box-reviews .reviews .em-box-review dl {
	margin-bottom:0
}
.box-reviews .reviews .em-box-review dt {
	font-weight:500
}
.box-reviews .reviews .em-box-review dd {
	padding-bottom:20px
}
.box-reviews .reviews .em-box-review table td {
	padding:9px 30px 3px 0
}
.box-reviews .reviews .em-box-review table td p {
	margin-bottom:10px
}
.box-reviews .reviews .em-box-review table td.em-review-nickname {
	white-space:nowrap
}
.box-reviews .reviews .em-box-review .ratings-table th {
	padding:0 5px 8px 0
}
.box-reviews .reviews .em-box-review .ratings-table td {
	padding:4px 5px 8px 0
}
.box-reviews .reviews .pager .amount {
	margin:10px 0
}
.box-reviews #review-form {
	display:block;
	margin:0
}
.product-view-detail .em-product-shop .product-shop .next:before, .product-view-detail .em-product-shop .product-shop .prev:before {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	-moz-backface-visibility:hidden;
	font-weight:400
}
.box-reviews .form-add {
	margin-bottom:50px
}
.box-reviews .form-add h3 {
	font-size:100%
}
.box-reviews .form-add h4 {
	margin-top:7px;
	margin-bottom:4px
}
.box-reviews .form-add .form-list li {
	margin-right:0
}
.box-reviews .form-add .buttons-set {
	margin-top:18px
}
.product-view-detail .em-product-shop .product-shop .next {
	padding:7px!important;
	position:absolute;
	font-size:0;
	top:20px;
	width:35px;
	border:1px solid #e8e8e8;
	right:20px;
	-moz-border-radius:0 5px 5px 0;
	-webkit-border-radius:0 5px 5px 0;
	border-radius:0 5px 5px 0
}
.product-view-detail .em-product-shop .product-shop .next:before {
	content:"\f054";
	font-size:19px;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	margin-left:1px
}
.em-variation .em-variation-btn, .product-view-detail .em-product-shop .product-shop .prev:before {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-webkit-backface-visibility:hidden
}
.product-view-detail .em-product-shop .product-shop .prev {
	padding:7px!important;
	position:absolute;
	font-size:0;
	top:20px;
	width:35px;
	border:1px solid #e8e8e8;
	right:54px;
	-moz-border-radius:5px 0 0 5px;
	-webkit-border-radius:5px 0 0 5px;
	border-radius:5px 0 0 5px
}
.product-view-detail .em-product-shop .product-shop .prev:before {
	content:"\f053";
	font-size:19px;
transition:all .5s ease;
	backface-visibility:hidden;
	margin-left:3px
}
.block-related .block-subtitle {
	display:none
}
.block-related .em-line-01 .em-block-title {
	padding-right:50px
}
.block-related .item {
	position:relative
}
.block-related .item .checkbox {
	position:absolute;
	left:10px;
	top:10px
}
.block-related .item .link-wishlist {
	display:inline-block
}
.block-related .em-related-noslider .item .product-image {
	float:left
}
.block-related .em-related-noslider .item .product-shop {
	position:initial
}
.block-related .em-related-noslider .item .checkbox {
	right:10px;
	top:10px
}
.block-related .em-related-noslider .item a.link-wishlist {
	position:relative;
	z-index:1;
	display:inline-block
}
.product-shop-fixed-top.em-has-options .em-addthis-plug, .product-shop-fixed-top.em-has-options .em-review-email, .product-shop-fixed-top.em-has-options .em-sku-availability, .product-shop-fixed-top.em-has-options .short-description, .product-view.quickshop .em-details-tabs.product-collateral, .product-view.quickshop .em-product-view-secondary:before {
	display:none
}
.box-up-sell .em-upsell-noslider li.item {
	float:left
}
.product-view.quickshop {
	padding:15px
}
.product-view.quickshop .product-view-detail button.button {
	font-size:115%;
margin-right:.8rem;
	width:42%
}
.send-friend .buttons-set p.limit {
	float:left;
	margin-top:6px
}
.review-product-list .em-product-view .buttons-set {
	float:left
}
.one-column .product-view .em-product-view-primary {
	padding-right:4rem
}
.one-column .product-view .em-product-view-secondary {
	padding-left:0
}
.product-shop-fixed-top, .product-shop-fixed-top .add-to-cart {
	position:relative
}
.product-shop-fixed-top.fix_menu {
	padding-top:70px
}
.block-account .block-content {
	padding-top:11px;
	padding-bottom:29px
}
.block-account .block-content ul {
	margin-bottom:0
}
.block-account .block-content ul li {
	position:relative;
	padding:7px 0 6px
}
.block-account .block-content ul li a, .block-account .block-content ul li strong {
	display:inline-block;
	font-weight:400
}
.dashboard .welcome-msg {
	padding:2rem;
	border:1px solid #e8e8e8;
	margin-bottom:2rem
}
.dashboard .welcome-msg p.hello {
	margin:0 0 5px
}
.dashboard .welcome-msg p.hello strong {
	font-weight:400
}
.dashboard .welcome-msg p {
	margin:0
}
.dashboard .box-account {
	clear:both;
	padding:10px 0 0
}
.box-account-content {
	padding:2rem;
	overflow:hidden;
	border:1px solid #e8e8e8;
	margin-bottom:2rem
}
.my-account .page-title {
	margin-bottom:0
}
.my-account .data-table {
	margin-bottom:20px
}
.my-account .data-table#my-orders-table tr td {
	padding:20px
}
.my-account .data-table#my-orders-table tr td .price {
	display:block
}
.my-account .box {
	border:none;
	padding:0
}
.my-account .box address {
	margin-bottom:17px;
	clear:both;
	width:100%
}
.my-account .col2-set:first-child {
	margin-bottom:8px
}
.my-account .box-content h3, .my-account .box-content h4, .my-account .box-title h3, .my-account .box-title h4 {
	margin:8px 20px 8px 0;
	float:left;
	min-width:250px
}
.my-account .box-content h3.address-book, .my-account .box-title h3.address-book {
	margin-bottom:15px
}
.my-account .box-content a.link-edit, .my-account .box-title a.link-edit {
	float:left;
	margin:8px 0
}
.my-account .box-content p {
	margin-bottom:16px
}
.my-account .box-content p.change-pass {
	text-transform:uppercase
}
.dashboard .box-reviews .number, .dashboard .box-tags .number {
	float:left;
	margin:0 -20px 0 0
}
.dashboard .box-reviews .details, .dashboard .box-tags .details {
	margin-left:20px
}
.dashboard .box-reviews li.item, .dashboard .box-tags li.item {
	padding:7px 0 10px
}
.dashboard .box-reviews .ratings {
	display:inline-block;
	clear:both;
	margin-top:0
}
.dashboard .box-reviews .ratings strong {
	display:none
}
.dashboard .box-tags li.item strong {
	font-weight:400;
	float:left;
	display:inline
}
.dashboard .box-tags li.item li {
	float:left;
	display:inline;
	padding-left:5px
}
.customer-account-edit .my-account .fieldset {
	width:100%
}
.customer-account-edit .my-account .form-list li.control {
	margin:15px 0 18px
}
.customer-account-edit .my-account .buttons-set p.required {
	float:left;
	clear:none;
	margin-top:6px
}
.customer-address-index .my-account .em-box-02 {
	display:none
}
.customer-address-index .my-account .box-account-content {
	padding-bottom:9px
}
.customer-address-index .my-account .col2-set .col-1, .customer-address-index .my-account .col2-set .col-2 {
	padding-top:7px
}
.customer-address-index .my-account .col2-set .col-1 h2, .customer-address-index .my-account .col2-set .col-1 h3, .customer-address-index .my-account .col2-set .col-2 h2, .customer-address-index .my-account .col2-set .col-2 h3 {
	margin-bottom:7px
}
.customer-address-index .my-account .col2-set .col-1 address, .customer-address-index .my-account .col2-set .col-2 address {
	margin-bottom:8px
}
.customer-address-index .my-account .addresses-primary ol {
	margin-bottom:0;
	oveflow:hidden
}
.customer-address-index .my-account .addresses-primary ol li.item {
	display:block
}
.customer-address-index .my-account .addresses-primary ol li.item:first-child {
	padding-bottom:13px
}
.sales-order-history .my-account .box-account-content {
	padding:0;
	border:0;
	margin-bottom:0
}
.my-account .pager {
	margin:10px 0
}
.my-account .pager .amount {
	margin:7px 10px 0 0;
	font-weight:400
}
.my-account .pager .amount strong {
	font-weight:500
}
.my-account .pager .limiter {
	margin:0
}
.my-account .pager .limiter label {
	font-size:100%
}
.my-wishlist .data-table tr td {
	padding:20px
}
.my-wishlist textarea {
	width:90%;
	margin-top:5px
}
.my-wishlist .cart-cell {
	display:inline-block;
	margin-top:-8px
}
.my-wishlist .cart-cell .price-box {
	margin-bottom:10px
}
.my-wishlist .cart-cell button.button {
	margin-top:2px
}
.customer-address-form .fieldset {
	width:100%
}
.data-table a.product-image img {
	width:100px;
	height:100px;
	border:1px solid #EE4D5D;
	display:block;
	margin:0 auto
}
#wishlist-table .cart-cell .link-edit {
	float:left;
	clear:both;
	margin-top:10px
}
#wishlist-table .cart-cell input.qty {
	height:34px;
	line-height:34px
}
.review-customer-view .product-review .product-details, .review-product-view .product-review .product-details {
	margin-left:170px
}
.review-customer-view .product-review .product-img-box, .review-product-view .product-review .product-img-box {
	width:150px;
	margin-right:10px;
	float:left
}
.review-customer-view .buttons-set, .review-product-view .buttons-set {
	padding-top:15px
}
.account-login .page-title {
	margin-bottom:18px
}
.account-login .col2-set {
	padding:0
}
.account-login .form-list {
	width:90%
}
.account-login .registered-users .buttons-set button.button {
	margin-right:20px
}
.account-login .registered-users .buttons-set a {
	display:inline-block;
	margin-top:6px
}
.account-login .new-users .content {
	margin-bottom:32px
}
.account-login .registered-users .content {
	margin-bottom:20px; padding:

}
.account-create .page-title {
	margin-bottom:18px
}
.account-create .fieldset {
	width:100%;
	margin-bottom:25px
}
.page-popup.catalog-product-compare-index {
	padding:20px
}
.block-compare .product-name:after:after, .block-compare .product-name:after:before {
content:" ";
display:table
}
.block-compare .product-name:after:after {
clear:both
}
.link-print {
	position:relative;
	float:right;
	margin-top:-7px;
	padding:10px;
	border:2px solid;
	font-weight:700;
	text-transform:uppercase;
	font-size:85%
}
.em-variation .em-variation-btn:before, .link-print:before {
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
.link-print:before {
	display:inline-block;
	content:"\f02f";
	font-size:14px;
	margin-right:5px
}
.compare-table.data-table thead th {
	background:0 0
}
.compare-table.data-table th {
	padding:10px 20px
}
.compare-table .product-shop-row.top td {
	padding-bottom:0;
	border-bottom:0
}
.compare-table .product-shop-row.bottom td {
	padding-top:0
}
.compare-table a.product-image {
	text-align:center
}
.compare-table a.product-image img {
	width:auto
}
.compare-table .em-add-to-button p {
	float:left
}
.page-popup.catalog-product-compare-index .buttons-set {
	float:right
}
.page-popup.catalog-product-compare-index .buttons-set button.button {
	margin-top:20px;
	margin-right:0
}
.em-variation {
	position:fixed;
	right:0;
	top:0;
	z-index:9999;
	background-color:#fff;
	height:100%;
	-webkit-box-shadow:0 -2px 10px #a3a3a3;
	-moz-box-shadow:0 -2px 10px #a3a3a3;
	box-shadow:0 -2px 10px #a3a3a3
}
.em-variation .em-variation-btn {
	z-index:0;
	padding:10px 11px;
	color:#888;
	font-size:0;
	display:block;
	overflow:hidden;
	vertical-align:middle;
	text-align:left;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden;
	text-indent:0;
	position:absolute;
	left:-48px;
	top:20%;
	width:52px;
	height:52px;
	box-shadow:0 0 5px 0 rgba(0, 0, 0, .2);
	background:#fff;
	border-radius:3px 0 0 3px;
	cursor:pointer
}
.em-variation .em-variation-btn:before, .em-variation .reset-button {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-webkit-backface-visibility:hidden;
	display:inline-block
}
.em-variation .em-variation-btn:before {
	content:"\f013";
	font-size:33px;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-variation .reset-button {
	padding:5px;
	border:1px solid #000;
	background-color:#000;
	color:#fff;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden;
	text-indent:0;
	font-size:16px;
	height:38px;
	width:100%;
	text-align:center;
	text-transform:uppercase
}
.em-color, .em-variation .em-variation-wrapper {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.em-variation .reset-button:hover {
	background-color:#fff;
	color:#000;
	border-color:#000
}
.em-variation .em-variation-wrapper {
	width:350px;
	padding:1.5rem 2rem;
	background:#fff;
	position:relative;
transition:all .5s ease;
	backface-visibility:hidden;
	display:block!important;
	visibility:hidden
}
.em-variation .em-variation-wrapper.show {
	visibility:visible
}
.em-variation .em-variation-wrapper .wrapper-input-color {
	margin:0 30px 10px;
	overflow:hidden;
	text-align:left
}
.em-variation .em-variation-wrapper .wrapper-input-color .label {
	display:inline-block;
	margin-bottom:5px
}
.em-variation .em-variation-wrapper .wrapper-input-color .em-custom-color {
	overflow:hidden;
	border:1px solid #ccc;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	width:200px
}
.em-variation .em-variation-wrapper .wrapper-input-color .em-custom-color .input-lg {
	border:0;
	border-right:1px solid #ccc;
	height:35px;
	padding:5px;
	margin-bottom:0;
	font-size:14px;
	color:#000;
	width:103px;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	float:left
}
.em-variation .em-variation-wrapper .wrapper-input-color .em-custom-color a.select-color {
	padding:0 10px;
	height:35px;
	line-height:35px;
	width:95px;
	background:#f6f6f6;
	-moz-border-radius:0 3px;
	-webkit-border-radius:0 3px 0 3px;
	border-radius:0 3px;
	float:left
}
.em-variation .copyright {
	overflow:hidden;
	text-align:center;
	padding-top:10px
}
.em-wrapper-variation {
	display:inline-block;
	width:100%;
	clear:both;
	text-align:center;
	margin-bottom:1.5rem
}
.em-wrapper-variation h1.title-custom {
	font-size:14px;
	text-transform:uppercase;
	font-weight:700
}
.em-color span, .em-font-family span {
	font-size:0;
	overflow:hidden;
	vertical-align:middle;
	text-align:left;
	text-indent:-99999px
}
.em-wrapper-variation .color-picker {
	width:40px;
	height:40px;
	background-image:url(../images/icon_colorpicker.png);
	background-position:0 0;
	background-repeat:no-repeat;
	display:inline;
	margin-top:-5px;
	border:none;
	background-color:transparent!important;
	cursor:pointer
}
.em-color, .em-color span, .em-font-family, .em-font-family span {
	display:inline-block
}
.em-color {
	margin-right:1rem;
	margin-bottom:1rem;
	padding:2px;
transition:all .5s ease;
	backface-visibility:hidden;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%
}
.em-font-family, .em-language-currency .toolbar-switch span.current {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease
}
.em-color.selected, .em-color:hover {
	-webkit-box-shadow:0 0 5px #8e8e8e;
	-moz-box-shadow:0 0 5px #8e8e8e;
	box-shadow:0 0 5px #8e8e8e
}
.em-color span {
	width:36px;
	height:36px;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	background:#e72d04;
	position:relative
}
.em-color span.emColorStyle01 {
	background:#322923
}
.em-color span.emColorStyle02 {
	background:#313959
}
.em-color span.emColorStyle03 {
	background:#46674c
}
.em-color span.emColorStyle04 {
	background:#4b1f11
}
.em-font-family span.adventpro, .em-font-family span.architectsdaughter, .em-font-family span.arvo, .em-font-family span.dancingscript, .em-font-family span.lato, .em-font-family span.novasquare, .em-font-family span.opensans, .em-font-family span.raleway {
	height:40px;
	background-image:url(../images/icons.png);
	background-repeat:no-repeat
}
.em-font-family {
	border:1px solid #a7a7a7;
	padding:5px;
	min-width:95px;
	margin:3px 2px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	opacity:.5;
	filter:alpha(opacity=50);
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-font-family.selected, .em-font-family:hover {
	opacity:1;
	filter:alpha(opacity=100)
}
.em-font-family:hover {
	-webkit-box-shadow:0 0 5px #8e8e8e;
	-moz-box-shadow:0 0 5px #8e8e8e;
	box-shadow:0 0 5px #8e8e8e
}
.em-font-family span.architectsdaughter {
	width:70px;
	background-position:-4px -458px
}
.em-font-family span.arvo {
	width:36px;
	background-position:-89px -458px
}
.em-font-family span.dancingscript {
	width:78px;
	background-position:-146px -456px
}
.em-font-family span.lato {
	width:38px;
	background-position:-243px -457px
}
.em-font-family span.novasquare {
	width:83px;
	background-position:-298px -457px
}
.em-font-family span.opensans {
	width:72px;
	background-position:-402px -456px
}
.em-font-family span.adventpro {
	width:64px;
	background-position:-491px -457px
}
.em-font-family span.raleway {
	width:57px;
	background-position:-572px -457px
}
#em-variation-store {
	margin:2rem -2rem 0;
	background-color:#272727
}
#em-variation-store .title-custom {
	padding:10px 2rem;
	font-size:14px;
	background:#363636;
	color:#aaa;
	margin:0
}
#em-variation-store .em-wrapper-variation-store {
	overflow:auto
}
#em-variation-store .em-wrapper-variation-store .em-store-item {
	float:left;
	width:50%;
	padding:1px
}
#em-variation-store .em-wrapper-variation-store .em-store-item a.em-store-switch {
	display:block;
	position:relative
}
#em-variation-store .em-wrapper-variation-store .em-store-item a.em-store-switch:before {
	width:100%;
	bottom:0;
	right:0;
	content:'';
	position:absolute;
	z-index:1;
	height:0;
-moz-transition:all .2s ease;
-webkit-transition:all .2s ease;
-o-transition:all .2s ease;
transition:all .2s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	background:rgba(0, 0, 0, .8)
}
#loader::after, #loader::before {
content:""
}
#em-variation-store .em-wrapper-variation-store .em-store-item a.em-store-switch .bkg-hover {
	position:absolute;
	top:35%;
	left:0;
	width:100%;
	opacity:0;
	filter:alpha(opacity=0);
	color:#dbdbdb;
	text-align:center;
	text-transform:uppercase;
	font-size:14px;
	font-weight:700;
	z-index:1
}
#em-variation-store .em-wrapper-variation-store .em-store-item img {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
#em-variation-store .em-wrapper-variation-store .em-store-item:hover a.em-store-switch:before {
	height:100%
}
#em-variation-store .em-wrapper-variation-store .em-store-item:hover a.em-store-switch .bkg-hover {
	opacity:1;
	filter:alpha(opacity=100)
}
.em-wrapper-variation-store::-webkit-scrollbar {
width:0
}
.em-wrapper-variation-store::-moz-scrollbar {
width:0
}
@-moz-document url-prefix(http://), url-prefix(https://) {
.em-wrapper-variation-store[orient=vertical], .em-wrapper-variation-store[orient=horizontal] {
width:0!important
}
}
.em-rtl .em-variation {
	left:0;
	right:auto
}
.em-rtl .em-variation .em-variation-btn {
	right:-48px;
	left:auto
}
#loader-wrapper {
	position:fixed;
	top:0;
	left:0;
	width:100%;
	height:100%;
	z-index:9999
}
#loader {
	z-index:1001;
	margin:-24px 0 0 -24px;
	height:49px;
	width:49px;
	-o-animation:cssload-rotate 1.15s linear infinite;
	-ms-animation:cssload-rotate 1.15s linear infinite;
	-webkit-animation:cssload-rotate 1.15s linear infinite;
	-moz-animation:cssload-rotate 1.15s linear infinite
}
.logo_loader {
	position:absolute;
	top:35%;
	left:40%;
	z-index:1001
}
#loader, #loader::after, #loader::before {
position:absolute;
top:50%;
left:50%;
border:1px solid #bfbfbf;
border-left-color:#f02222;
border-radius:974px;
-o-border-radius:974px;
-ms-border-radius:974px;
-webkit-border-radius:974px;
-moz-border-radius:974px
}
#loader::before {
margin:-22px 0 0 -22px;
height:43px;
width:43px;
animation:cssload-rotate 1.15s linear infinite;
-o-animation:cssload-rotate 1.15s linear infinite;
-ms-animation:cssload-rotate 1.15s linear infinite;
-webkit-animation:cssload-rotate 1.15s linear infinite;
-moz-animation:cssload-rotate 1.15s linear infinite
}
#loader::after {
margin:-28px 0 0 -28px;
height:55px;
width:55px;
animation:cssload-rotate 2.3s linear infinite;
-o-animation:cssload-rotate 2.3s linear infinite;
-ms-animation:cssload-rotate 2.3s linear infinite;
-webkit-animation:cssload-rotate 2.3s linear infinite;
-moz-animation:cssload-rotate 2.3s linear infinite
}
@keyframes cssload-rotate {
100% {
transform:rotate(360deg)
}
}
@-o-keyframes cssload-rotate {
100% {
-o-transform:rotate(360deg)
}
}
@-ms-keyframes cssload-rotate {
100% {
-ms-transform:rotate(360deg)
}
}
@-webkit-keyframes cssload-rotate {
100% {
-webkit-transform:rotate(360deg)
}
}
@-moz-keyframes cssload-rotate {
100% {
-moz-transform:rotate(360deg)
}
}
#loader-wrapper .loader-section {
	position:fixed;
	top:0;
	width:51%;
	height:100%;
	background:#f1f1f1;
	z-index:1000;
	-webkit-transform:translateX(0);
	-ms-transform:translateX(0);
	transform:translateX(0)
}
#loader-wrapper .loader-section.section-left {
	left:0
}
#loader-wrapper .loader-section.section-right {
	right:0
}
.loaded #loader-wrapper .loader-section.section-left, .loaded #loader-wrapper .loader-section.section-right {
	-webkit-transform:translateY(-100%);
	-ms-transform:translateY(-100%);
	transform:translateY(-100%);
-webkit-transition:all .7s .3s cubic-bezier(.645, .045, .355, 1);
transition:all .7s .3s cubic-bezier(.645, .045, .355, 1)
}
.loaded #loader, .loaded .logo_loader {
	top:-40%;
	-webkit-transition:all 1s ease;
	transition:all 1s ease
}
.em-wrapper-loading {
	bottom:-400px;
	position:relative
}
.loaded .em-wrapper-loading {
	bottom:0;
	-webkit-transition:all 2s ease;
	transition:all 2s ease
}
.loaded #loader-wrapper {
	visibility:hidden;
	-webkit-transform:translateY(-100%);
	-ms-transform:translateY(-100%);
	transform:translateY(-100%);
-webkit-transition:all .3s 1s ease-out;
transition:all .3s 1s ease-out
}
.no-js #loader-wrapper {
	display:none
}
.em-wrapper-header .toolbar-switch .toolbar-title {
	margin:0 -10px;
	padding:0 10px
}
.em-wrapper-header .toolbar-switch .toolbar-title select {
	margin:7px 10px;
	background:#fff;
	color:#a1a1a1
}
.em-language-currency {
	display:inline-block;
	vertical-align:top
}
.em-language-currency .block {
	border:none;
	margin:0;
	padding:0
}
.em-language-currency .toolbar-switch {
	float:left;
	margin:0
}
.em-language-currency .toolbar-switch span.current {
	padding:10px;
	border:0;
	position:relative;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	float:right
}
.em-language-currency .toolbar-switch:hover span.current {
	z-index:2
}
.em-language-currency .toolbar-dropdown {
	width:auto
}
.em-language-currency .toolbar-dropdown ul {
	top:50px;
	right:auto;
	left:0;
	width:130px;
	border:none;
	box-shadow:0 1px 2px #ddd;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-language-currency .toolbar-dropdown li {
	padding:5px;
	background-color:#fff;
	float:none;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-language-currency .block-currency li {
	padding-top:2px
}
.em-language-currency .block-currency, .em-language-currency .form-language {
	float:left
}
.em-language-currency .block-currency ul, .em-language-currency .form-language ul {
	float:left;
	margin-bottom:0
}
.em-language-currency .block-currency li, .em-language-currency .form-language li {
	float:left;
	padding-bottom:0;
	margin-right:10px
}
.em-language-currency .block-currency .toolbar-dropdown li, .em-language-currency .form-language .toolbar-dropdown li {
	padding-bottom:5px;
	margin-right:0;
	width:100%
}
.em-language-currency .block-currency span.current, .em-language-currency .form-language span.current {
	background-position:20px center
}
.em-language-currency .block-currency li a, .em-language-currency .form-language li a, .em-language-currency .toolbar-dropdown li a {
	display:block;
	text-align:right
}
.em-account, .em-header-style27 .em_nav .hnav .menu-item-depth-0 ul, .em-header-style27 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0 ul, .em-header-style27 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>.menu-container, .em-header-style27 .em_nav .hnav .menu-item-depth-0>.menu-container, .em-language-currency .em-currency-style01 .toolbar-dropdown ul li a, .em-language-currency .em-language-style01 .toolbar-dropdown ul li a {
	text-align:left
}
.em-language-currency .block-currency .toolbar-dropdown ul {
	width:60px
}
.em-language-currency .form-language.toolbar-switch span.current {
	padding-left:45px
}
.em-language-currency .form-language .toolbar-dropdown ul li a {
	background-position:0 4px;
	padding-left:25px
}
.em-language-currency .em-currency-style01 .toolbar-dropdown ul li, .em-language-currency .em-language-style01 .toolbar-dropdown ul li {
	padding:5px 20px
}
.account-link {
	position:relative;
	padding-bottom:20px;
	margin-bottom:-20px
}
.account-link a.link-account, .account-link a.link-login, .account-link a.link-logout {
	display:inline-block;
	clear:none;
	position:relative
}
.account-link a.link-logout:before {
	content:'\f05e'
}
.em-account {
	position:absolute;
	top:50px;
	right:0;
	z-index:20;
	padding-top:0
}
.em-account .block-content {
	width:260px;
	padding:17px 20px 9px;
	background:#fff;
	border:1px solid #f6f6f6;
	-webkit-box-shadow:0 0 5px #f6f6f6;
	-moz-box-shadow:0 0 5px #f6f6f6;
	box-shadow:0 0 5px #f6f6f6;
	overflow:hidden
}
.em-account .block-content .login-title {
	text-transform:uppercase;
	margin-bottom:3px;
	display:none
}
.em-account .block-content .login-desc {
	display:none
}
.em-account .block-content .action-forgot .actions {
	float:left;
	margin-top:2rem;
	clear:both
}
.em-account .block-content .action-forgot .login_forgotpassword {
	float:left;
	width:100%;
	margin-top:2rem
}
.em-account .block-content .action-forgot .login_forgotpassword a.create-account-link-wishlist {
	margin-left:5px
}
.em-account .block-content .action-forgot .login_forgotpassword p {
	margin-bottom:5px
}
.em-account .form-list li {
	width:100%;
	margin:0 0 4px!important;
	padding:0!important
}
.em-account .form-list li:after, .em-account .form-list li:before {
	display:none!important
}
.em-account .form-list p.required {
	font-size:85%;
	padding:0;
	margin:0
}
.em-header-style27 .em-account-link:before, .em-header-style27 .em-links-wishlist a:before, .em-header-style27 .em-logout-link:before, .em-header-style27 .em-register-link:before, .em-header-style27 .link-account:before, .em-header-style27 .top-link-deal:before {
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	font-size:100%;
	margin-right:10px;
	display:inline-block
}
.pull-right .account-link .em-account {
	right:0;
	left:auto
}
.pull-right .account-link .em-account .form-list {
	float:none
}
.fancybox-outer .em-account {
	position:static
}
.em-header-style27 .em-account-link, .em-header-style27 .em-links-wishlist a, .em-header-style27 .em-logout-link, .em-header-style27 .em-register-link, .em-header-style27 .link-account, .em-header-style27 .top-link-deal {
	position:relative
}
.fancybox-outer .em-account .block-content {
	width:400px;
	padding:40px;
	border:0;
	-webkit-box-shadow:none;
	-moz-box-shadow:none;
	box-shadow:none
}
.em-header-style14 .em-search .em-search-icon, .em-header-style27 .em-search .em-search-icon {
	border-top-color:transparent;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-webkit-backface-visibility:hidden
}
.em-header-style27 .toolbar-switch .toolbar-title {
	padding:0!important
}
.em-header-style27 .em-logout-link:before, .em-header-style27 .link-account:before {
	content:"\f13e"
}
.em-header-style27 .em-account-link:before, .em-header-style27 .em-register-link:before {
	content:"\f084"
}
.em-header-style27 .em-links-wishlist a:before {
	content:"\f004"
}
.em-header-style27 .top-link-deal:before {
	content:"\f02c"
}
.em-header-style27 .em-top-cart .em-amount-topcart:after, .em-header-style27 .toolbar-switch:before {
	content:""
}
.em-header-style27 .account-link, .em-header-style27 .em-top-links ul.em-links-wishlist li, .em-header-style27 .em-top-links ul.links li, .em-header-style27 .em-top-links>ul li, .em-header-style27 .welcome-msg {
	padding:14px 0 15px;
	margin-left:30px;
	display:inline-block;
	vertical-align:middle;
	position:relative;
	overflow:hidden;
	overflow:initial
}
.em-header-style27 .account-link:before, .em-header-style27 .em-top-links ul.em-links-wishlist li:before, .em-header-style27 .em-top-links ul.links li:before, .em-header-style27 .em-top-links>ul li:before, .em-header-style27 .welcome-msg:before {
	content:"";
	position:absolute;
	left:50%;
	right:50%;
	z-index:1;
	background:#2098d1;
	height:4px;
	-webkit-transition-property:left, right;
	transition-property:left, right;
-webkit-transition-duration:.3s;
transition-duration:.3s;
	-webkit-transition-timing-function:ease-out;
	transition-timing-function:ease-out;
	opacity:0;
	filter:alpha(opacity=0);
	bottom:auto;
	top:0
}
.em-header-style27 .account-link:hover:before, .em-header-style27 .em-top-links ul.em-links-wishlist li:hover:before, .em-header-style27 .em-top-links ul.links li:hover:before, .em-header-style27 .em-top-links>ul li:hover:before, .em-header-style27 .welcome-msg:hover:before {
	left:0;
	right:0;
	opacity:1;
	filter:alpha(opacity=100)
}
.em-header-style27 .em-language-currency {
	margin:0 -15px
}
.em-header-style27 .toolbar-switch {
	margin:0 15px;
	vertical-align:middle;
	position:relative;
	overflow:hidden;
	overflow:initial
}
.em-header-style27 .toolbar-switch:before {
	position:absolute;
	left:50%;
	right:50%;
	z-index:1;
	background:#2098d1;
	height:4px;
	-webkit-transition-property:left, right;
	transition-property:left, right;
-webkit-transition-duration:.3s;
transition-duration:.3s;
	-webkit-transition-timing-function:ease-out;
	transition-timing-function:ease-out;
	opacity:0;
	filter:alpha(opacity=0);
	bottom:auto;
	top:0
}
.em-header-style27 .toolbar-switch:hover:before {
	left:0;
	right:0;
	opacity:1;
	filter:alpha(opacity=100)
}
.em-header-style27 .toolbar-switch span.current {
	padding:14px 0 15px;
	min-height:49px
}
.em-header-style27 .em-language-currency .form-language.toolbar-switch span.current {
	padding-left:25px;
	background-position:0 center
}
.em-header-style27 .em-logo .logo {
	margin:20px 0
}
.em-header-style27 .em-phone {
	margin:40px 0 0;
	font-size:110%
}
.em-header-style27 .em-phone span {
	margin:0 10px
}
.em-header-style27 .em-menu-hoz, .em-header-style27 .em-search {
	display:inline-block;
	vertical-align:top
}
.em-header-style27 .em-search .em-search-icon {
	display:block;
	padding:17px 20px;
	border-top-width:2px;
	border-top-style:solid;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-header-style27 .em-search .em-search-icon span:before {
	font-size:1.4rem
}
.em-header-style27 .line-top {
	border-top-style:solid;
	border-top-width:1px
}
.em-header-style27 .em_nav .hnav .menu-item-depth-0, .em-header-style27 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0 {
	margin:0
}
.em-header-style27 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>a span, .em-header-style27 .em_nav .hnav .menu-item-depth-0>a span {
	padding:15px 20px;
	border-top-width:2px;
	text-transform:uppercase
}
.em-header-style27 .em-top-cart {
	margin-top:25px
}
.em-header-style27 .em-top-cart .em-amount-topcart {
	display:inline-block;
	padding-bottom:30px;
	margin-bottom:-30px;
	width:50px
}
.em-header-style27 .em-top-cart .em-amount-topcart:before {
	content:"\e835";
	font-family:maki, sans-serif;
	width:50px;
	height:50px;
	font-size:1.8rem;
	text-align:center;
	display:inline-block;
	padding:15px 5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em-header-style27 .em-fixed-top.navbar-fixed-top .em-search, .em-header-style27 .em-top-cart .em-amount-topcart .em-topcart-text {
	display:none
}
.em-header-style27 .em-top-cart .em-amount-topcart .em-topcart-qty {
	position:absolute;
	left:33%;
	top:-8px;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:0 6px;
	font-size:12px
}
.em-rtl .em-header-style27 .account-link, .em-rtl .em-header-style27 .em-top-links ul.em-links-wishlist li, .em-rtl .em-header-style27 .em-top-links ul.links li, .em-rtl .em-header-style27 .em-top-links>ul li, .em-rtl .em-header-style27 .welcome-msg {
	margin-left:0;
	margin-right:30px
}
.em-rtl .em-header-style27 .em-account-link:before, .em-rtl .em-header-style27 .em-links-wishlist a:before, .em-rtl .em-header-style27 .em-logout-link:before, .em-rtl .em-header-style27 .em-register-link:before, .em-rtl .em-header-style27 .link-account:before {
	margin-right:0;
	margin-left:10px
}
.em-header-style08 .em-account-link:before, .em-header-style08 .em-links-wishlist a:before, .em-header-style08 .em-logout-link:before, .em-header-style08 .em-register-link:before, .em-header-style08 .link-account:before, .em-header-style08 .top-link-deal:before {
	font-style:normal;
	font-weight:400;
	font-size:100%;
	margin-right:10px;
	display:inline-block;
	font-family:FontAwesome;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
.em-rtl .em-header-style27 .em_nav .hnav .menu-item-depth-0 ul, .em-rtl .em-header-style27 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0 ul, .em-rtl .em-header-style27 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>.menu-container, .em-rtl .em-header-style27 .em_nav .hnav .menu-item-depth-0>.menu-container {
	text-align:right
}
.em-rtl .em-header-style27 .em-top-cart .em-amount-topcart .em-topcart-qty {
	left:25%
}
.em-header-style08 .em-header-top {
	border-bottom-style:solid;
	border-bottom-width:1px;
	border-top-style:solid;
	border-top-width:3px
}
.em-header-style08 .em-header-bottom {
	position:relative;
	background-color:rgba(0, 0, 0, .58)
}
.em-header-style08 .em-header-bottom:before {
	content:"";
	position:absolute;
	left:0;
	bottom:0;
	right:0;
	top:auto;
	height:4px
}
.em-header-style08 .em-account-link, .em-header-style08 .em-links-wishlist a, .em-header-style08 .em-logout-link, .em-header-style08 .em-register-link, .em-header-style08 .link-account, .em-header-style08 .top-link-deal {
	position:relative
}
.em-header-style08 .em-logout-link:before, .em-header-style08 .link-account:before {
	content:"\f13e"
}
.em-header-style08 .em-account-link:before, .em-header-style08 .em-register-link:before {
	content:"\f084"
}
.em-header-style08 .em-links-wishlist a:before {
	content:"\f004"
}
.em-header-style08 .top-link-deal:before {
	content:"\f02c"
}
.em-header-style08 .em-top-links ul.em-links-wishlist li, .em-header-style08 .em-top-links ul.links li, .em-header-style08 .em-top-links>ul li, .em-header-style08 .link-account, .em-header-style08 .welcome-msg {
	padding:14px 7px;
	display:inline-block;
	border-style:solid;
	border-width:0 1px;
	margin-left:-1px;
	min-height:50px
}
.em-header-style08 .em-top-links ul.links li {
	margin-right:-1px
}
.em-header-style08 .toolbar-switch {
	border-style:solid;
	border-width:0 1px;
	margin-right:-1px
}
.em-header-style08 .toolbar-switch span.current {
	padding:14px 20px;
	min-height:50px
}
.em-header-style08 .em-search {
	display:inline-block;
	border-style:solid;
	border-width:0 1px;
	margin-left:-1px;
	position:relative
}
.em-header-style08 .em-search .em-search-icon {
	padding:18px 20px;
	display:block
}
.em-header-style08 .em-search .em-search-icon span:before {
	font-size:1.4rem
}
.em-header-style08 .em-search .em-search-style01 .em-container-js-search {
	top:51px
}
.em-header-style08 .em-logo .logo {
	margin:10px 0
}
.em-header-style08 .em-header-bottom .em_nav {
	padding-right:70px;
	padding-top:20px
}
.em-header-style08 .em-header-bottom .em_nav .hnav .menu-item-depth-0, .em-header-style08 .em-header-bottom .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0 {
	margin:0
}
.em-header-style08 .em-header-bottom .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>a.em-menu-link span, .em-header-style08 .em-header-bottom .em_nav .hnav .menu-item-depth-0>a.em-menu-link span {
	padding-bottom:31px;
	border-top-width:2px;
	text-transform:uppercase
}
.em-header-style08 .em-header-bottom .em_nav .hnav .menu-item-depth-0>.menu-container {
	top:66px
}
.em-header-style08 .em-header-bottom .em-top-cart {
	display:inline-block!important;
	position:absolute;
	right:10px;
	top:25px;
	margin:0
}
.em-header-style08 .em-header-bottom .em-top-cart .em-amount-topcart {
	display:inline-block;
	padding-bottom:30px;
	margin-bottom:-30px;
	width:50px
}
.em-header-style08 .em-header-bottom .em-top-cart .em-amount-topcart:after {
	content:""
}
.em-header-style08 .em-header-bottom .em-top-cart .em-amount-topcart:before {
	content:"\f07a";
	font-family:FontAwesome;
	width:50px;
	height:50px;
	font-size:1.6rem;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	text-align:center;
	display:inline-block;
	padding:19px 5px 0;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em-header-style08 .em-header-bottom .em-top-cart .em-amount-topcart .em-topcart-text {
	display:none
}
.em-header-style08 .em-header-bottom .em-top-cart .em-amount-topcart .em-topcart-qty {
	position:absolute;
	left:33%;
	top:-8px;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:0 6px;
	font-size:12px
}
.em-header-style08 .em-header-bottom .em-top-cart .topcart-popup {
	top:50px
}
.em-rtl .em-header-style08 .em-account-link:before, .em-rtl .em-header-style08 .em-links-wishlist a:before, .em-rtl .em-header-style08 .em-logout-link:before, .em-rtl .em-header-style08 .em-register-link:before, .em-rtl .em-header-style08 .link-account:before {
	margin-right:0;
	margin-left:10px
}
.em-header-style14 .em-account-link:before, .em-header-style14 .em-links-wishlist a:before, .em-header-style14 .em-logout-link:before, .em-header-style14 .em-register-link:before, .em-header-style14 .link-account:before, .em-header-style14 .top-link-deal:before {
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	font-size:100%;
	margin-right:10px;
	display:inline-block
}
.em-rtl .em-header-style08 .em-header-bottom .em_nav {
	padding-right:0;
	padding-left:70px
}
.em-rtl .em-header-style08 .em-header-bottom .em-top-cart {
	right:auto;
	left:10px
}
.em-rtl .em-header-style08 .em-header-bottom .em-top-cart .em-amount-topcart .em-topcart-qty {
	left:23%
}
.em-header-style14 .info-right {
	padding:23px 0
}
.em-header-style14 .em-header-bottom {
	position:relative
}
.em-header-style14 .em-header-bottom:before {
	content:"";
	position:absolute;
	left:0;
	bottom:0;
	right:0;
	top:auto;
	height:4px;
	box-shadow:0 3px 3px #CACACA;
	-moz-box-shadow:0 3px 3px #CACACA;
	-webkit-box-shadow:0 3px 3px #CACACA
}
.em-header-style14 .em-account-link, .em-header-style14 .em-links-wishlist a, .em-header-style14 .em-logout-link, .em-header-style14 .em-register-link, .em-header-style14 .link-account, .em-header-style14 .top-link-deal {
	position:relative
}
.em-header-style14 .em-logout-link:before, .em-header-style14 .link-account:before {
	content:"\f13e"
}
.em-header-style14 .em-account-link:before, .em-header-style14 .em-register-link:before {
	content:"\f084"
}
.em-header-style14 .em-links-wishlist a:before {
	content:"\f004"
}
.em-header-style14 .top-link-deal:before {
	content:"\f02c"
}
.em-header-style14 .account-link, .em-header-style14 .em-top-links ul.em-links-wishlist li, .em-header-style14 .em-top-links ul.links li, .em-header-style14 .em-top-links>ul li, .em-header-style14 .welcome-msg {
	padding:15px 20px;
	display:inline-block;
	vertical-align:middle;
	position:relative;
	overflow:hidden;
	overflow:initial
}
.em-header-style14 .account-link:before, .em-header-style14 .em-top-links ul.em-links-wishlist li:before, .em-header-style14 .em-top-links ul.links li:before, .em-header-style14 .em-top-links>ul li:before, .em-header-style14 .welcome-msg:before {
	content:"";
	position:absolute;
	left:50%;
	right:50%;
	z-index:1;
	background:#2098d1;
	height:4px;
	-webkit-transition-property:left, right;
	transition-property:left, right;
-webkit-transition-duration:.3s;
transition-duration:.3s;
	-webkit-transition-timing-function:ease-out;
	transition-timing-function:ease-out;
	opacity:0;
	filter:alpha(opacity=0);
	bottom:auto;
	top:0
}
.em-header-style14 .account-link:hover:before, .em-header-style14 .em-top-links ul.em-links-wishlist li:hover:before, .em-header-style14 .em-top-links ul.links li:hover:before, .em-header-style14 .em-top-links>ul li:hover:before, .em-header-style14 .welcome-msg:hover:before {
	left:0;
	right:0;
	opacity:1;
	filter:alpha(opacity=100)
}
.em-header-style14 .account-link:after, .em-header-style14 .em-top-links ul.em-links-wishlist li:after, .em-header-style14 .em-top-links ul.links li:after, .em-header-style14 .em-top-links>ul li:after, .em-header-style14 .welcome-msg:after {
	content:"|";
	position:absolute;
	left:0;
	top:auto
}
.em-header-style14 .account-link:after, .em-header-style14 .toolbar-switch:before {
	content:""
}
.em-header-style14 .toolbar-switch {
	vertical-align:middle;
	position:relative;
	overflow:hidden;
	overflow:initial
}
.em-header-style14 .toolbar-switch:before {
	position:absolute;
	left:50%;
	right:50%;
	z-index:1;
	background:#2098d1;
	height:4px;
	-webkit-transition-property:left, right;
	transition-property:left, right;
-webkit-transition-duration:.3s;
transition-duration:.3s;
	-webkit-transition-timing-function:ease-out;
	transition-timing-function:ease-out;
	opacity:0;
	filter:alpha(opacity=0);
	bottom:auto;
	top:0
}
.em-header-style14 .toolbar-switch:hover:before {
	left:0;
	right:0;
	opacity:1;
	filter:alpha(opacity=100)
}
.em-header-style14 .toolbar-switch:after {
	content:"|";
	position:absolute;
	left:0;
	top:14px
}
.em-header-style14 .toolbar-switch span.current {
	padding:15px 20px
}
.em-header-style14 .toolbar-switch .toolbar-dropdown ul {
	right:0;
	left:auto
}
.em-header-style14 .toolbar-switch.em-currency-style01 span.current {
	padding-right:0
}
.em-header-style14 .em-logo .logo {
	margin:20px 0
}
.em-header-style14 .em-phone {
	margin:40px 0 0;
	font-size:110%
}
.em-header-style14 .em-phone span {
	margin:0 10px
}
.em-header-style14 .em-search {
	width:100%;
	padding-right:50px;
	display:inline-block
}
.em-header-style14 .em-search .em-search-icon {
	display:inline-block;
	padding:17px 15px;
	border-top-width:2px;
	border-top-style:solid;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-header-style14 .em-top-cart .em-amount-topcart:before, .em-header-style28 .em-search .em-search-icon {
-moz-transition:all .5s ease;
-o-transition:all .5s ease;
	-webkit-backface-visibility:hidden;
-webkit-transition:all .5s ease
}
.em-header-style14 .em-search .em-search-icon span:before {
	font-size:1.4rem
}
.em-header-style14 .em_nav .hnav .menu-item-depth-0, .em-header-style14 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0 {
	margin:0
}
.em-header-style14 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>a span, .em-header-style14 .em_nav .hnav .menu-item-depth-0>a span {
	padding:25px 20px;
	border-width:2px 1px 1px;
	border-color:transparent;
	text-transform:uppercase
}
.em-header-style14 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>.menu-container, .em-header-style14 .em_nav .hnav .menu-item-depth-0>.menu-container {
	top:70px
}
.em-header-style14 .em-wrapper-searchcart {
	padding-top:14px;
	position:relative
}
.em-header-style14 .em-top-cart {
	margin-left:15px;
	position:absolute;
	right:0;
	top:14px
}
.em-header-style14 .em-top-cart .em-amount-topcart {
	display:inline-block;
	padding-bottom:30px;
	margin-bottom:-30px;
	float:right;
	width:44px
}
.em-header-style14 .em-top-cart .em-amount-topcart:after {
	content:""
}
.em-header-style14 .em-top-cart .em-amount-topcart:before {
	content:"\e835";
	font-family:maki, sans-serif;
	width:44px;
	height:44px;
	font-size:1.8rem;
	text-align:center;
	display:inline-block;
	padding:11px 5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-header-style14 .em-top-cart .em-amount-topcart .em-topcart-text {
	display:none
}
.em-header-style14 .em-top-cart .em-amount-topcart .em-topcart-qty {
	position:absolute;
	left:34%;
	top:-7px;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:1px 5px;
	font-size:10px
}
.em-header-style03 .em-account-link, .em-header-style03 .em-links-wishlist a, .em-header-style03 .em-logout-link, .em-header-style03 .em-register-link, .em-header-style03 .link-account, .em-header-style03 .top-link-deal {
	position:relative
}
.em-header-style14 .em-fixed-top .em-top-cart-sticky {
	display:block!important
}
.em-header-style14 .em-fixed-top.navbar-fixed-top .em-search {
	display:none;
	width:auto
}
@media (min-width:980px) {
.em-header-style14 .form-search input.input-text {
width:310px
}
}
.em-rtl .em-header-style14 .em-account-link:before, .em-rtl .em-header-style14 .em-links-wishlist a:before, .em-rtl .em-header-style14 .em-logout-link:before, .em-rtl .em-header-style14 .em-register-link:before, .em-rtl .em-header-style14 .link-account:before {
	margin-right:0;
	margin-left:10px
}
.em-header-style03 .em-account-link:before, .em-header-style03 .em-links-wishlist a:before, .em-header-style03 .em-logout-link:before, .em-header-style03 .em-register-link:before, .em-header-style03 .link-account:before, .em-header-style03 .top-link-deal:before {
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	font-size:100%;
	margin-right:10px;
	display:inline-block
}
.em-rtl .em-header-style14 .account-link:after, .em-rtl .em-header-style14 .em-top-links ul.em-links-wishlist li:after, .em-rtl .em-header-style14 .em-top-links ul.links li:after, .em-rtl .em-header-style14 .em-top-links>ul li:after, .em-rtl .em-header-style14 .toolbar-switch:after, .em-rtl .em-header-style14 .welcome-msg:after {
	right:0;
	left:auto
}
.em-rtl .em-header-style14 .toolbar-switch {
	padding-left:0;
	padding-right:20px
}
.em-rtl .em-header-style14 .em-header-bottom .em-top-cart {
	right:auto;
	left:0;
	margin-left:0
}
.em-rtl .em-header-style14 .em-header-bottom .em-top-cart .em-amount-topcart .em-topcart-qty {
	left:23%
}
.em-rtl .em-header-style14 .em-search {
	padding-right:0;
	padding-left:50px
}
.em-header-style03 .toolbar-switch .toolbar-title {
	padding:0!important
}
.em-header-style03 .em-logout-link:before, .em-header-style03 .link-account:before {
	content:"\f13e"
}
.em-header-style03 .em-account-link:before, .em-header-style03 .em-register-link:before {
	content:"\f084"
}
.em-header-style03 .em-links-wishlist a:before {
	content:"\f004"
}
.em-header-style03 .top-link-deal:before {
	content:"\f02c"
}
.em-header-style03 .em-top-cart .em-amount-topcart:after, .em-header-style03 .toolbar-switch:before {
	content:""
}
.em-header-style03 .account-link, .em-header-style03 .em-top-links ul.em-links-wishlist li, .em-header-style03 .em-top-links ul.links li, .em-header-style03 .em-top-links>ul li, .em-header-style03 .welcome-msg {
	padding:14px 0 15px;
	margin-left:30px;
	display:inline-block;
	vertical-align:middle;
	position:relative;
	overflow:hidden;
	overflow:initial
}
.em-header-style03 .account-link:before, .em-header-style03 .em-top-links ul.em-links-wishlist li:before, .em-header-style03 .em-top-links ul.links li:before, .em-header-style03 .em-top-links>ul li:before, .em-header-style03 .welcome-msg:before {
	content:"";
	position:absolute;
	left:50%;
	right:50%;
	z-index:1;
	background:#2098d1;
	height:4px;
	-webkit-transition-property:left, right;
	transition-property:left, right;
-webkit-transition-duration:.3s;
transition-duration:.3s;
	-webkit-transition-timing-function:ease-out;
	transition-timing-function:ease-out;
	opacity:0;
	filter:alpha(opacity=0);
	bottom:auto;
	top:0
}
.em-header-style03 .account-link:hover:before, .em-header-style03 .em-top-links ul.em-links-wishlist li:hover:before, .em-header-style03 .em-top-links ul.links li:hover:before, .em-header-style03 .em-top-links>ul li:hover:before, .em-header-style03 .welcome-msg:hover:before {
	left:0;
	right:0;
	opacity:1;
	filter:alpha(opacity=100)
}
.em-header-style03 .em-language-currency {
	margin:0 -15px
}
.em-header-style03 .toolbar-switch {
	margin:0 15px;
	vertical-align:middle;
	position:relative;
	overflow:hidden;
	overflow:initial
}
.em-header-style03 .toolbar-switch:before {
	position:absolute;
	left:50%;
	right:50%;
	z-index:1;
	background:#2098d1;
	height:4px;
	-webkit-transition-property:left, right;
	transition-property:left, right;
-webkit-transition-duration:.3s;
transition-duration:.3s;
	-webkit-transition-timing-function:ease-out;
	transition-timing-function:ease-out;
	opacity:0;
	filter:alpha(opacity=0);
	bottom:auto;
	top:0
}
.em-header-style03 .em-search .form-search .input_cat .catsearch-dropdown ul, .em-header-style03 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>.menu-container, .em-header-style03 .em_nav .hnav .menu-item-depth-0>.menu-container {
	top:49px
}
.em-header-style03 .toolbar-switch:hover:before {
	left:0;
	right:0;
	opacity:1;
	filter:alpha(opacity=100)
}
.em-header-style03 .toolbar-switch span.current {
	padding:14px 0 15px;
	min-height:49px
}
.em-header-style03 .em-language-currency .form-language.toolbar-switch span.current {
	padding-left:25px;
	background-position:0 center
}
.em-header-style03 .em-logo .logo {
	margin:30px 0
}
.em-header-style03 .em-phone {
	margin:12px 0 0;
	font-size:110%
}
.em-header-style03 .em-phone .fa {
	font-size:130%;
	vertical-align:middle
}
.em-header-style03 .em-phone span {
	margin:0 10px;
	vertical-align:middle
}
.em-header-style03 .em-line {
	float:left;
	width:100%;
	border-top-style:solid;
	border-top-width:1px;
	border-bottom-style:solid;
	border-bottom-width:1px
}
.em-header-style03 .em-line .em-menu-hoz {
	padding-left:0
}
.em-header-style03 .em-search {
	width:60%;
	margin-top:32px
}
.em-header-style03 .em-search .form-search input.input-text {
	height:49px;
	line-height:39px
}
.em-header-style03 .em-search .form-search .input_cat select {
	height:49px;
	line-height:48px
}
.em-header-style03 .em-search .form-search .input_cat .catsearch-dropdown span.current {
	padding:15px 30px 13px 5px;
	min-height:49px
}
.em-header-style03 .em-search .em-search-style02 .form-search button.button {
	padding:18px 17px 17px;
	width:49px
}
.em-header-style03 .em_nav .hnav .menu-item-depth-0, .em-header-style03 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0 {
	margin:0
}
.em-header-style03 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>a span, .em-header-style03 .em_nav .hnav .menu-item-depth-0>a span {
	padding:15px 20px;
	border-width:2px 1px 1px;
	border-color:transparent;
	text-transform:uppercase;
	margin-top:-1px;
	margin-bottom:-1px
}
.em-header-style03 .em-top-cart {
	margin-top:32px;
	margin-left:10px
}
.em-header-style03 .em-top-cart .em-amount-topcart {
	display:inline-block;
	padding-bottom:30px;
	margin-bottom:-30px;
	width:51px
}
.em-header-style03 .em-top-cart .em-amount-topcart:before {
	content:"\e835";
	font-family:maki, sans-serif;
	width:51px;
	height:51px;
	font-size:1.8rem;
	text-align:center;
	display:inline-block;
	padding:15px 5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em-header-style28 .em-account-link:before, .em-header-style28 .em-links-wishlist a:before, .em-header-style28 .em-logout-link:before, .em-header-style28 .em-register-link:before, .em-header-style28 .link-account:before, .em-header-style28 .top-link-deal:before {
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
.em-header-style03 .em-fixed-top.navbar-fixed-top .em-phone, .em-header-style03 .em-top-cart .em-amount-topcart .em-topcart-text {
	display:none
}
.em-header-style03 .em-top-cart .em-amount-topcart .em-topcart-qty {
	position:absolute;
	left:33%;
	top:-8px;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:1px 6px;
	font-size:12px
}
.em-header-style03 .em-fixed-top.navbar-fixed-top .em-search {
	width:auto
}
.em-wrapper-header .em-header-style03 .em-fixed-top.navbar-fixed-top .em-line {
	border:0!important;
	padding:1px 0
}
.em-rtl .em-header-style03 .account-link, .em-rtl .em-header-style03 .em-top-links ul.em-links-wishlist li, .em-rtl .em-header-style03 .em-top-links ul.links li, .em-rtl .em-header-style03 .em-top-links>ul li, .em-rtl .em-header-style03 .welcome-msg {
	margin-left:0;
	margin-right:30px
}
.em-rtl .em-header-style03 .em-account-link:before, .em-rtl .em-header-style03 .em-links-wishlist a:before, .em-rtl .em-header-style03 .em-logout-link:before, .em-rtl .em-header-style03 .em-register-link:before, .em-rtl .em-header-style03 .link-account:before {
	margin-right:0;
	margin-left:10px
}
.em-rtl .em-header-style03 .em-top-cart .em-amount-topcart .em-topcart-qty {
	left:25%
}
.cms-index-index .em-header-style28 .em-menu-fix-pos .em-wrapper-header-top {
	position:absolute;
	width:100%;
	padding-top:40px
}
.em-header-style28 .navbar-fixed-top .em-menu-fix-pos .em-wrapper-header-top {
	position:static;
	padding-top:0!important;
	padding-bottom:0!important
}
.em-wrapper-header .em-header-style28 .toolbar-switch .toolbar-title {
	margin:-8px -10px;
	padding:0 10px;
	min-height:38px
}
.em-wrapper-header .em-header-style28 .toolbar-switch .toolbar-title select {
	margin:0 5px
}
.em-header-style28 .em-menu-fix-pos {
	position:relative;
	z-index:2
}
.em-header-style28 .em-menu-fix-pos .em-wrapper-header-top {
	position:static;
	width:100%;
	padding:30px 0 10px
}
.em-header-style28 .em-lan-cur-link-search {
	background:#fff;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	padding:14px 10px 11px;
	min-height:45px;
	float:right
}
.em-header-style28 .em-lan-cur-link-search .em-language-currency, .em-header-style28 .em-lan-cur-link-search .em-search, .em-header-style28 .em-lan-cur-link-search .em-top-links {
	float:left
}
.em-header-style28 .em-account {
	top:34px;
	right:-43px
}
.em-header-style28 .em-logout-link, .em-header-style28 .link-account {
	position:relative;
	font-size:0;
	padding:0 10px;
	border-right:1px solid #cdcdcd;
	display:inline-block
}
.em-header-style28 .em-logout-link:before, .em-header-style28 .link-account:before {
	display:inline-block;
	content:"\f007";
	font-size:14px
}
.em-header-style28 .em-account-link, .em-header-style28 .em-register-link {
	position:relative;
	font-size:0;
	padding:0 10px;
	border-right:1px solid #cdcdcd;
	display:inline-block
}
.em-header-style28 .em-account-link:before, .em-header-style28 .em-register-link:before {
	display:inline-block;
	content:"\f084";
	font-size:14px
}
.em-header-style28 .em-links-wishlist a {
	position:relative;
	font-size:0;
	padding:0 10px;
	border-right:1px solid #cdcdcd;
	display:inline-block
}
.em-header-style28 .em-links-wishlist a:before {
	display:inline-block;
	content:"\f004";
	font-size:14px
}
.em-header-style28 .top-link-deal {
	position:relative;
	font-size:0;
	padding:0 10px;
	border-right:1px solid #cdcdcd
}
.em-header-style28 .top-link-deal:before {
	display:inline-block;
	content:"\f02c";
	font-size:14px
}
.em-header-style28 .em-language-currency .toolbar-dropdown ul {
	top:34px;
	width:104px
}
.em-header-style28 .toolbar-switch {
	margin:0;
	overflow:initial
}
.em-header-style28 .toolbar-switch:before {
	bottom:auto;
	top:0
}
.em-header-style28 .toolbar-switch span.current {
	padding:0 10px 30px;
	margin:2px 0 -26px;
	line-height:1.45rem
}
.em-header-style28 .toolbar-switch span.current:after {
	font-size:12px;
	display:none
}
.em-header-style28 .toolbar-switch span.current:before {
	border-right:1px solid #cdcdcd;
	position:absolute;
	top:0;
	right:0;
	height:14px;
	content:''
}
.em-header-style28 .toolbar-switch.form-language span.current {
	font-size:0;
	width:18px;
	height:14px;
	padding-left:28px;
	background-position:10px 1px
}
.em-header-style28 .em-phone {
	margin:40px 0 0;
	font-size:110%
}
.em-header-style28 .em-phone span {
	margin:0 10px
}
.em-header-style28 .em-search {
	display:inline-block!important;
	vertical-align:top
}
.em-header-style28 .em-search .em-search-icon {
	display:inline-block;
	vertical-align:top;
	padding:1px 10px 30px;
	margin-bottom:-30px;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-header-style28 .em-search .em-search-icon span:before {
	font-size:1.4rem
}
.em-header-style28 .em-search .em-search-style01 #em-search-content-fixed-top {
	top:34px;
	right:-10px
}
.em-header-style28 .line-top {
	border-top-style:solid;
	border-top-width:1px
}
.em-header-style28 .em-menu-hoz {
	text-align:center;
	display:inline-block;
	vertical-align:top
}
.em-header-style28 .em-menu-hoz #em-main-megamenu {
	display:inline-block
}
.em-header-style28 .em_nav .hnav .menu-item-depth-0>.menu-container {
	top:46px
}
.em-header-style28 .em_nav .hnav .menu-item-depth-0, .em-header-style28 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0 {
	margin:0
}
.em-header-style28 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>a span, .em-header-style28 .em_nav .hnav .menu-item-depth-0>a span {
	padding:13px 20px;
	border-top-width:2px;
	text-transform:uppercase
}
.em-header-style28 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>.menu-container, .em-header-style28 .em_nav .hnav .menu-item-depth-0>.menu-container {
	text-align:left
}
.em-header-style28 .em-top-cart {
	float:right;
	margin-left:10px;
	display:inline-block!important
}
.em-header-style28 .em-top-cart .em-amount-topcart {
	display:inline-block;
	padding-bottom:30px;
	margin-bottom:-30px;
	width:46px
}
.em-header-style28 .em-top-cart .em-amount-topcart:after {
	content:""
}
.em-header-style28 .em-top-cart .em-amount-topcart:before {
	content:"\e835";
	font-family:maki, sans-serif;
	width:46px;
	height:46px;
	font-size:1.8rem;
	text-align:center;
	display:inline-block;
	padding:11px 5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em-header-style28 .em-fixed-top.navbar-fixed-top .em-lan-cur-link-search>div, .em-header-style28 .em-top-cart .em-amount-topcart .em-topcart-text {
	display:none
}
.em-header-style28 .em-top-cart .em-amount-topcart .em-topcart-qty {
	position:absolute;
	left:26%;
	top:-8px;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:1px 6px;
	font-size:12px
}
.em-header-style29 .em-account-link:before, .em-header-style29 .em-links-wishlist a:before, .em-header-style29 .em-logout-link:before, .em-header-style29 .em-register-link:before, .em-header-style29 .link-account:before, .em-header-style29 .top-link-deal:before {
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	font-size:100%;
	margin-right:10px
}
.em-header-style28 .em-fixed-top.navbar-fixed-top .em-logo-sticky {
	position:absolute;
	z-index:1
}
.em-header-style28 .em-fixed-top.navbar-fixed-top .header-top-cart {
	position:absolute;
	right:0
}
.em-header-style29 .em-account-link, .em-header-style29 .em-links-wishlist a, .em-header-style29 .em-logout-link, .em-header-style29 .em-register-link, .em-header-style29 .link-account, .em-header-style29 .top-link-deal {
	position:relative
}
.em-header-style28 .em-fixed-top.navbar-fixed-top .em-menu-hoz {
	width:100%
}
.em-header-style28 .em-fixed-top.navbar-fixed-top .em-lan-cur-link-search {
	background:0 0
}
.em-header-style28 .em-fixed-top.navbar-fixed-top .em-lan-cur-link-search>div.em-search {
	display:inline-block
}
.em-header-style28 .navbar-fixed-top .em-menu-hoz #em-main-megamenu {
	display:inline
}
.em-wrapper-header .em-header-style28 .em-fixed-top.navbar-fixed-top .em_nav {
	padding-right:0
}
.em-rtl .em-header-style28 .em-fixed-top.navbar-fixed-top .header-top-cart {
	left:0;
	right:auto
}
.em-header-style29 .em-header-top {
	border-bottom-width:1px;
	border-bottom-style:solid
}
.em-header-style29 .em-logout-link:before, .em-header-style29 .link-account:before {
	display:inline-block;
	content:"\f13e"
}
.em-header-style29 .em-account-link:before, .em-header-style29 .em-register-link:before {
	display:inline-block;
	content:"\f084"
}
.em-header-style29 .em-links-wishlist a:before {
	display:inline-block;
	content:"\f004"
}
.em-header-style29 .top-link-deal:before {
	display:inline-block;
	content:"\f02c"
}
.em-header-style29 .em-top-cart .em-amount-topcart:after, .em-header-style29 .toolbar-switch:before {
	content:""
}
.em-header-style29 .account-link, .em-header-style29 .em-top-links ul.em-links-wishlist li, .em-header-style29 .em-top-links ul.links li, .em-header-style29 .em-top-links>ul li, .em-header-style29 .welcome-msg {
	padding:14px 0 15px;
	margin-left:30px;
	display:inline-block;
	vertical-align:middle;
	position:relative;
	overflow:hidden;
	overflow:initial
}
.em-header-style29 .account-link:before, .em-header-style29 .em-top-links ul.em-links-wishlist li:before, .em-header-style29 .em-top-links ul.links li:before, .em-header-style29 .em-top-links>ul li:before, .em-header-style29 .welcome-msg:before {
	content:"";
	position:absolute;
	left:50%;
	right:50%;
	z-index:1;
	background:#2098d1;
	height:4px;
	-webkit-transition-property:left, right;
	transition-property:left, right;
-webkit-transition-duration:.3s;
transition-duration:.3s;
	-webkit-transition-timing-function:ease-out;
	transition-timing-function:ease-out;
	opacity:0;
	filter:alpha(opacity=0);
	bottom:auto;
	top:0
}
.em-header-style29 .account-link:hover:before, .em-header-style29 .em-top-links ul.em-links-wishlist li:hover:before, .em-header-style29 .em-top-links ul.links li:hover:before, .em-header-style29 .em-top-links>ul li:hover:before, .em-header-style29 .welcome-msg:hover:before {
	left:0;
	right:0;
	opacity:1;
	filter:alpha(opacity=100)
}
.em-header-style29 .em-language-currency {
	margin:0 -15px
}
.em-header-style29 .toolbar-switch {
	margin:0 15px;
	vertical-align:middle;
	position:relative;
	overflow:hidden;
	overflow:initial
}
.em-header-style29 .toolbar-switch:before {
	position:absolute;
	left:50%;
	right:50%;
	z-index:1;
	background:#2098d1;
	height:4px;
	-webkit-transition-property:left, right;
	transition-property:left, right;
-webkit-transition-duration:.3s;
transition-duration:.3s;
	-webkit-transition-timing-function:ease-out;
	transition-timing-function:ease-out;
	opacity:0;
	filter:alpha(opacity=0);
	bottom:auto;
	top:0
}
.em-header-style29 .toolbar-switch:hover:before {
	left:0;
	right:0;
	opacity:1;
	filter:alpha(opacity=100)
}
.em-header-style29 .toolbar-switch span.current {
	padding:14px 0 15px;
	min-height:49px
}
.em-header-style29 .em-language-currency .form-language.toolbar-switch span.current {
	padding-left:25px;
	background-position:0 center
}
.em-header-style29 .em-logo .logo {
	margin:30px 0
}
.em-header-style29 .em-search {
	width:41%;
	margin-top:38px
}
.em-header-style29 .em-search .form-search {
	border-color:transparent
}
.em-header-style29 .em-search .form-search button.button {
	border:none;
	top:0
}
.em-header-style29 .em_nav {
	padding-right:70px
}
.em-header-style29 .em_nav .hnav .menu-item-depth-0, .em-header-style29 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0 {
	margin:0
}
.em-header-style29 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>a span, .em-header-style29 .em_nav .hnav .menu-item-depth-0>a span {
	padding:20px;
	border-top-width:2px;
	border-color:transparent;
	text-transform:uppercase
}
.em-header-style29 .em_nav .hnav .menu-item-depth-0.menu_custom, .em-header-style29 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0.menu_custom {
	margin-right:20px
}
.em-header-style29 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>.menu-container, .em-header-style29 .em_nav .hnav .menu-item-depth-0>.menu-container {
	top:60px
}
.em-header-style29 .em-top-cart {
	margin-left:10px;
	display:block!important;
	position:absolute;
	right:30px;
	top:11px
}
.em-header-style29 .em-top-cart .em-amount-topcart {
	display:inline-block;
	padding-bottom:10px;
	margin-bottom:-10px;
	width:40px
}
.em-header-style29 .em-top-cart .em-amount-topcart:before {
	content:"\e835";
	font-family:maki, sans-serif;
	width:40px;
	height:40px;
	font-size:1.7rem;
	text-align:center;
	display:inline-block;
	padding:10px 5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em-header-style30 .em-account-link:before, .em-header-style30 .em-links-wishlist a:before, .em-header-style30 .em-logout-link:before, .em-header-style30 .em-register-link:before, .em-header-style30 .link-account:before, .em-header-style30 .top-link-deal:before {
	font-family:FontAwesome;
	font-weight:400;
	line-height:1;
	font-style:normal;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
.em-header-style29 .em-top-cart .em-amount-topcart .em-topcart-text {
	display:none
}
.em-header-style29 .em-top-cart .em-amount-topcart .em-topcart-qty {
	position:absolute;
	left:33%;
	top:-6px;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:0 5px;
	font-size:10px
}
.em-header-style29 .em-fixed-top.navbar-fixed-top .em-search {
	width:auto
}
.em-rtl .em-header-style29 .account-link, .em-rtl .em-header-style29 .em-top-links ul.em-links-wishlist li, .em-rtl .em-header-style29 .em-top-links ul.links li, .em-rtl .em-header-style29 .em-top-links>ul li, .em-rtl .em-header-style29 .welcome-msg {
	margin-left:0;
	margin-right:30px
}
.em-rtl .em-header-style29 .em-account-link:before, .em-rtl .em-header-style29 .em-links-wishlist a:before, .em-rtl .em-header-style29 .em-logout-link:before, .em-rtl .em-header-style29 .em-register-link:before, .em-rtl .em-header-style29 .link-account:before {
	margin-right:0;
	margin-left:10px
}
.em-rtl .em-header-style29 .em-top-cart {
	right:auto;
	left:30px
}
.em-rtl .em-header-style29 .em-top-cart .em-amount-topcart .em-topcart-qty {
	left:25%
}
.em-rtl .em-header-style29 .em_nav {
	padding-right:0;
	padding-left:70px
}
.em-rtl .em-header-style29 .em_nav .hnav .menu-item-depth-0.menu_custom, .em-rtl .em-header-style29 .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0.menu_custom {
	margin-right:0;
	margin-left:20px
}
.em-area-header {
	left:0;
	position:fixed;
	top:auto;
	bottom:0;
	z-index:3;
	padding:0;
	height:100%;
	width:280px
}
.em-area-header .em-wrapper-header {
	height:100%;
	padding:0 2rem
}
.em-wrapper-header .em-header-style30 .toolbar-switch .toolbar-title {
	margin:-6px -10px
}
.em-wrapper-header .em-header-style30 .toolbar-switch .toolbar-title select {
	margin:0 5px
}
.em-header-style30 .em-header-top {
	background-color:transparent
}
.em-header-style30 .em-wrapper-header-bottom {
	position:absolute;
	bottom:0;
	left:0;
	right:0;
	margin:2rem
}
.em-header-style30 .em-logo {
	text-align:center;
	display:inline-block;
	width:100%;
	margin:3rem 0
}
.em-header-style30 .em-lan-cur-link-search {
	display:block;
	width:100%;
	text-align:center;
	margin-bottom:2rem
}
.em-header-style30 .em-lan-cur-link-search .em-language-currency, .em-header-style30 .em-lan-cur-link-search .em-search, .em-header-style30 .em-lan-cur-link-search .em-top-links {
	display:inline-block
}
.em-header-style30 .em-account {
	top:-150px;
	right:auto;
	left:50px;
	display:none!important
}
.em-header-style30 .menuleft .mega-menu .em-prepend-menu:before, .em-header-style30 .menuleft .mega-menu .vnav .menu-item-link.fa:before {
	left:0
}
.em-header-style30 .em-logout-link, .em-header-style30 .link-account {
	position:relative;
	font-size:0;
	padding:0 10px;
	display:inline-block
}
.em-header-style30 .em-logout-link:before, .em-header-style30 .link-account:before {
	display:inline-block;
	content:"\f007";
	font-size:14px
}
.em-header-style30 .em-account-link, .em-header-style30 .em-register-link {
	position:relative;
	font-size:0;
	padding:0 10px;
	border-right:1px solid #cdcdcd;
	display:inline-block
}
.em-header-style30 .em-account-link:before, .em-header-style30 .em-register-link:before {
	display:inline-block;
	content:"\f084";
	font-size:14px
}
.em-header-style30 .em-links-wishlist a {
	position:relative;
	font-size:0;
	padding:0 10px;
	border-right:1px solid #cdcdcd;
	display:inline-block
}
.em-header-style30 .em-links-wishlist a:before {
	display:inline-block;
	content:"\f004";
	font-size:14px
}
.em-header-style30 .top-link-deal {
	position:relative;
	font-size:0;
	padding:0 10px;
	border-right:1px solid #cdcdcd
}
.em-header-style30 .top-link-deal:before {
	display:inline-block;
	content:"\f02c";
	font-size:14px
}
.em-header-style30 .em-language-currency .toolbar-dropdown ul {
	top:auto;
	bottom:25px;
	width:104px
}
.em-header-style30 .toolbar-switch {
	margin:0;
	overflow:initial
}
.em-header-style30 .toolbar-switch span.current {
	padding:0 10px 30px;
	margin:2px 0 -26px;
	line-height:1.7rem
}
.em-header-style30 .toolbar-switch span.current:after {
	font-size:12px;
	display:none
}
.em-header-style30 .toolbar-switch span.current:before {
	border-right:1px solid #cdcdcd;
	position:absolute;
	top:0;
	right:0;
	height:16px;
	content:''
}
.em-header-style30 .toolbar-switch.form-language span.current {
	font-size:0;
	width:18px;
	height:14px;
	padding-left:28px;
	background-position:10px 1px
}
.em-header-style30 .em-search {
	margin:0 -2rem
}
.em-header-style30 .em-search .form-search {
	border:none
}
.em-header-style30 .menuleft .mega-menu, .em-rtl .em-header-style30 .toolbar-switch span.current:before {
	border-width:0
}
.em-header-style30 .em-search .form-search input.input-text {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.em-header-style30 .em-search .form-search button.button {
	right:0;
	top:0;
	border-width:0
}
.em-header-style30 .menuleft {
	position:static
}
.em-header-style30 .menuleft .mega-menu .menu-item-link.fa>.em-menu-link span {
	padding-left:25px!important
}
.em-header-style30 .menuleft .mega-menu .em-prepend-menu {
	padding-left:25px
}
.em-header-style30 .em-top-cart .em-amount-topcart {
	display:block;
	padding:1rem;
	text-align:center;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em-header-style30 .em-top-cart .em-amount-topcart:after {
	content:""
}
.em-header-style30 .em-top-cart .em-amount-topcart:before {
	content:"\e835";
	font-family:maki, sans-serif;
	font-size:1.4rem;
	text-align:center;
	display:inline-block;
	padding:0 1rem
}
.em-header-style30 .em-top-cart .em-summary-topcart {
	margin-right:-3rem;
	padding-right:3rem;
	float:none
}
.em-header-style30 .em-top-cart .topcart-popup {
	left:260px;
	top:auto;
	bottom:0
}
.em-rtl .em-area-header {
	left:auto;
	right:0
}
.em-rtl .em-wrapper-content {
	padding-left:0!important;
	padding-right:280px!important
}
.em-rtl .em-header-style30 .em-top-cart .topcart-popup {
	left:auto;
	right:260px
}
.em-rtl .em-header-style30 .em-search .form-search button.button {
	right:auto;
	left:0
}
.em-top-links ol, .em-top-links ul {
	margin:0
}
.em-top-links .list-inline>li {
	padding:0
}
.em-logo .logo {
	display:inline-block
}
.em-logo .logo img {
	max-width:236px
}
.em-logo strong {
	font-size:0;
	text-indent:-99999px;
	overflow:hidden;
	vertical-align:middle;
	text-align:left;
	display:none
}
.welcome-msg {
	display:inline-block;
	margin-bottom:0
}
.list-link-mobile {
	overflow:hidden;
	margin:0!important;
	padding:10px 0 0
}
.list-link-mobile li {
	padding:10px 0
}
.list-link-mobile li a:before {
	font-size:16px;
	text-align:center;
	width:40px;
	height:40px;
	line-height:40px;
	border:1px solid #fff;
	-moz-border-radius:50%;
	-webkit-border-radius:50%;
	border-radius:50%;
	margin-bottom:1px;
	font-family:FontAwesome;
	display:inline-block;
	color:#fff;
	background:0 0
}
.list-link-mobile li a.fa-shopping-cart:before {
	color:#fff;
	background:#50b200;
	border:1px solid #50b200
}
.list-link-mobile li span {
	display:inline-block;
	width:100%
}
.em-wrapper-header .em-fixed-top .em-search-sticky, .em-wrapper-header .em-fixed-top .em-top-cart-sticky, .em-wrapper-header .em-fixed-top.navbar-fixed-top .em-logo {
	display:none
}
.list-link-mobile li:first-child {
	border-left:0
}
.em-wrapper-header .navbar-fixed-top {
	position:fixed!important;
	-webkit-animation-name:fadeInDown;
	-webkit-animation-direction:normal;
	-webkit-animation-iteration-count:1;
	-moz-animation-iteration-count:1;
-webkit-animation-duration:.7s;
-moz-animation-duration:.7s;
	-webkit-animation-delay:0s;
	-moz-animation-delay:0s;
	-webkit-animation-timing-function:ease-out;
	-moz-animation-timing-function:ease-out
}
.em-wrapper-ads-09 .em-wrapper-ads-item:nth-child(1) .fa:hover, .em-wrapper-ads-09 .em-wrapper-ads-item:nth-child(2) .fa:hover, .em-wrapper-ads-09 .em-wrapper-ads-item:nth-child(3) .fa:hover, .em-wrapper-ads-09 .em-wrapper-ads-item:nth-child(4) .fa:hover {
-webkit-animation-iteration-count:infinite;
-webkit-animation-direction:normal;
-webkit-animation-play-state:running;
-webkit-animation-fill-mode:forwards;
-moz-animation-name:anim_titles;
-moz-animation-iteration-count:infinite;
-moz-animation-direction:normal;
-moz-animation-play-state:running;
-moz-animation-fill-mode:forwards;
-webkit-animation-duration:4s;
-moz-animation-duration:4s;
-webkit-animation-delay:0s;
-moz-animation-delay:0s;
-webkit-animation-timing-function:ease-out;
-moz-animation-timing-function:ease-out
}
.em-wrapper-header .em-fixed-top .em-logo-sticky {
	float:left;
	margin-right:1rem;
	display:none
}
.em-wrapper-header .em-fixed-top .em-logo-sticky img {
	width:0;
	height:0;
	opacity:0;
	filter:alpha(opacity=0);
	visibility:hidden
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em_nav {
	padding-top:0;
	padding-right:90px;
	padding-left:60px
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em_nav .hnav .menu-item-depth-0>.em-catalog-navigation>li.level0>a.em-menu-link span, .em-wrapper-header .em-fixed-top.navbar-fixed-top .em_nav .hnav .menu-item-depth-0>a.em-menu-link span {
	padding-bottom:21px;
	padding-top:21px;
	border-top-width:2px;
	text-transform:uppercase
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em_nav .hnav .menu-item-depth-0>.menu-container {
	top:62px
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-logo-sticky {
	display:block;
	width:50px;
	padding:4px 0;
	position:absolute
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-logo-sticky img {
	width:100%;
	height:auto;
	opacity:1;
	filter:alpha(opacity=100);
	visibility:visible;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-payment .em-payment-icon, .em-social-icon {
-webkit-transition:all .5s ease;
	-moz-backface-visibility:hidden
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-search-sticky {
	display:block;
	position:absolute;
	top:15px;
	right:52px;
	padding:0;
	margin:0;
	width:auto;
	border:none
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-search-sticky .em-search-icon {
	padding:0;
	border:none
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-search-sticky .em-search-icon span:before {
	width:35px;
	height:35px;
	padding:10px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	font-size:1.4rem
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-search-sticky .em-search-style01 .em-container-js-search {
	top:47px
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-search-sticky .em-search-style01 .em-container-js-search .form-search input.input-text {
	height:42px;
	line-height:34px
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-search-sticky .em-search-style01 .em-container-js-search .form-search button.button {
	padding:13px 15px
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-top-cart {
	position:absolute;
	right:10px;
	top:15px;
	margin:0;
	display:block
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-top-cart .em-amount-topcart {
	display:inline-block;
	padding-bottom:30px;
	margin-bottom:-30px;
	width:35px
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-top-cart .em-amount-topcart:after {
	content:""
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-top-cart .em-amount-topcart:before {
	content:"\f07a";
	font-family:FontAwesome;
	width:35px;
	height:35px;
	font-size:1.5rem;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	text-align:center;
	display:inline-block;
	padding:12px 5px 0;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em-footer-style13 .newsletter-boder-input-act .block-subscribe input.input-text, .em-footer-style14 .newsletter-boder-input-act .block-subscribe input.input-text {
	height:40px
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-top-cart .em-amount-topcart .em-topcart-text, .hidden-2col, .hidden-3col {
	display:none
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-top-cart .em-amount-topcart .em-topcart-qty {
	position:absolute;
	left:33%;
	top:-6px;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:0 4px;
	font-size:9px
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-top-cart .topcart-popup {
	top:47px
}
.em-wrapper-header .em-fixed-top.navbar-fixed-top .em-menu-hoz {
	float:left!important
}
.em-rtl .em-wrapper-header .em-fixed-top .em-logo-sticky {
	float:right;
	margin-right:0;
	margin-left:1rem
}
.em-rtl .em-wrapper-header .em-fixed-top.navbar-fixed-top .em-menu-hoz {
	float:right!important
}
.em-rtl .em-wrapper-header .em-fixed-top.navbar-fixed-top .em_nav {
	padding-right:0;
	padding-left:130px
}
.em-rtl .em-wrapper-header .em-fixed-top.navbar-fixed-top .em-search {
	right:auto;
	left:52px
}
.em-rtl .em-wrapper-header .em-fixed-top.navbar-fixed-top .em-search .em-search-style01 .em-container-js-search {
	right:auto;
	left:0
}
.em-rtl .em-wrapper-header .em-fixed-top.navbar-fixed-top .em-top-cart {
	right:auto;
	left:10px
}
.padding-top20 {
	padding-top:20px
}
.em-footer-style11 {
	padding-top:70px
}
.em-footer-style11 .em-block-content {
	margin-bottom:20px
}
.em-footer-style11 .four-block-middle {
	padding:40px 0 10px;
	margin:20px 0 30px;
	display:inline-block;
	width:100%;
	border-top-width:1px;
	border-bottom-width:1px;
	border-top-style:solid;
	border-bottom-style:solid
}
.em-footer-style11 .em-social .em-social-icon {
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:6px 8px;
	font-size:1.6rem
}
.em-footer-style11 .em-links li {
	margin-bottom:10px
}
.em-footer-style11 .em-tags-list {
	padding-top:10px;
	margin:0 -10px
}
.em-footer-style11 .em-tags-list li {
	display:inline-block;
	margin:0 5px 10px
}
.em-footer-style11 .em-tags-list li a {
	padding:10px;
	border-width:1px;
	border-style:solid;
	display:inline-block
}
.em-footer-style11 .block-info .em-address, .em-footer-style11 .block-info .em-email, .em-footer-style11 .block-info .em-phone {
	margin:0 -5px 1.55rem
}
.em-footer-style11 .block-info .fa {
	margin:0 5px
}
.em-footer-style11 .block-subscribe button.button {
	border-style:solid;
	border-width:2px
}
.em-rtl .em-footer-style11 .newsletter-boder-input-act .block-subscribe .actions {
	float:left
}
.em-rtl .em-footer-style11 .newsletter-boder-input-act .block-subscribe .actions button.button {
	margin-left:0
}
.em-rtl .em-footer-style11 .block-subscribe .input-box {
	float:right
}
.em-footer-style09 .em-footer-top {
	padding-top:40px
}
.em-footer-style09 .em-footer-info-bottom {
	border-top-style:solid;
	border-top-width:1px;
	padding-top:45px;
	padding-bottom:15px;
	margin-top:20px
}
.em-footer-style09 .em-footer-bottom {
	padding-top:20px
}
.em-footer-style09 address {
	margin-top:5px
}
.em-footer-style09 .em-links li {
	padding:0 0 10px
}
.em-footer-style09 .em-wrapper-newsletter .block-subscribe {
	overflow:hidden
}
.em-footer-style09 .em-wrapper-newsletter .block-subscribe input.input-text {
	border-width:0
}
.em-footer-style09 .em-wrapper-newsletter .block-subscribe .actions button.button {
	font-size:100%
}
.em-footer-style09 .em-wrapper-social {
	overflow:hidden;
	margin-bottom:10px
}
.em-footer-style09 .em-wrapper-social .em-block-title {
	float:left;
	margin-right:20px;
	margin-top:10px
}
.em-footer-style09 .em-wrapper-social .em-block-content {
	overflow:hidden;
	float:left
}
.em-footer-style09 .em-wrapper-social .em-social .em-social-icon {
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:6px 8px;
	font-size:1.6rem
}
.em-rtl .em-newsletter-style05 .block-subscribe .actions button.button {
	right:auto;
	left:0;
	margin-left:0
}
.em-rtl .em-newsletter-style05 .block-subscribe .input-box input.input-text {
	padding-right:10px;
	padding-left:100px
}
.em-footer-style08 {
	padding-top:70px
}
.em-footer-style08 .em-block-content {
	margin-bottom:20px
}
.em-footer-style08 .four-block-middle {
	padding:40px 0 10px;
	margin:20px 0 30px;
	display:inline-block;
	width:100%;
	border-top-width:1px;
	border-bottom-width:1px;
	border-top-style:solid;
	border-bottom-style:solid
}
.em-footer-style07 .block-subscribe button.button, .em-footer-style08 .block-subscribe button.button {
	border-width:2px;
	border-style:solid
}
.em-footer-style08 .em-social .em-social-icon {
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:6px 8px;
	font-size:1.6rem
}
.em-footer-style08 .em-links li {
	margin-bottom:10px
}
.em-footer-style08 .em-container-ads {
	margin-bottom:30px
}
.em-footer-style08 .em-container-ads .box-date {
	display:inline-block;
	padding:8px 15px;
	text-transform:uppercase;
	margin:0 10px;
	float:left;
	text-align:center;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%
}
.em-footer-style08 .em-container-ads .box-date span {
	display:block;
	font-weight:700
}
.em-footer-style08 .em-container-ads .box-date span.em-blog-date {
	font-size:140%;
	line-height:1
}
.em-footer-style08 .em-container-ads .box-date span.em-blog-month {
	font-size:85%
}
.em-footer-style08 .em-container-ads .em-detail {
	overflow:hidden;
	padding:0 10px
}
.em-footer-style08 .block-info .em-address, .em-footer-style08 .block-info .em-email, .em-footer-style08 .block-info .em-phone {
	margin:0 -5px 1.55rem
}
.em-footer-style08 .block-info .fa {
	margin:0 5px
}
.em-rtl .em-footer-style08 .newsletter-boder-input-act .block-subscribe .actions {
	float:left
}
.em-rtl .em-footer-style08 .newsletter-boder-input-act .block-subscribe .actions button.button {
	margin-left:0
}
.em-rtl .em-footer-style08 .block-subscribe .input-box, .em-rtl .em-footer-style08 .em-container-ads .box-date {
	float:right
}
.em-footer-style12 .em-footer-top {
	padding-top:5.5rem;
	padding-bottom:4rem
}
.em-footer-style12 .em-footer-bottom {
	padding-top:35px
}
.em-footer-style12 address {
	margin-top:5px
}
.em-footer-style12 .em-links li {
	padding:0 0 10px
}
.em-footer-style12 .em-wrapper-social {
	overflow:hidden
}
.em-footer-style12 .em-wrapper-social .em-block-title {
	float:left;
	margin-right:20px;
	margin-top:10px
}
.em-footer-style12 .em-wrapper-social .em-block-content {
	overflow:hidden;
	float:left
}
.em-footer-style12 .em-wrapper-social .em-social .em-social-icon {
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:6px 8px;
	font-size:1.6rem
}
.em-footer-style07 .em-footer-top {
	padding-top:5rem;
	padding-bottom:3rem
}
.em-footer-style07 .em-footer-bottom {
	padding-top:3rem
}
.em-footer-style07 .em-social .em-social-icon {
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:6px 8px;
	font-size:1.6rem
}
.em-footer-style07 .em-links li {
	margin-bottom:10px
}
.em-footer-style07 .em-tags-list {
	padding-top:10px
}
.em-footer-style07 .em-tags-list li {
	display:inline-block;
	margin-right:10px;
	margin-bottom:10px
}
.em-footer-style07 .em-tags-list li a {
	padding:10px;
	border-width:1px;
	border-style:solid;
	display:inline-block
}
.em-rtl .em-footer-style07 .block-subscribe .input-box {
	float:right
}
.em-footer-style13 {
	padding-top:80px
}
.em-footer-style13 .em-footer-top {
	padding-bottom:80px
}
.em-footer-style13 .em-logo-footer {
	display:inline-block
}
.em-footer-style13 .newsletter-boder-input-act .block-subscribe {
	padding:9px 10px
}
.em-footer-style13 .newsletter-boder-input-act .block-subscribe .actions button.button {
	padding:9px 20px
}
.em-footer-style13 .block-subscribe button.button {
	border-style:solid;
	border-width:2px
}
.em-footer-style13 ul.footer-menu-links li {
	display:inline-block;
	margin:0 10px
}
.em-footer-style13 ul.footer-menu-links a {
	text-transform:uppercase
}
.em-footer-style13 .em-footer-bottom {
	border-top:1px solid #343434;
	padding-top:30px
}
.em-footer-style13 .em-footer-bottom a {
	text-transform:uppercase
}
.em-footer-style13 .em-footer-bottom ul.links {
	margin-bottom:0
}
.em-rtl .em-footer-style13 .newsletter-boder-input-act .block-subscribe .actions {
	float:left
}
.em-rtl .em-footer-style13 .newsletter-boder-input-act .block-subscribe .actions button.button {
	margin-left:0
}
.em-rtl .em-footer-style13 .block-subscribe .input-box {
	float:right
}
.em-footer-style14 {
	padding-top:80px
}
.em-footer-style14 .em-footer-top {
	padding-bottom:80px
}
.em-footer-style14 .em-logo-footer {
	display:inline-block
}
.em-footer-style14 .newsletter-boder-input-act .block-subscribe {
	padding:9px 10px
}
.em-footer-style14 .newsletter-boder-input-act .block-subscribe .actions button.button {
	padding:9px 20px
}
.em-payment-icon.em-express, .em-payment-icon.em-master, .em-payment-icon.em-other, .em-payment-icon.em-paypal, .em-payment-icon.em-visa, .em-pay, .em-sbi {
	height:22px;
	background-image:url(../img/icons.png);
	background-repeat:no-repeat
}
.em-footer-style14 .block-subscribe button.button {
	border-style:solid;
	border-width:2px
}
.em-footer-style14 ul.footer-menu-links {
	margin-bottom:40px
}
.em-footer-style14 ul.footer-menu-links li {
	display:inline-block;
	margin:0 20px
}
.em-footer-style14 .em-footer-bottom {
	border-top:1px solid #343434;
	padding-top:30px
}
.em-footer-style14 .em-footer-bottom a {
	text-transform:uppercase
}
.em-footer-style14 .em-footer-bottom ul.links {
	margin-bottom:20px
}
.em-rtl .em-footer-style14 .newsletter-boder-input-act .block-subscribe .actions {
	float:left
}
.em-rtl .em-footer-style14 .block-subscribe .input-box, .em-rtl .em-wrapper-newsletter03 .block-subscribe .input-box, .newsletter-boder-input-act .block-subscribe .actions {
	float:right
}
.em-rtl .em-footer-style14 .newsletter-boder-input-act .block-subscribe .actions button.button {
	margin-left:0
}
.em-wrapper-footer address {
	line-height:1.35;
	font-size:13px
}
.em-wrapper-footer .em-footer-bottom {
	padding-bottom:0
}
.em-payment {
	text-align:center;
	margin:0 -5px
}
.em-payment .em-payment-icon {
	margin:0 5px 10px;
-moz-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	float:left
}
.em-social-icon, .em-wrapper-ads-10 .em-box {
-moz-transition:all .5s ease;
-o-transition:all .5s ease;
	-webkit-backface-visibility:hidden
}
.em-payment-icon.em-visa {
	width:40px;
	background-position:0 -38px
}
.em-payment-icon.em-master {
	width:28px;
	background-position:-54px -38px
}
.em-payment-icon.em-paypal {
	width:57px;
	background-position:-122px -38px
}
.em-payment-icon.em-express {
	width:16px;
	background-position:-93px -38px
}
.em-payment-icon.em-other {
	width:68px;
	background-position:-187px -38px
}
.twitter-tweet {
	width:100%!important
}
.em-social {
	margin:0 -5px
}
.em-social .em-social-border {
	margin-bottom:5px
}
.em-social-icon {
	font-size:18px;
	margin:0 5px 5px;
transition:all .5s ease;
	backface-visibility:hidden
}
.em-wrapper-ads-10 .em-box, .em-wrapper-ads-13 .icon-banner-left {
-webkit-transition:all .5s ease;
	-moz-backface-visibility:hidden
}
.em-social-icon.last {
	margin-right:0
}
.em-social-border .em-social-icon {
	display:inline-block;
	padding:6px;
	border:1px solid
}
.multidealpro_products .products-grid {
	clear:both
}
.multidealpro_products .products-grid .item {
	width:263px;
	margin:0 10px;
	display:inline-table;
	float:none!important
}
.multidealpro_products .products-grid .item .product-item {
	padding:0;
	border:0
}
.em_multidealpro .price-box, .em_multidealpro .show_clock {
	display:inline-block;
	vertical-align:middle
}
.em_multidealpro .ratings {
	padding-bottom:0
}
.multidealpro_products .toolbar-top {
	margin:0 -10px;
	padding:0 20px
}
.products-grid .show_clock .clock {
	position:absolute
}
.multidealpro_recent .multidealpro_products {
	border:1px solid #e5e5e5;
	border-top:0
}
.show_clock .price-box p.special-price .price-label {
	display:inline-block
}
.show_clock .price-box p:first-child {
	margin-top:0
}
.em-special-deals {
	padding-bottom:20px
}
.em-special-deals .widget-title h2 {
	margin-bottom:15px
}
.em-special-deals .products-grid li.item {
	border:1px solid;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	padding:10px;
	text-align:center
}
.em-special-deals .products-grid .msg_soldout {
	margin-top:10px
}
.em-special-deals .products-grid .clock_style_1 {
	margin-top:3px;
	margin-bottom:0
}
.em-special-deals .owl-carousel .owl-item .item {
	border-width:1px;
	border-style:solid;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	padding:10px;
	text-align:center
}
.em-special-deals .owl-carousel .owl-item .item .product-shop {
	position:static
}
.em-special-deals .owl-carousel .owl-item .item .show_clock .clock {
	position:absolute;
	margin-bottom:0;
	top:260px;
	left:41px;
	text-align:center
}
.multidealpro_products {
	border:1px solid #e5e5e5;
	padding:10px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em_multidealpro .products-grid li.item {
	display:inline-block;
	float:none;
	vertical-align:top;
	border:1px solid #e8e8e8;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	padding:10px
}
.em_multidealpro .products-list, .multidealpro_products .products-list {
	clear:both;
	margin-top:20px
}
.em_multidealpro .products-list li.item, .multidealpro_products .products-list li.item {
	margin-bottom:20px
}
.em_multidealpro .products-list li.item .deal_qty, .em_multidealpro .products-list li.item .show_clock, .multidealpro_products .products-list li.item .deal_qty, .multidealpro_products .products-list li.item .show_clock {
	text-align:left
}
.em-sidebar .multidealpro_recent .multidealpro_products {
	-moz-border-radius:0 0 5px 5px;
	-webkit-border-radius:0 0 5px 5px;
	border-radius:0 0 5px 5px;
	padding-top:0
}
.em-sidebar .multidealpro_recent .multidealpro_products .products-grid li.item {
	padding:15px;
	margin:0;
	width:100%
}
.em-sidebar .em_multidealpro .products-grid li.item {
	width:100%;
	margin-right:0!important;
	border:0!important
}
.em_multidealpro .products-grid.owl-carousel .owl-item .item {
	min-height:425px
}
.em-wrapper-newsletter .em-block-title {
	margin-right:20px;
	margin-top:0
}
.block-subscribe {
	border:none;
	margin:0;
	padding:0
}
.block-subscribe:after, .block-subscribe:before {
	position:static;
	height:0
}
.block-subscribe .input-box {
	float:left;
	width:200px
}
.block-subscribe .actions {
	float:left
}
.block-subscribe .actions button.button {
	margin-right:0;
	padding:10px
}
.block-subscribe input.input-text {
	width:100%;
	margin-bottom:10px;
	white-space:nowrap;
	text-overflow:ellipsis;
	overflow:hidden
}
.newsletter-clearbutton .block-subscribe .actions {
	float:none
}
.newsletter-clearbutton .block-subscribe .actions button.button, .newsletter-clearbutton .block-subscribe .actions button.button span {
	float:none;
	display:inline-block
}
.newsletter-clearbutton .block-subscribe .input-box {
	width:100%;
	float:none
}
.newsletter-clearbutton .block-subscribe .input-box input.input-text {
	margin-right:0
}
.newsletter-fullbutton button.button {
	width:100%
}
.newsletter-boder-input-act .block-subscribe {
	border:1px solid;
	padding:15px 30px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.newsletter-boder-input-act .block-subscribe input.input-text {
	border:none;
	margin-bottom:0;
	background:0 0;
	height:50px
}
.em-background-wrapper-ads, .em-wrapper-newsletter03 {
	background-position:center top;
	background-size:cover!important;
	overflow:hidden
}
.newsletter-boder-input-act .block-subscribe .input-box {
	width:65%;
	margin-right:0
}
.newsletter-boder-input-act .block-subscribe .actions button.button {
	margin-bottom:0;
	padding:13px 20px
}
.em-wrapper-newsletter {
	margin-bottom:20px
}
.em-newsletter-style05 .block-subscribe {
	position:relative
}
.em-newsletter-style05 .block-subscribe .input-box {
	width:100%
}
.em-newsletter-style05 .block-subscribe .input-box input.input-text {
	padding-right:100px
}
.em-newsletter-style05 .block-subscribe .actions button.button {
	position:absolute;
	right:0;
	top:0;
	font-size:100%
}
.em-wrapper-newsletter03 {
	background-image:url(../images/em_ads_15.jpg);
	background-repeat:no-repeat;
	-webkit-background-size:cover;
	-moz-background-size:cover;
	-o-background-size:cover;
	padding:6rem 0
}
.em-wrapper-newsletter03 .block-subscribe {
	padding:10px
}
.em-wrapper-newsletter03 .block-subscribe .actions button.button {
	padding:9px 20px;
	border-width:2px
}
.em-wrapper-newsletter03 .block-subscribe input.input-text {
	height:40px
}
.em-rtl .em-wrapper-newsletter03 .block-subscribe .actions {
	float:left
}
.em-wrapper-ads-10 {
	overflow:hidden
}
.em-wrapper-ads-10.em-line-01 {
	margin-bottom:0
}
.em-wrapper-ads-10 img {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.em-wrapper-ads-10 .em-blog-item {
	padding:1px;
	margin-bottom:50px
}
.em-wrapper-ads-10 .em-blog-time {
	position:absolute;
	left:20px;
	top:20px;
	display:inline-block;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:13px 20px;
	z-index:0;
	font-weight:700;
	color:#fff;
	text-transform:uppercase
}
.em-wrapper-ads-10 .em-blog-time:before {
	content:"";
	background-color:#000;
	opacity:.6;
	filter:alpha(opacity=60);
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	display:inline-block;
	position:absolute;
	top:0;
	left:0;
	bottom:0;
	right:0;
	z-index:-1
}
.em-wrapper-ads-10 .em-blog-time p {
	margin:0;
	line-height:1.2
}
.em-wrapper-ads-10 .em-blog-time p:first-child {
	font-size:170%
}
.em-wrapper-ads-10 .em-box {
	padding:17px 20px 5px;
	border-top:0;
	-moz-border-radius:0 0 5px 5px;
	-webkit-border-radius:0 0 5px 5px;
	border-radius:0 0 5px 5px;
transition:all .5s ease;
	backface-visibility:hidden;
	position:relative
}
.em-wrapper-ads-10 .em-box .em-blog-des {
	clear:both
}
.em-wrapper-ads-10 .em-box .link-more {
	font-weight:700
}
.em-wrapper-ads-10 .em-box .link-more:before {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	content:"\f101";
	font-size:100%;
	padding-right:5px
}
.em-background-wrapper-ads {
	background-image:url(../images/media/home/layout_glass/em_ads_06.jpg);
	background-repeat:no-repeat;
	-webkit-background-size:cover;
	-moz-background-size:cover;
	-o-background-size:cover;
	padding:5rem 0 1rem
}
.em-rtl .em-wrapper-ads-10 .em-box .link-more:before {
	padding-right:0;
	padding-left:5px;
	content:"\f100"
}
.em-wrapper-ads-13 .icon-banner-left {
	padding:12px 15px 14px;
	margin-right:20px;
	font-size:1.6rem;
	-moz-border-radius:50%;
	-webkit-border-radius:50%;
	border-radius:50%;
-moz-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-wrapper-ads-13 .em-banner-right {
	overflow:hidden
}
.em-wrapper-ads-13 .em-banner-right p {
	margin:0;
	color:#212020
}
.em-wrapper-ads-13 .em-banner-right h5 {
margin-bottom:.5rem;
	font-weight:700;
	text-transform:uppercase
}
.em-wrapper-ads-13 .text-box {
	margin-bottom:20px
}
.em-sidebar .em-wrapper-ads-13 .text-box {
	clear:both;
	width:100%;
	border-top-width:1px;
	border-top-style:dashed;
	margin:2rem 0 0;
	padding:2rem 0 0
}
.em-sidebar .em-wrapper-ads-13 .text-box:first-child {
	border-top-width:0;
	margin-top:0;
	padding-top:0
}
.em-rtl .em-wrapper-ads-13 .icon-banner-left {
	margin-right:0;
	margin-left:20px
}
.em-wrapper-ads-15 {
	margin-bottom:50px
}
.em-wrapper-ads-15 .em-ads-item {
	padding:35px 20px 40px;
	margin-top:30px;
	position:relative;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em-wrapper-ads-15 .em-ads-item .em-ads-img {
	position:absolute;
	right:40px;
	top:-30px;
	width:70px;
	height:70px;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	border-style:solid;
	border-width:2px
}
.em-wrapper-ads-15 .em-ads-item .em-ads-img img {
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%
}
.em-wrapper-ads-15 .em-ads-item .em-ads-des .fa {
	font-size:300%
}
.em-wrapper-ads-15 .em-ads-item .em-ads-author span {
	font-weight:italic
}
.em-wrapper-ads-09 .em-wrapper-ads-item h4, .em-wrapper-ads-15 .em-ads-item .em-ads-author span:first-child {
	font-weight:700
}
.em-wrapper-ads-15 .owl-theme .owl-controls .owl-pagination {
	position:absolute;
	left:25px;
	bottom:20px;
	margin:0
}
.em-wrapper-ads-15 .owl-theme .owl-controls .owl-pagination span {
	border-width:2px;
	width:10px!important;
	height:10px!important;
	margin:0 0 0 5px!important
}
.em-rtl .em-wrapper-ads-15 .em-ads-item .em-ads-img {
	right:auto;
	left:40px
}
.em-rtl .em-wrapper-ads-15 .owl-theme .owl-controls .owl-pagination {
	left:auto;
	right:25px
}
.em-wrapper-ads-09 {
	margin-bottom:50px;
	border-width:1px;
	border-style:solid;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	padding:0 10px
}
.em-wrapper-ads-09 .line-right {
	margin-right:-1px;
	border-right-style:dotted
}
.em-wrapper-ads-09 .line-left {
	border-left-style:dotted
}
.em-wrapper-ads-09 .em-wrapper-ads-item {
	margin-top:30px;
	margin-bottom:30px
}
.em-wrapper-ads-09 .em-wrapper-ads-item .fa {
	font-size:340%
}
.em-wrapper-ads-09 .em-wrapper-ads-item .fa:hover {
	cursor:default
}
.em-wrapper-ads-09 .em-wrapper-ads-item .em-ads-content p {
	margin-bottom:0
}
.em-wrapper-ads-09 .em-wrapper-ads-item:nth-child(1) .fa:hover {
-webkit-animation-name:tada
}
.em-wrapper-ads-09 .em-wrapper-ads-item:nth-child(2) .fa:hover {
-webkit-animation-name:flash
}
.em-wrapper-ads-09 .em-wrapper-ads-item:nth-child(3) .fa:hover {
-webkit-animation-name:wobble
}
.em-wrapper-ads-09 .em-wrapper-ads-item:nth-child(4) .fa:hover {
-webkit-animation-name:rotateIn
}
.em-wrapper-ads-11 {
	margin-bottom:50px
}
.em-wrapper-ads-11 .em-ads-item {
	padding:3.2rem 2rem 4rem;
	position:relative;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em-wrapper-ads-11 .em-ads-item .em-ads-img {
	width:100px;
	height:100px;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%
}
.em-wrapper-ads-11 .em-ads-item .em-ads-img img {
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%
}
.em-wrapper-ads-11 .em-ads-item .em-ads-content {
	overflow:hidden;
	padding:0 20px
}
.em-wrapper-ads-11 .em-ads-item .em-ads-des .fa {
	font-size:300%
}
.em-wrapper-ads-11 .em-ads-item .em-ads-author span {
	font-weight:italic
}
.em-wrapper-ads-11 .em-ads-item .em-ads-author span:first-child, .em-wrapper-ads-12 .link-more {
	font-weight:700
}
.em-wrapper-ads-11 .owl-theme .owl-controls .owl-pagination {
	position:absolute;
	left:10%;
	bottom:20px;
	margin:0
}
.em-wrapper-ads-11 .owl-theme .owl-controls .owl-pagination span {
	border-width:2px;
	width:10px!important;
	height:10px!important;
	margin:0 0 0 5px!important
}
.em-wrapper-ads-12, .em-wrapper-ads-16 .em-box {
	border-width:1px;
	border-style:solid;
	overflow:hidden
}
.em-rtl .em-wrapper-ads-11 .owl-theme .owl-controls .owl-pagination {
	left:auto;
	right:10%
}
.em-wrapper-ads-12 {
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	margin-bottom:50px
}
.em-wrapper-ads-12.em-line-01 {
	margin-bottom:0
}
.em-wrapper-ads-12 img {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.em-wrapper-ads-12 .em-blog-content {
	padding-left:0
}
.em-wrapper-ads-12 .em-blog-time {
	position:absolute;
	left:20px;
	top:20px;
	display:inline-block;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	padding:13px 20px;
	z-index:0;
	font-weight:700;
	color:#fff;
	text-transform:uppercase
}
.em-wrapper-ads-12 .em-blog-time:before {
	content:"";
	background-color:#000;
	opacity:.6;
	filter:alpha(opacity=60);
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	display:inline-block;
	position:absolute;
	top:0;
	left:0;
	bottom:0;
	right:0;
	z-index:-1
}
.em-wrapper-ads-12 .em-blog-time p {
	margin:0;
	line-height:1.2
}
.em-wrapper-ads-12 .em-blog-time p:first-child {
	font-size:170%
}
.em-wrapper-ads-12 .link-more:before, .em-wrapper-ads-16 .em-box .em-box-left .block-info li.em-links-item a:before {
	font-family:FontAwesome;
	font-style:normal;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	font-size:100%;
	display:inline-block
}
.em-wrapper-ads-12 .em-blog-title {
	margin-top:2rem
}
.em-wrapper-ads-12 .em-blog-des {
	clear:both
}
.em-wrapper-ads-12 .link-more:before {
	font-weight:400;
	content:"\f101";
	padding-right:5px
}
.em-rtl .em-wrapper-ads-12 .link-more:before {
	padding-right:0;
	padding-left:5px;
	content:"\f100"
}
.em-rtl .em-wrapper-ads-12 .em-blog-content {
	padding-right:0;
	padding-left:10px
}
.em-wrapper-ads-16 {
	margin-bottom:30px
}
.em-wrapper-ads-16 .em-box {
	position:relative;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	margin-bottom:20px;
	padding:2.5rem 3rem
}
.em-wrapper-ads-16 .em-box h4 {
	font-weight:700;
	text-transform:uppercase
}
.em-wrapper-ads-16 .em-box .em-box-right {
	position:absolute;
	right:0;
	bottom:0
}
.em-wrapper-ads-16 .em-box .em-box-left {
	position:relative;
	z-index:1
}
.em-wrapper-ads-16 .em-box .em-box-left .block-info li.em-links-item {
	padding-top:1rem
}
.em-wrapper-ads-16 .em-box .em-box-left .block-info li.em-links-item a {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-wrapper-ads-16 .em-box .em-box-left .block-info li.em-links-item a span {
	margin:0 10px
}
.em-wrapper-ads-16 .em-box .em-box-left .block-info li.em-links-item a:before {
	font-weight:400;
	content:"\f101"
}
.em-wrapper-ads-16 .em-box .em-box-left .block-info li.em-links-item a:hover {
	margin-left:10px
}
.em-wrapper-ads-17 {
	background-image:url(../images/media/home/bkg-parallax.jpg);
	background-position:center center;
	background-repeat:no-repeat;
	background-attachment:fixed;
	overflow:hidden;
	background-size:cover!important;
	-webkit-background-size:cover;
	-moz-background-size:cover;
	-o-background-size:cover;
	filter:inherit;
	padding:15.5rem 0;
	text-align:center;
	margin-bottom:55px
}
.em-wrapper-ads-17 .button-link {
	float:none;
	display:inline-block;
	padding:13px 20px
}
@media only screen and (min-device-width:0) and (max-device-width:1024px) {
.em-wrapper-ads-17 {
background-attachment:scroll!important
}
}
.em-rtl .em-box .em-box-right {
	left:0;
	right:auto
}
.em-rtl .em-box .em-box-left .block-info li.em-links-item a:before {
	content:"\f100"
}
.em-wrapper-product-04 .owl-theme .owl-controls {
	text-align:left;
	padding:1rem
}
.em-wrapper-product-04 .owl-theme .owl-controls .owl-buttons div {
	position:static;
margin-right:.5rem!important
}
.em-wrapper-product-12 {
	margin-bottom:20px
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs {
	margin-bottom:10px
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-nav {
	padding-left:0;
	padding-right:0
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-nav .r-tabs-anchor {
	padding:0!important;
	margin-right:0;
	margin-bottom:0;
	display:block;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	text-transform:none
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-nav .r-tabs-anchor img {
	width:100%;
	height:auto;
	-moz-border-radius:5px 5px 0 0;
	-webkit-border-radius:5px 5px 0 0;
	border-radius:5px 5px 0 0
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-nav .r-tabs-tab {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	width:-moz-calc(100%/3);
	width:-webkit-calc(100%/3);
	width:-o-calc(100%/3);
	width:calc(100%/3);
	float:left;
	vertical-align:top;
	padding:0 10px;
	margin-bottom:20px
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-nav .product-thumb-tab {
	border:1px solid #e1e1e1;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	text-align:center;
	padding-bottom:18px
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-nav .product-thumb-tab .product-thumb a span {
	padding:0 20px;
	text-align:center;
	display:inline-block;
	max-height:35px;
	margin:20px 0 10px;
	overflow:hidden
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-nav .product-thumb-tab .price-box {
	text-align:center
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-nav .product-thumb-tab .ratings a {
	display:inline-block
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-panel {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	opacity:0;
	filter:alpha(opacity=0)
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-panel a.product-image .em-alt-hover {
	position:absolute;
	left:0;
	top:0;
	opacity:0!important;
	filter:alpha(opacity=0)!important;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-panel:hover a.product-image .em-alt-org {
	opacity:0!important;
	filter:alpha(opacity=0)!important;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-panel:hover a.product-image .em-alt-hover {
	opacity:1!important;
	filter:alpha(opacity=100)!important;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	position:absolute;
	left:0;
	top:0
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-panel.r-tabs-state-active {
	opacity:1;
	filter:alpha(opacity=100);
	padding:0!important;
	margin-bottom:20px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	border:1px solid #e1e1e1
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-panel .product-image {
	display:block;
	margin-bottom:20px;
	position:relative;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-panel .product-image img {
	max-width:100%;
	display:block
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-accordion-title .r-tabs-anchor img {
	display:none
}
.em-wrapper-product-12 .em-filterproducts-grid .r-tabs .r-tabs-accordion-title .r-tabs-anchor span {
	display:block
}
.em-wrapper-product-12 .em-filterproducts-grid .productlabels_icons {
	display:none
}
.em-wrapper-product-12 .em-filterproducts-grid .em-item-tabs .em-more-views {
	clear:both;
	border-top:1px solid #e1e1e1;
	margin:20px 0 0;
	padding:20px 10px;
	width:100%;
	display:inline-block;
	position:relative
}
.em-wrapper-product-12 .em-filterproducts-grid .em-item-tabs .em-more-views:after, .em-wrapper-product-12 .em-filterproducts-grid .em-item-tabs .em-more-views:before {
	position:absolute;
	left:49%;
	top:-19px;
	content:"";
	width:15px;
	height:12px;
	z-index:1;
	border:9px solid transparent;
	border-bottom-color:#eee
}
.em-wrapper-product-12 .em-filterproducts-grid .em-item-tabs .em-more-views:after {
	border-bottom-color:#fff;
	top:-17px
}
.em-wrapper-product-12 .em-filterproducts-grid .em-item-tabs .em-more-views img {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.em-wrapper-product-12 .em-filterproducts-grid .em-item-tabs .product-shop {
	display:block;
	overflow:hidden;
	padding:0 20px;
	text-align:center
}
.em-wrapper-product-15 {
	border-style:solid;
	border-width:1px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	overflow:hidden;
	margin-bottom:50px
}
.em-wrapper-product-15 .em-widget-title {
	padding:13px 20px;
	text-transform:uppercase;
	-moz-border-radius:5px 5px 0 0;
	-webkit-border-radius:5px 5px 0 0;
	border-radius:5px 5px 0 0
}
.em-wrapper-product-15 .em-widget-title h2, .em-wrapper-product-15 .em-widget-title h3 {
	margin:0
}
.em-wrapper-product-15 .products-list {
	display:flex;
	width:100%;
	padding:0 20px;
	clear:both
}
.em-wrapper-product-15 .products-list:first-child div.item {
	border-top-width:0;
	padding-top:0;
	padding-bottom:20px;
	margin-top:20px;
	margin-bottom:0
}
.em-wrapper-product-15 .products-list div.item {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	width:-moz-calc(100%/3);
	width:-webkit-calc(100%/3);
	width:-o-calc(100%/3);
	width:calc(100%/3);
	padding:20px 10px 0;
	float:left;
	border:none;
	border-top-style:dotted;
	border-top-width:1px;
	border-right-style:dotted;
	border-right-width:1px;
	margin:0 0 20px
}
.em-wrapper-product-15 .products-list div.item.first, .em-wrapper-product-16 .products-list {
	padding-left:0
}
.em-wrapper-product-15 .products-list div.item.last {
	border-right-width:0;
	padding-right:0
}
.em-wrapper-product-16 {
	border-style:solid;
	border-width:1px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	overflow:hidden;
	margin-bottom:50px
}
.em-wrapper-product-16 .em-widget-title {
	padding:13px 20px;
	text-transform:uppercase;
	-moz-border-radius:5px 5px 0 0;
	-webkit-border-radius:5px 5px 0 0;
	border-radius:5px 5px 0 0
}
.em-wrapper-product-16 .em-widget-title h2, .em-wrapper-product-16 .em-widget-title h3 {
	margin:0
}
.em-wrapper-product-16 .em-inner-product {
	clear:both;
	width:100%;
	padding:3rem 2rem;
	float:left
}
.em-wrapper-product-16 .em-item-big {
	margin-right:-1px;
	text-align:center
}
.em-wrapper-product-16 .em-item-big .item {
	margin-bottom:0
}
.em-wrapper-product-16 .em-item-big .product-item {
	border-width:0
}
.em-wrapper-product-16 .em-item-big .product-item a.product-image {
	position:relative
}
.em-wrapper-product-16 .em-item-big .product-item a.product-image:after {
	content:"";
	position:absolute;
	left:50%;
	right:50%;
	top:50%;
	bottom:50%;
	z-index:0;
	display:inline-block;
	opacity:0;
	filter:alpha(opacity=0);
transition:all .3s ease-in-out 0s;
-moz-transition:all .3s ease-in-out 0s;
-webkit-transition:all .3s ease-in-out 0s;
	background-color:#000
}
.em-wrapper-product-16 .em-item-big .product-item:hover a.product-image:after {
	opacity:.6;
	filter:alpha(opacity=60);
	left:-10px;
	right:-10px;
	top:-10px;
	bottom:0
}
.em-wrapper-product-16 .products-list .item {
	padding:2.8rem 1rem 1.8rem 3rem;
	margin:0;
	border-width:1px 0 0;
	border-top-style:dotted;
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.em-wrapper-product-16 .products-list .item.first, .em-wrapper-product-16 .products-list .item:first-child {
	border-top-color:transparent;
	padding-top:0
}
.em-wrapper-product-16 .products-list .item a.product-image {
	margin-bottom:1rem
}
.em-wrapper-product-16 .products-list .item a.product-image.loading-process {
	width:120px
}
.em-rtl .em-wrapper-product-16 .em-item-big {
	margin-right:0;
	margin-left:-1px
}
.em-rtl .em-wrapper-product-16 .products-grid .item {
	margin-right:0;
	margin-left:2rem
}
.em-rtl .em-wrapper-product-16 .line-right {
	border-right-width:0;
	border-left-width:1px;
	border-left-style:solid
}
.em-rtl .em-wrapper-product-16 .line-left {
	border-left-width:0;
	border-right-width:1px;
	border-right-style:solid
}
.em-rtl .em-wrapper-product-16 .products-list .item {
	padding-left:1rem;
	padding-right:3rem
}
.em-wrapper-product-18 {
	border-style:solid;
	border-width:1px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	overflow:hidden;
	margin-bottom:50px;
	display:flex
}
.em-wrapper-product-18 .em-widget-title {
	padding:13px 30px;
	text-transform:uppercase
}
.em-wrapper-product-18 .em-widget-title h2, .em-wrapper-product-18 .em-widget-title h3 {
	margin:0
}
.em-wrapper-product-18>div {
	align-items:stretch
}
.em-wrapper-product-18 .em-line {
	border-left:1px solid #e1e1e1;
	border-right:1px solid #e1e1e1
}
.em-wrapper-product-18 .em-wrapper-area06 {
	display:flex
}
.em-wrapper-product-18 .em-featured-products {
	width:100%
}
.em-wrapper-product-18 .em-featured-products .em-featured-top {
	margin-bottom:10px
}
.em-wrapper-product-18 .em-featured-products .em-widget-products {
	padding-top:10px
}
.em-wrapper-product-18 .em-featured-products .products-list {
	display:flex;
	width:100%;
	padding:0 40px;
	clear:both
}
.em-wrapper-product-18 .em-featured-products .products-list:first-child div.item {
	border-top-width:0;
	padding-top:0;
	padding-bottom:23px;
	margin-top:20px;
	margin-bottom:0
}
.em-wrapper-product-18 .em-featured-products .products-list div.item {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0;
	width:-moz-calc(100%/2);
	width:-webkit-calc(100%/2);
	width:-o-calc(100%/2);
	width:calc(100%/2);
	padding:20px 10px 0;
	float:left;
	border:none;
	border-top-style:dotted;
	border-top-width:1px;
	border-right-style:dotted;
	border-right-width:1px;
	margin:0 0 20px
}
.em-wrapper-product-18 .em-featured-products .products-list div.item.last {
	border-right-width:0;
	padding-right:0
}
.em-wrapper-product-18 .em-featured-products .products-list div.item.first {
	padding-left:0
}
.em-wrapper-product-18 .em-sale-off .products-list {
	padding:30px 30px 15px
}
.em-wrapper-product-18 .em-sale-off .products-list div.item {
	padding:20px 0 0;
	border-top:1px dashed #eee
}
.em-wrapper-product-18 .em-sale-off .products-list div.item:first-child {
	border-color:transparent;
	padding-top:0
}
.em-wrapper-product-18 .em-sale-off .products-list div.item.last {
	padding-bottom:0;
	margin-bottom:0
}
.em-wrapper-product-18 .em-sale-off .products-list div.item .product-shop, .em-wrapper-product-18 .em-sale-off .products-list div.item .product-shop .price-box {
	margin-bottom:0
}
.em-wrapper-product-18 .em-best-products .products-grid .item {
	margin-right:0;
	border:0;
	width:100%
}
.em-wrapper-product-18 .em-best-products .products-grid .item .product-item {
	border:0;
	padding:30px 30px 10px;
	-webkit-box-shadow:none;
	-moz-box-shadow:none;
	box-shadow:none
}
.em-wrapper-product-18 .em-best-products .products-grid .item .product-item img {
	-moz-border-radius:0;
	-webkit-border-radius:0;
	border-radius:0
}
.em-wrapper-product-18 .em-best-products .products-grid .item .product-item .em-more-views {
	margin:0 -30px;
	padding:20px 30px 0;
	border-top:1px solid #e1e1e1;
	position:relative
}
.em-wrapper-product-18 .em-best-products .products-grid .item .product-item .em-more-views:after, .em-wrapper-product-18 .em-best-products .products-grid .item .product-item .em-more-views:before {
	position:absolute;
	left:46%;
	top:-19px;
	content:"";
	width:15px;
	height:12px;
	z-index:1;
	border:9px solid transparent;
	border-bottom-color:#eee
}
.em-wrapper-product-18 .em-best-products .products-grid .item .product-item .em-more-views:after {
	border-bottom-color:#fff;
	top:-17px
}
.em-rtl .em-wrapper-product-15 .products-list div.item {
	border-right-width:0;
	border-left-style:dotted;
	border-left-width:1px;
	padding-left:10px;
	padding-right:10px
}
.em-rtl .em-wrapper-product-15 .products-list div.item.first {
	padding-right:0
}
.em-rtl .em-wrapper-product-15 .products-list div.item.last {
	padding-left:0;
	padding-right:10px;
	border-left-width:0
}
.em-wrapper-product-19 {
	margin-bottom:50px
}
.em-wrapper-product-19 .product-item {
	display:inline-block;
	width:100%;
	border-style:solid;
	border-width:1px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em-wrapper-product-19 .product-item .big-img {
	border-bottom-style:solid;
	border-bottom-width:1px;
	overflow:hidden
}
.em-wrapper-product-19 .product-item .big-img .product-image {
	float:left;
	width:73%;
	margin:0;
	position:relative
}
.em-wrapper-product-19 .product-item .big-img .product-image:before {
	border-right-style:solid;
	border-right-width:1px;
	content:'';
	position:absolute;
	top:0;
	right:-1px;
	height:100%;
	width:1px
}
.em-wrapper-product-19 .product-item .big-img .em-more-views {
	overflow:hidden;
	width:27%;
	position:relative
}
.em-wrapper-product-19 .product-item .big-img .em-more-views:before {
	border-right-style:solid;
	border-right-width:1px;
	content:'';
	position:absolute;
	top:0;
	left:0;
	height:100%;
	width:1px
}
.em-wrapper-product-19 .product-item .big-img .em-more-views .more-view-img {
	border-top-style:solid;
	border-top-width:1px;
	text-align:center
}
.em-wrapper-product-19 .product-item .big-img .em-more-views .more-view-img:first-child {
	border-top-width:0
}
.em-wrapper-product-19 .product-item .big-img img {
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	width:100%
}
.em-wrapper-product-19 .product-item .product-shop {
	overflow:hidden;
	clear:both;
	padding:20px 30px 30px
}
.em-wrapper-product-19 .product-item .product-shop .price-box {
	float:left;
	margin-bottom:1.55rem;
	margin-right:10px
}
.em-wrapper-product-19 .product-item .product-shop .em-btn-addto, .toolbar .sort-by>a, .toolbar .sort-by>a .v-middle {
	float:right
}
.em-wrapper-product-20 {
	margin-bottom:50px
}
.em-wrapper-product-20 .em-item-right {
	padding:0
}
.em-wrapper-product-20 .em-item-right .item {
	padding:0 1rem;
	width:50%;
	float:left
}
.em-wrapper-product-20 .em-item-right .item img {
	width:100%;
	height:auto
}
.em-wrapper-product-20 .product-item {
	position:relative
}
.em-wrapper-product-20 .product-item .product-shop-top .price-box {
	position:absolute;
	left:0;
	bottom:0;
	margin:0;
	padding:1rem;
	display:inline-block
}
.em-wrapper-product-20 .product-item .product-shop-top img {
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	width:100%;
	height:auto
}
.em-wrapper-product-20 .product-item .product-shop {
	position:absolute;
	left:0;
	bottom:0;
	right:0;
	height:0;
	top:100%;
	padding:1rem;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	opacity:0;
	filter:alpha(opacity=0);
	visibility:hidden;
	background-color:rgba(0, 0, 0, .25);
	text-align:center
}
.toolbar .sort-by>a, .view-mode .list {
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden
}
.em-wrapper-product-20 .product-item .product-shop .product-name a, .em-wrapper-product-20 .product-item .product-shop .ratings .amount a {
	color:#fff
}
.em-wrapper-product-20 .product-item:hover .product-shop {
	height:100%;
	top:0;
	opacity:1;
	filter:alpha(opacity=100);
	visibility:initial;
	display:flex;
	align-items:center;
	align-content:center;
	justify-content:center
}
.em-wrapper-product-23.em-line-01 {
	margin-bottom:0
}
.em-wrapper-product-23 .em-item-big {
	text-align:center
}
.em-wrapper-product-23 .em-item-big .item .product-item, .em-wrapper-product-23 .em-item-big .item .product-shop {
	padding-bottom:20px
}
.em-wrapper-product-23 .em-item-big .item .em-more-views {
	margin:20px 0 0;
	padding:20px 30px 0;
	border-top:1px solid #e1e1e1;
	position:relative;
	display:inline-block;
	vertical-align:top;
	width:100%
}
.em-wrapper-product-23 .em-item-big .item .em-more-views:after, .em-wrapper-product-23 .em-item-big .item .em-more-views:before {
	position:absolute;
	left:46%;
	top:-19px;
	content:"";
	width:15px;
	height:12px;
	z-index:99999;
	border:9px solid transparent;
	border-bottom-color:#eee
}
.input, .input__field, .input__label-content, .toolbar, .toolbar .sort-by>a {
	position:relative
}
.em-wrapper-product-23 .em-item-big .item .em-more-views:after {
	border-bottom-color:#fff;
	top:-17px
}
.em-wrapper-product-23 .em-item-right .item {
	margin-bottom:20px
}
.breadcrumbs ul, .em-wrapper-product-23 .em-item-right .item .product-shop .price-box {
	margin-bottom:0
}
.em-wrapper-product-23 .em-item-right .item .product-shop {
	padding-bottom:8px
}
.em-wrapper-product-23 .products-grid .item {
	margin-right:0
}
.em-wrapper-main .container-main {
	padding-top:20px
}
.em-wrapper-main .container-main.container-fluild {
	padding-top:0
}
.em-box {
	border-style:solid;
	border-width:1px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.em-line-top-bottom {
	border-top:1px solid #e8e8e8;
	border-bottom:1px solid #e8e8e8
}
.breadcrumbs {
	padding:2rem 0;
	overflow:hidden
}
.breadcrumbs ul li {
	display:inline;
	text-transform:uppercase
}
.breadcrumbs ul li strong {
	font-weight:600
}
.breadcrumbs ul li .separator:before, .toolbar .toolbar-dropdown span.current:after, .view-mode .grid:before, .view-mode .list:before {
	font-family:FontAwesome;
	font-weight:400;
	font-style:normal;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale
}
.breadcrumbs ul li .separator {
	position:relative;
	font-size:0;
	margin:0 12px
}
.breadcrumbs ul li .separator:before {
	display:inline-block;
	content:"\f178";
	font-size:11px
}
.wrapper-breadcrums {
	background-image:url(../images/bkg-breadcrums.png);
	background-position:0 0;
	background-repeat:repeat
}
.category-description, .category-image, .category-title {
	margin-bottom:20px
}
.category-products .products-list {
	margin:0
}
.category-products .products-list li.item {
	padding:10px;
	border:1px solid #e1e1e1;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	margin-bottom:20px
}
.toolbar .sort-by>a, .view-mode {
	border-style:solid;
	border-width:1px
}
.category-products .products-list li.item .product-shop, .category-products .products-list li.item a.product-image, .toolbar {
	margin-bottom:0
}
.category-products .products-grid .item .product-name {
	overflow:hidden;
	text-overflow:ellipsis;
	white-space:nowrap
}
.category-products .products-grid .item:hover .product-name {
	white-space:normal
}
.category-products .pager, .category-products .pager .amount, .category-products .pager .pages, .category-products .to-top {
	display:none
}
.toolbar {
	width:100%
}
.toolbar .limiter, .toolbar .sort-by {
	margin-bottom:2rem
}
.toolbar .toolbar-switch {
	display:inline-block;
	float:right
}
.toolbar .toolbar-dropdown {
	width:150px
}
.toolbar .toolbar-dropdown span.current {
	padding:6px 10px;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	font-size:90%
}
.toolbar .toolbar-dropdown span.current:after {
	margin-top:5px;
	display:inline-block;
	content:"\f078";
	font-size:10px
}
.toolbar .toolbar-dropdown ul {
	width:150px;
	top:28px
}
.toolbar .sort-by {
	margin-left:2rem
}
.toolbar .sort-by>a {
	margin-left:2rem;
	padding:1rem 1.4rem;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
transition:all .5s ease;
	backface-visibility:hidden
}
.view-mode .grid, .view-mode .list {
	vertical-align:middle;
	text-indent:0;
	float:left;
	overflow:hidden;
	text-align:left
}
.toolbar-bottom {
	margin-bottom:0!important
}
.toolbar-bottom .pager {
	display:block;
	margin:0
}
.toolbar-bottom .pager .amount {
	display:inline;
	margin:0;
	padding:13px 0
}
.toolbar-bottom .pager .pages {
	display:inline;
	margin:0
}
.toolbar-bottom .toolbar .pager .limiter, .toolbar-bottom .toolbar .sorter, .view-mode label {
	display:none
}
.view-mode {
padding:.9rem 1rem;
	margin-bottom:2rem;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px
}
.view-mode .list {
	font-size:0;
	display:block;
transition:all .5s ease;
	backface-visibility:hidden
}
.view-mode .list:before {
	display:inline-block;
	content:"\f00b";
	font-size:14px
}
.view-mode .grid {
	font-size:0;
	display:block;
-moz-transition:all .5s ease;
-webkit-transition:all .5s ease;
-o-transition:all .5s ease;
transition:all .5s ease;
	-moz-backface-visibility:hidden;
	-webkit-backface-visibility:hidden;
	backface-visibility:hidden;
	margin-right:1rem;
	padding-right:1rem;
	border-right-style:solid;
	border-right-width:1px
}
.view-mode .grid:before {
	display:inline-block;
	content:"\f00a";
	font-size:14px
}
.emcatalog-desktop-1, .emcatalog-desktop-small-1, .emcatalog-mobile-1, .emcatalog-tablet-1 {
	margin:0 -10px
}
.emcatalog-desktop-1 .item, .emcatalog-desktop-small-1 .item, .emcatalog-mobile-1 .item, .emcatalog-tablet-1 .item {
	width:100%;
	padding:0 10px;
	margin:0 0 20px;
	border:none
}
.emcatalog-desktop-1 .item .product-item, .emcatalog-desktop-small-1 .item .product-item, .emcatalog-mobile-1 .item .product-item, .emcatalog-tablet-1 .item .product-item {
	margin:0
}
.emcatalog-desktop-2, .emcatalog-desktop-small-2, .emcatalog-mobile-2, .emcatalog-tablet-2 {
	margin:0 -10px
}
.emcatalog-desktop-2 .item, .emcatalog-desktop-small-2 .item, .emcatalog-mobile-2 .item, .emcatalog-tablet-2 .item {
	width:50%;
	padding:0 10px;
	margin:0 0 20px;
	border:none
}
.emcatalog-desktop-2 .item .product-item, .emcatalog-desktop-small-2 .item .product-item, .emcatalog-mobile-2 .item .product-item, .emcatalog-tablet-2 .item .product-item {
	margin:0
}
.emcatalog-desktop-3, .emcatalog-desktop-small-3, .emcatalog-mobile-3, .emcatalog-tablet-3 {
	margin:0 -10px
}
.emcatalog-desktop-3 .item, .emcatalog-desktop-small-3 .item, .emcatalog-mobile-3 .item, .emcatalog-tablet-3 .item {
	width:33.33333%;
	padding:0 10px;
	margin:0 0 45px;
	border:none
}
.emcatalog-desktop-3 .item .product-item, .emcatalog-desktop-small-3 .item .product-item, .emcatalog-mobile-3 .item .product-item, .emcatalog-tablet-3 .item .product-item {
	margin:0
}
.emcatalog-desktop-4, .emcatalog-desktop-small-4, .emcatalog-mobile-4, .emcatalog-tablet-4 {
	margin:0 -10px
}
.emcatalog-desktop-4 .item, .emcatalog-desktop-small-4 .item, .emcatalog-mobile-4 .item, .emcatalog-tablet-4 .item {
	width:25%;
	padding:0 10px;
	margin:0 0 20px;
	border:none
}
.emcatalog-desktop-4 .item .product-item, .emcatalog-desktop-small-4 .item .product-item, .emcatalog-mobile-4 .item .product-item, .emcatalog-tablet-4 .item .product-item {
	margin:0
}
.emcatalog-desktop-5, .emcatalog-desktop-small-5, .emcatalog-mobile-5, .emcatalog-tablet-5 {
	margin:0 -10px
}
.emcatalog-desktop-5 .item, .emcatalog-desktop-small-5 .item, .emcatalog-mobile-5 .item, .emcatalog-tablet-5 .item {
	width:20%;
	padding:0 10px;
	margin:0 0 20px;
	border:none
}
.emcatalog-desktop-5 .item .product-item, .emcatalog-desktop-small-5 .item .product-item, .emcatalog-mobile-5 .item .product-item, .emcatalog-tablet-5 .item .product-item {
	margin:0
}
.emcatalog-desktop-6, .emcatalog-desktop-small-6, .emcatalog-mobile-6, .emcatalog-tablet-6 {
	margin:0 -10px
}
.emcatalog-desktop-6 .item, .emcatalog-desktop-small-6 .item, .emcatalog-mobile-6 .item, .emcatalog-tablet-6 .item {
	width:16.66667%;
	padding:0 10px;
	margin:0 0 20px;
	border:none
}
.emcatalog-desktop-6 .item .product-item, .emcatalog-desktop-small-6 .item .product-item, .emcatalog-mobile-6 .item .product-item, .emcatalog-tablet-6 .item .product-item {
	margin:0
}
.breadcrumbs ul li a span, .home a {
	color:#2B2A2A
}
.map {
	margin-bottom:30px
}
.input {
	z-index:1;
	display:inline-block;
	margin:1em 0;
	max-width:350px;
	width:calc(100% - 2em);
	vertical-align:top;margin-left: 14px;
}
.input__field {
	display:block;
	float:right;
	padding:.8em;
	width:60%;
	border:none;
	border-radius:0;
	font-size:15px;
	background:#f0f0f0;
	-webkit-appearance:none
}
.input__label {
	display:inline-block;
	float:right;
	padding:0 1em;
	width:40%;
	color:#6a7989;
	font-weight:700;
	font-size:70.25%;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	-webkit-user-select:none;
	-moz-user-select:none;
	-ms-user-select:none;
	user-select:none
}
.input__label-content {
	display:block;
	padding:.6em 0;
	width:100%;
	font-size:14px
}
.btn-4, .content-form, .input--hoshi {
	overflow:hidden
}
.graphic, .input__label--hoshi, .input__label-content--hoshi {
	position:absolute
}
.graphic {
	top:0;
	left:0;
	fill:none
}
.icon {
	color:#ddd;
	font-size:150%
}
.input__field--hoshi {
	margin-top:1em;
	padding:1em .15em;
	width:100%;
	background:0 0;
	color:#595F6E
}
.input__label--hoshi {
	bottom:0;
	left:0;
	padding:0 .25em;
	width:100%;
	height:calc(100% - 1em);
	text-align:left;
	pointer-events:none
}
.input__label--hoshi::after, .input__label--hoshi::before {
content:'';
position:absolute;
top:0;
left:0;
width:100%;
height:calc(100% - 10px);
border-bottom:1px solid #B9C1CA
}
.input__label--hoshi::after {
margin-top:2px;
border-bottom:4px solid red;
-webkit-transform:translate3d(-100%, 0, 0);
transform:translate3d(-100%, 0, 0);
-webkit-transition:-webkit-transform .3s;
transition:transform .3s
}
.btn1, .btn1:after {
-webkit-transition:all .3s;
-moz-transition:all .3s
}
.input__label--hoshi-color-1::after {
border-color:#00a9ff
}
.input__label--hoshi-color-2::after {
border-color:#0fa
}
.input__label--hoshi-color-3::after {
border-color:#f50
}
.input--filled .input__label--hoshi::after, .input__field--hoshi:focus+.input__label--hoshi::after {
-webkit-transform:translate3d(0, 0, 0);
transform:translate3d(0, 0, 0)
}
.input--filled .input__label-content--hoshi, .input__field--hoshi:focus+.input__label--hoshi .input__label-content--hoshi {
-webkit-animation:anim-1 .3s forwards;
animation:anim-1 .3s forwards
}
@-webkit-keyframes anim-1 {
50% {
opacity:0;
-webkit-transform:translate3d(1em, 0, 0);
transform:translate3d(1em, 0, 0)
}
51% {
opacity:0;
-webkit-transform:translate3d(-1em, -40%, 0);
transform:translate3d(-1em, -40%, 0)
}
100% {
opacity:1;
-webkit-transform:translate3d(0, -40%, 0);
transform:translate3d(0, -40%, 0)
}
}
@keyframes anim-1 {
50% {
opacity:0;
-webkit-transform:translate3d(1em, 0, 0);
transform:translate3d(1em, 0, 0)
}
51% {
opacity:0;
-webkit-transform:translate3d(-1em, -40%, 0);
transform:translate3d(-1em, -40%, 0)
}
100% {
opacity:1;
-webkit-transform:translate3d(0, -40%, 0);
transform:translate3d(0, -40%, 0)
}
}
.topcart-content {
	width:100%;
	margin:0 auto
}
#CartItems {
	max-height:200px;
	overflow-y:scroll;
	padding:0 6px 0 0
}
#CartItems .item-in-cart {
	margin:0 0 5px;
	padding:0 0 5px;
	border-bottom:1px dotted #4CBFBB
}
#CartItems .image {
	float:left;
	margin-right:5px;
	width:24%
}
#CartItems .desc {
	float:left;
	margin:0 5px 0 0;
	width:45%
}
#CartItems .desc a {
	color:#4A4A4A;
	font-size:13px
}
#CartItems .amt-detail {
	float:right;
	width:27%
}
.amt-detail .price, .amt-detail .quantity {
	font-size:12px;
	text-align:right
}
.amt-detail .price {
	color:#434343;
	font-weight:500;
	margin:10px 0
}
.total-amount {
	background:#E2B71A;
	padding:8px;
	font-size:13px;
	color:#FFF;
	margin:5px -5px
}
.total-amount small {
	font-size:15px;
	margin:0 4px;
	color:red
}
.total-amount .checkout {
	float:right;
	background:#FFF;
	padding:6px;
	margin-right:15px;
	border-top-left-radius:5px;
	border-top-right-radius:5px
}
.total-amount .checkout a {
	color:#10739A!important;
	font-weight:600
}
.sort-page {
	border-bottom:1px solid #EFEFEF;
	margin-bottom:10px;
	overflow:hidden
}
.sort-page .sort-list {
	margin-bottom:0
}
.sort-page .sort-list li {
	position:relative;
	display:inline-block;
	margin:0 10px 0 0;
	padding:0 2px 5px
}
.sort-page .sort-list li a {
	color:#000
}
.product-amt-price {
	overflow:hidden;
	position:relative;
	float:left;
	width:50%
}
.product-amt-price h6 {
	position:relative;
	overflow:hidden;
	margin:2px 0
}
.product-amt-price .view-discnt {
	font-size:16px;
	font-weight:400;
	text-decoration:line-through;
	color:#949494;
	margin:6px 15px 0 0;
	float:left;
	vertical-align:middle
}
.product-amt-price .view-act-amt {
	float:left;
	font-size:24px;
	font-weight:600;
	color:#2D2929;
	margin:0 15px 0 0
}
.product-amt-price .view-amt-dcnt {
	float:left;
	margin:2px 0 0;
	background:#EE3826;
	padding:6px;
	color:#fff;
	font-weight:600;
	font-size:13px
}
.adr-main-block {
	min-height:260px;
	box-shadow:0 0 10px #E0E0E0;
	margin-bottom:15px
}
.address-block1 {
	float:left;
	width:30%;
	background-image:url(../img/social/map.png);
	padding:10px;
	height:258px;
	box-shadow:0 0 10px #D4D4D4
}
.address-icon {
	margin:0 auto
}
.address-icon, .login-social a {
	display:block;
	text-align:center
}
.address-icon i {
	font-size:22px;
	color:#C3A05F;
	background:#F1F1F1;
	padding:12px 17px;
	border-radius:50%
}
.address-block1 .address-icon h3 {
	font-size:18px;
	font-weight:600;
	color:#C3A05F;
	margin:8px 0
}
.address-block2 {
	float:left;
	width:70%
}
.address-block2 .address-user-detail {
	padding:10px
}
.address-user-detail .bill-head-sty {
	margin:0 0 2px;
	padding:0 0 2px!important;
	font-size:20px!important;
	border-bottom:1px solid #EF3B26;
	color:#EF3B26!important
}
.account-create .fieldset .legend, .account-login .col2-set h2 {
	 
	font-size:15px;
	margin-bottom:10px;
	color:#434343;
	font-weight:400;
	padding:20px 0
}
.address-block2 .address-user-detail h3 {
	margin:5px 0;
	padding:0;
	font-size:15px;
	font-weight:600;
	color:#10739A;
	text-transform:capitalize
}
.address-block2 .address-user-detail p {
	margin:5px 0;
	font-size:13px;
	font-weight:400;
	color:#7D7D7D;
	text-align:justify
}
.address-block2 .address-user-detail h5 {
	font-size:14px;
	margin:10px 0;
	color:#10739A;
	font-weight:600
}
.address-block2 .address-user-detail h5 span {
	margin:0 10px;
	color:#7D7D7D;
	font-weight:400
}
.btn2 {
	border:none;
	font-family:inherit;
	background:#10739A;
	padding:10px 18px;
	margin:15px 0;
	letter-spacing:1px;
	font-weight:700;
transition:all .3s;
	color:#FFF!important;
	position:absolute;
	bottom:0;
	right:0
}
.bg .add-address, .my-wishlist .title-buttons h3, .payment-section h4 {
	font-weight:600;
	font-family:Lato, Helvetica Neue, Helvetica, Arial, sans-serif
}
.content .bg {
	background:#fff;
	padding:20px
}
.content .bg, .content-form {
	box-shadow:0 0 10px #E0E0E0
}
.adr-main-block, .bg .address {
	position:relative;
	overflow:hidden
}
.detail-erase {
	background:#FF5151;
	top:40%;
	text-align:center
}
.detail-edit, .detail-erase {
	padding:10px;
	width:40px;
	height:40px;
	position:absolute;
	right:0
}
.detail-edit {
	background:#41BB8E;
	top:65%;
	text-align:center
}
.default-addr {
	position:absolute;
	right:0;
	top:5px;
	font-family:Lato, Helvetica Neue, Helvetica, Arial, sans-serif
}
.bg .add-address {
	border:1px solid #10739A;
	padding:15px;
	margin:10px;
	background:#FFF;
	box-shadow:0 0 10px #A0A0A0
}
.bg .add-address, .my-wishlist .title-buttons h3 {
	color:#10739A
}
.bg .add-address, .btn2 {
	text-transform:uppercase;
	font-size:12px;
	display:inline-block
}
.payment-section h2 {
	font-size:20px!important;
	background:#F1F1F1;
	padding:10px 0!important;
	text-align:center;
	color:#576573!important
}
.payment-section h4 {
	font-size:13px;
	color:#576573;
	margin:0;
	text-transform:capitalize;
	padding:10px
}
.payment-section .payment-mode {
	overflow:hidden;
	position:relative;
	padding:3px 5px;
	border:1px dashed #D2D2D2;
	margin:5px 0;
	background:#F1F1F1;
	min-height:80px
}
.payment-mode img {
	width:70px;
	float:left
}
.payment-mode h6 {
	margin:22px 10px 10px;
	float:left;
	text-transform:capitalize;
	font-size:15px;
	width:70%
}
.payment-mode h6 i {
	float:right;
	margin:6px 0
}
.CSSTableGenerator {
	margin:0;
	padding:0;
	width:100%;
	-moz-border-radius-bottomleft:0;
	-moz-border-radius-bottomright:0;
	-moz-border-radius-topright:0;
	-moz-border-radius-topleft:0;
	-webkit-border-radius:0;
	border-radius:0
}
.CSSTableGenerator h2 {
	font-size:20px
}
.CSSTableGenerator table {
	width:100%;
	height:100%;
	margin:0;
	padding:0
}
.CSSTableGenerator tr:last-child td:last-child {
	-moz-border-radius-bottomright:0;
	-webkit-border-bottom-right-radius:0;
	border-bottom-right-radius:0
}
.CSSTableGenerator table tr:first-child td:first-child {
	-moz-border-radius-topleft:0;
	-webkit-border-top-left-radius:0;
	border-top-left-radius:0
}
.CSSTableGenerator table tr:first-child td:last-child {
	-moz-border-radius-topright:0;
	-webkit-border-top-right-radius:0;
	border-top-right-radius:0
}
.CSSTableGenerator tr:last-child td:first-child {
	-moz-border-radius-bottomleft:0;
	-webkit-border-bottom-left-radius:0;
	border-bottom-left-radius:0
}
.CSSTableGenerator tr:nth-child(odd) {
background-color:#F1F1F1
}
.CSSTableGenerator tr:nth-child(even) {
background-color:#fff
}
.CSSTableGenerator td {
	vertical-align:middle;
	border:1px solid #D6D6D6;
	border-width:0 1px 1px 0;
	text-align:center;
	padding:7px;
	font-size:12px;
	 
	font-weight:400;
	color:#000
}
.CSSTableGenerator tr:last-child td {
	border-width:0 1px 0 0
}
.CSSTableGenerator tr td:last-child {
	border-width:0 0 1px
}
.CSSTableGenerator tr:last-child td:last-child {
	border-width:0
}
.CSSTableGenerator thead tr:first-child td {
	background-color:#576573;
	border:0 solid #D6D6D6;
	text-align:center;
	border-width:0 0 1px 1px;
	font-size:14px;
	 
	font-weight:700;
	color:#fff
}
.CSSTableGenerator tr:first-child td:first-child {
	border-width:0 0 1px
}
.CSSTableGenerator tr:first-child td:last-child {
	border-width:0 0 1px 1px
}
.col2-set .CSSTableGenerator .sub-head {
	background:#4A5967;
	margin:0;
	padding:10px;
	color:#fff;
	font-weight:600
}
.security-list {
	border-top:1px dotted #10739A;
	padding:20px 0 0;
	margin-top:20px
}
.security li {
	font-size:12px;
	margin:5px 10px 5px 0;
	display:inline-block;
	color:#232323
}
.security li i {
	background:#F1F1F1;
	padding:8px;
	color:#EF3B26;
	border:1px solid #E6E6E6;
	margin-right:10px;
	font-size:14px
}
.re-sumry-center, .re-sumry-right {
	text-align:left;
	font-weight:600;
	color:#333
}
.security li img {
	width:50px
}
.account-login .col2-set .product-name {
	margin:0;
	padding:0
}
.re-sumry-right {
	font-size:16px
}
.re-sumry-center {
	font-size:14px
}
input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {
-webkit-appearance:none;
margin:0
}
input[type=number] {
	-moz-appearance:textfield
}
.em-col-main .order {
	padding:20px;
	border:1px solid #E2E2E2;
	box-shadow:0 0 10px #DCDCDC;
	overflow:hidden;
	margin-bottom:25px
}
#allcomments .comment-button button.button:hover, .em_post-items .post-title h2, .em_post-items .post-title h2 a, .post-title {
	color:#434343
}
.comment-by, .comment-count, .comment-user, .fb_comments_count, .post-by, .post-time {
	float:left;
	position:relative;
	padding-top:1px;
	margin-right:20px;
	padding-bottom:3px;
	margin-bottom:10px;
	font-size:12px;
	color:#424242
}
.comment-by:before, .comment-count:before, .comment-user:before, .fb_comments_count:before, .post-by:before, .post-time:before {
	display:inline-block;
	font-family:FontAwesome;
	font-style:normal;
	font-weight:400;
	line-height:1;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
	content:"\f07a";
	font-size:16px;
	margin-right:7px;
	color:#E83E74
}
.post-time .time-stamp {
	font-size:12px
}
.em_post-item .post-content {
	height:100%;
	overflow:hidden
}
.customer-pro-detail {
	background:#fff;
	padding:15px;
	margin:0 0 10px
}
.cstm-detail .cstm-inner {
	margin:0 0 30px
}
.cstm-detail h2 {
	margin:0 0 3px;
	font-size:18px;
	font-weight:400;
	color:#313131
}
.em_post-item .post-content p {
	margin-bottom:17px
}
.cstm-detail p {
	font-size:13px;
	color:#7B7B7B
}
.amount-deducted-list {
	overflow:hidden;
	position:relative
}
.amount-deducted-list li {
	display:block;
	border-bottom:1px dotted #2FB1EB;
	overflow:hidden;
	padding-bottom:10px
}
.amount-deducted-list .amnt-name {
	float:left;
	width:70%
}
.amount-deducted-list .amnt-name .sub-tot {
	padding:0;
	font-size:16px
}
.amount-deducted-list .amnt-name .sub-tot, .amount-deducted-list .amnt-price .amnt-val {
	color:#565656;
	margin:5px 0
}
.amount-deducted-list .amnt-price {
	float:right;
	width:30%
}
.amount-deducted-list .amnt-price .amnt-val {
	text-align:right;
	font-weight:600
}
.amount-deducted-list .order-last {
	border-bottom:none
}
.amnt-price .final-cost {
	text-align:right
}
.amnt-price .final-cost, .amount-deducted-list .amnt-name .final {
	font-size:20px;
	margin:10px 0;
	padding:5px 0;
	color:#434343;
	font-weight:700
}
.amnt-price .delivery-free {
	margin:20px 0;
	color:#35C6B7
}
.amnt-price .delivery-cost, .amnt-price .delivery-free {
	padding:0;
	font-size:13px;
	font-weight:400;
	text-align:right
}
.amnt-name .extra {
	margin:20px 0;
	padding:0!important;
	font-size:13px!important;
	color:#525252!important
}
.seller-block {
	background:#F9F9F9;
	padding:20px!important;
	margin-bottom:10px;
	height:200px;
	border:1px solid #FFF;
	box-shadow:0 0 15px #E6E6E6;
	display:block;
	overflow:hidden
}
.seller-block .seller-detail {
	font-size:18px;
	color:#313131;
	font-weight:400
}
.seller-block .seller-name {
	font-size:14px;
	color:#313131
}
.seller-block .seller-address, .seller-numb {
	font-size:14px;
	color:#636363
}
.seller-block .seller-numb span {
	color:#000
}
.order-summ {
	margin-bottom:15px;
	border-bottom:1px solid #F5F5F5;
	margin-top:5px;
	padding-bottom:15px
}
.myacnt p {
	margin:0;
	color:#383838
}
.myacnt .track-acnt {
	padding:20px 10px;
	background:#F1F1F1
}
.myacnt .order-process li {
	display:inline-block;
	margin-right:10px;
	color:#383838
}
.myacnt .order-process li i {
	font-size:22px;
	margin-right:5px
}
.btn-warning.active, .btn-warning:active, .btn-warning:focus, .btn-warning:hover, .open .btn-warning.dropdown-toggle {
	color:#fff!important;
	background-color:#ed9c28!important;
	border-color:#d58512!important
}
.btn-warning {
	color:#fff!important;
	background-color:#f0ad4e!important;
	border-color:#eea236!important
}
.order-track .product-name .price {
	font-size:12px;
	color:#484848;
	font-weight:400;
	display:block;
	margin:5px 0
}
.jho-inner .product-image, .product-item .arrival-img .product-image {
	display:flex;
	align-content:center;
	text-align:center;
	overflow:hidden;
	position:relative
}
.jho-inner .product-image {
	width:100%;
	height:200px;
	margin:5px auto;
	flex:1;
	align-items:center
}
.jho-inner {
	height:230px
}
.product-item .arrival-img .product-image {
	align-items:center;
	height:238px;
	margin-bottom:10px
}
.icon-remove-sign:before {
	content:"\f1f8";
	font-family:FontAwesome;
	font-size:14px;
	vertical-align:middle;
	color:#DC2510
}
.btn-4:active, .btn-4:focus, .btn-4:hover {
	background:#24b662;
	color:#fff
}
.btn-4, .detail-edit a, .detail-erase a {
	color:#fff!important
}
.btn-4 {
	border-radius:0;
	border:3px solid #fff
}
.arrive .product-shop .f-fix .product-name {
	color:#333
}
.arrive .product-shop .f-fix .title {
	text-align:left
}
.arrive .product-shop .f-fix .title .price1 {
	font-size:18px;
	font-weight:600;
	color:#333
}
</style>
</head>
<body class="res layout-1">
<div id="wrapper" class="wrapper-fluid banners-effect-5">
  <!-- Header Container  -->
  <?php include('header.php'); ?>
  <!-- //Header Container  -->
  <section style="background: #ececec;margin-bottom:30px;">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Checkout Process</a></li>
      </ul>
    </div>
  </section>
  <!-- Main Container  -->
  <div class="main-container container" style="max-width: 1170px !important;background: #fff;padding-bottom: 3rem;">
    <div class="row">
      <div class="em-col-main col-sm-12 hebbo-font">
        <div class="account-login">
          <input name="form_key" type="hidden" value="" />
          <div class="col2-set">
            <div class="col-1 new-users">
              <div class="content">
                <h2 class="active"><a href="#log1"><span class="login-icons" ><i class="fa fa-sign-in"></i></span>Login / Register</a></h2>
                <h2><span class="login-icons"><i class="fa fa-map-marker"></i></span>Confirm Address</h2>
                <h2><span class="login-icons"><i class="fa fa-thumbs-up"></i></span>Review Order</h2>
                <!--  <h2><span class="login-icons"><i class="fa fa-barcode"></i></span>Coupon Code</h2>-->
                <h2><span class="login-icons"><i class="fa fa-money"></i></span>Make Payment</h2>
              </div>
            </div>
            <div class="col-2 registered-users">
              <div class="content">
                <div class="content-form" id="log1">
                  <div id="head_pas1" class="content-form-header">
                    <h3>Login / Register</h3>
                    <p>We will send order details to this email address</p>
                  </div>
                  <div id="head_pas2" style="display:none;" class="content-form-header">
                    <h3>Forgot Password</h3>
                    <p>We will send your password </p>
                  </div>
                  <div class="panel-body1">
                    <div class="row">
                      <div class="col-md-14">
                        <form action="" method="post" id="auto_submit" >
                          <div class="new_list"></div>
                        </form>
                        <?   if((!empty($email))&&(!empty($user_id))){ ?>
                        <h2><span class="input input--hoshi"><? echo $email; ?></span></h2>
                        <section class="color-4">
                          <p><a href="checkout_step_2.php"  class="btn1 btn-4 btn-4c icon-arrow-right"> Continue </a> </p>
                        </section>
                        <? } else { ?>
                        <form class="form-horizontal" method="POST" id="email_match"  style="margin-top: 0;color:#55518a;">
                        
                        <section class="content"> <span class="input input--hoshi">
                          <input class="input__field input__field--hoshi" type="text" id="email"  >
                          <label class="input__label input__label--hoshi input__label--hoshi-color-1" > <span class="input__label-content input__label-content--hoshi">Please enter your email address</span> </label>
                          </span>
                          <div class="formprocessgif" style="display:none"><img src="images/ajax-loader.gif" align="center"> We are processing your submssion. Please wait...</div>
                        </section>
                        <section class="color-4">
                          <p>
                            <input type="button" name="pro_1" id="pro_1" value="Continue"   class="btn1 btn-4 btn-4c icon-arrow-right" >
                          </p>
                        </section>
                        <p class="frgt-pswd"> <a href="#" id="getpass"><i class="fa fa-lock"></i> Forgot your password? </a> </p>
                        </form>
                        <div id="passopn" style="display:none;">
                          <form action="#" name="forg" >
                            <span class="input input--hoshi">
                            <input class="input__field input__field--hoshi" type="text" id="passwrd"  >
                            <label class="input__label input__label--hoshi input__label--hoshi-color-1" > <span class="input__label-content input__label-content--hoshi">Please Enter Your Mobile Number Or Email Id </span> </label>
                            </span>
                            <section class="color-4">
                              <p>
                                <input type="button" name="fpass" id="fpass" value="Get Password"   class="btn1 btn-4 btn-4c icon-arrow-right" >
                              </p>
                            </section>
                          </form>
                        </div>
                        <p class="frgt-pswd"> <a href="#" id="back_b" style="display:none;"><i class="fa fa-arrow-left"></i> Back </a> </p>
                        <form class="form-horizontal" id="regis_form"  action="checkout_function.php" method="POST" style="display:none"/>
                        <span class="input input--hoshi">
                        <input type="hidden" name="email" value="" id="hi" />
                        <input class="input__field input__field--hoshi" type="password" id="pass"   name="pass" autofocus="autofocus" >
                        <label class="input__label input__label--hoshi input__label--hoshi-color-2" > <span class="input__label-content input__label-content--hoshi">Password</span> </label>
                        </span>
                        <div class="new_list"></div>
                        <section class="color-4">
                          <p>
                            <input type="submit" name="login"  id="pro_pass"     class="btn1 btn-4 btn-4c icon-arrow-right" value="Login" >
                          </p>
                        </section>
                        </form>
                        <form class="form-horizontal" id="login_form" action="checkout_function.php" method="POST" style="display:none"/>
                        <strong> You Do Not have an account with us. Please Register</strong> <br/>>
                        <div onClick="relod()" id="chang" > <span id="chang1"></span> &nbsp; <a href="javascript:void(0);" style="color:#000099">Change</a></div>
                        <br/>
                        <br/>
                        <input type="hidden" name="email" value="" id="ema" />
                        <span class="input input--hoshi">
                        <input class="input__field input__field--hoshi" type="password"  name="password" id="passs"  autofocus="autofocus" >
                        <label class="input__label input__label--hoshi input__label--hoshi-color-2" > <span class="input__label-content input__label-content--hoshi">Password</span> </label>
                        </span> <span class="input input--hoshi">
                        <input class="input__field input__field--hoshi" type="password" name="cpass" id="cpass">
                        <label class="input__label input__label--hoshi input__label--hoshi-color-3" > <span class="input__label-content input__label-content--hoshi">Confirm Password</span> </label>
                        <span id="mes"> </span> </span>
                        <div class="new_list"></div>
                        <section class="color-4">
                          <input type="submit" name="registration" id="pro_pass"   value="Register"   class="btn1 btn-4 btn-4c icon-arrow-right" >
                        </section>
                        </form>
                        <? } ?>
                      </div>
                      <!--<div class="col-md-10">

<div class="social-login-separator"></div>



  <a href="login-facebook.php"><img src="img/social/face_checkout.png" /></a>

<a href="#"><img src="img/social/google_checkout.png" class="hide" /></a>

 

</div>

</div>

-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.account-login -->
      </div>
    </div>
  </div>
  <!-- //Main Container -->
  <!-- Footer Container -->
  <?php include('footer.php'); ?>
  <!-- //end Footer Container -->
</div>
<!-- Include Libs & Plugins
============================================ -->
<!-- Placed at the end of the document so the pages load faster -->
<?php include('script.php'); ?>

  <script>	var password = new LiveValidation('passs');

password.add( Validate.Presence );

var f19 = new LiveValidation('cpass');

f19.add( Validate.Confirmation, { match: 'passs' } );

</script> 

      

<?php  include('js/checkout_start.php');  ?>
</body>
</html>
