<!doctype html>
<html class="no-js" lang="en">
<link rel="stylesheet" type="text/css" href="js/chosen/chosen.min.css" />

<style>
    .em-wrapper-main,
    .em-wrapper-slideshow {
        background: #F8F8F8;
    }

    .adr-main-block {
        min-height: 260px;
        box-shadow: 0 0 10px #E0E0E0;
        margin-bottom: 15px;
    }

    .address-block1 {
        float: left;
        width: 30%;
        background-image: url(img/social/map.png);
        padding: 10px;
        height: 258px;
        box-shadow: 0 0 10px #D4D4D4;
    }
    select.input-sm { 
        line-height: initial !important;
    }
    .address-icon {
        margin: 0 auto;
    }

    .address-icon,
    .login-social a {
        display: block;
        text-align: center;
    }

    .address-icon i {
        font-size: 22px;
        color: #C3A05F;
        background: #F1F1F1;
        padding: 12px 17px;
        border-radius: 50%;
    }

    .address-block1 .address-icon h3 {
        font-size: 18px;
        font-weight: 600;
        color: #C3A05F;
        margin: 8px 0;
    }

    .address-block2 {
        float: left;
        width: 70%;
    }

    .address-block2 .address-user-detail {
        padding: 10px;
    }

    .address-user-detail .bill-head-sty {
        margin: 0 0 2px;
        padding: 0 0 2px !important;
        font-size: 20px !important;
        border-bottom: 1px solid #EF3B26;
        color: #EF3B26 !important;
    }

    .account-create .fieldset .legend,
    .account-login .col2-set h2 {
        font-family: Lato, Helvetica Neue, Helvetica, Arial, sans-serif;
        font-size: 15px;
        margin-bottom: 10px;
        color: #434343;
        font-weight: 400;
        padding: 20px 0;
    }

    .address-block2 .address-user-detail h3 {
        margin: 5px 0;
        padding: 0;
        font-size: 15px;
        font-weight: 600;
        color: #10739A;
        text-transform: capitalize;
    }

    .address-block2 .address-user-detail p {
        margin: 5px 0;
        font-size: 13px;
        font-weight: 400;
        color: #7D7D7D;
        text-align: justify;
    }

    .address-block2 .address-user-detail h5 {
        font-size: 14px;
        margin: 10px 0;
        color: #10739A;
        font-weight: 600;
    }

    .address-block2 .address-user-detail h5 span {
        margin: 0 10px;
        color: #7D7D7D;
        font-weight: 400;
    }

    .btn2 {
        border: none;
        font-family: inherit;
        background: #10739A;
        padding: 10px 18px;
        margin: 15px 0;
        letter-spacing: 1px;
        font-weight: 700;
        transition: all .3s;
        color: #FFF !important;
        position: absolute;
        bottom: 0;
        right: 0;
    }

    .bg .add-address,
    .my-wishlist .title-buttons h3,
    .payment-section h4 {
        font-weight: 600;
        font-family: Lato, Helvetica Neue, Helvetica, Arial, sans-serif;
    }

    .content .bg {
        background: #fff;
        padding: 20px;
    }

    .content .bg,
    .content-form {
        box-shadow: 0 0 10px #E0E0E0;
    }

    .adr-main-block,
    .bg .address {
        position: relative;
        overflow: hidden;
    }

    .detail-erase {
        background: #FF5151;
        top: 40%;
        text-align: center;
    }

    .detail-edit,
    .detail-erase {
        padding: 10px;
        width: 40px;
        height: 40px;
        position: absolute;
        right: 0;
    }

    .detail-edit {
        background: #41BB8E;
        top: 65%;
        text-align: center;
    }

    .default-addr {
        position: absolute;
        right: 0;
        top: 5px;
        font-family: Lato, Helvetica Neue, Helvetica, Arial, sans-serif;
    }

    .bg .add-address {
        border: 1px solid #10739A;
        padding: 15px;
        margin: 10px;
        background: #FFF;
        box-shadow: 0 0 10px #A0A0A0;
    }

    .bg .add-address,
    .my-wishlist .title-buttons h3 {
        color: #10739A;
    }

    .bg .add-address,
    .btn2 {
        text-transform: uppercase;
        font-size: 12px;
        display: inline-block;
    }
</style>

<head>
    <?php include('link.php');
@session_start();
$country_type= $_SESSION['country_type'];
$category_id =  $_REQUEST['catid'];
$search = $_REQUEST['search'];
$keyword = $_REQUEST['keyword'];
 ?>
    <link rel="stylesheet" type="text/css" href="css/livevalid.css" />
    <script type="text/javascript" src="js/livevalidation_standalone.compressed.js"></script>
    <link rel="stylesheet" type="text/css" href="css/livevalid.css" />

</head>

<body class="cms-index-index">
    <div class="wrapper">
        <noscript>
            <div class="global-site-notice noscript">
                <div class="notice-inner">

                    <!-- <p> <strong>JavaScript seems to be disabled in your browser.</strong>

<br /> You must have JavaScript enabled in your browser to utilize the functionality of this website.</p>  -->
                </div>
            </div>
        </noscript>
        <div class="page one-column">
            <div class="em-wrapper-header">
                <?php include('header.php');
 					$address=$db->getRowCount('profile_address','*',NULL,"user_id='".$user_id."'",'');
 ?>
            </div>
            <section style="background: #ececec;margin-bottom:30px;">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                        <li><a href="#">Review Order</a></li>
                    </ul>
                </div>
            </section>
            <div class=" ">
                <div class="main-container container"
                    style="max-width: 1170px !important;background: #fff;padding-bottom: 3rem;">
                    <div class="em-inner-main">
                        <div class="em-wrapper-area02"></div>
                        <div class="em-main-container em-col1-layout">
                            <div class="row">
                                <div class="em-col-main col-sm-12 hebbo-font">
                                    <div class="account-login">
                                        <input name="form_key" type="hidden" value="" />
                                        <div class="col2-set">
                                            <div class="col-1 new-users">
                                                <div class="content">
                                                    <a href="checkout_process.php">
                                                        <h2><span class="login-icons"><i
                                                                    class="fa fa-sign-in"></i></span>Login / Register
                                                        </h2>
                                                    </a>
                                                    <h2 class="active"><a href="#adr1"><span class="login-icons"><i
                                                                    class="fa fa-map-marker"></i></span>Confirm
                                                            Address</a></h2>
                                                    <h2><span class="login-icons"><i
                                                                class="fa fa-thumbs-up"></i></span>Review Order</h2>
                                                    <!-- <h2><span class="login-icons"><i class="fa fa-barcode"></i></span>Coupon Code</h2>
-->
                                                    <h2><span class="login-icons"><i class="fa fa-money"></i></span>Make
                                                        Payment</h2>
                                                </div>
                                            </div>
                                            <div class="col-2 registered-users">
                                                <div class="content" id="adr1">
                                                    <div id="reload_tab"> </div>
                                                    <div class="panel-body1">
                                                        <div id="legend">
                                                            <div class="">
                                                                <form class="form-horizontal  " id="check_2"
                                                                    method="POST" action="checkout_function.php"
                                                                    style="margin-top: 0;color:#55518a; <? if($address!=0){ ?> display:none <? } ?>">
                                                                    <div class="content-form">
                                                                        <div class="content-form-header">
                                                                            <h3>Confirm Address </h3>
                                                                            <p>We will deliver your order here</p>
                                                                            <div class="back-adr-btn">
                                                                                <? if($address!=0){ ?> <a
                                                                                    onClick="bck()" id="bck"> <i
                                                                                        class="fa fa-arrow-left"></i> Go
                                                                                    Back </a>
                                                                                <? } ?>
                                                                            </div>

                                                                        </div>

                                                                        <div class="col-sm-6">

                                                                            <div class="bill-edit-method">

                                                                                <h4>Shipping Address</h4>

                                                                            </div>

                                                                            <fieldset> <input type="hidden"
                                                                                    value="<? echo getCourierToken(); ?>"
                                                                                    id="token">

                                                                                <!-- Name input-->

                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4  control-label"
                                                                                        for="name"
                                                                                        style="text-align:left">Pincode</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <input id="pcode" name="pincode"
                                                                                            type="text"
                                                                                            placeholder="Your Pincode"
                                                                                            class="form-control"
                                                                                            autofocus="autofocus" />

                                                                                        <div id="msg" align="center">
                                                                                        </div>

                                                                                    </div>

                                                                                </div>

                                                                                <!-- Email input-->

                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4  control-label"
                                                                                        for="first_name"
                                                                                        style="text-align:left">First
                                                                                        Name</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <input id="first_name"
                                                                                            name="first_name"
                                                                                            type="text"
                                                                                            placeholder="Your First Name"
                                                                                            class="form-control" />

                                                                                    </div>

                                                                                </div>

                                                                                <!-- Message body -->

                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4  control-label"
                                                                                        for="last_name"
                                                                                        style="text-align:left">Last
                                                                                        Name</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <input id="last_name"
                                                                                            name="last_name" type="text"
                                                                                            placeholder="Your Last Name"
                                                                                            class="form-control" />

                                                                                    </div>

                                                                                </div>

                                                                                <!-- Message body -->

                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4   control-label"
                                                                                        for="cadd1"
                                                                                        style="text-align:left">Address</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <textarea name="cadd1"
                                                                                            id="cadd1" cols="57"
                                                                                            rows="5"
                                                                                            class="form-control"></textarea>

                                                                                    </div>

                                                                                </div>

                                                                                <!-- Form actions -->

                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4   control-label"
                                                                                        for="mobile"
                                                                                        style="text-align:left">Mobile</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <input id="mobile" name="cmobno"
                                                                                            type="text"
                                                                                            placeholder="Your Mobile"
                                                                                            class="form-control" />

                                                                                    </div>

                                                                                </div>















                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4   control-label"
                                                                                        for="state"
                                                                                        style="text-align:left">Countries
                                                                                    </label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <select name="ccountry"
                                                                                            class="form-control input-sm chosen"
                                                                                            id="ccountry1"
                                                                                            onchange="getmystate1(this.value);">
                                                                                            <!--onChange="getcity(this.value,1)">  -->

                                                                                            <?php

                if($country_type==0){ $st =  "SELECT * FROM all_countries  WHERE `id` =101"; }

                else{

                 

                 $st =  "SELECT * FROM all_countries ";  ?>

                                                                                            <option value=""> --- Select
                                                                                                Country --- </option>

                                                                                            <?

                }

 				$rr = mysql_query($st);

				while($state = mysql_fetch_array($rr)){

				?>

                                                                                            <option
                                                                                                value="<?php echo $state['id']?>"
                                                                                                <?
                                                                                                if($state['id']==$_SESSION['country_id']){
                                                                                                ?> selected
                                                                                                <? } ?> >
                                                                                                <?php echo $state['name']; ?>
                                                                                            </option>

                                                                                            <?php } ?>

                                                                                        </select>

                                                                                    </div>

                                                                                </div>





                                                                                <div class="form-group3" id="sthid">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4   control-label"
                                                                                        for="state"
                                                                                        style="text-align:left">State</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <select
                                                                                            class="form-control input-sm chosen"
                                                                                            id="state1" name="cstate"
                                                                                            onChange="getmycity1(this.value)">

                                                                                            <?php

              //  if($country_type==0){  ?>

                                                                                            <option value=""> --- Select
                                                                                                State --- </option>

                                                                                            <?php $st = "SELECT * FROM `all_states` WHERE `country_id` =".$_SESSION['country_id'];

				$rr = mysql_query($st);

				while($state = mysql_fetch_array($rr)){

				?>

                                                                                            <option
                                                                                                value="<?php echo $state['id']?>">
                                                                                                <?php echo $state['name']; ?>
                                                                                            </option>

                                                                                            <?php } ?>

                                                                                            <?php /* } else { ?><option
                                                                                                value=""> --- Select
                                                                                                State --- </option>
                                                                                            <?php }*/ ?>

                                                                                        </select>

                                                                                    </div>

                                                                                </div>





                                                                                <div id="st1"> </div>



                                                                                <div id="ct1"> </div>























                                                                                <div id="citydiv1">
                                                                                    <div class="form-group3">

                                                                                        <label
                                                                                            class="col-sm-4 col-md-4 col-lg-4   control-label"
                                                                                            for="city"
                                                                                            style="text-align:left">City
                                                                                        </label>

                                                                                        <div
                                                                                            class="col-md-8 col-sm-8 col-lg-8">

                                                                                            <select id="ship_city"
                                                                                                class="input-text 	"
                                                                                                style="width:100%">

                                                                                                <option value=""> ---
                                                                                                    Select City ---
                                                                                                </option>

                                                                                                <?php  $ct =  "SELECT * FROM all_cities where state_id='".$_REQUEST['state_id1']."'"; 

				$rrs= mysql_query($ct);

				while($city = mysql_fetch_array($rrs)){

				?> <option value="<?php echo $city['id']?>" <?php  if($sel_res['city']==$city['id']){ ?> selected="selected" <? }
                                                                                                    ?>>
                                                                                                    <?php echo $city['name']; ?>
                                                                                                </option> <?php } ?>

                                                                                            </select>

                                                                                        </div>

                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4 control-label"
                                                                                        for="name"
                                                                                        style="text-align:left">Address
                                                                                        Type</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <input type="radio"
                                                                                            name="address_type"
                                                                                            id="address_type"
                                                                                            value="Home" checked> <label
                                                                                            for="address_type"> Home
                                                                                        </label>

                                                                                        &nbsp; <input type="radio"
                                                                                            name="address_type"
                                                                                            id="address_type"
                                                                                            value="Office"> <label
                                                                                            for="address_type"> Office
                                                                                        </label>

                                                                                        &nbsp; <input type="radio"
                                                                                            name="address_type"
                                                                                            id="address_type"
                                                                                            value="Other"> <label
                                                                                            for="address_type"> Other
                                                                                        </label>

                                                                                    </div>

                                                                                </div>





                                                                            </fieldset>

                                                                        </div>

                                                                        <div class="col-sm-6">

                                                                            <div class="bill-edit-method">



                                                                                <h4>Billing Address <p><input
                                                                                            type="checkbox"
                                                                                            name="default1" value="1"
                                                                                            id="samebilling"
                                                                                            onClick="samebillingval();" />
                                                                                        <span style="font-size:14px">
                                                                                            Same as shipping address
                                                                                        </span></p>
                                                                                </h4>



                                                                            </div>

                                                                            <fieldset>

                                                                                <!-- Name input-->

                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4  control-label"
                                                                                        for="name"
                                                                                        style="text-align:left">Pincode</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <input id="bpcode"
                                                                                            name="bpincode" type="text"
                                                                                            placeholder="Your Pincode"
                                                                                            class="form-control"
                                                                                            autofocus="autofocus" />

                                                                                        <div id="msg" align="center">
                                                                                        </div>

                                                                                    </div>

                                                                                </div>

                                                                                <!-- Email input-->

                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4  control-label"
                                                                                        for="first_name"
                                                                                        style="text-align:left">First
                                                                                        Name</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <input id="bfirst_name"
                                                                                            name="bfirst_name"
                                                                                            type="text"
                                                                                            placeholder="Your First Name"
                                                                                            class="form-control" />

                                                                                    </div>

                                                                                </div>

                                                                                <!-- Message body -->

                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4  control-label"
                                                                                        for="last_name"
                                                                                        style="text-align:left">Last
                                                                                        Name</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <input id="blast_name"
                                                                                            name="blast_name"
                                                                                            type="text"
                                                                                            placeholder="Your Last Name"
                                                                                            class="form-control" />

                                                                                    </div>

                                                                                </div>

                                                                                <!-- Message body -->

                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4   control-label"
                                                                                        for="cadd1"
                                                                                        style="text-align:left">Address</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <textarea name="bcadd1"
                                                                                            id="bcadd1" cols="57"
                                                                                            rows="5"
                                                                                            class="form-control"></textarea>

                                                                                    </div>

                                                                                </div>

                                                                                <!-- Form actions -->

                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4   control-label"
                                                                                        for="mobile"
                                                                                        style="text-align:left">Mobile</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <input id="bmobile"
                                                                                            name="bcmobno" type="text"
                                                                                            placeholder="Your Mobile"
                                                                                            class="form-control" />

                                                                                    </div>

                                                                                </div>























                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4   control-label"
                                                                                        for="state"
                                                                                        style="text-align:left">Countries</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <select name="bccountry"
                                                                                            class="form-control input-sm"
                                                                                            id="ccountry2"
                                                                                            onChange="getmystate2(this.value);">
                                                                                            <!--onChange="getcity(this.value,2)"> -->

                                                                                            <?php

                if($country_type==0){ $st =  "SELECT * FROM all_countries  WHERE `id` =101"; }

                else{

                 

                 $st =  "SELECT * FROM all_countries ";  ?>

                                                                                            <option value=""> --- Select
                                                                                                Country --- </option>

                                                                                            <?

                }

 				$rr = mysql_query($st);

				while($state = mysql_fetch_array($rr)){

				?>

                                                                                            <option
                                                                                                value="<?php echo $state['id']?>"
                                                                                                <?
                                                                                                if($state['id']==$_SESSION['country_id']){
                                                                                                ?> selected
                                                                                                <? } ?> >
                                                                                                <?php echo $state['name']; ?>
                                                                                            </option>

                                                                                            <?php } ?>

                                                                                        </select>

                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group3" id="sthid2">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4   control-label"
                                                                                        for="state"
                                                                                        style="text-align:left">State</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <select
                                                                                            class="form-control input-sm"
                                                                                            id="state2" name="bcstate"
                                                                                            onChange="getmycity2(this.value)">

                                                                                            <?php  ?> <option value="">
                                                                                                --- Select State ---
                                                                                            </option> <!--  -->

                                                                                            <?php $st = "SELECT * FROM `all_states` WHERE `country_id` =".$_SESSION['country_id'];

				$rr = mysql_query($st);

				while($state = mysql_fetch_array($rr)){

				?>

                                                                                            <option
                                                                                                value="<?php echo $state['id']?>">
                                                                                                <?php echo $state['name']; ?>
                                                                                            </option>

                                                                                            <?php } ?>



                                                                                        </select>

                                                                                    </div>

                                                                                </div>







                                                                                <div id="st2"> </div>



                                                                                <div id="ct2"> </div>



























                                                                                <div id="citydiv2">
                                                                                    <div class="form-group3">

                                                                                        <label
                                                                                            class="col-sm-4 col-md-4 col-lg-4   control-label"
                                                                                            for="state"
                                                                                            style="text-align:left">City</label>

                                                                                        <div
                                                                                            class="col-md-8 col-sm-8 col-lg-8">

                                                                                            <select name="bcity"
                                                                                                id="bill_city"
                                                                                                class="input-text"
                                                                                                style="width:100%">

                                                                                                <?php  $ct =  "SELECT * FROM all_cities where state_id='".$_REQUEST['state_id2']."'"; 

				$rrs= mysql_query($ct);

				while($city = mysql_fetch_array($rrs)){

				?>

                                                                                                <option
                                                                                                    value="<?php echo $city['id']?>"
                                                                                                    <?php  if($sel_res['city']==$city['id']){ ?>
                                                                                                    selected="selected"
                                                                                                    <? } ?>>
                                                                                                    <?php echo $city['name']; ?>
                                                                                                </option>

                                                                                                <?php } ?>

                                                                                            </select>

                                                                                        </div>

                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group3">

                                                                                    <label
                                                                                        class="col-sm-4 col-md-4 col-lg-4   control-label"
                                                                                        for="name"
                                                                                        style="text-align:left">Address
                                                                                        Type</label>

                                                                                    <div
                                                                                        class="col-md-8 col-sm-8 col-lg-8">

                                                                                        <input type="radio"
                                                                                            name="baddress_type"
                                                                                            id="baddress_type"
                                                                                            value="Home" checked> <label
                                                                                            for="address_type"> Home
                                                                                        </label>

                                                                                        &nbsp; <input type="radio"
                                                                                            name="baddress_type"
                                                                                            id="baddress_type"
                                                                                            value="Office"> <label
                                                                                            for="address_type"> Office
                                                                                        </label>

                                                                                        &nbsp; <input type="radio"
                                                                                            name="baddress_type"
                                                                                            id="baddress_type"
                                                                                            value="Other"> <label
                                                                                            for="address_type"> Other
                                                                                        </label>

                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group3">
                                                                                    <div
                                                                                        class="col-sm-24 col-md-24 col-lg-12 col-lg-offset-4 col-sm-offset-4 col-md-offset-4">

                                                                                        <input type="checkbox"
                                                                                            name="default1" value="1" />
                                                                                        <strong> Make this my default
                                                                                            address </strong> </div>

                                                                                </div>

                                                                                <div class="control-group"
                                                                                    style="margin-left: 38px;">

                                                                                    <!-- Button -->

                                                                                    <div class="controls"
                                                                                        style="text-align: right;">

                                                                                        <input align="middle"
                                                                                            type="button"
                                                                                            name="confirm_add1"
                                                                                            id="confirm_add1"
                                                                                            value="SAVE AND CONTINUE"
                                                                                            class="btn1 btn-4 btn-4c icon-arrow-right" />

                                                                                    </div>

                                                                                </div>

                                                                            </fieldset>

                                                                        </div>
                                                                    </div>



                                                                </form>

                                                                <form class="form-horizontal " id="checkout_edit"
                                                                    method="POST" style="margin-top: 0;color:#55518a;">



                                                                    <!-- SHOW FOR EDIT VALUES OF ADDRESS  -->

                                                                </form>









                                                            </div>







                                                        </div>







                                                    </div>



                                                </div>

                                            </div>

                                        </div><!-- /.account-login -->

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div><!-- /.em-wrapper-main -->



                <script src="countery_ajax.js"></script>

                <?php include('footer.php'); ?>
                <?php include('script.php'); ?>


            </div><!-- /.page -->



        </div><!-- /.wrapper -->



</body>
<script type="text/javascript" src="js/chosen/chosen.jquery.min.js"></script>
<script src="js/checkout_step2.js"></script>





</html>