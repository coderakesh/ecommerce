<!DOCTYPE html>
<html lang="en">


<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>

	<!-- Basic page needs
    ============================================ -->
	<title>Size chart</title>
	<meta charset="utf-8">


	<!-- Mobile specific metas
    ============================================ -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href="https://fonts.googleapis.com/css?family=Heebo:400,500&display=swap" rel="stylesheet">

	<!-- Favicon
    ============================================ -->




	<!-- Libs CSS
    ============================================ -->
	<link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">

	<style>
		.title-product h3 {
			font-size: 24px;
			padding: 0px 0 18px 0;
			border-bottom: 1px solid #e0e0e0;
			font-weight: 500;
			margin: 0;
		}

		.p-lg-o {
			padding: 0;
			;
		}

		.hebbo-font {
         font-family: 'Be Vietnam', sans-serif;
        }

		.table-bordered th {
			text-align: center;
			background: #e8e8e8;
		}
		.img-center{
			text-align: center;             
		}
	</style>


</head>

<body class="res layout-subpage">
	<div id="wrapper" class="wrapper-full ">
		<!-- Main Container  -->
		<div class="main-container container">

			<div class="row">
				<!--Middle Part Start-->
				<div id="content" class="col-md-12 col-sm-12">

					<div class="product-view row quickview-w">


						<div class="title-product">
							<h3>Size chart</h3>
						</div>

						<div class="content-product-right col-md-6 col-sm-6 col-xs-12 p-lg-o">

							<table class="table table-bordered text-center hebbo-font">
								<thead>
									<th>UK/India</th>
									<th>Length (in cm)</th>
									<th>Brand Size</th>
								</thead>
								<tbody>
									<tr>
										<td>6</td>
										<td>25.3</td>
										<td>6</td>
										 
									</tr>
									<tr>
										<td>7</td>
										<td>26</td>
										<td>7</td>
										 
									</tr>
									<tr>
										<td>8</td>
										<td>26.7</td>
										<td>8</td>
										 
									</tr>
									<tr>
										<td>9</td>
										<td>27.3</td>
										<td>9</td>
										 
									</tr>
									<tr>
										<td>10</td>
										<td>28</td>
										<td>10</td>
										 
									</tr>
								</tbody>
							</table>







							<!-- end box info product -->

						</div>


						<div class="content-product-right col-md-6 col-sm-6 col-xs-12 p-lg-o">

							<div class="product-label form-group hebbo-font" style="border-left: 1px solid #e0e0e0;
							padding: 24px 24px 0;margin: 0;">
									<p>Not sure about your shoe size? Follow these simple steps to figure it out:<br><strong><em>1.</em></strong> Place your foot on a blank sheet of paper<br><strong><em>2.</em></strong> Make one marking at your longest toe and one marking at the backside of your heel<br><strong><em>3.</em></strong> Measure (in centimetres) the length of your foot between these two markings<br><strong><em>4.</em></strong> Compare the value to our measurement chart to know your shoe size
									</p> 

									<div class="img-center"><img src="image/size/shoe.jpg"></div>
								 
							</div>

						</div>

					</div>




				</div>


			</div>
			<!--Middle Part End-->
		</div>
		<!-- //Main Container -->

		<style type="text/css">
			#wrapper {
				box-shadow: none;
			}

			#wrapper>*:not(.main-container) {
				display: none;
			}

			#content {
				margin: 0
			}

			.container {
				width: 100%;
			}

			.product-info .product-view,
			.left-content-product,
			.box-info-product {
				margin: 0;
			}

			.left-content-product .content-product-right .box-info-product .cart input {
				padding: 12px 16px;
			}

			.left-content-product .content-product-right .box-info-product .add-to-links {
				width: auto;
				float: none;
				margin-top: 0px;
				clear: none;
			}

			.add-to-links ul li {
				margin: 0;
			}
		</style>
	</div>

	<!-- Include Libs & Plugins
	============================================ -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
	<script type="text/javascript" src="js/themejs/libs.js"></script>
	<script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
	<script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
	<script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
	<script type="text/javascript" src="js/datetimepicker/moment.js"></script>
	<script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>


	<!-- Theme files
============================================ -->
	<script type="text/javascript" src="js/themejs/homepage.js"></script>

	<script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
	<script type="text/javascript" src="js/themejs/addtocart.js"></script>
	<script type="text/javascript" src="js/themejs/application.js"></script>

</body>


<!-- Mirrored from demo.smartaddons.com/templates/html/emarket/quickview.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 Jul 2018 06:51:31 GMT -->

</html>