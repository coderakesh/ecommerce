<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<!-- Basic page needs
    ============================================ -->
<title>Spare parts, Apex Marketing</title>
<meta charset="utf-8">
<!-- Mobile specific metas
    ============================================ -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Favicon
    ============================================ -->
<link rel="shortcut icon" type="image/png" href="ico/favicon.ico"/>
<!-- Libs CSS
    ============================================ -->
<link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="css/themecss/lib.css" rel="stylesheet">
<link href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="js/minicolors/miniColors.css" rel="stylesheet">
<!-- Theme CSS
    ============================================ -->
<link href="css/themecss/so_searchpro.css" rel="stylesheet">
<link href="css/themecss/so_megamenu.css" rel="stylesheet">
<link href="css/themecss/so-categories.css" rel="stylesheet">
<link href="css/themecss/so-listing-tabs.css" rel="stylesheet">
<link href="css/themecss/so-newletter-popup.css" rel="stylesheet">
<link href="css/footer/footer1.css" rel="stylesheet">
<link href="css/header/header1.css" rel="stylesheet">
<link id="color_scheme" href="css/theme.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<!-- Google web fonts
    ============================================ -->
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700' rel='stylesheet' type='text/css'>
<style type="text/css">
body {
	font-family:'Roboto', sans-serif
}
.module.banners-effect-2 {
	border-bottom: 1px dashed #c6c6c6;
	border-right: 1px dashed #c6c6c6;
}
.bor-bottom {
	border-bottom: 0px !important;
}
.bor-right {
	border-right: 0px!important;
}
.col-lg-2 {
	padding:0;
}
@media all and (max-width: 480px) {
.module.banners-effect-2 {
margin: 0 auto;
text-align: center;
}
.typeheader-1 {
padding:0 15px;
}
header.typeheader-1 .search-header-w .icon-search i {
display:none;
}
}
</style>
</head>
<body class="res layout-1">
<div id="wrapper" class="wrapper-fluid">
  <!-- Header Container  -->
  <?php include('header.php'); ?>
  <!-- //Header Container  -->
  <section style="background: #ececec;margin-bottom:30px;">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Car We Deal</a></li>
      </ul>
    </div>
  </section>
  <!-- Main Container  -->
  <div class="main-container container banners-demo-w">
    <div class="row">
      <div id="content" class="col-sm-12">
        <div class="banners-demo">
          <div class="row">
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/1.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6 col-xs-6">
              <div class="module banners-effect-2">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/2.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/3.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/4.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/5.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2 bor-right">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/6.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/7.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/8.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/9.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/10.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/11.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2 bor-right">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/12.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2 bor-bottom">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/13.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2 bor-bottom">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/14.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2 bor-bottom">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/15.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2 bor-bottom">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/16.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-6">
              <div class="module banners-effect-2 bor-bottom">
                <div class="banners">
                  <div> <a href="contact.php" ><img src="image/catalog/car-we-deal/17.jpg" alt=""></a> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- //Main Container -->
  <!-- Footer Container -->
  <?php include('footer.php'); ?>
  <!-- //end Footer Container -->
</div>
<!-- Include Libs & Plugins
	============================================ -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="js/themejs/libs.js"></script>
<script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
<script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
<script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
<script type="text/javascript" src="js/datetimepicker/moment.js"></script>
<script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
<!-- Theme files
	============================================ -->
<script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
<script type="text/javascript" src="js/themejs/addtocart.js"></script>
<script type="text/javascript" src="js/themejs/application.js"></script>
</body>
</html>
