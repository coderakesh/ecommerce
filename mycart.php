<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
    .box-category ul li a:hover {
        color: black
    }

    .modal-choices li a:hover {
        color: black
    }

    .btn-remove {
        float: 10px;
        float: left;
        margin-top: 10px;
        background-color: #ececec;
    }

    .qty-ctl {
        display: inline;
    }

    td {
        vertical-align: middle !important;
    }
    .table-bordered>thead>tr>td{
    border: 1px solid #6156a9 !important;
    }
    span.price1-1 {
    font-weight: 600;
    font-size: 18px;
    }
    table.table-bordered thead {
    background-color: #6156a9;
    font-weight: bold;
    color: #fff;
    }
    </style>
    <?php include('link.php');
	 ?>
    <title>
        <? echo $cats['page_title']; ?>
    </title>
    <meta name="description" content="<? echo $cats['meta_description']; ?>">
    <meta name="keywords" content="<? echo $cats['meta_keyword']; ?>">
    <!-- Basic page needs
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <style>
    .box-category ul li a:hover {
        color: black;
    }

    .box-category>ul>li ul {
        margin-left: 0px;
    }
    h3.subtitle {
    color: #5c529d;
    font-size: 20px;
    margin-bottom: 6px !important;
    }
    p.final-cost {
    font-size: 24px;
    margin: 0;
    font-weight: 600;
    text-align: left;
    color: #0f6fb6;
    }
    </style>
</head>

<body class="res layout-1">
    <div id="wrapper" class="wrapper-fluid banners-effect-5">
        <!-- Header Container  -->
        <?php include('header.php'); ?>
        <!-- //Header Container  -->
        <!-- Main Container  -->
        <div class="main-container" style="background: #ffffff;">
            <div class="bg-bred">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                        <li><a href="#">Shopping Cart</a></li>
                    </ul>
                </div>
            </div>
            <div class="container hebbo-font">
                <div class="row">
                    <!--Middle Part Start-->
                    <div id="content" class="col-sm-12">
                        <h2 class="title">Shopping Cart</h2>
                        <div class="table-responsive form-group">
                            <table class="table table-bordered text-center">
                                <thead>
                                    <tr>
                                        <td>Image</td>
                                        <td>Product Name</td>
                                        <td>Unit Price</td>
                                        <td>Quantity</td>
                                        <td>Total</td>
                                    </tr>
                                </thead>
                                <tbody class="cart-items-list clearfix" id="list">
                                </tbody>
                            </table>
                        </div>
                        <h3 class="subtitle no-margin">What would you like to do next?</h3>
                        <p>Choose if you have a discount code or reward points you want to use or would like to estimate
                            your delivery cost.</p>
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-8">
                                <table class="table table-bordered">
                                    <tfoot>
                                        <tr>
                                            <td style="" colspan="1"><strong>Grand Total</strong></td>
                                            <td style="">
                                                <p class="final-cost   "><i class="fa fa-inr"></i> <span
                                                        class="ftotal"></span></p>
                                            </td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <tr>
                                            <td> Subtotal</td>
                                            <td><span class="ntotal"></span></td>
                                        </tr>
                                        <tr style="display:none">
                                            <td> Shippment In </td>
                                            <td><input type="radio" checked="checked" value="0" name="country"
                                                    class="country" onclick="select_country();" />
                                                Within India <br />
                                                <p>
                                                    <input type="radio" value="1" name="country" class="country"
                                                        onclick="select_country();" />
                                                    Outside From India (All Other Countries)</p>
                                            </td>
                                        </tr>
                                        <tr style="display:none">
                                            <td> Courier Time </td>
                                            <td><input type="radio" checked="checked" value="0" name="courier_type"
                                                    class="courier_type" onclick="ship_manage();" />
                                                Surface Shipment ( 7 to 10 days) <br />
                                                <input type="radio" value="1" name="courier_type" class="courier_type"
                                                    onClick="ship_manage();" />
                                                Air Shipment ( 5 to 7 days) </td>
                                        </tr>
                                        <tr style="display:none">
                                            <td> Select Country</td>
                                            <td class="country_id"><select name="countr" id="countr"
                                                    class="  form-control" style="width:100%">
                                                    <?php 

                                                  $ct =  "SELECT * FROM all_countries where id=101"; 

                                                $rrs= mysql_query($ct);

                                                  while($coun = mysql_fetch_array($rrs)){

                                                  ?>
                                                    <option value="<?php echo $coun['id']?>">
                                                        <?php echo $coun['name']; ?>
                                                    </option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> Shipping Charges</td>
                                            <td><span class="total_weight" style="display:none">0</span> <span
                                                    class="total_weight_outside" style="display:none">0</span> <span
                                                    class="total_ship">0</span><span class="price"></span> </td>
                                        </tr>
                                        <tr style="display:none">
                                            <td> Installation Area</td>
                                            <td><select name="area" id="directions" class="  form-control"
                                                    onchange="instal_manage();">
                                                    <option value="NO">No Needs</option>
                                                    <? 	$db->select('master_installation','*',NULL,"",'');

                                                  $instl = $db->getResult(); foreach($instl as $instl1){ ?>
                                                    <option value="<? echo $instl1['area']; ?>">
                                                        <? echo $instl1['area']; ?>
                                                    </option>
                                                    <? } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr style="display:none">
                                            <td> Installation Charges</td>
                                            <td><span class="instal_amt">0</span> </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="buttons">
                            <div class="pull-left"><a href="index.php" class="btn btn-mega btn-lg pro-btn  btn-primary">Continue Shopping</a>
                            </div>
                            <div class="pull-right"><a href="checkout_process.php" class="btn btn-mega btn-lg pro-btn btn-bg-red">Checkout</a>
                            </div>
                        </div>
                    </div>
                    <!--Middle Part End -->
                    
                </div>
            </div>
        </div>
        <!-- //Main Container -->
        <!-- Footer Container -->
        <?php include('footer.php'); ?>
        <!-- //end Footer Container -->
    </div>
    <script type="text/javascript" src="js/add_cart.js"></script>
    <!-- Include Libs & Plugins ============================================ -->
    <?php include('script.php'); ?>
</body>

</html>