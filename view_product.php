<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" />
    <style>
    .pin-code-sty {
        padding: 5px 0;
    }

    .pin-code-sty {
        border-radius: 4px;
        display: inline-block;
        margin-top: 20px;
        display: block;
        float: left;
        width: 100%;
    }

    .avail {
        font-size: 16px;
        color: #1d1d1d;
        font-weight: 400;
        margin: 0 10px 10px 0px;
        align-items: center;
        font-family: 'Be Vietnam', sans-serif;
        display: block;
    }

    .avail i {
        font-size: 20px;
        padding: 0;
        border-radius: 50%;
        background-color: #ffffff;
        color: #1b1b1b;
        margin-right: 10px;
    }

    .pin-code-sty .pin-val {
        /* width: 35%; */
        height: 40px;
        padding: 10px;
        border: 1px solid #0880AF;
    }

    .form-group {
        margin-bottom: 1px !important;
    }

    /*.left-content-product .content-product-right .title-product h1 {
		
			margin: 0 0 0px!important;
		
		}
		*/
    p {
        margin: 5px 0 5px !important;
    }

    .attribute .size h2 a.borderlist {

        padding: 5px 10px;
        font-size: 17px;
        color: #FFFFFF;
        font-weight: 500;
        background-color: #535766;
        border: 1px solid #535766 !important;
        min-width: 50px;
        height: 50px;
        text-align: center;

    }

    .before_add span {
        margin: auto;
    }

    label.h-css {

        font-size: 16px;
        color: #464646;
        margin-bottom: 15px;
    }

    .size-chart {
        margin-bottom: 15px;
    }

    .before_add {
        display: inline-block;
        margin: 2px 10px 2px 0;
    }

    .attribute {
        margin-bottom: 10px;
    }
    </style>

    <? include ('link.php');
		@$product_id = $_REQUEST['pid'];
		$cat = mysql_query("SELECT categoryid FROM `product_category_mapping` where productid = " . $product_id);
		$cats = mysql_fetch_array($cat);
		$row_count = mysql_num_rows($cat);
		$com_qty = $db->getRowCount('product_comb', 'id', NULL, "qty!=0   and  productid =" . $product_id, '', ''); //mysql_num_rows($qw12);
		if (($row_count == 0) || ($com_qty == 0))
			{
		?>
    <?php	}$category_id = $cats['categoryid'];
		$db->select('catalog_product', '*', NULL, "product_id='" . $product_id . "'", '');
		$product = $db->getResult(); //print_r($cover_img);
		$product = $product[0];
		$db->select('product_image', '*', NULL, "id='" . $product['coverphoto'] . "'", '');
		$cover_img = $db->getResult(); //print_r($cover_img);
		$cover_img = $cover_img[0];
		?>

    <title><?php
		echo $product['meta_title']; ?></title>
    <meta name="description" content="<?php
		echo $product['meta_description']; ?>">
    <meta name="keywords" content="<?php
		echo $product['meta_keywords']; ?>">

    <!-- Basic page needs
			============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Favicon
			============================================ -->
</head>

<body class="res layout-subpage layout-1 banners-effect-5">
    <div id="wrapper" class="wrapper-fluid">
        <?php include ('header.php');?>
        <!-- //Header Container  -->
        <!-- Main Container  -->
        <div class="main-container" style="background: #ffffff;">
            <?php include('bread_crumb.php'); ?>
            <div class="container">
                <div class="row">
                    <!--Left Part Start -->
                    <!--Left Part End -->
                    <!--Middle Part Start-->
                    <div id="content" class="col-md-12 col-sm-8">




                        <div class="product-view">
                            <div class="left-content-product">
                                <div class="row">
                                    <div class="content-product-left col-md-6 col-sm-12 col-xs-12">
                                        <div id="thumb-slider-vertical" class="thumb-vertical-outer">
                                            <!-- <span class="btn-more prev-thumb nt"><i class="fa fa-angle-up"></i></span>
									<span class="btn-more next-thumb nt"><i class="fa fa-angle-down"></i></span> -->
                                            <ul class="thumb-vertical">


                                                <?php
									   $i=0; 
									   $proimgs = mysql_query("select * from product_image where productid='".$product_id."' ORDER BY FIND_IN_SET(id, '".$product['coverphoto']."') desc ");
										$fimg = "";
				  						while($proimg = mysql_fetch_array($proimgs))
										{  
										if($i==0) { $fimg = "upload/product/".$product_id."/".$proimg['filename']; }
										?>
                                                <li class="owl2-item">
                                                    <a data-index="<?=$i++?>" class="img thumbnail"
                                                        data-image="upload/product/<? echo $product_id; ?>/<? echo $proimg['filename']; ?>"
                                                        title="">
                                                        <img src="upload/product/<? echo $product_id; ?>/<? echo $proimg['filename']; ?>"
                                                            alt='image<? echo $i; ?>' data-gc-caption=""
                                                            id="<?php echo $product_id;?>img">
                                                    </a>
                                                </li>



                                                <?php  } ?>
                                            </ul>


                                        </div>
                                        <div class="large-image  vertical">
                                            <img itemprop="image" class="product-image-zoom" src="<?=$fimg?>"
                                                data-zoom-image="<?=$fimg?>" title="<?php echo $product['name']; ?>"
                                                alt="<?php echo $product['name']; ?>">
                                        </div>
                                        <!--<a class="thumb-video pull-left" href="https://www.youtube.com/watch?v=HhabgvIIXik"><i class="fa fa-youtube-play"></i></a>-->

                                    </div>

                                    <div class="content-product-right col-md-6 col-sm-12 col-xs-12">
                                        <div class="title-product">
                                            <h1 class="hebbo-font" id="<?php echo $product_id;?>name"><?php
                                echo $product['name']; ?></h1>
                                        </div>
                                        <!-- Review 
							<div class="box-review form-group">
								<div class="ratings">
									<div class="rating-box">
										<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
										<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
										<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
										<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
										<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
									</div></div></div>---->

                                        <div class="product-label form-group product-layout-v">
                                            <div class="product_page_price price" itemprop="offerDetails" itemscope=""
                                                itemtype="">
                                            </div>
                                            <div class="product_page_price price hebbo-font" itemprop="offerDetails"
                                                itemscope="" itemtype="">
                                                <? if($product['discount']==0){
   										 $dis_amount =  cal_discount($product['discount'],$product['retail_price']); ?>
                                                <span class="price-new hebbo-font" itemprop="price "
                                                    id="<?php echo $product_id;?>price"> <i class="fa fa-inr"></i>
                                                    <?php echo $dis_amount;?></span>

                                                <span class="price-old">
                                                    &#8377;
                                                    <? echo $product['retail_price']; ?>
                                                </span>
                                                <span class="view-amt-dcnt"> (
                                                    <? echo $product['discount']; ?>% OFF)
                                                </span>
                                                <p class="tax-sel-price hebbo-font">
                                                    <span class="tax-gstInfo">
                                                        Additional tax shall apply, charged at checkout
                                                    </span>
                                                </p>

                                                <? } else { ?>
                                                <span class="view-act-amt hebbo-font"> <i class="fa fa-inr"></i>
                                                    <?php echo $product['retail_price'];?> </span>
                                                <span class="price-old hebbo-font price_combo_<?php echo $product_id;?>"
                                                    id="<?php echo $product_id;?>price">

                                                    &#8377;
                                                    <? echo $product['retail_price']; ?>

                                                </span>
                                                <p class="tax-sel-price hebbo-font">
                                                    <span class="tax-gstInfo">
                                                        Additional tax shall apply, charged at checkout
                                                    </span>
                                                </p>
                                                <? } ?>

                                            </div>
                                            <div class="stock stock hebbo-font hide"><span>Availability:</span> <span
                                                    class="status-stock">In Stock</span></div>
                                        </div>

                                        <div id="product">
                                            <div class="attribute">

                                                <label class="h-css">
                                                    Select Size (UK Size)
                                                    <input type="hidden" value="0" id="attr_1" lass="checkcombo_1">
                                                </label>

                                                <span class="size-chart">
                                                    <a href="size-chart.html" title="Quick view"
                                                        class="iframe-link quickview quickview_handler "
                                                        data-fancybox-type="iframe">
                                                        Size Chart
                                                        <b class="car-icon"> &#x203A; </b>
                                                    </a>
                                                </span>




                                                <div class="size">


                                                    <h2 class="before_add"><a href="javascript:void(0);" title=""
                                                            class=" pdpAttr add_1" id="2"
                                                            onclick="get_com_val(this.id,1,6914);"><span>
                                                                7 </span></a>
                                                    </h2>


                                                    <h2 class="before_add"><a href="javascript:void(0);" title=""
                                                            class=" pdpAttr add_1" id="3"
                                                            onclick="get_com_val(this.id,1,6914);"><span>
                                                                8 </span></a>
                                                    </h2>

                                                    <h2 class="before_add"><a href="javascript:void(0);" title=""
                                                            class=" pdpAttr add_1" id="5"
                                                            onclick="get_com_val(this.id,1,6924);"><span>
                                                                9 </span></a>
                                                    </h2>

                                                    <h2 class="before_add"><a href="javascript:void(0);" title=""
                                                            class=" pdpAttr add_1" id="6"
                                                            onclick="get_com_val(this.id,1,6934);"><span>
                                                                10 </span></a>
                                                    </h2>


                                                </div>
                                            </div>


                                            <div class="col-sm-24">
                                                <!-- <h4>Available Options</h4> -->

                                                <? $chk_first = true; $num_comb= $db->getRowCount('product_comb','id',NULL,"productid='".$product_id."' and  lov = 1",'');



							if($num_comb!=0) { ?>

                                                <div class="attribute">



                                                    <?	  			

							$q= 	mysql_query("select * from product_comb_att_mapping comb inner join catalog_product_attribute attr on attr.attribute_id = comb.attrid where  comb.productid  =  ".$product_id." group by comb.attrid order by attr.attribute_id desc");

 				  			$combo_count =  mysql_num_rows($q);

							  $combo_count =  $db->getRowCount('product_comb_att_mapping','DISTINCT attrid',NULL,"productid='".$product_id."'",''); ?>

                                                    <input type="hidden" value="<? echo $combo_count; ?>"
                                                        name="combo_count" id="combo_count">

                                                    <?  if($combo_count!=0){ 

							$prod_attr12 = mysql_query("select combinationid from product_comb_att_mapping where productid =".$product_id);

 		 					while($pro_attr12 = mysql_fetch_array($prod_attr12)){  

							$combs = $combs.','.$pro_attr12['combinationid'];

 						}	  	$combs = ltrim($combs, ',');					

						$prod_attr = mysql_query("select * from product_comb_att_mapping comb inner join catalog_product_attribute attr on attr.attribute_id = comb.attrid where  comb.productid  =  ".$product_id." group by comb.attrid  "); //attr.view_display = 1 and

 						$k=1;	while($pro_attr = mysql_fetch_array($prod_attr)){  

				 		$prod_combsn2 =mysql_query("select sum(qty) as qty from product_comb  where id in (".$combs.")")or die(mysql_error());

 			 			$prod_combsn = mysql_fetch_array($prod_combsn2); 

 				   		?>

                                                    <div class="size">
                                                        <?  if($prod_combsn['qty']!=0) {?> <label>Select
                                                            <?  echo get_attribute_name($pro_attr['attrid']); }?>

                                                            : <input type="hidden" value="0"
                                                                id="attr_<? echo $pro_attr['attrid']; ?>"
                                                                class="checkcombo_<? echo $k; ?> blanck_com">

                                                            <input type="hidden"
                                                                value="<? echo  $pro_attr['attrid']; ?>"
                                                                id="attrdec_<? echo $pro_attr['attrid']; ?>"
                                                                class="ser_<? echo $k; ?>">

                                                        </label>
                                                        <p class="atrval_<? echo $k; ?>" style="display:none">
                                                            <? echo  $pro_attr['attrid']; ?>
                                                        </p>

                                                        <select name=""
                                                            class="form-control atr_id_<? echo  $pro_attr['attrid']; ?> related_<? echo $k; ?> "
                                                            id="atrs<? echo  $pro_attr['attrid']; ?>" style="width:20%"
                                                            onChange="get_com_select(<? echo $pro_attr['attrid']; ?>,<? echo $product_id; ?>);">

                                                            <option value="">--select--</option>

                                                            <? if($chk_first==true){

  						$db->select('product_comb_att_mapping','DISTINCT attrvalueid',NULL,"productid='".$product_id."' and  attrid = '".$pro_attr['attrid']."'",''); $prod_attr_val = $db->getResult();  

   						foreach($prod_attr_val as $pro_attr_val1){ 

  

  						$db->select('product_comb_att_mapping','combinationid',NULL,"productid='".$product_id."' and  attrvalueid = '".$pro_attr_val1['attrvalueid']."'",''); $prod_coo = $db->getResult(); 	$prod_coo = $prod_coo[0];

  						 $prod_combsn2 =mysql_query("select * from product_comb where id = ".$prod_coo['combinationid'])or die(mysql_error());

 						 $prod_combsn12 = mysql_fetch_array($prod_combsn2); 

 						if($prod_combsn12['qty']!=0) { ?>

                                                            <option value="<? echo $pro_attr_val1['attrvalueid']; ?>">
                                                                <?  echo get_attribute_value($pro_attr_val1['attrvalueid']) ;   ?>
                                                            </option>



                                                            <?  }  ?>



                                                            <? } 	$chk_first = false;	 } ?>

                                                        </select>

                                                    </div>







                                                    <?  $k++; }  }     ?>

                                                </div>







                                                <? } else{?>



                                                <div class="attribute">

                                                    <?	  			

							$q= 	mysql_query("select * from product_comb_att_mapping comb inner join catalog_product_attribute attr on attr.attribute_id = comb.attrid where attr.view_display = 1 and comb.productid  =  ".$product_id." group by comb.attrid ");

 								  $combo_count =  mysql_num_rows($q);

							//  $combo_count =  $db->getRowCount('product_comb_att_mapping','DISTINCT attrid',NULL,"productid='".$product_id."'",''); ?>

                                                    <input type="hidden" value="<? echo $combo_count; ?>"
                                                        name="combo_count" id="combo_count">

                                                    <?  if($combo_count!=0){ 

							$prod_attr12 = mysql_query("select combinationid from product_comb_att_mapping where productid =".$product_id) or die(mysql_error());

 		 					while($pro_attr12 = mysql_fetch_array($prod_attr12)){  

							$combs = $combs.','.$pro_attr12['combinationid'];



							}	  	$combs = ltrim($combs, ',');					

							$prod_attr = mysql_query("select * from product_comb_att_mapping comb inner join catalog_product_attribute attr on attr.attribute_id = comb.attrid where attr.view_display = 1 and comb.productid  =  ".$product_id." group by comb.attrid ");

 							$k=1;	while($pro_attr = mysql_fetch_array($prod_attr)){  

								 $prod_combsn2 =mysql_query("select sum(qty) as qty from product_comb  where id in (".$combs.")")or die(mysql_error());

 							 $prod_combsn = mysql_fetch_array($prod_combsn2); 

 			

							   ?>

                                                    <div class="size">

                                                        <?  if($prod_combsn['qty']!=0) {?> <label>Select

                                                            <?  echo get_attribute_name($pro_attr['attrid']); }?>

                                                            : <input type="hidden" value="0"
                                                                id="attr_<? echo $pro_attr['attrid']; ?>"
                                                                class="checkcombo_<? echo $k; ?>">

                                                        </label>
                                                        <p class="atrval_<? echo $k; ?>" style="display:none">
                                                            <? echo  $pro_attr['attrid']; ?>
                                                        </p>

                                                        <?

  									$db->select('product_comb_att_mapping','DISTINCT attrvalueid',NULL,"productid='".$product_id."' and  attrid = '".$pro_attr['attrid']."'",''); $prod_attr_val = $db->getResult();  

   									foreach($prod_attr_val as $pro_attr_val1){ 

  

  									$db->select('product_comb_att_mapping','combinationid',NULL,"productid='".$product_id."' and  attrvalueid = '".$pro_attr_val1['attrvalueid']."'",''); $prod_coo = $db->getResult(); 	$prod_coo = $prod_coo[0];

  									 $prod_combsn2 =mysql_query("select * from product_comb where id = ".$prod_coo['combinationid'])or die(mysql_error());

 									 $prod_combsn12 = mysql_fetch_array($prod_combsn2); 

 										if($prod_combsn12['qty']!=0) { ?>
                                                        <h2 class="before_add"><a href="javascript:void(0);" title=""
                                                                class=" pdpAttr add_<? echo $pro_attr['attrid']; ?>"
                                                                id="<? echo $pro_attr_val1['attrvalueid']; ?>"
                                                                onClick="get_com_val(this.id,<? echo $pro_attr['attrid']; ?>,<? echo $prod_coo['combinationid']; ?>);"><span>

                                                                    <?	 echo get_attribute_value($pro_attr_val1['attrvalueid']) ;   ?>

                                                                </span></a></h2>

                                                        <?  }  ?>





                                                        <? } 		 ?>



                                                    </div>







                                                    <?  $k++; }  }    ?>

                                                </div>



                                                <? } $prod_comb =  $db->select('product_comb','id',NULL,"productid='".$product_id."'",'');

				 			  $prod_comb = $db->getResult();  $prod_comb = $prod_comb[0]; $comb_id = $prod_comb['id'];   ?>

                                                <input type="hidden" name="comb_id" id="comb_id"
                                                    value="<? echo $comb_id; ?>">

                                                <input type="hidden" name="final_combo" class="final_combo" value="0" />

                                                <input type="hidden" name="weight" class="weight"
                                                    value="<? echo $product['weight']; ?>" />
                                                <input type="hidden" name="weight_outside" class="weight_outside"
                                                    value="<? echo $product['weight_outside']; ?>" />

                                                <input type="hidden" name="vol_weight" class="vol_weight"
                                                    value="<? echo $product['vol_weight']; ?>" />

                                                <div id="valid_msg" style="color:#F00"></div>



                                            </div>
                                            <div class="form-group box-info-product">
                                                <div class="option quantity">
                                                    <div class="input-group quantity-control" unselectable="on"
                                                        style="-webkit-user-select: none;display:none;">
                                                        <label>Qty</label>
                                                        <input class="form-control" type="text" name="quantity"
                                                            value="1">
                                                        <input type="hidden" name="product_id" value="50">
                                                        <span class="input-group-addon product_quantity_down">−</span>
                                                        <span class="input-group-addon product_quantity_up">+</span>
                                                    </div>
                                                </div>
                                                <div class="cart">
                                                    <a href="javascript:void(0)" class="buy_now">

                                                        <button type="button" id="button-cart" title="Add to Cart"
                                                            class="btn btn-mega btn-lg pro-btn btn-bg-red mr-10">
                                                            <span><span> <i class="fa fa-credit-card"></i> BUY NOW
                                                                </span></span>

                                                        </button>

                                                    </a>

                                                    <a id="<?php echo $product['product_id'];$qty=50;?>"
                                                        class="add_cart" href="javascript:void(0)">

                                                        <button type="button" id="button-cart" title="Add to Cart"
                                                            class="btn btn-mega btn-lg pro-btn btn-bg-blue">
                                                            <span><span> <i class="fa fa-shopping-cart"></i> ADD TO
                                                                    CART</span></span>
                                                        </button>

                                                    </a>

                                                </div>
                                                <div class="add-to-links wish_comp">
                                                    <ul class="blank list-inline">
                                                        <li class="wishlist">
                                                            <a class="icon <? if($user_id==''){ ?> iframe <? } ?>"
                                                                data-toggle="tooltip"
                                                                id="<?php echo $product['product_id']; ?>"
                                                                onClick="add_wishlist(this.id)" <? if($user_id=='' ){ ?>
                                                                href="login_signup.php"
                                                                <? } ?> data-original-title="Add to Wish List"><i
                                                                    class="fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                    </ul>
                                                </div>

                                            </div>



                                        </div>

                                        <div class="pin-code-sty">
                                            <label class="avail"><i class="fa fa-map-marker"></i>Check availablity
                                                at</label>

                                            <input class="pin-val" onChange="pin(this.value)" id="pcode" type="number"
                                                name="pincode" placeholder="Type pincode">

                                            <input class="pin-check" type="button" name="Check" value="Check"
                                                id="pin_click">

                                            <div id="msg" align="center"></div>
                                        </div>


                                        <div class="product-box-desc">
                                            <div class="inner-box-desc">
                                                <ul class="add-info">
                                                    <li> <span><img
                                                                src="https://rukminim1.flixcart.com/www/36/36/promos/06/09/2016/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png?q=90"
                                                                width="18" height="18" class="img-icon"></span> 100%
                                                        Original Products</li>
                                                    <li> <span><img
                                                                src="https://rukminim1.flixcart.com/www/36/36/promos/06/09/2016/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png?q=90"
                                                                width="18" height="18" class="img-icon"></span> Free
                                                        Delivery on order above Rs. 1199</li>
                                                    <li> <span><img
                                                                src="https://rukminim1.flixcart.com/www/36/36/promos/06/09/2016/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png?q=90"
                                                                width="18" height="18" class="img-icon"></span> Cash on
                                                        delivery might be available</li>
                                                    <li> <span><img
                                                                src="https://rukminim1.flixcart.com/www/36/36/promos/06/09/2016/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png?q=90"
                                                                width="18" height="18" class="img-icon"></span> Easy 30
                                                        days returns and exchanges</li>
                                                    <li> <span><img
                                                                src="https://rukminim1.flixcart.com/www/36/36/promos/06/09/2016/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png?q=90"
                                                                width="18" height="18" class="img-icon"></span> Try &
                                                        Buy might be available</li>
                                                </ul>

                                                <!-- <div class="price-tax">
										<span>SKU:</span><?php if ($product['ref_code'] != ''){	echo '(' . $product['ref_code'] . ')';	} ?>
									</div>-->

                                                <div class="price-tax">
                                                    <p>
                                                        Product Code: <span>2353337</span>
                                                    </p>

                                                    <p>
                                                        Sold by: <span>Shoooz - The Shoeroom</span>
                                                    </p>


                                                </div>





                                            </div>



                                            <!-- <p> <? echo trim(stripslashes($product['short_description'])); ?> </p> -->
                                        </div>



                                        <!-- end box info product -->

                                    </div>
                                </div>
                            </div>


                        </div>












                        <!-- Product Tabs -->
                        <div class="producttab hebbo-font hide">
                            <div class="tabsslider  vertical-tabs col-xs-12">
                                <ul class="nav nav-tabs col-lg-2 col-sm-3">
                                    <li class="active"><a data-toggle="tab" href="#tab-1">Item Description</a></li>
                                    <?  $feature_count =  $db->getRowCount('product_featues','id',NULL,"productid='".$product_id."'",''); if($feature_count!=0){ ?>
                                    <li class="item_nonactive"><a data-toggle="tab" href="#tab-2">Product Feature</a>
                                    </li>
                                    <? } ?>
                                    <? if(strlen($product['techndescription'])!=0) { ?>
                                    <li class="item_nonactive"><a data-toggle="tab" href="#tab-3">Technical Detail</a>
                                    </li>
                                    <? } ?>


                                </ul>
                                <div class="tab-content col-lg-10 col-sm-9 col-xs-12">
                                    <div id="tab-1" class="tab-pane fade active in">
                                        <? echo stripslashes($product['description']); ?>

                                    </div>

                                    <div id="tab-2" class="tab-pane fade">
                                        <? echo stripslashes($product['keyfeaturedes']); ?>
                                    </div>
                                    <div id="tab-3" class="tab-pane fade">
                                        <ol>
                                            <? $feature =  $db->select('product_featues','*',NULL,"productid='".$product_id."'",''); 
								$feature = $db->getResult(); foreach($feature as $fea){

								?>
                                            <li style="list-style-type: disc;">
                                                <? echo get_feature_name($fea['featureid']); ?>
                                            </li>

                                            <? } ?>

                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- //Product Tabs -->

                        <!-- Related Products -->
                        <div class="related titleLine products-list grid module ">
                            <?php
				$qry1 = "SELECT cat_map.id,cat_map.productid FROM product_category_mapping cat_map inner join catalog_product pro on pro.product_id = cat_map.productid where cat_map.categoryid ='" . $category_id . "'  and cat_map.productid !=" . $product_id . " and pro.status='Y'";
				$qry1 = $qry1 . " ORDER BY RAND() ";
				$catlist = mysql_query($qry1);
				$num_pro = mysql_num_rows($catlist);

				if ($num_pro != 0)
					{ ?>
                            <h3 class="modtitle">Related Products </h3>

                            <div class="releate-products yt-content-slider products-list" data-rtl="no" data-loop="yes"
                                data-autoplay="Yes" data-autoheight="no" data-autowidth="no" data-delay="4"
                                data-speed="0.6" data-margin="30" data-items_column0="5" data-items_column1="3"
                                data-items_column2="3" data-items_column3="2" data-items_column4="1" data-arrows="yes"
                                data-pagination="no" data-lazyload="yes" data-hoverpause="yes">

                                <?php
					$i = 0;
					while ($cat = mysql_fetch_array($catlist))
					{
					$db->select('catalog_product', '*', NULL, "product_id='" . $cat['productid'] . "'  and  status = 'Y'  ", '');
						$product = $db->getResult(); // print_r($product);
							$product = $product[0];
						$product_id = $product['product_id'];
						$db->select('product_image', '*', NULL, "id='" . $product['coverphoto'] . "'", '');
						$cover_img = $db->getResult(); //print_r($cover_img);
						$cover_img = $cover_img[0];
						$pro_qty = mysql_fetch_array(mysql_query("select sum(qty) as qty from product_comb where productid = " . $product['product_id']));
							if ($pro_qty['qty'] != 0)
							{ ?><div class="item">
                                    <div class="item-inner product-layout transition product-grid">
                                        <div class="product-item-container">
                                            <div class="left-block">
                                                <div class="product-image-container ">
                                                    <a href="<?php 	echo get_pro_url($product_id); ?>"> <img
                                                            src="<? echo return_img_pro_relate($product['coverphoto'],$product_id); ?>"
                                                            class="img-1 img-responsive" alt="">
                                                        <? echo  ucwords(strtolower(substr($product['name'],0,30))); ?></a>
                                                </div>
                                                <div class="button-group so-quickview cartinfo--left">
                                                    <button type="button" class="wishlist btn-button"
                                                        title="Add to Wish List" onClick="wishlist.add('60');"><i
                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                    </button>


                                                </div>
                                            </div>
                                            <div class="right-block">
                                                <div class="caption">
                                                    <h4><a href="product.html" title="Pastrami bacon"
                                                            target="_self"></a></h4>
                                                    <div class="price fa fa-inr"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
							$i++;
							}
							} ?>
                            </div>




                            <?php
						} ?>



                        </div>

                        <!-- end Related  Products-->
                    </div>






                </div>


            </div>
        </div>
        <!--Middle Part End-->
    </div>

    <!-- //Main Container -->


    <!-- Footer Container -->
    <?php include('footer.php'); ?>
    <!-- //end Footer Container -->

    </div>







    <!-- Include Libs & Plugins
	============================================ -->

</body>
<?php include('script.php'); ?>


<script type="text/javascript" src="js/em_product_view.js"></script>


</html>