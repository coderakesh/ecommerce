<!-- Include Libs & Plugins
============================================ -->
<!-- Placed at the end of the document so the pages load faster -->
<script >$(function(){$.ui.autocomplete.prototype._renderItem=function(ul,item){item.label=item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("+$.ui.autocomplete.escapeRegex(this.term)+")(?![^<>]*>)(?![^&;]+;)","gi"),"<span style='color:#2B2BFF'>$1</span>");return $("<li></li>").data("item.autocomplete",item).append("<a>"+item.label+"</a>").appendTo(ul);};$('.tt').autocomplete({source:"search_product.php",minLength:2,select:function(event,ui){$("#search_cat").val(ui.item.product_id);$("#search_pro").val(ui.item.product_id);$("#ssss").val(ui.item.form_action);if(ui.item.cat_id!=''){$('.search-product').prop('disabled',false);$("#searchthis").attr('action',ui.item.form_action);$('.search-product').click();}}});});$('#theme-setting > a').click(function(){$(this).next().animate({width:'toggle'},500,function(){if($(this).is(":hidden")){$('#theme-setting > a > i').attr('class','fa fa-gears fa-2x');}else{$('#theme-setting').css("background","green");$('#theme-setting > a > i').attr('class','fa fa-times fa-2x');}});$(this).next().css('display','inline-block');});function add_wishlist(product_id){$.ajax({type:'post',url:'add_wishlist.php',data:'product_id='+product_id,success:function(response){response=$.trim(response);if(response==1){$.alert.open('info','Your Item Has Been Added to Shortlist');}else{}}});}</script>
<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="js/themejs/libs.js"></script>
<script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
<script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
<script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
<script type="text/javascript" src="js/datetimepicker/moment.js"></script>
<script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/modernizr/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="js/minicolors/jquery.miniColors.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>

<!-- Theme files
============================================ -->
<script type="text/javascript" src="js/themejs/application.js"></script>
<script type="text/javascript" src="js/themejs/homepage.js"></script>
<script type="text/javascript" src="js/themejs/toppanel.js"></script>
<script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
<script type="text/javascript" src="js/themejs/addtocart.js"></script>  
<script type="text/javascript" src="js/jquery-ui-min.js"></script>
<script type="text/javascript" src="js/lightslider/lightslider.js"></script>

