<?php include('link.php'); ?>
    <!-- /.em-wrapper-header -->
<div class="page two-columns-left">

<?php include('header.php'); ?>
    <!-- /.em-wrapper-header -->
<style>
.std .hoz li {
line-height:25px;	
color:#4E4E4E; 
}
 .post-content p, span{
text-align:justify;	color:#4E4E4E;
}
.v-std p {
    margin-bottom: 12px !important;
	 
	
}
.em_post-item .post-content {
    height: auto !important;
}
.pbottom25{
padding-bottom:25px;	
}
.post-title .v-h1{
	    font-size: 21px;
    color: #282828;
    font-weight: 600;
    margin-bottom: 25px;
    background: #F1F1F1;
    padding: 12px 8px 12px;
    border-top: 3px solid rgb(244, 206, 0);
    text-align: center;
}
</style>    
    <div class="em-wrapper-main">
                    <div class="container container-main pbottom25">
                        <div class="em-inner-main">
                            
                            <div class="em-main-container em-col2-left-layout">
                                <div class="row">
                                    <div class="col-sm-24  em-col-main">
                                        
                                        <div class="em_post-item">
                                            <div class="post-title">
                                                <h1 class="v-h1">Delivery & Shipping Policy</h1>
                                            </div>
                                            <div class="post-title">
                                                <h2>Delivery / Shipping </h2>
                                            </div>
                                            <div class="post-content std">
                                                <p><span>At apexcarparts we are committed to deliver your order in best and acceptable condition within the stipulated timeframe. 
Unlike other e-commerce sites, apexcarparts didn't have any special rules and fine print; we absolutely respect you and 
your time and investment in procuring a deal/product at apexcarparts.  We follow some agreeable shipping rules so that no 
parties will bear any loss or may arises any conflict, and definitely for smooth shipping operations.</span>
                                                </p>
                                                <ul class="hoz">
                                                <li>We ship only in India and maximum delivery time for anywhere in India is around 15 working days.</li>
                                                <li>Next working day delivery in Bhopal and 7-15 working days anywhere in India.</li>
                                                <li>apexcarparts will ship all the products within Bhopal and for shipping outside Bhopal (anywhere in India) will be done

through a recognized courier.</li>
                                                <li>apexcarparts will send the SMS to customer's registered mobile number for tracking their order.</li>
                                                
                                                <li>The region/areas which are not serviced by associated couriers, we will ship through the Government Registered posts.</li>
                                                <li>We ship Monday through Saturday, excluding all public holidays.</li>
                                                <li>For all the areas serviced by reputed couriers the estimated delivery time is between 3-7 business days. For those

serviced by the Indian Postal service (the delivery time can be 1-2 weeks depending on the location).</li>
                                                <li>In case if the packaging is damaged or is tampered at the time of delivery or if you think the product is in a bad condition

on delivery, please refuse to accept the package. Call our apexcarparts helpline at 810 959 5945 or send an email at 

info@apexcarparts.in with your Order ID. apexcarparts will ensure that the product is replaced and delivered to you at the 

earliest.</li>
                                               
                                            </ul>
                                            
                                                 
                                            </div>
                                            
                                            <div class="post-title">
                                                <h2>Payments</h2>
                                            </div>
                                            <div class="post-content v-std std">
                                                <p><span>apexcarparts supports all the major payment modes like Credit Cards, Debits Cards, Cash Cards.</span>
                                                </p>
                                                
                                                <p><span>Credit Cards - Master Card, Visa & Diners Card</span>
                                                </p>
                                                
                                                <p><span>apexcarparts supports all the major payment modes like Credit Cards, Debits Cards, Cash Cards.</span>
                                                </p>
                                                
                                                <p><span>Debit Card - Axis Bank, HDFC Bank, ICICI Bank, Deutsche Bank, Karur Vysya Bank, Citibank, State Bank of India, Indian 
Overseas Bank, ING Vysya, Corporation Bank and keep on adding</span>
                                                </p>
                                                
                                                <p><span>Net Banking Options - Axis Bank, HDFC Bank, ICICI Bank, J & K Bank, Citibank, Federal Bank and all major banks.</span>
                                                </p>
                                                
                                                <p><span>Cash Card - ITZ Cash Card</span>
                                                </p>
                                                
                                                <p><span>Cash on Delivery (COD):  No service</span>
                                                </p>
                                                
                                                <p><span>Note- <strong>Installation Charges and other service charges</strong></span>
                                                </p>
                                                
                                                <p><span>All the online payment transactions will be process over secured encrypted connection.</span>
                                                </p>
                                                 
                                            
                                                 
                                            </div>
                                             
                                            <div class="post-title">
                                                <h2>Payment Failures</h2>
                                            </div>
                                            
                                            <div class="post-content std">
                                                <p><span>In case of payment failures you need to make sure that:</span>
                                                </p>                                               
                                               <ul class="hoz">
                                               
                                                <li>The details you have submitted to the payment gateway are correct, especially in case of Net banking like account details, passwords etc.</li>
                                                <li>Your Internet connection running proper and not disordered during the payment process
What you should do when account has been debited after a payment failure?
No need to get panic, it will roll back to your account within maximum 7 days. Still you can reach us at our helpline with order number if generated at 810 959 5945 for further information or clarification.
</li>
                                               
                                            </ul>
                                            </div>
                                            
                                            <!-- /.comments -->
                                        </div>
                                    </div><!-- /.em-col-main -->

                                    <!-- /. em-sidebar -->
                                </div>
                            </div><!-- /.em-main-container -->
                        </div>
                    </div>
                </div>
    <!-- /.em-wrapper-main -->
    
    
<?php include('footer.php'); ?>    
  <!-- /.em-wrapper-footer -->  
    
    
  </div>
  <!-- /.page --> 
</div>
<!-- /.wrapper -->

</body>
</html>